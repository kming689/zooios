//
//  MFDataBaseMgr.h
//  Pods
//
//  Created by tanfameng on 2017/6/14.
//
//

/*
 数据库操作注意事项：
 1、如果自己没有指定主键，sqlite引擎会帮忙生成自增长的主键
 2、通过sql语句"select last_insert_rowid() from TABLE"可以获取到刚刚insert语句的主键值
 3、如果通过事物进行insert操作，那么获取上面的sql语句要紧跟着执行才能正确获取，否则获取的id是0
 */

#import <Foundation/Foundation.h>
#import <LKDBHelper/LKDBHelper.h>

typedef BOOL (^MFTransactionBlock)();

/*
 MFDataBaseMgr类用于每个模块管理数据库操作
 1、每个模块都需要有一个DBMgr继承自该类,并重载createTables方法，在方法类创建该模块的所有table
 2、使用之前必须调用两个接口初始化数据库
 2.1、获取单例
 2.2、连接数据库
 */

@class MFDBTable;
@class MFDBTranscation;

@interface MFDBManager : NSObject

//数据库操作的句柄
@property (nonatomic, strong, readonly) LKDBHelper *dataBase;

//连接数据库
- (BOOL)connectToDBWithPathUrl:(NSURL *)pathUrl;

- (BOOL)connectToDBWithPath:(NSString *)path;

//需要被重载的方法，返回需要创建表的对象，类型为MFDBTable或者子类类型
- (NSArray<MFDBTable *> *)createTables;
//需要被重载，切换用户时候被调用，返回新的数据库路径,userInfo为客户代码在调用MFDBSwitch的+(void) postSwitchDBWithUserInfo:(NSDictionary *)userInfo方法时传递的参数
- (NSString *)dataBaseWillChange:(NSDictionary *)userInfo;

//切换到指定路径数据库
- (BOOL)switchDataBaseToPath:(NSString *)path;


/**
 子类重载,不能直接调用

 @param close 是否成功关闭
 */
- (void)globalDataBaseDidClosed:(BOOL)close;

/**
 返回一个transaction对象，客户程序获取到该对象后调用begin，commit，rollback控制事物
 
 @return 事物对象
 */
//- (MFDBTranscation *)getTransaction;

/**
 通过事物操作数据库，不需要调用getTransaction方法获取transaction对象
 
 @param block 业务代码block，在block中做数据库操作，block返回值为YES，表示commit，为NO表示rollback
 @return 是否commit
 */
- (BOOL)runTransaction:(MFTransactionBlock)block;

@end
