//
//  LKDBHelper+Convenient.h
//  Pods
//
//  Created by tanfameng on 2017/8/11.
//
//

#import <LKDBHelper/LKDBHelper.h>

@interface LKDBHelper (Convenient)

- (long)getLastInsertRowIDFromTable:(Class)tableClass;

@end
