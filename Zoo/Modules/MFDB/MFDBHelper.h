//
//  MFDBSwitch.h
//  Pods
//
//  Created by tanfameng on 2017/6/15.
//
//

#import <Foundation/Foundation.h>

@interface MFDBHelper : NSObject

//通知DBMgr，切换数据库，userInfo为用户参数，会在MFDataBaseMgr的重载方法- (NSString *)dataBaseWillChange:(NSDictionary *)userInfo中被获取，重载方法中根据此传入的参数判断需要切换到哪个数据库
+(void) postSwitchDBWithUserInfo:(NSDictionary *)userInfo;
//通知DBMgr，关闭数据库
+ (void)closeDataBaseWithUserInfo:(NSDictionary *)userInfo;

@end
