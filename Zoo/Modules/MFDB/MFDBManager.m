//
//  MFDataBaseMgr.m
//  Pods
//
//  Created by tanfameng on 2017/6/14.
//
//

#import "MFDBManager.h"
#import "MFDBDao.h"
#import "MFDBTable.h"
#import "MFTableInfoTable.h"
#import "MFDBTranscation.h"

#define kChangeDataBaseNotification @"changeusernotification"
#define kCloseDataBaseNotification @"kCloseDataBaseNotification"

@implementation MFDBManager

@synthesize dataBase = _dataBase;

- (instancetype)init
{
    self = [super init];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeDataBase:) name:kChangeDataBaseNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeDataBase:) name:kCloseDataBaseNotification object:nil];
    }
    return self;
}

#pragma mark -- 切换数据库通知
- (void)changeDataBase:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification object];
    
    NSString *newPath = [self dataBaseWillChange:userInfo];
    
    if (newPath) {
        if (![self switchDataBaseToPath:newPath]) {
            NSLog(@"切换数据库失败");
        }
    }else{
        NSLog(@"没有指定新的数据库路径");
    }
}

- (BOOL)closeDataBase:(NSNotification *)notification
{
    __block BOOL result = NO;
    [self.dataBase executeDB:^(FMDatabase * _Nonnull db) {
        result = [db close];
    }];
    if (result) {
        self.dataBase = nil;
    }
    
    [self globalDataBaseDidClosed:result];
    
    return result;
}

- (LKDBHelper *)dataBase
{
    return _dataBase;
}

- (void)setDataBase:(LKDBHelper *)dataBase
{
    if (_dataBase != dataBase) {
        _dataBase = dataBase;

        //全局错误监控
        
        //创建所有表
        [self createAllTables:[self createTables]];
    }
}

- (BOOL)connectToDBWithPathUrl:(NSURL *)pathUrl
{
    if (self.dataBase) {
        [self.dataBase executeDB:^(FMDatabase * _Nonnull db) {
            [db close];
        }];
        self.dataBase = nil;
    }
    LKDBHelper *database = [[LKDBHelper alloc] initWithDBPath:pathUrl.path];
    self.dataBase = database;
    return (database != nil);
}

- (BOOL)connectToDBWithPath:(NSString *)path
{
    if (self.dataBase) {
        [self.dataBase executeDB:^(FMDatabase * _Nonnull db) {
            [db close];
        }];
        self.dataBase = nil;
    }
    LKDBHelper *database = [[LKDBHelper alloc] initWithDBPath:path];
    self.dataBase = database;
    return (database != nil);
}

-(NSArray<MFDBTable *> *)createTables
{
    return nil;
}

- (NSString *)dataBaseWillChange:(NSDictionary *)userInfo
{
    return nil;
}

- (BOOL)switchDataBaseToPath:(NSString *)path
{
    //关闭以前数据连接
    if (self.dataBase) {
        [self.dataBase executeDB:^(FMDatabase * _Nonnull db) {
            [db close];
        }];
        self.dataBase = nil;
    }
    //打开新的连接
    if (!self.dataBase) {

        BOOL result = [self connectToDBWithPath:path];
        if (!result) {
            NSLog(@"连接数据库失败");
        }
        
        return result;
    }
    
    return NO;
}


- (void)globalDataBaseDidClosed:(BOOL)close
{

}

#pragma mark -- transcation
- (MFDBTranscation *)getTransaction
{
    MFDBTranscation *transcation = [[MFDBTranscation alloc] init];
    return transcation;
}

- (BOOL)runTransaction:(MFTransactionBlock)block
{
    if (_dataBase){
        [_dataBase executeForTransaction:block];
        return YES;
    }else{
        NSLog(@"数据库链接尚未建立");
        return NO;
    }
}

#pragma mark -- private
- (BOOL)createAllTables:(NSArray *)tables
{
//    MFDBManager *mgr = [MFDBManager shareInstance];
//    //创建版本表
//    //判断表是否存在
//    if (![mgr.dataBase getTableCreatedWithTableName:MFTableInfoTable.tableName]) {
//        //不存在，创建
////        BOOL result = [mgr.dataBase createTableAndIndexesOfName:MFTableInfoTable.tableName
////                                                      withClass:MFTableInfoTable.class];
//        BOOL result = [mgr.dataBase cr];
//        if (!result) {
//            NSLog(@"创建版本表失败");
//            return NO;
//        }
//    }
//    
//    
//    //创建用户的所有表
//    if(tables){
//        for (MFDBTable *table in tables) {
//            
//            int oldVersion = 0;
//            
//            //判断表是否存在
//            if (![mgr.dataBase isTableExists:table.tableName]) {
//                //不存在，创建
//                BOOL result = [mgr.dataBase createTableAndIndexesOfName:table.tableName
//                                                              withClass:table.class];
//                if (!result) {
//                    NSLog(@"创建表失败");
//                    return NO;
//                }
//            }
//            //已经存在，更新
//            else{
//                //从数据库中获取当前表的老版本号
//                NSArray <MFTableInfoTable *> *tableInfos = [mgr.dataBase getObjectsOfClass:MFTableInfoTable.class fromTable:MFTableInfoTable.tableName where:MFTableInfoTable.name==table.tableName];
//                if (tableInfos.count > 0) {
//                    MFTableInfoTable *infoTable = [tableInfos lastObject];
//                    oldVersion = infoTable.version;
//                }
//                
//                //更新表的版本
//                if (table.tableVersion > oldVersion) {
//                    
//                    //更新表结构
//                    WCTPropertyList propertys = [[table class] AllProperties];
//                    for (const WCTProperty &property:propertys) {
//                        std::string dbPropertyName = property.getName();
//                        const char *cName = dbPropertyName.c_str();
//                        NSString *ocName = [NSString stringWithCString:cName encoding:NSUTF8StringEncoding];
//                        std::shared_ptr<WCTColumnBinding> core = property.getColumnBinding();
//                        WCTColumnDef columnDef = core->getColumnDef();
//                        BOOL flag = [table addColumn:columnDef];
//                        if (flag) {
//                            XLOG_INFO(@"当前被添加的column名字为%@",ocName);
//                            NSLog(@"当前被添加的column名字为%@",ocName);
//                        }
//                    }
//                    
//                    [table updateVersion:oldVersion];
//                }else{
//                    return YES;
//                }
//            }
//            
//            //更新数据库中表的版本信息
//            MFTableInfoTable *newInfo = [[MFTableInfoTable alloc] init];
//            newInfo.version = table.tableVersion;
//            newInfo.name = table.tableName;
//            BOOL updateResult = [mgr.dataBase insertOrReplaceObject:newInfo into:MFTableInfoTable.tableName];
//            if (!updateResult) {
//                NSLog(@"更新表版本信息失败");
//            }else{
//                NSLog(@"更新表版本信息成功");
//            }
//        }
//    }
    
    //直接更新版本号
    for (MFDBTable *table in tables) {
        
        //检查表是否存在，不存在就创建
        NSString *existSql = [NSString stringWithFormat:@"SELECT COUNT(*) FROM sqlite_master where type='table' and name='%@'",[table.class getTableName]];
        __block int tableCount = 0;
        [self.dataBase executeDB:^(FMDatabase * _Nonnull db) {
            FMResultSet *rs = [db executeQuery:existSql];
            while ([rs next]) {
                tableCount = [rs intForColumnIndex:0];
            }
        }];
        if (tableCount == 0) {
            BOOL result = [self.dataBase updateToDB:table where:nil];
            if (!result) {
                return result;
            }
        }
        
        //获取旧版本号
        MFTableInfoTable *tableInfo = [self.dataBase searchSingle:[MFTableInfoTable class] where:@{@"name":[table.class getTableName]} orderBy:nil];
        if (tableInfo) {
            int newVersion = table.tableVersion;
            int oldVersion = tableInfo.version;
            if (newVersion > oldVersion) {
                [table updateVersion:oldVersion];
                
                //更新数据库中表的版本信息
                MFTableInfoTable *newInfo = [[MFTableInfoTable alloc] init];
                newInfo.version = table.tableVersion;
                newInfo.name = [table.class getTableName];
                BOOL updateResult = [self.dataBase updateToDB:newInfo where:nil];
                if (!updateResult) {
                    NSLog(@"更新表版本信息失败");
                }else{
                    NSLog(@"更新表版本信息成功");
                }
            }
        }else{
            //更新数据库中表的版本信息
            MFTableInfoTable *newInfo = [[MFTableInfoTable alloc] init];
            newInfo.version = table.tableVersion;
            newInfo.name = [table.class getTableName];
            BOOL updateResult = [self.dataBase insertToDB:newInfo];
            if (!updateResult) {
                NSLog(@"插入表版本信息失败");
            }else{
                NSLog(@"插入表版本信息成功");
            }
        }
    }
    return YES;
}

@end
