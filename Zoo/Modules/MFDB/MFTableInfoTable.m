//
//  MFTableInfoTable.m
//  Pods
//
//  Created by tanfameng on 2017/6/16.
//
//

/*
 数据库表本身相关信息，包括
 1、版本号
 */

#import "MFTableInfoTable.h"

@implementation MFTableInfoTable

//表名
+(NSString *)getTableName
{
    return @"tableversioninfo";
}

//主键
+(NSString *)getPrimaryKey
{
    return @"name";
}

@end
