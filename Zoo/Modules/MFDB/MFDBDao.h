//
//  MFBaseDAO.h
//  Pods
//
//  Created by tanfameng on 2017/6/14.
//
//

#import <Foundation/Foundation.h>

@class MFDBTable;

@interface MFDBDao : NSObject

- (instancetype)initWithTable:(MFDBTable *)table;

//- (BOOL)insertObj:(MFDBTable *)object;
//
//- (BOOL)deleteObj:(MFDBTable *)object;
//
//- (BOOL)deleteAllObject;
//
//- (BOOL)updateObj:(MFDBTable *)object;
//
//- (NSArray *)getObjectsOfClass:(Class)cls;

@end
