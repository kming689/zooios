//
//  MFDBSwitch.m
//  Pods
//
//  Created by tanfameng on 2017/6/15.
//
//

#import "MFDBHelper.h"

#define kChangeDataBaseNotification @"changeusernotification"
#define kCloseDataBaseNotification @"kCloseDataBaseNotification"

@implementation MFDBHelper

+(void) postSwitchDBWithUserInfo:(NSDictionary *)userInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kChangeDataBaseNotification object:userInfo];
}

+ (void)closeDataBaseWithUserInfo:(NSDictionary *)userInfo
{
    [[NSNotificationCenter defaultCenter] postNotificationName:kCloseDataBaseNotification object:userInfo];
}

@end
