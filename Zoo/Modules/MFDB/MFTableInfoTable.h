//
//  MFTableInfoTable.h
//  Pods
//
//  Created by tanfameng on 2017/6/16.
//
//

#import "MFDB.h"

@interface MFTableInfoTable : MFDBTable

@property(retain) NSString *name;
@property int version;


@end
