//
//  MFDBTranscation.h
//  Pods
//
//  Created by tanfameng on 2017/6/23.
//
//

#import <Foundation/Foundation.h>

@interface MFDBTranscation : NSObject

- (BOOL)begin;

- (BOOL)commit;

- (BOOL)rollback;

@end
