//
//  LKDBHelper+Convenient.m
//  Pods
//
//  Created by tanfameng on 2017/8/11.
//
//

#import "LKDBHelper+Convenient.h"
#import "MFDBManager.h"

@implementation LKDBHelper (Convenient)

- (long)getLastInsertRowIDFromTable:(Class)tableClass
{
    NSString *sql = [[NSString alloc] initWithFormat:@"select last_insert_rowid() from %@",[tableClass getTableName]];
    __block long lastRowID = 0;
    [self executeDB:^(FMDatabase * _Nonnull db) {
        FMResultSet *rs = [db executeQuery:sql];
        while ([rs next]) {
            lastRowID = [rs longForColumnIndex:0];
            
        }
    }];
    
    return lastRowID;
}

@end
