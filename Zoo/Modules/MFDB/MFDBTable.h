//
//  MFBaseTable.h
//  Pods
//
//  Created by tanfameng on 2017/6/14.
//
//

#import <Foundation/Foundation.h>

@interface MFDBTable : NSObject

- (int)tableVersion;

- (BOOL)updateVersion:(int)oldVersion;

@end
