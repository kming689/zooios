//
//  MFBaseDAO.m
//  Pods
//
//  Created by tanfameng on 2017/6/14.
//
//

#import "MFDBDao.h"
#import "MFDBManager.h"
#import "MFDBTable.h"

@implementation MFDBDao
{
    MFDBTable *_table;
}

- (instancetype)initWithTable:(MFDBTable *)table
{
    self = [super init];
    if (self) {
        _table = table;
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

//- (BOOL)insertObj:(MFDBTable *)object
//{
//    MFDBManager *mgr = [MFDBManager shareInstance];
//    BOOL result = [mgr.dataBase insertToDB:object];
//    return result;
//}
//
//- (BOOL)deleteObj:(MFDBTable *)object
//{
//    MFDBManager *mgr = [MFDBManager shareInstance];
//    BOOL result = [mgr.dataBase deleteToDB:object];
//    return result;
//}
//
//- (BOOL)deleteAllObject
//{
//    MFDBManager *mgr = [MFDBManager shareInstance];
//    BOOL result = [mgr.dataBase deleteWithClass:_table.class where:nil];
//    return result;
//}
//
//- (BOOL)updateObj:(MFDBTable *)object
//{
//    MFDBManager *mgr = [MFDBManager shareInstance];
//    BOOL result = [mgr.dataBase updateToDB:object where:nil];
//    return result;
//}
//
//- (NSArray *)getObjectsOfClass:(Class)cls
//{
//    MFDBManager *mgr = [MFDBManager shareInstance];
//    NSString *sql = [[NSString alloc] initWithFormat:@"select * from %@",[cls getTableName]];
//    NSArray *objects = [mgr.dataBase searchWithSQL:sql toClass:cls];
//    return objects;
//}

@end
