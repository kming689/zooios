//
//  MFRequestOperationManager.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFRequestOperationManager.h"
#import "AFHTTPRequestOperation.h"

@interface MFHTTPRequestQueue : NSOperationQueue

@property (nonatomic, strong) id delegate;

@end

@implementation MFHTTPRequestQueue

@end

@implementation MFRequestOperationManager
{
    AFHTTPRequestOperation *lastOperation;
    MFHTTPRequestQueue *operationQueue;
    MFRequestOperationCompletion allCompletion;
}

- (void)startHttpRequestOperation:(NSArray *)operations withCompletion:(MFRequestOperationCompletion)completion
{
    if (completion) {
        allCompletion = completion;
    }
    
    int nIndex = 0;
    for (AFHTTPRequestOperation *operation in operations) {
        if (nIndex > 0) {
            [operation addDependency:lastOperation];
        }
        
        lastOperation = operation;
        
        nIndex++;
    }
    
    operationQueue = [[MFHTTPRequestQueue alloc] init];
    operationQueue.delegate = self;
    
    [operationQueue addObserver:self forKeyPath:@"operationCount" options:0 context:nil];
    
    [operationQueue addOperations:operations waitUntilFinished:NO];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context
{
    if (object == operationQueue && [keyPath isEqualToString:@"operationCount"])
    {
        if (0 == operationQueue.operationCount)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                allCompletion(lastOperation);
                operationQueue.delegate = nil;
            });
        }
    }
    else
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
    
}

- (void)dealloc
{
    [operationQueue removeObserver:self forKeyPath:@"operationCount"];
    //NSLog(@"http request manager dealloc");
}

@end
