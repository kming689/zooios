//
//  MFRequestManager.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFRequestManager.h"
#import "MFHTTPSessionManager.h"
#import <AFNetworking/AFNetworking.h>
#import <sys/utsname.h>
#import "MFNetConstants.h"
#import "MFBaseDataFormatter.h"

#import "MFAccountManager.h"
#import "MFAccount.h"

@implementation MFRequestManager
{

}

- (id)init
{
    self = [super init];
    if (self) {
        //init member varible here
        self.timeOut = 30.f;
        self.isOffline = YES;
    }
    return self;
}

+ (MFRequestManager *)manager
{
    MFRequestManager *manager = [[[self class] alloc] init];
    return manager;
}

- (void)addCompletionBlock:(void (^)(id))completion
{
    MFHTTPSessionManager *httpManager = [MFHTTPSessionManager sharedManager];
    [httpManager addCompletionBlock:[completion copy]];
}

#pragma mark - 合成参数字符串，用于保存在数据库中作为查询条件
- (NSString *)composParaStringFromDict:(NSDictionary *)dict
{
    if (!dict) {
        return nil;
    }
    
    NSMutableString *lastString = [[NSMutableString alloc] init];
    //[lastString appendString:[URL copy]];
    
    NSArray *keys = [dict allKeys];
    for (NSString *key in keys) {
        [lastString appendFormat:@"&%@=%@",key,[dict valueForKey:key]];
    }
    
    return lastString;
}



#pragma mark -- 添加共有参数，包括USERID，ACCOUNT，SYSID,deviceid,terminal
- (void)addPublicParameter:(NSMutableDictionary *)dict
{
    MFHTTPSessionManager *manager = [MFHTTPSessionManager sharedManager];
    MFAccountManager *mgr = [MFAccountManager shareInstance];
    MFAccount *account = mgr.account;
    if (mgr.isLogin) {
//        [manager.requestSerializer setValue:@"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjA1NDk1ZWRmZjQ5ODJjOWUwOWNjZjg0N2JmZWI4MTM4OThkYzQxMTYzZDE2ZDY1MzcxZDI1YWZkZGJjOTM5ZjZhNTJlZWRjMzYyMDkxODEwIn0.eyJhdWQiOiI0IiwianRpIjoiMDU0OTVlZGZmNDk4MmM5ZTA5Y2NmODQ3YmZlYjgxMzg5OGRjNDExNjNkMTZkNjUzNzFkMjVhZmRkYmM5MzlmNmE1MmVlZGMzNjIwOTE4MTAiLCJpYXQiOjE1MjU0OTEzODQsIm5iZiI6MTUyNTQ5MTM4NCwiZXhwIjoxNTI2Nzg3Mzg0LCJzdWIiOiIyNyIsInNjb3BlcyI6W119.PTSOeA2Y2-28mJ1X_yiz45RBxN84mPrScSfWyKpsaTLNFrZ_Eh8a8lde4uI5Ug3xqUyS1I7maeDFlXoKWpWkHuII6SXSFWjxO235ObQPO5xawSMptgTEkot8IQC4Mr1YrLFTNfO8UVQZlrWTHgIcKgdejSwDXlAiz1bPCw1LpLKSIsj42x9SQ5LDP7z7uFWX8JCcGEVEjZ7eVa-N1GyDTVqXB7H0hOmjGrz4Ock2LHeGM4mKUWGB1wOiNuFEM3CPzocwRwcB5XpJSwCT1pI49WJ3KHeSNIps5SKi2tU9h0NjxnKVB75no3a31L3yr2xsfOCqHNP0oV1GYR5LrsOb0Vv8jBxTM8Rg0lzJAPvffdU1fXYf0X-berEgGkF3wrBLh32xxt9J98M-b1m5ccEILDtVZxZf55x3i2AWHMCRHSw-5zfhf6fuKb6mMN_AGePtxORs_rcqWWPfdtZE-DBI45jAXoMs8NZBYWj-PQHzjLM9DbACLyKrvSGgs01YKbGGBAFbG8-a7QPpl6BrWvE0_yBenr_28WnbyK3wjEPLjtOLbaPzVeDLsO7oDuYM3KNUcGIYgQjoXOtEBy52SFs5LNJNHqR9wU1jfdu6xspqkah6XuqVN2ZycN6_maM5-ZmVc_lpx5sjE8xzS3mTHbl32vVzX7-STHJ4HUgkAaaHQDE" forHTTPHeaderField:@"Authorization"];
        [manager.requestSerializer setValue:[NSString stringWithFormat:@"Bearer %@",account.accessToken] forHTTPHeaderField:@"Authorization"];
    }
}

- (void)generateReslutData:(NSDictionary *)firstDict
{

}

#pragma mark - 离线数据操作
//读取离线数据
- (NSString *)readOfflineDataFromDB:(NSString *)key
{
    return nil;
}

//保存成功获取的离线数据到数据库
- (BOOL)saveOfflineDataToDB:(NSString *)data WithKey:(NSString *)key
{
    if (key == nil||data == nil) {
        return NO;
    }
    
    //保存离线数据到数据库
    return YES;
}

#pragma mark - Network operation
-(NSURLSessionTask *)getResultWithURL:(NSString *)url params:(NSDictionary *)parameters formatter:(MFBaseDataFormatter *)formatter success:(MFRequestSuccessCompletion)successCompletion failure:(MFRequestFailedCompletion)failureCompletion
{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    //添加共有参数
    [self addPublicParameter:params];
    //转码请求路径
    NSString *path = url;//[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    
    MFHTTPSessionManager *manager = [MFHTTPSessionManager sharedManager];
    //发起请求前给控制权限给子类实现逻辑,可以在里面设置超时时间等等
    [self willStartRequest:params Manger:(MFHTTPSessionManager *)manager];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
    NSURLSessionTask *task = [manager GET:path parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"responseObject is %@", NSStringFromClass([responseObject class]));
        [self onSuccessRequest:responseObject formatter:formatter success:successCompletion failure:failureCompletion];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"AFNetworking Error: %@", error);
        
        for (id block in manager.blocks) {
            void(^addAompletion)(id response) = block;
            addAompletion(task.response);
        }
        
        NSError *lastError = [self generateErrorWithError:error];
        if (failureCompletion) {
            failureCompletion(lastError);
        }
    }];
#pragma clang diagnostic pop
    return task;
}

-(NSURLSessionTask *)postDataWithPostURL:(NSString *)url params:(NSDictionary *)parameters formatter:(MFBaseDataFormatter *)formatter success:(MFRequestSuccessCompletion)successCompletion failure:(MFRequestFailedCompletion)failureCompletion
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    //添加共有参数
    [self addPublicParameter:params];
    //转码请求路径
    NSString *path = url;//[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    
    MFHTTPSessionManager *manager = [MFHTTPSessionManager sharedManager];
    //发起请求前给控制权限给子类实现逻辑,可以在里面设置超时时间等等
    [self willStartRequest:params Manger:(MFHTTPSessionManager *)manager];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
    NSURLSessionTask *task = [manager POST:path parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"responseObject is %@", NSStringFromClass([responseObject class]));
        [self onSuccessRequest:responseObject formatter:formatter success:successCompletion failure:failureCompletion];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"AFNetworking Error: %@", error);
        for (id block in manager.blocks) {
            void(^addAompletion)(id response) = block;
            addAompletion(task.response);
        }
        NSError *lastError = [self generateErrorWithError:error];
        if (failureCompletion) {
            failureCompletion(lastError);
        }
    }];
#pragma clang diagnostic pop
    return task;
}

- (NSURLSessionTask *)deleteWithURL:(NSString *)url params:(NSDictionary *)parameters formatter:(MFBaseDataFormatter *)formatter success:(MFRequestSuccessCompletion)successCompletion failure:(MFRequestFailedCompletion)failureCompletion
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    //添加共有参数
    [self addPublicParameter:params];
    //转码请求路径
    NSString *path = url;//[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    
    MFHTTPSessionManager *manager = [MFHTTPSessionManager sharedManager];
    //发起请求前给控制权限给子类实现逻辑,可以在里面设置超时时间等等
    [self willStartRequest:params Manger:(MFHTTPSessionManager *)manager];
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunused-variable"
    NSURLSessionTask *task = [manager DELETE:path parameters:params success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        //NSLog(@"responseObject is %@", NSStringFromClass([responseObject class]));
        [self onSuccessRequest:responseObject formatter:formatter success:successCompletion failure:failureCompletion];
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"AFNetworking Error: %@", error);
        for (id block in manager.blocks) {
            void(^addAompletion)(id response) = block;
            addAompletion(task.response);
        }
        NSError *lastError = [self generateErrorWithError:error];
        if (failureCompletion) {
            failureCompletion(lastError);
        }
    }];
#pragma clang diagnostic pop
    return task;
}


- (AFHTTPRequestOperation *)httpOperationWithURL:(NSString *)url httpMethod:(NSString *)method header:(NSDictionary *)header params:(NSDictionary *)parameters dataFormatter:(MFBaseDataFormatter *)formatter success:(MFRequestSuccessCompletion)successCompletion failure:(MFRequestFailedCompletion)failureCompletion
{
    //添加公共参数
    NSMutableDictionary *params = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    [self addPublicParameter:params];
    
    //转码请求路径
    NSString *path = url;//[url stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLPathAllowedCharacterSet]];
    
    AFHTTPRequestOperationManager *requestMgr = [[AFHTTPRequestOperationManager alloc] init];
    
    NSMutableURLRequest *request = [self requestWithMethod:method URL:path Params:params];
    for (NSString *key in header) {
       [request addValue:[header valueForKey:key] forHTTPHeaderField:key];
    }
    if (request) {
        AFHTTPRequestOperation *requestOperation = [requestMgr HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [self onSuccessRequest:responseObject formatter:formatter success:successCompletion failure:failureCompletion];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            NSLog(@"AFNetworking Error: %@", error);
            NSError *lastError = [self generateErrorWithError:error];
            if (failureCompletion) {
                failureCompletion(lastError);
            }
        }];
        return requestOperation;
    }
    
    return nil;
}

#pragma mark -- private
//返回请求对象
- (NSMutableURLRequest *)requestWithMethod:(NSString *)httpMethodName URL:(NSString *)url Params:(NSDictionary *)params
{
    if (url.length == 0) {
        return nil;
    }
    
    NSMutableString *urlString;
    if ([url containsString:@"http://"]) {
        urlString = [[NSMutableString alloc] initWithString:url];
    }else{
        urlString = [[NSMutableString alloc] initWithFormat:@"%@%@",BASE_URL,url];
    }
    NSMutableURLRequest *request;// = [[NSMutableURLRequest alloc] init];
    if ([httpMethodName isEqualToString:@"POST"]) {
        request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
        request.HTTPMethod = httpMethodName;
        NSString *bodyString = [self queryString:params];
        request.HTTPBody = [bodyString dataUsingEncoding:NSUTF8StringEncoding];
        
    }else{
        
        NSString *queryString = [self queryString:params];
        [urlString appendString:@"?"];
        [urlString appendString:queryString];
        
        request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:urlString]];
    }
    
    return request;
}

//合成请求参数
- (NSString *)queryString:(NSDictionary *)params
{
    if (params == nil) {
        return nil;
    }
    
    NSMutableArray *paramsArray = [[NSMutableArray alloc] init];
    [params enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        
        NSString *valueStr= [[NSString alloc] initWithFormat:@"%@",obj];
        
        NSString *value = [valueStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]];
        NSString *str = [[NSString alloc] initWithFormat:@"%@=%@",key,value];
        
        [paramsArray addObject:str];
    }];
    
    return [paramsArray componentsJoinedByString:@"&"];
}
//生成error对象
- (NSError *)generateErrorWithError:(NSError *)originError
{
    NSError *error = originError;
    switch ([originError code]) {
            case NSURLErrorCannotFindHost:
        {
            error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorCannotFindHost  userInfo:@{NSLocalizedDescriptionKey:@"没有发现服务器"}];
        }
            break;
            case NSURLErrorCannotConnectToHost:
        {
            error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorCannotConnectToHost  userInfo:@{NSLocalizedDescriptionKey:@"无法连接服务器"}];
        }
            break;
            case NSURLErrorTimedOut:
        {
            error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorTimedOut  userInfo:@{NSLocalizedDescriptionKey:@"请求超时"}];
        }
            break;
            case NSURLErrorNotConnectedToInternet:
        {
            error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet  userInfo:@{NSLocalizedDescriptionKey:@"无法连接到网络"}];
        }
            break;
        default:
            break;
    }
    return error;
}

#pragma mark -- 返回结果处理
- (void)onSuccessRequest:(id)responseObject formatter:(MFBaseDataFormatter *)formatter success:(MFRequestSuccessCompletion)successCompletion failure:(MFRequestFailedCompletion)failureCompletion
{
    //如果获取空数据
    if (responseObject == nil) {
        [self failedEndRequest:nil];
        NSError *error = [NSError errorWithDomain:NSURLErrorDomain code:1001  userInfo:@{NSLocalizedDescriptionKey:@"没有获取到任何数据"}];
        failureCompletion(error);
        return;
    }
    //成功返回之前，先把控制权限给子类，用于处理返回数据的初始化
    [self successEndRequest:responseObject];
    if (successCompletion) {
        id lastResponse = responseObject;
        if (formatter) {
            lastResponse = [formatter convertModelFromJsonObject:responseObject];
            successCompletion(lastResponse);
        }else{
            successCompletion(lastResponse);
        }
    }
}

#pragma mark -- 请求过程跟踪函数，开始，成功返回，失败返回
- (void)willStartRequest:(NSDictionary *)param Manger:(MFHTTPSessionManager *)manger
{
    //设置超时时间
    [manger.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manger.requestSerializer.timeoutInterval = self.timeOut;
    [manger.requestSerializer didChangeValueForKey:@"timeoutInterval"];
}

- (void)successEndRequest:(NSDictionary *)responseObject
{

}

- (void)failedEndRequest:(NSDictionary *)param
{
    
}

- (NSString *)replaceSpaceCharWithString:(NSString *)originString
{
    NSString *trimmedString = [originString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    return trimmedString;
}

- (BOOL)checkNullOrNilValue:(id)value
{
    if (value == nil||value == [NSNull null]) {
        return YES;
    }
    
    return NO;
}

- (void)dealloc
{
    //NSLog(@"销毁请求对象----------");
}

@end
