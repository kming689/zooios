//
//  MFRequestOperationManager.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@class AFHTTPRequestOperation;

typedef void(^MFRequestOperationCompletion)(AFHTTPRequestOperation *operation);

@interface MFRequestOperationManager : MFObject

//将有序请求按顺序添加到队列发起任务
- (void)startHttpRequestOperation:(NSArray *)operations withCompletion:(MFRequestOperationCompletion)completion;

@end
