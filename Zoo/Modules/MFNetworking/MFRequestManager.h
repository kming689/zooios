//
//  MFRequestManager.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@class AFHTTPRequestOperation;
@class MFHTTPSessionManager;
@class MFBaseDataFormatter;

typedef void(^MFRequestSuccessCompletion)(id respose);
typedef void(^MFRequestFailedCompletion)(NSError *error);
typedef void(^MFRequestManagerCompletion)(NSError *error);

@interface MFRequestManager : MFObject

//连接超时
@property (nonatomic, assign) float timeOut;

//该请求是否存储离线数据跟访问离线数据，默认为YES，登陆接口下为NO
@property (nonatomic, assign) BOOL  isOffline;

+ (MFRequestManager *)manager;

- (void)addCompletionBlock:(void(^)(id response))completion;

/**
 GET方式获取数据

 @param url 接口路径
 @param parameters 参数
 @param formatter 数据格式化工具，通过重写方法将网络请求返回的数据进行格式化
 @param successCompletion 成功
 @param failureCompletion 失败
 @return 网络任务，可用于取消
 */
-(NSURLSessionTask *)getResultWithURL:(NSString *)url params:(NSDictionary *)parameters formatter:(MFBaseDataFormatter *)formatter success:(MFRequestSuccessCompletion)successCompletion failure:(MFRequestFailedCompletion)failureCompletion;


/**
 POST方式获取数据
 
 @param url 接口路径
 @param parameters 参数
 @param formatter 数据格式化工具，通过重写方法将网络请求返回的数据进行格式化
 @param successCompletion 成功
 @param failureCompletion 失败
 @return 网络任务，可用于取消
 */
-(NSURLSessionTask *)postDataWithPostURL:(NSString *)url params:(NSDictionary *)parameters formatter:(MFBaseDataFormatter *)formatter success:(MFRequestSuccessCompletion)successCompletion failure:(MFRequestFailedCompletion)failureCompletion;

/**
 DELETE方式获取数据
 
 @param url 接口路径
 @param parameters 参数
 @param formatter 数据格式化工具，通过重写方法将网络请求返回的数据进行格式化
 @param successCompletion 成功
 @param failureCompletion 失败
 @return 网络任务，可用于取消
 */
-(NSURLSessionTask *)deleteWithURL:(NSString *)url params:(NSDictionary *)parameters formatter:(MFBaseDataFormatter *)formatter success:(MFRequestSuccessCompletion)successCompletion failure:(MFRequestFailedCompletion)failureCompletion;


/**
 发起一个请求，用NSURLConnecttion的方式，需要自己启动返回的operation对象才能真正发起请求

 @param url 接口路径
 @param method 方法类型，GET或者POST
 @param parameters 参数
 @param formatter 数据格式化工具
 @param successCompletion 成功
 @param failureCompletion 失败
 @return 任务对象
 */
- (AFHTTPRequestOperation *)httpOperationWithURL:(NSString *)url httpMethod:(NSString *)method header:(NSDictionary *)header params:(NSDictionary *)parameters dataFormatter:(MFBaseDataFormatter *)formatter success:(MFRequestSuccessCompletion)successCompletion failure:(MFRequestFailedCompletion)failureCompletion;

#pragma mark -- util method
//去掉前后空格
- (NSString *)replaceSpaceCharWithString:(NSString *)originString;

//检查是否是空指针
- (BOOL)checkNullOrNilValue:(id)value;

#pragma mark -- override method
//参数param表示请求参数字典
- (void)willStartRequest:(NSDictionary *)param Manger:(MFHTTPSessionManager *)manger;
//成功返回请求
- (void)successEndRequest:(NSDictionary *)responseObject;
//失败返回
- (void)failedEndRequest:(NSDictionary *)param;

@end
