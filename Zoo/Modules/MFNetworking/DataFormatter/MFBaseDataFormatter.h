//
//  MFBaseDataFormatter.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@interface MFBaseDataFormatter : MFObject

/**
 把网络返回的json对象转换成业务需要的模型对象，需要子类实现该方法，父类直接返回原始数据。只有特别复杂的，json跟模型完全对不上的才需要用到该类，json跟模型能对上的，直接返回后用yymode进行转换就行了

 @param jsonObject json对象
 @return 模型对象
 */
- (id)convertModelFromJsonObject:(id)jsonObject;

//检查是否是空指针
- (BOOL)checkNullOrNilValue:(id)value;

@end
