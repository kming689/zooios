//
//  MFBaseDataFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBaseDataFormatter.h"

@implementation MFBaseDataFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    return jsonObject;
}

- (BOOL)checkNullOrNilValue:(id)value
{
    if (value == nil||value == [NSNull null]) {
        return YES;
    }
    
    return NO;
}

@end
