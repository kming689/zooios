//
//  MFHTTPSessionManager.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFHTTPSessionManager.h"
#import "MFNetConstants.h"

@interface MFHTTPSessionManager()

@property (nonatomic, strong, readwrite) NSMutableArray *blocks;

@end

@implementation MFHTTPSessionManager

+(MFHTTPSessionManager *)sharedManager{
    static MFHTTPSessionManager *manager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[MFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:BASE_URL]];
        NSArray *acceptTypes = [NSArray arrayWithObjects:@"text/plain",@"text/html", nil];
        AFHTTPResponseSerializer *jsonSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingMutableContainers];
        jsonSerializer.acceptableContentTypes = [jsonSerializer.acceptableContentTypes setByAddingObjectsFromArray:acceptTypes];
        
        manager.responseSerializer = jsonSerializer;
        manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];
        
        manager.blocks = [[NSMutableArray alloc] init];
    });
    
    return manager;
}

- (void)addCompletionBlock:(void (^)(id))completion
{
    [self.blocks addObject:completion];
}

- (void)dealloc
{

}

@end
