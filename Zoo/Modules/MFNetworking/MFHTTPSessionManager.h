//
//  MFHTTPSessionManager.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

@interface MFHTTPSessionManager : AFHTTPSessionManager

@property (nonatomic, strong, readonly) NSMutableArray *blocks;

+(MFHTTPSessionManager *)sharedManager;

- (void)addCompletionBlock:(void(^)(id response))completion;

@end
