//
//  MFPopupItem.h
//  MFPopView
//
//  Created by tanfameng on 2017/6/20.
//  Copyright © 2017年 tanfameng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void(^MFPopupItemHandler)(NSInteger index);

@interface MFPopupItem : NSObject

@property (nonatomic, assign) BOOL     highlight;
@property (nonatomic, assign) BOOL     disabled;

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) UIColor  *color;

@property (nonatomic, copy)   MFPopupItemHandler handler;

@end

typedef NS_ENUM(NSUInteger, MFItemType) {
    MFItemTypeNormal,
    MFItemTypeHighlight,
    MFItemTypeDisabled
};

NS_INLINE MFPopupItem* MFItemMake(NSString *title, MFItemType type, MFPopupItemHandler handler)
{
    MFPopupItem *item = [MFPopupItem new];
    
    item.title = title;
    item.handler = handler;
    
    switch (type)
    {
        case MFItemTypeNormal:
        {
            break;
        }
        case MFItemTypeHighlight:
        {
            item.highlight = YES;
            break;
        }
        case MFItemTypeDisabled:
        {
            item.disabled = YES;
            break;
        }
        default:
            break;
    }
    
    return item;
}
