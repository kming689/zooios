//
//  MFPopupView.h
//  MFPopView
//
//  Created by tanfameng on 2017/6/20.
//  Copyright © 2017年 tanfameng. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MFPopupItem.h"
#import "MFPopupWindow.h"
#import "MFPopupCategory.h"
#import "MFPopupDefine.h"

typedef NS_ENUM(NSUInteger, MFPopupType) {
    MFPopupTypeAlert,
    MFPopupTypeSheet,
    MFPopupTypeCustom,
};

@class MFPopupView;

typedef void(^MFPopupBlock)(MFPopupView *);
typedef void(^MFPopupCompletionBlock)(MFPopupView *, BOOL);

@interface MFPopupView : UIView

@property (nonatomic, assign, readonly) BOOL           visible;             // default is NO.

@property (nonatomic, strong          ) UIView         *attachedView;       // default is MFPopupWindow. You can attach MFPopupView to any UIView.

@property (nonatomic, assign          ) MFPopupType    type;                // default is MFPopupTypeAlert.
@property (nonatomic, assign          ) NSTimeInterval animationDuration;   // default is 0.3 sec.
@property (nonatomic, assign          ) BOOL           withKeyboard;        // default is NO. When YES, alert view with be shown with a center offset (only effect with MFPopupTypeAlert).

@property (nonatomic, copy            ) MFPopupCompletionBlock   showCompletionBlock; // show completion block.
@property (nonatomic, copy            ) MFPopupCompletionBlock   hideCompletionBlock; // hide completion block

@property (nonatomic, copy            ) MFPopupBlock   showAnimation;       // custom show animation block.
@property (nonatomic, copy            ) MFPopupBlock   hideAnimation;       // custom hide animation block.

/**
 *  override this method to show the keyboard if with a keyboard
 */
- (void) showKeyboard;

/**
 *  override this method to hide the keyboard if with a keyboard
 */
- (void) hideKeyboard;


/**
 *  show the popup view
 */
- (void) show;

/**
 *  show the popup view with completiom block
 *
 *  @param block show completion block
 */
- (void) showWithBlock:(MFPopupCompletionBlock)block;

/**
 *  hide the popup view
 */
- (void) hide;

/**
 *  hide the popup view with completiom block
 *
 *  @param block hide completion block
 */
- (void) hideWithBlock:(MFPopupCompletionBlock)block;

/**
 *  hide all popupview with current class, eg. [MFAlertview hideAll];
 */
+ (void) hideAll;

@end
