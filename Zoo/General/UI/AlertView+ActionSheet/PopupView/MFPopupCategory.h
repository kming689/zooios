//
//  UIView+MFPopup.h
//  MFPopView
//
//  Created by tanfameng on 2017/6/20.
//  Copyright © 2017年 tanfameng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MFPopup)

+ (UIColor *) mf_colorWithHex:(NSUInteger)hex;

@end


@interface UIImage (MFPopup)

+ (UIImage *) mf_imageWithColor:(UIColor *)color;

+ (UIImage *) mf_imageWithColor:(UIColor *)color Size:(CGSize)size;

- (UIImage *) mf_stretched;

@end


@interface UIButton (MFPopup)

+ (id) mf_buttonWithTarget:(id)target action:(SEL)sel;

@end


@interface NSString (MFPopup)

- (NSString *)mf_truncateByCharLength:(NSUInteger)charLength;

@end


@interface UIView (MFPopup)

@property (nonatomic, strong, readonly ) UIView            *mf_dimBackgroundView;
@property (nonatomic, assign, readonly ) BOOL              mf_dimBackgroundAnimating;
@property (nonatomic, assign           ) NSTimeInterval    mf_dimAnimationDuration;

@property (nonatomic, strong, readonly ) UIView            *mf_dimBackgroundBlurView;
@property (nonatomic, assign           ) BOOL              mf_dimBackgroundBlurEnabled;
@property (nonatomic, assign           ) UIBlurEffectStyle mf_dimBackgroundBlurEffectStyle;

- (void) mf_showDimBackground;
- (void) mf_hideDimBackground;

- (void) mf_distributeSpacingHorizontallyWith:(NSArray*)view;
- (void) mf_distributeSpacingVerticallyWith:(NSArray*)view;


@end
