//
//  Header.h
//  MFPopView
//
//  Created by tanfameng on 2017/6/20.
//  Copyright © 2017年 tanfameng. All rights reserved.
//

#ifndef Header_h
#define Header_h

#define MFWeakify(o)        __weak   typeof(self) mmwo = o;
#define MFStrongify(o)      __strong typeof(self) o = mmwo;
#define MFHexColor(color)   [UIColor mf_colorWithHex:color]
#define MF_SPLIT_WIDTH      (1/[UIScreen mainScreen].scale)

#endif /* Header_h */
