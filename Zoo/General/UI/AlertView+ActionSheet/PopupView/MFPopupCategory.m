//
//  UIView+MFPopup.m
//  MFPopView
//
//  Created by tanfameng on 2017/6/20.
//  Copyright © 2017年 tanfameng. All rights reserved.
//

#import "MFPopupCategory.h"

#import "MFPopupDefine.h"
#import "MFPopupWindow.h"
#import <Masonry/Masonry.h>
#import <objc/runtime.h>
#import "MFPopupView.h"

@implementation UIColor (MFPopup)

+ (UIColor *) mf_colorWithHex:(NSUInteger)hex {
    
    float r = (hex & 0xff000000) >> 24;
    float g = (hex & 0x00ff0000) >> 16;
    float b = (hex & 0x0000ff00) >> 8;
    float a = (hex & 0x000000ff);
    
    return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a/255.0];
}

@end

@implementation UIImage (MFPopup)

+ (UIImage *) mf_imageWithColor:(UIColor *)color {
    return [UIImage mf_imageWithColor:color Size:CGSizeMake(4.0f, 4.0f)];
}

+ (UIImage *) mf_imageWithColor:(UIColor *)color Size:(CGSize)size
{
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return [image mf_stretched];
}

- (UIImage *) mf_stretched
{
    CGSize size = self.size;
    
    UIEdgeInsets insets = UIEdgeInsetsMake(truncf(size.height-1)/2, truncf(size.width-1)/2, truncf(size.height-1)/2, truncf(size.width-1)/2);
    
    return [self resizableImageWithCapInsets:insets];
}

@end

@implementation UIButton (MFPopup)

+ (id) mf_buttonWithTarget:(id)target action:(SEL)sel
{
    id btn = [self buttonWithType:UIButtonTypeCustom];
    [btn addTarget:target action:sel forControlEvents:UIControlEventTouchUpInside];
    [btn setExclusiveTouch:YES];
    return btn;
}

@end

@implementation NSString (MFPopup)

- (NSString *)mf_truncateByCharLength:(NSUInteger)charLength
{
    __block NSUInteger length = 0;
    [self enumerateSubstringsInRange:NSMakeRange(0, [self length])
                             options:NSStringEnumerationByComposedCharacterSequences
                          usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                              
                              if ( length+substringRange.length > charLength )
                              {
                                  *stop = YES;
                                  return;
                              }
                              
                              length+=substringRange.length;
                          }];
    
    return [self substringToIndex:length];
}

@end


static const void *mf_dimReferenceCountKey            = &mf_dimReferenceCountKey;

static const void *mf_dimBackgroundViewKey            = &mf_dimBackgroundViewKey;
static const void *mf_dimAnimationDurationKey         = &mf_dimAnimationDurationKey;
static const void *mf_dimBackgroundAnimatingKey       = &mf_dimBackgroundAnimatingKey;

static const void *mf_dimBackgroundBlurViewKey        = &mf_dimBackgroundBlurViewKey;
static const void *mf_dimBackgroundBlurEnabledKey     = &mf_dimBackgroundBlurEnabledKey;
static const void *mf_dimBackgroundBlurEffectStyleKey = &mf_dimBackgroundBlurEffectStyleKey;

@interface UIView (MFPopupInner)

@property (nonatomic, assign, readwrite) NSInteger mf_dimReferenceCount;

@end

@implementation UIView (MFPopupInner)

@dynamic mf_dimReferenceCount;


- (NSInteger)mf_dimReferenceCount {
    return [objc_getAssociatedObject(self, mf_dimReferenceCountKey) integerValue];
}

- (void)setMf_dimReferenceCount:(NSInteger)mf_dimReferenceCount
{
    objc_setAssociatedObject(self, mf_dimReferenceCountKey, @(mf_dimReferenceCount), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end

@implementation UIView (MFPopup)

@dynamic mf_dimBackgroundView;
@dynamic mf_dimAnimationDuration;
@dynamic mf_dimBackgroundAnimating;
@dynamic mf_dimBackgroundBlurView;


//mf_dimBackgroundView
- (UIView *)mf_dimBackgroundView
{
    UIView *dimView = objc_getAssociatedObject(self, mf_dimBackgroundViewKey);
    
    if ( !dimView )
    {
        dimView = [UIView new];
        [self addSubview:dimView];
        [dimView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self);
        }];
        dimView.alpha = 0.0f;
        dimView.backgroundColor = MFHexColor(0x0000007F);
        dimView.layer.zPosition = FLT_MAX;
        
        self.mf_dimAnimationDuration = 0.3f;
        
        //添加点击事件，隐藏弹出窗口
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewBeTaped)];
        [dimView addGestureRecognizer:tap];
        
        objc_setAssociatedObject(self, mf_dimBackgroundViewKey, dimView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return dimView;
}

- (void)viewBeTaped
{
    for ( UIView *v in self.mf_dimBackgroundView.subviews )
    {
        if ( [v isKindOfClass:[MFPopupView class]] )
        {
            MFPopupView *popupView = (MFPopupView*)v;
            [popupView hide];
        }
    }
}

//mf_dimBackgroundBlurEnabled
- (BOOL)mf_dimBackgroundBlurEnabled
{
    return [objc_getAssociatedObject(self, mf_dimBackgroundBlurEnabledKey) boolValue];
}

- (void)setMf_dimBackgroundBlurEnabled:(BOOL)mf_dimBackgroundBlurEnabled
{
    objc_setAssociatedObject(self, mf_dimBackgroundBlurEnabledKey, @(mf_dimBackgroundBlurEnabled), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    if ( mf_dimBackgroundBlurEnabled )
    {
        self.mf_dimBackgroundView.backgroundColor = MFHexColor(0x00000000);
        self.mf_dimBackgroundBlurEffectStyle = self.mf_dimBackgroundBlurEffectStyle;
        self.mf_dimBackgroundBlurView.hidden = NO;
    }
    else
    {
        self.mf_dimBackgroundView.backgroundColor = MFHexColor(0x0000007F);
        self.mf_dimBackgroundBlurView.hidden = YES;
    }
}

//mf_dimBackgroundBlurEffectStyle
- (UIBlurEffectStyle)mf_dimBackgroundBlurEffectStyle
{
    return [objc_getAssociatedObject(self, mf_dimBackgroundBlurEffectStyleKey) integerValue];
}

- (void)setMf_dimBackgroundBlurEffectStyle:(UIBlurEffectStyle)mf_dimBackgroundBlurEffectStyle
{
    objc_setAssociatedObject(self, mf_dimBackgroundBlurEffectStyleKey, @(mf_dimBackgroundBlurEffectStyle), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    if ( self.mf_dimBackgroundBlurEnabled )
    {
        [self.mf_dimBackgroundBlurView removeFromSuperview];
        self.mf_dimBackgroundBlurView = nil;
        
        UIView *blurView = [self mf_dimBackgroundBlurView];
        [self.mf_dimBackgroundView addSubview:blurView];
        [blurView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.equalTo(self.mf_dimBackgroundView);
        }];
    }
}

//mf_dimBackgroundBlurView
- (UIView *)mf_dimBackgroundBlurView
{
    UIView *blurView = objc_getAssociatedObject(self, mf_dimBackgroundBlurViewKey);
    
    if ( !blurView )
    {
        blurView = [UIView new];
        
        if ( [UIVisualEffectView class] )
        {
            UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:self.mf_dimBackgroundBlurEffectStyle]];
            [blurView addSubview:effectView];
            [effectView mas_makeConstraints:^(MASConstraintMaker *make) {
                make.edges.equalTo(blurView);
            }];
        }
        else
        {
            blurView.backgroundColor = @[MFHexColor(0x000007F),MFHexColor(0xFFFFFF7F),MFHexColor(0xFFFFFF7F)][self.mf_dimBackgroundBlurEffectStyle];
        }
        blurView.userInteractionEnabled = NO;
        
        objc_setAssociatedObject(self, mf_dimBackgroundBlurViewKey, blurView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
    
    return blurView;
}

- (void)setMf_dimBackgroundBlurView:(UIView *)mf_dimBackgroundBlurView
{
    objc_setAssociatedObject(self, mf_dimBackgroundBlurViewKey, mf_dimBackgroundBlurView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

//mf_dimBackgroundAnimating
- (BOOL)mf_dimBackgroundAnimating
{
    return [objc_getAssociatedObject(self, mf_dimBackgroundAnimatingKey) boolValue];
}

- (void)setMf_dimBackgroundAnimating:(BOOL)mf_dimBackgroundAnimating
{
    objc_setAssociatedObject(self, mf_dimBackgroundAnimatingKey, @(mf_dimBackgroundAnimating), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

//mf_dimAnimationDuration
- (NSTimeInterval)mf_dimAnimationDuration
{
    return [objc_getAssociatedObject(self, mf_dimAnimationDurationKey) doubleValue];
}

- (void)setMf_dimAnimationDuration:(NSTimeInterval)mf_dimAnimationDuration
{
    objc_setAssociatedObject(self, mf_dimAnimationDurationKey, @(mf_dimAnimationDuration), OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (void)mf_showDimBackground
{
    ++self.mf_dimReferenceCount;
    
    if ( self.mf_dimReferenceCount > 1 )
    {
        return;
    }
    
    self.mf_dimBackgroundView.hidden = NO;
    self.mf_dimBackgroundAnimating = YES;
    
    if ( self == [MFPopupWindow sharedWindow].attachView )
    {
        [MFPopupWindow sharedWindow].hidden = NO;
        [[MFPopupWindow sharedWindow] makeKeyAndVisible];
    }
    else if ( [self isKindOfClass:[UIWindow class]] )
    {
        self.hidden = NO;
        [(UIWindow*)self makeKeyAndVisible];
    }
    else
    {
        [self bringSubviewToFront:self.mf_dimBackgroundView];
    }
    
    [UIView animateWithDuration:self.mf_dimAnimationDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         self.mf_dimBackgroundView.alpha = 1.0f;
                         
                     } completion:^(BOOL finished) {
                         
                         if ( finished )
                         {
                             self.mf_dimBackgroundAnimating = NO;
                         }
                         
                     }];
}

- (void)mf_hideDimBackground
{
    --self.mf_dimReferenceCount;
    
    if ( self.mf_dimReferenceCount > 0 )
    {
        return;
    }
    
    self.mf_dimBackgroundAnimating = YES;
    [UIView animateWithDuration:self.mf_dimAnimationDuration
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn | UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         
                         self.mf_dimBackgroundView.alpha = 0.0f;
                         
                     } completion:^(BOOL finished) {
                         
                         if ( finished )
                         {
                             self.mf_dimBackgroundAnimating = NO;
                             
                             if ( self == [MFPopupWindow sharedWindow].attachView )
                             {
                                 [MFPopupWindow sharedWindow].hidden = YES;
                                 [[[UIApplication sharedApplication].delegate window] makeKeyWindow];
                             }
                             else if ( self == [MFPopupWindow sharedWindow] )
                             {
                                 self.hidden = YES;
                                 [[[UIApplication sharedApplication].delegate window] makeKeyWindow];
                             }
                         }
                     }];
}

- (void) mf_distributeSpacingHorizontallyWith:(NSArray*)views
{
    NSMutableArray *spaces = [NSMutableArray arrayWithCapacity:views.count+1];
    
    for ( int i = 0 ; i < views.count+1 ; ++i )
    {
        UIView *v = [UIView new];
        [spaces addObject:v];
        [self addSubview:v];
        
        [v mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(v.mas_height);
        }];
    }
    
    UIView *v0 = spaces[0];
    
    [v0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.mas_left);
        make.centerY.equalTo(((UIView*)views[0]).mas_centerY);
    }];
    
    UIView *lastSpace = v0;
    for ( int i = 0 ; i < views.count; ++i )
    {
        UIView *obj = views[i];
        UIView *space = spaces[i+1];
        
        [obj mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(lastSpace.mas_right);
        }];
        
        [space mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(obj.mas_right);
            make.centerY.equalTo(obj.mas_centerY);
            make.width.equalTo(v0);
        }];
        
        lastSpace = space;
    }
    
    [lastSpace mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.mas_right);
    }];
    
}

- (void) mf_distributeSpacingVerticallyWith:(NSArray*)views
{
    NSMutableArray *spaces = [NSMutableArray arrayWithCapacity:views.count+1];
    
    for ( int i = 0 ; i < views.count+1 ; ++i )
    {
        UIView *v = [UIView new];
        [spaces addObject:v];
        [self addSubview:v];
        
        [v mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(v.mas_height);
        }];
    }
    
    UIView *v0 = spaces[0];
    
    [v0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.mas_top);
        make.centerX.equalTo(((UIView*)views[0]).mas_centerX);
    }];
    
    UIView *lastSpace = v0;
    for ( int i = 0 ; i < views.count; ++i )
    {
        UIView *obj = views[i];
        UIView *space = spaces[i+1];
        
        [obj mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(lastSpace.mas_bottom);
        }];
        
        [space mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(obj.mas_bottom);
            make.centerX.equalTo(obj.mas_centerX);
            make.height.equalTo(v0);
        }];
        
        lastSpace = space;
    }
    
    [lastSpace mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.mas_bottom);
    }];
    
}

@end
