//
//  MFPopupWindow.h
//  MFPopView
//
//  Created by tanfameng on 2017/6/20.
//  Copyright © 2017年 tanfameng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MFPopupWindow : UIWindow

@property (nonatomic, assign) BOOL touchWildToHide; // default is NO. When YES, popup views will be hidden when user touch the translucent background.
@property (nonatomic, readonly) UIView* attachView;

+ (MFPopupWindow *)sharedWindow;

/**
 *  cache the window to prevent the lag of the first showing.
 */
- (void) cacheWindow;

@end
