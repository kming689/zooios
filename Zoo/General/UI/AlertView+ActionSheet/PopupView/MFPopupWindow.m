//
//  MFPopupWindow.m
//  MFPopView
//
//  Created by tanfameng on 2017/6/20.
//  Copyright © 2017年 tanfameng. All rights reserved.
//

#import "MFPopupWindow.h"

#import "MFPopupCategory.h"
#import "MFPopupDefine.h"
#import "MFPopupView.h"

@interface MFPopupWindow()<UIGestureRecognizerDelegate>

@end

@implementation MFPopupWindow

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if ( self )
    {
        self.windowLevel = UIWindowLevelStatusBar + 1;
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionTap:)];
        gesture.cancelsTouchesInView = NO;
        gesture.delegate = self;
        [self addGestureRecognizer:gesture];
    }
    return self;
}

+ (MFPopupWindow *)sharedWindow
{
    static MFPopupWindow *window;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        window = [[MFPopupWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        window.rootViewController = [UIViewController new];
    });
    
    return window;
}

- (void)cacheWindow
{
    [self makeKeyAndVisible];
    [[[UIApplication sharedApplication].delegate window] makeKeyAndVisible];
    
    [self attachView].mf_dimBackgroundView.hidden = YES;
    self.hidden = YES;
}

- (void)actionTap:(UITapGestureRecognizer*)gesture
{
    if ( self.touchWildToHide && !self.mf_dimBackgroundAnimating )
    {
        for ( UIView *v in [self attachView].mf_dimBackgroundView.subviews )
        {
            if ( [v isKindOfClass:[MFPopupView class]] )
            {
                MFPopupView *popupView = (MFPopupView*)v;
                [popupView hide];
            }
        }
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    return ( touch.view == self.attachView.mf_dimBackgroundView );
}

- (UIView *)attachView
{
    return self.rootViewController.view;
}

@end
