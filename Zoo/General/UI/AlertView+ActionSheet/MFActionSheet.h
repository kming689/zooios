//
//  MFActionSheet.h
//  MFPopView
//
//  Created by tanfameng on 2017/6/20.
//  Copyright © 2017年 tanfameng. All rights reserved.
//

#import "MFPopupView.h"
#import "MFPopupDefine.h"


@interface MFSheetViewConfig : NSObject

+ (MFSheetViewConfig*) globalConfig;

@property (nonatomic, assign) CGFloat buttonHeight;         // Default is 50.
@property (nonatomic, assign) CGFloat innerMargin;          // Default is 19.

@property (nonatomic, assign) CGFloat titleFontSize;        // Default is 14.
@property (nonatomic, assign) CGFloat buttonFontSize;       // Default is 17.

@property (nonatomic, strong) UIColor *backgroundColor;     // Default is #FFFFFF.
@property (nonatomic, strong) UIColor *titleColor;          // Default is #666666.
@property (nonatomic, strong) UIColor *splitColor;          // Default is clear. user effect

@property (nonatomic, strong) UIColor *itemNormalColor;     // Default is #333333. effect with MFItemTypeNormal
@property (nonatomic, strong) UIColor *itemDisableColor;    // Default is #CCCCCC. effect with MFItemTypeDisabled
@property (nonatomic, strong) UIColor *itemHighlightColor;  // Default is #E76153. effect with MFItemTypeHighlight
@property (nonatomic, strong) UIColor *itemPressedColor;    // Default is #EFEDE7.

@property (nonatomic, strong) NSString *defaultTextCancel;  // Default is "取消"

@end

@interface MFActionSheet : MFPopupView

//

/**
 初始化actionSheet

 @param title 标题
 @param items 参数items为MFPopupItem类型对象的数组NSArray<MFPopupItem *> *items
 @return 实例对象
 */
- (instancetype) initWithTitle:(NSString *)title
                         items:(NSArray *)items;

- (instancetype) initWithTitle:(NSString *)title
                         items:(NSArray *)items config:(MFSheetViewConfig *)config;

@end
