//
//  MFAlertView.h
//  MFPopView
//
//  Created by tanfameng on 2017/6/20.
//  Copyright © 2017年 tanfameng. All rights reserved.
//

/*
 使用步骤：
 1、使用提供的初始化方法创建MFAlertView对象
 2、调用show方法
 */

#import "MFPopupView.h"

#import "MFPopupDefine.h"

typedef void(^MFPopupInputHandler)(NSString *text,MFPopupView *alertView);

@interface MFAlertViewConfig : NSObject

+ (MFAlertViewConfig*) globalConfig;

@property (nonatomic, assign) CGFloat width;                // Default is 275.
@property (nonatomic, assign) CGFloat buttonHeight;         // Default is 50.
@property (nonatomic, assign) CGFloat innerMargin;          // Default is 25.
@property (nonatomic, assign) CGFloat cornerRadius;         // Default is 5.

@property (nonatomic, assign) CGFloat titleFontSize;        // Default is 18.
@property (nonatomic, assign) CGFloat detailFontSize;       // Default is 14.
@property (nonatomic, assign) CGFloat buttonFontSize;       // Default is 17.

@property (nonatomic, strong) UIColor *backgroundColor;     // Default is #FFFFFF.
@property (nonatomic, strong) UIColor *titleColor;          // Default is #333333.
@property (nonatomic, strong) UIColor *detailColor;         // Default is #333333.
@property (nonatomic, strong) UIColor *splitColor;          // Default is #CCCCCC.

@property (nonatomic, strong) UIColor *itemNormalColor;     // Default is #333333. effect with MFItemTypeNormal
@property (nonatomic, strong) UIColor *itemHighlightColor;  // Default is #E76153. effect with MFItemTypeHighlight
@property (nonatomic, strong) UIColor *itemPressedColor;    // Default is #EFEDE7.

@property (nonatomic, strong) NSString *defaultTextOK;      // Default is "好".
@property (nonatomic, strong) NSString *defaultTextCancel;  // Default is "取消".
@property (nonatomic, strong) NSString *defaultTextConfirm; // Default is "确定".

@end

@interface MFAlertView : MFPopupView

@property (nonatomic, assign) NSUInteger maxInputLength;    //默认是0，表示没有长度限制

/**
 实例化一个带输入框alertView

 @param title 标题
 @param detail 详细内容
 @param inputPlaceholder 输入框占位符
 @param inputHandler 字符串输入回调
 @return 实例对象
 */
- (instancetype) initWithInputTitle:(NSString *)title
                             detail:(NSString *)detail
                        placeholder:(NSString *)inputPlaceholder
                            handler:(MFPopupInputHandler)inputHandler;

- (instancetype) initWithInputTitle:(NSString *)title
                             detail:(NSString *)detail
                        placeholder:(NSString *)inputPlaceholder
                            handler:(MFPopupInputHandler)inputHandler
                             config:(MFAlertViewConfig *)config;

/**
 实例化一个带确定按钮的alertView

 @param title 标题
 @param detail 详细内容
 @return 实例对象
 */
- (instancetype) initWithConfirmTitle:(NSString *)title
                               detail:(NSString *)detail;

- (instancetype) initWithConfirmTitle:(NSString *)title
                               detail:(NSString *)detail
                                config:(MFAlertViewConfig *)config;

/**
 自定义按钮个数的alertView

 @param title 标题
 @param detail 详细内容
 @param items 自定义按钮数组NSArray<MFPopupItem *> *items
 @return 实例对象
 */
- (instancetype) initWithTitle:(NSString *)title
                        detail:(NSString *)detail
                         items:(NSArray *)items;

- (instancetype) initWithTitle:(NSString *)title
                        detail:(NSString *)detail
                         items:(NSArray *)items
                         config:(MFAlertViewConfig *)config;

@end
