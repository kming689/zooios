//
//  MFFileCategorySegmentedView.m
//  Pods
//
//  Created by tanfameng on 2017/7/3.
//
//

#import "MFFileCategorySegmentedView.h"
#import "MFColor.h"

#define UIColorFromHEX(hex) [UIColor colorWithRed:((float)((hex & 0xFF0000) >> 16))/255.0 green:((float)((hex & 0xFF00) >> 8))/255.0 blue:((float)(hex & 0xFF))/255.0 alpha:1.0]

@interface MFSegmentedButton : UIButton
@end

@implementation MFSegmentedButton
- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent*)event{
    CGRect bounds = self.bounds;
    CGFloat widthDelta = MAX(44.0 - bounds.size.width, 0);
    CGFloat heightDelta = MAX(44.0 - bounds.size.height, 0);
    bounds = CGRectInset(bounds, -0.5 * widthDelta, -0.5 * heightDelta);
    return CGRectContainsPoint(bounds, point);
}
@end


@interface MFFileCategorySegmentedView ()
{
    NSUInteger lastIndex;
    MFSegmentedButton *currentButton;
}

@property(nonatomic,strong) NSArray *segmentedTitles;
@property(nonatomic,strong) UIScrollView *scrollView;
@property(nonatomic,strong) NSMutableArray *btns;
@property(nonatomic,strong) UIView *indicatorBar;
@property(nonatomic,assign) CGSize indicatorBarSize;
@property(nonatomic,assign) CGFloat pageWidth;

@end

@implementation MFFileCategorySegmentedView

-(instancetype)initWithTitls:(NSArray *)titles delegate:(id<MFFileSegmentedControlDelegate>)delegate {
    if (self = [super init]) {
        _indicatorBarHeight = 2;
        self.delegate = delegate;
        self.segmentedTitles = titles;
        [self setupViews];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame titls:(NSArray *)titles delegate:(id<MFFileSegmentedControlDelegate>)delegate {
    if (self = [super initWithFrame:frame]) {
        _indicatorBarHeight = 2;
        self.delegate = delegate;
        self.segmentedTitles = titles;
        [self setupViews];
    }
    return self;
}

#pragma mark - action

-(void)segmentSelectedAction:(MFSegmentedButton *)sender {
    
    currentButton = sender;
    
    NSInteger index = [self.btns indexOfObject:sender];
    if (lastIndex == index) {
        return;
    }
    
    self.indicatorBarSize = CGSizeMake(sender.titleLabel.intrinsicContentSize.width, _indicatorBarHeight);
    
    lastIndex = index;
    
    for (UIButton *segBtn in self.btns) {
        segBtn.selected = NO;
    }
    sender.selected = YES;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.indicatorBar.centerX = sender.centerX;
        CGRect frame = self.indicatorBar.frame;
        frame.size.width = self.indicatorBarSize.width;
        self.indicatorBar.frame = frame;
    }];
    
    [self adjustContentOffsetWithCenterX:sender.centerX];
    
    if ([self.delegate respondsToSelector:@selector(segmentedControl:didSelectIndex:)]) {
        [self.delegate segmentedControl:self didSelectIndex:index];
    }
}

#pragma mark - private method

-(void)setupViews {
    self.pageWidth = [UIScreen mainScreen].bounds.size.width;
    
    [self addSubview:self.scrollView];
    
    [self.scrollView addSubview:self.indicatorBar];
    
    __weak typeof(self)weakSelf = self;
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationDidEnterBackgroundNotification object:nil queue:[NSOperationQueue mainQueue] usingBlock:^(NSNotification * _Nonnull note) {
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.indicatorBar.width = strongSelf.indicatorBarSize.width;
    }];
}

-(void)adjustContentOffsetWithCenterX:(CGFloat)centerX {
    if (self.scrollView.width >= self.scrollView.contentSize.width) {
        return;
    }
    if (centerX > self.scrollView.width / 2) {
        if (centerX < self.scrollView.contentSize.width - self.scrollView.width / 2) {
            [self.scrollView setContentOffset:CGPointMake(self.indicatorBar.centerX - self.scrollView.width / 2, 0) animated:YES];
        }else {
            [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentSize.width - self.scrollView.width, 0) animated:YES];
        }
    }else {
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

#pragma mark - setter
-(void)setPageWidth:(CGFloat)pageWidth {
    _pageWidth = pageWidth;
    
    [self setOffsetX:_offsetX];
}
-(void)setSegmentedTitles:(NSArray *)segmentedTitles {
    
    _segmentedTitles = segmentedTitles;
    
    [self.btns removeAllObjects];
    
    for (UIView *view in self.scrollView.subviews) {
        if ([view isKindOfClass:[MFSegmentedButton class]]) {
            [view removeFromSuperview];
        }
    }
    
    NSInteger i = 0;
    for (NSString *title in self.segmentedTitles) {
        MFSegmentedButton *segBtn = [MFSegmentedButton buttonWithType:UIButtonTypeCustom];
        [segBtn setTitle:title forState:UIControlStateNormal];
        [segBtn setTitleColor:[MFColor mf_navigationBarTitleColor] forState:UIControlStateNormal];
        [segBtn setTitleColor:[MFColor redColor] forState:UIControlStateSelected];
        segBtn.titleLabel.font = [UIFont systemFontOfSize:15];
        [segBtn addTarget:self action:@selector(segmentSelectedAction:) forControlEvents:UIControlEventTouchUpInside];
        [segBtn sizeToFit];
        segBtn.bounds = CGRectMake(0, 0, segBtn.titleLabel.intrinsicContentSize.width, segBtn.titleLabel.intrinsicContentSize.height);
        
        [self.btns addObject:segBtn];
        [self.scrollView addSubview:segBtn];
        i ++ ;
    }
    
    UIButton *firstBtn = [self.btns firstObject];
    firstBtn.selected = YES;
    currentButton = (MFSegmentedButton *)firstBtn;
    self.indicatorBarSize = CGSizeMake(firstBtn.titleLabel.intrinsicContentSize.width, _indicatorBarHeight);
    [self setIndex:0];
}
-(void)setSegmentedTitleFont:(UIFont *)segmentedTitleFont {
    _segmentedTitleFont = segmentedTitleFont;
    
    for (MFSegmentedButton *btn in self.btns) {
        btn.titleLabel.font = segmentedTitleFont;
    }
    
    [self layoutIfNeeded];
}
-(void)setSegmentedTitleNormalColor:(UIColor *)segmentedTitleNormalColor {
    _segmentedTitleNormalColor = segmentedTitleNormalColor;
    for (MFSegmentedButton *btn in self.btns) {
        [btn setTitleColor:segmentedTitleNormalColor forState:UIControlStateNormal];
    }
}
-(void)setSegmentedTitleSelectedColor:(UIColor *)segmentedTitleSelectedColor {
    _segmentedTitleSelectedColor = segmentedTitleSelectedColor;
    for (MFSegmentedButton *btn in self.btns) {
        [btn setTitleColor:segmentedTitleSelectedColor forState:UIControlStateSelected];
    }
}
-(void)setIndicatorBarSize:(CGSize)indicatorBarSize {
    _indicatorBarSize = indicatorBarSize;
    self.indicatorBar.size = _indicatorBarSize;
    self.indicatorBar.layer.cornerRadius = self.indicatorBar.height / 2;
}

-(void)setIndicatorBarColor:(UIColor *)indicatorBarColor {
    _indicatorBarColor = indicatorBarColor;
    self.indicatorBar.backgroundColor = indicatorBarColor;
}

-(void)setIndicatorBarHeight:(CGFloat)indicatorBarHeight
{
    _indicatorBarHeight = indicatorBarHeight;
    self.indicatorBarSize = CGSizeMake(currentButton.titleLabel.intrinsicContentSize.width, _indicatorBarHeight);
}

-(void)setOffsetX:(CGFloat)offsetX {
    if (self.pageWidth == 0) {
        return;
    }
    NSInteger index = offsetX / self.pageWidth;
    CGFloat rOffset = fmodf(offsetX, self.pageWidth);
    CGFloat width = self.indicatorBarSize.width;
    
    CGFloat centerX = ((UIButton *)self.btns[index]).centerX;
    CGFloat nextCenterX = ((UIButton *)self.btns[index + 1 >= self.btns.count ? self.btns.count - 1 : index + 1]).centerX;
    CGFloat gapWidth = fabs(nextCenterX - centerX);
    
    CGFloat bOffsetX = (rOffset / self.pageWidth) * gapWidth;
    
    if (bOffsetX < gapWidth / 2) {
        CGRect frame = self.indicatorBar.frame;
        frame.origin.x = centerX - width / 2;
        self.indicatorBar.frame = frame;
        self.indicatorBar.width = bOffsetX * 2 + width;
    }else {
        CGRect frame = self.indicatorBar.frame;
        frame.origin.x = nextCenterX + width / 2 - self.indicatorBar.width;
        self.indicatorBar.frame = frame;
        self.indicatorBar.width = (gapWidth - bOffsetX) * 2 + width;
    }
}
-(void)setIndex:(NSUInteger)index {
    _index = index;
    [self segmentSelectedAction:self.btns[index]];
}

#pragma mark - getter

-(UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] init];
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}
-(UIView *)indicatorBar {
    if (!_indicatorBar) {
        _indicatorBar = [[UIView alloc] init];
        _indicatorBar.backgroundColor = UIColorFromHEX(0xffd321);
        _indicatorBar.layer.cornerRadius = 1.5;
    }
    return _indicatorBar;
}

-(NSMutableArray *)btns {
    if (!_btns) {
        _btns = [NSMutableArray array];
    }
    return _btns;
}

#pragma mark - override

-(void)layoutSubviews {
    
    self.scrollView.frame = self.bounds;
    
    //根据按钮文字长度计算按钮总宽度
    CGFloat allBtnWidth = self.bounds.size.width;
    
    CGFloat btnsWidth = 0;
    for (MFSegmentedButton *btn in self.btns) {
        btnsWidth += btn.titleLabel.intrinsicContentSize.width + 20;
    }
    
    allBtnWidth = btnsWidth;
    
    CGFloat btnSpaceWidth = 0;
    CGFloat superViewWidth = self.bounds.size.width;
    //如果总长度小于屏幕宽度，重新设置宽度
    if (allBtnWidth < superViewWidth) {
        //重新计算按钮宽度,如果所有按钮宽度加上间隙的总宽度小于父view的宽度，那么要加长每个按钮的宽度
        CGFloat allSpaceWidth = self.bounds.size.width - allBtnWidth;
        
        btnSpaceWidth = allSpaceWidth/(self.btns.count);
        
        allBtnWidth = self.bounds.size.width;
    }
    
    //重新设置按钮frame
    for (MFSegmentedButton *btn in self.btns) {
        CGRect originBounds = btn.bounds;
        originBounds.size.width = btn.titleLabel.intrinsicContentSize.width + 10 + btnSpaceWidth;
        originBounds.size.height = self.bounds.size.height - self.indicatorBarSize.height*2;
        btn.bounds = originBounds;
    }
    
    CGSize contentSize = CGSizeMake(allBtnWidth, 0);
    self.scrollView.contentSize = CGSizeMake(allBtnWidth, 0);
    
    MFSegmentedButton *lastBtn = nil;
    MFSegmentedButton *selectedBtn = nil;
    for (int j = 0; j < self.btns.count; j ++ ) {
        MFSegmentedButton *segBtn = self.btns[j];
        CGPoint btnCenter = CGPointMake(segBtn.bounds.size.width/2, segBtn.bounds.size.height/2);
        if (j == 0) {
            segBtn.center = CGPointMake(segBtn.bounds.size.width/2, segBtn.bounds.size.height/2);
        }else{
            btnCenter = CGPointMake(CGRectGetMaxX(lastBtn.frame) + segBtn.bounds.size.width/2, segBtn.bounds.size.height/2);
            segBtn.center = CGPointMake(CGRectGetMaxX(lastBtn.frame) + segBtn.bounds.size.width/2, segBtn.bounds.size.height/2);
        };
        if (j == self.index) {
            selectedBtn = segBtn;
        }
        lastBtn = segBtn;
    }
    
    CGFloat lastX = CGRectGetMaxX(lastBtn.frame);
    
    self.indicatorBar.center = CGPointMake(selectedBtn.center.x, CGRectGetMaxY(lastBtn.frame) + self.indicatorBarSize.height/2);
}

@end
