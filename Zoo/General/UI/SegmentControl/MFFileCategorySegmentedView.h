//
//  MFFileCategorySegmentedView.h
//  Pods
//
//  Created by tanfameng on 2017/7/3.
//
//

#import "MFKitBases.h"
#import "MFKitCategorys.h"

@class MFFileCategorySegmentedView;

@protocol MFFileSegmentedControlDelegate <NSObject>

-(void)segmentedControl:(MFFileCategorySegmentedView *)segmentedControl didSelectIndex:(NSUInteger)index;

@end

@interface MFFileCategorySegmentedView : MFView

@property(nonatomic,assign) CGFloat offsetX;
//设置被选中的下标
@property(nonatomic,assign) NSUInteger index;
//只是条的颜色
@property(nonatomic,strong) UIColor *indicatorBarColor;
//title颜色
@property(nonatomic,strong) UIColor *segmentedTitleNormalColor;
//被选中的title颜色
@property(nonatomic,strong) UIColor *segmentedTitleSelectedColor;
//title字体
@property(nonatomic,strong) UIFont  *segmentedTitleFont;

@property(nonatomic,assign) CGFloat indicatorBarHeight;

@property(nonatomic,assign) id <MFFileSegmentedControlDelegate> delegate;

-(instancetype)initWithTitls:(NSArray *)titles delegate:(id <MFFileSegmentedControlDelegate>)delegate;

-(instancetype)initWithFrame:(CGRect)frame titls:(NSArray *)titles delegate:(id <MFFileSegmentedControlDelegate>)delegate;

@end
