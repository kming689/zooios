//
//  MWNormalLineChartView.h
//  Mica_MiniBI
//
//  Created by FMTAN on 16/6/21.
//  Copyright © 2016年 maywide. All rights reserved.
//

#import "MWChartBaseView.h"
#import "Zoo-Swift.h"
#import "Zoo-Bridging-Header.h"

@class MWChartBaseView;
@class MFChartModel;

@protocol MWNormalLineChartViewDelegate <NSObject>

@optional
- (NSString *)getMarkerData:(NSInteger)xIndex yIndex:(NSInteger)yIndex chartView:(UIView *)chartView;

- (NSString *)getLegend:(NSInteger)lineIndex chartView:(UIView *)chartView;

- (UIColor *)getLineColor:(NSInteger)lineIndex chartView:(UIView *)chartView;

- (BOOL)isDrawCublicLine:(NSInteger)lineIndex chartView:(UIView *)chartView;

@end

@interface MWNormalLineChartView : MWChartBaseView
{
    
}

@property (nonatomic, strong) LineChartView *chartView;

@property (nonatomic, weak) id<ChartViewDelegate,MWNormalLineChartViewDelegate,IChartAxisValueFormatter> chartViewDelegate;

- (instancetype)initWithFrame:(CGRect)frame ChartData:(MFChartModel *)chartData Delegate:(id<ChartViewDelegate,MWNormalLineChartViewDelegate>)delegate;

@end
