//
//  MWNormalLineChartView.m
//  Mica_MiniBI
//
//  Created by FMTAN on 16/6/21.
//  Copyright © 2016年 maywide. All rights reserved.
//

#import "MWNormalLineChartView.h"
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFChartModel.h"
#import "UIColor+MFAdd.h"
#import "MFColor.h"

@implementation MWNormalLineChartView
{
    MFChartModel *chartModal;
    
    double customMaxValue;
    double customMinValue;
    BOOL startAtZero;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

- (instancetype)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame])
    {
        _chartView = [[LineChartView alloc] init];
        self.userInteractionEnabled = YES;
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame ChartData:(MFChartModel *)chartData Delegate:(id<ChartViewDelegate,MWNormalLineChartViewDelegate>)delegate
{
    if(self = [super initWithFrame:frame])
    {
        _chartView = [[LineChartView alloc] init];
        self.userInteractionEnabled = YES;
        chartModal = chartData;
        if (delegate) {
            _chartViewDelegate = delegate;
        }
        
        [self initData];
    }
    
    return self;
}

- (void)initData
{
    double lastMaxValue = -10000000;
    double lastMinValue = 10000000;
    
    for (int nIndex = 0; nIndex < chartModal.chartSelect.count; nIndex++) {
        
        NSInteger yIndex = [[chartModal.chartSelect objectAtIndex:nIndex] integerValue];
        
        if (yIndex < chartModal.yValue.count) {
            
            NSArray *yArray = [chartModal.yValue objectAtIndex:yIndex];
            
            if (yArray.count > 1) {
                startAtZero = NO;
            }else{
                startAtZero = YES;
            }
            
            double maxValue = -10000000;
            double minValue = 10000000;
            
            for (int localIndex = 0; localIndex < yArray.count; localIndex++) {
                
                double value = [[yArray objectAtIndex:localIndex] doubleValue];
                if (value > maxValue) {
                    maxValue = value;
                }
                if (value < minValue) {
                    minValue = value;
                }
            }
            
            if (maxValue > lastMaxValue) {
                lastMaxValue = maxValue;
            }
            if (minValue < lastMinValue) {
                lastMinValue = minValue;
            }
        }
    }
    
    double interval = lastMaxValue - lastMinValue;
    customMaxValue = lastMaxValue + interval*0.2;
    customMinValue = lastMinValue - interval*0.2;
}

- (void)loadChartView
{
    [self initChartView:_chartViewDelegate];
}

- (void)reLoadWithData:(MFChartModel *)chartData
{
    chartModal = chartData;
    [self initData];
    [self loadChartView];
}

- (void)initChartView:(id<ChartViewDelegate,MWNormalLineChartViewDelegate,IChartAxisValueFormatter>)delegate
{
    _chartView.delegate = _chartViewDelegate;
    
    if (self.gestrueDelegate) {
        NSArray *gestureRecognizers = [_chartView gestureRecognizers];
        for (UIGestureRecognizer *gestrue in gestureRecognizers) {
            if ([gestrue isKindOfClass:[UIPanGestureRecognizer class]]) {
                gestrue.delegate = self.gestrueDelegate;
            }
        }
    }
    
    ChartDescription *description = [[ChartDescription alloc] init];
    description.text = @"";
    _chartView.chartDescription = description;
    _chartView.noDataText = @"请为图表提供数据";
    _chartView.dragEnabled = YES;
    _chartView.maxVisibleCount = 10;
    [_chartView setScaleEnabled:YES];
    _chartView.drawGridBackgroundEnabled = NO;
    _chartView.pinchZoomEnabled = NO;
    _chartView.scaleYEnabled = NO;
    
    _chartView.backgroundColor = [UIColor clearColor];
    
    _chartView.legend.enabled = NO;
    if (chartModal.chartSelect.count > 1) {
        _chartView.legend.enabled = YES;
        _chartView.legend.form = ChartLegendFormLine;
        _chartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
        _chartView.legend.textColor = [UIColor colorWithRed:51/255.f green:51/255.f blue:51/255.f alpha:1.f];
    }
    
    BalloonMarker *marker = nil;
    if (_chartViewDelegate&&[_chartViewDelegate respondsToSelector:@selector(getMarkerData:yIndex:chartView:)]) {
        NSString *markerStr = [_chartViewDelegate getMarkerData:0 yIndex:0 chartView:self];
        if (markerStr.length < 10) {
            marker = [[BalloonMarker alloc] initWithColor:[UIColor blueColor] font:[UIFont systemFontOfSize:10.0] textColor:[MFColor mf_colorWithHexString:@"00DAAA"] insets:UIEdgeInsetsMake(6.0, 2.0, 10.0, 2.0)];
            marker.minimumSize = CGSizeMake(80.0, 40.0);
        }else{
            marker = [[BalloonMarker alloc] initWithColor:[UIColor colorWithRed:239.0/255.f green:239.0/255.f blue:244.0/255.f alpha:1.f] font:[UIFont systemFontOfSize:10.0] textColor:[MFColor mf_colorWithHexString:@"00DAAA"] insets:UIEdgeInsetsMake(6.0, 2.0, 10.0, 2.0)];
            marker.minimumSize = CGSizeMake(80.0, 40.0);
        }
    }else{
        marker = [[BalloonMarker alloc] initWithColor:[UIColor colorWithRed:239.0/255.f green:239.0/255.f blue:244.0/255.f alpha:1.f] font:[UIFont systemFontOfSize:10.0] textColor:[MFColor mf_colorWithHexString:@"00DAAA"] insets:UIEdgeInsetsMake(6.0, 2.0, 10.0, 2.0)];
        marker.minimumSize = CGSizeMake(80.0, 40.0);
    }
    _chartView.marker = marker;
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.labelTextColor = [MFColor mf_fontGrayStyleColor];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.drawAxisLineEnabled = YES;
    xAxis.spaceMax = 2.0;
    xAxis.valueFormatter = delegate;
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.drawGridLinesEnabled = true;
    leftAxis.gridLineWidth = 0.5;
    leftAxis.gridLineDashPhase = 1.0;
    leftAxis.gridLineDashLengths = @[@(3.0)];
    leftAxis.gridColor = [UIColor colorWithRed:128.0/255.0 green:128.0/255.0 blue:128.0/255.0 alpha:0.2];
    leftAxis.labelFont = [UIFont systemFontOfSize:0.0];
    leftAxis.labelCount = 5;

    //leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.15;
    leftAxis.xOffset = 13;
    leftAxis.labelTextColor = [UIColor clearColor];
    leftAxis.axisLineWidth = 0.0;
    //_chartView?.leftAxis.enabled = false;
    leftAxis.drawZeroLineEnabled = NO;
//    if(customMaxValue != customMinValue)
//    {
//        leftAxis._customAxisMax = customMaxValue;
//        leftAxis._customAxisMin = customMinValue;
//    }
    
    ChartYAxis *rightAxis = _chartView.rightAxis;
    rightAxis.enabled = NO;
    
    [self addSubview:_chartView];
    
    [self setChartData];
    
    [_chartView animateWithXAxisDuration:0.8];
    
    @weakify(self);
    [_chartView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.and.top.and.bottom.equalTo(self);
    }];
}

- (void)setChartData
{
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < chartModal.xValue.count; i++)
    {
        [xVals addObject:chartModal.xValue[i]];
    }
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    //配置左边y轴对应的多条线
    for (int i = 0; i < chartModal.chartSelect.count; i++) {
        int yIndex = [[chartModal.chartSelect objectAtIndex:i] intValue];
        
        if (yIndex < chartModal.yValue.count) {
            
            NSMutableArray *yVals = [[NSMutableArray alloc] init];
            NSMutableArray *yArray = [chartModal.yValue objectAtIndex:yIndex];
            
            for (int xIndex = 0; xIndex < yArray.count; xIndex++) {
                NSString *yValueStr = [[NSString alloc] initWithFormat:@"%@",[yArray objectAtIndex:xIndex]];
                double yValue = [yValueStr doubleValue];
                ChartDataEntry *dataEntry;
                if (_chartViewDelegate&&[_chartViewDelegate respondsToSelector:@selector(getMarkerData:yIndex:chartView:)]) {
                    NSString *markerStr = [_chartViewDelegate getMarkerData:xIndex yIndex:yIndex chartView:self];
                    if (markerStr) {
                        dataEntry = [[ChartDataEntry alloc] initWithX:xIndex y:yValue data:markerStr];
                    }else{
                        dataEntry = [[ChartDataEntry alloc] initWithX:xIndex y:yValue];
                    }
                    
                }else{
                    dataEntry = [[ChartDataEntry alloc] initWithX:xIndex y:yValue];
                }
                
                [yVals addObject:dataEntry];
            }
            
            //获取线条颜色
            UIColor *lineColor = [UIColor colorWithRed:0.0/255.0 green:153.0/255 blue:255.0/255.0 alpha:1.0];
            if (_chartViewDelegate&&[_chartViewDelegate respondsToSelector:@selector(getLineColor:chartView:)]) {
                lineColor = [_chartViewDelegate getLineColor:i chartView:self];
            }
            //图例
            NSString *legendString = @"";
            if (_chartViewDelegate&&[_chartViewDelegate respondsToSelector:@selector(getLegend:chartView:)]) {
                legendString = [_chartViewDelegate getLegend:i chartView:self];
            }
            //数据
            LineChartDataSet *set1 = [[LineChartDataSet alloc] initWithValues:yVals label:legendString];
            if (_chartViewDelegate&&[_chartViewDelegate respondsToSelector:@selector(isDrawCublicLine:chartView:)]) {
                if ([_chartViewDelegate isDrawCublicLine:i chartView:self]) {
                    set1.mode = LineChartModeCubicBezier;
                    set1.drawIconsEnabled = YES;
                    set1.cubicIntensity = 0.2;
                    set1.drawCirclesEnabled = NO;
                    set1.lineWidth = 1.0;
                }else{
                    set1.drawCirclesEnabled = YES;
                    set1.lineWidth = 2.0;
                }
            }else{
                set1.drawCirclesEnabled = YES;
                set1.lineWidth = 2.0;
            }
            set1.axisDependency = AxisDependencyLeft;
            [set1 setColor:lineColor];
            set1.circleRadius = 3.0;
            set1.circleHoleRadius = 1.5;
            [set1 setCircleColor:lineColor];
            set1.circleHoleColor = [UIColor whiteColor];
            set1.highlightColor = lineColor;
            set1.drawCircleHoleEnabled = YES;
            
            NSArray *gradientColors = @[
                                        (id)[UIColor colorWithHexString:@"FDEBEB"].CGColor,
                                        (id)lineColor.CGColor];
            CGGradientRef gradient = CGGradientCreateWithColors(nil, (CFArrayRef)gradientColors, nil);
            
            set1.fillAlpha = 1.f;
            set1.fill = [ChartFill fillWithLinearGradient:gradient angle:90.f];
            set1.drawFilledEnabled = YES;
            
            [dataSets addObject:set1];
        }
    }
    
    LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];
    [data setValueTextColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0]];
    [data setValueFont:[UIFont systemFontOfSize:10.f]];
    
    _chartView.data = data;
}

@end
