//
//  MFChartModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/10.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@interface MFChartModel : MFObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *chartName;
@property (nonatomic, strong) NSString *chartType;
@property (nonatomic, strong) NSString *chartIntro;
@property (nonatomic, strong) NSMutableArray<NSString *> *xValue;
@property (nonatomic, strong) NSMutableArray *yValue;
@property (nonatomic, strong) NSMutableArray<NSNumber *> *chartSelect;
@property (nonatomic, strong) NSString *maxValue;
@property (nonatomic, strong) NSString *minValue;
@property (nonatomic, strong) NSString *amount;

@end
