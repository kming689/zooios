//
//  MWChartBaseView.h
//  Mica_MiniBI
//
//  Created by FMTAN on 16/3/9.
//  Copyright © 2016年 maywide. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MFChartModel;

@interface MWChartBaseView : UIView

@property (nonatomic, strong) NSMutableDictionary *userInfo;

@property (nonatomic, weak) id<UIGestureRecognizerDelegate> gestrueDelegate;

- (void)loadChartView;

- (void)reLoadWithData:(MFChartModel *)chartModel;

@end
