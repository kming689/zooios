//
//  MFDefaultTableViewCell.h
//  MicroVideo
//
//  Created by tanfameng on 2017/8/10.
//  Copyright © 2017年 Eshore. All rights reserved.
//

#import "MFKitBases.h"

@interface MFDefaultTableViewCell : UITableViewCell

@property (nonatomic, strong, readonly) MFLabel *titleLabel;

@property (nonatomic, strong, readonly) MFLabel *subTitleLabel;

@property (nonatomic, strong, readonly) MFLabel *detailLabel;

@property (nonatomic, strong, readonly) MFImageView *titleImageView;

@property (nonatomic, assign,setter=setShowIndicateView:) BOOL showIndicateView;//默认值YES

@property (nonatomic, assign,setter=setShowImageView:) BOOL showImageView;//默认值NO

- (void)setShowIndicateView:(BOOL)showIndicateView;

- (void)setShowImageView:(BOOL)showImageView;

- (void)setTitleImageFrame:(CGRect)frame;

@end
