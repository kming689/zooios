//
//  MFDefaultTableViewCell.m
//  MicroVideo
//
//  Created by tanfameng on 2017/8/10.
//  Copyright © 2017年 Eshore. All rights reserved.
//

#import "MFDefaultTableViewCell.h"
#import <Masonry/Masonry.h>
#import "UIImage+MFAdd.h"

@interface MFDefaultTableViewCell ()

@property (nonatomic, strong) MFImageView *indicateImageView;

@end

@implementation MFDefaultTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _showIndicateView = YES;
    _showImageView = NO;
    
    // Initialization code
    _titleImageView = [[MFImageView alloc] init];
    _titleImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_titleImageView];
    
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.font = [UIFont systemFontOfSize:15.0];
    _titleLabel.textColor = [MFColor mf_fontBlackStyleColor];
    [self.contentView addSubview:_titleLabel];
    
    _subTitleLabel = [[MFLabel alloc] init];
    _subTitleLabel.numberOfLines = 0;
    _subTitleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    _subTitleLabel.font = [UIFont systemFontOfSize:13.0];
    _subTitleLabel.textColor = [MFColor mf_fontGrayStyleColor];
    [self.contentView addSubview:_subTitleLabel];
    
    _indicateImageView = [[MFImageView alloc] init];
    _indicateImageView.image = [UIImage imageNamed:@"cell_right" bundleClass:[self class]];
    _indicateImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_indicateImageView];
    
    _detailLabel = [[MFLabel alloc] init];
    _detailLabel.font = [UIFont systemFontOfSize:14.0];
    _detailLabel.textAlignment = NSTextAlignmentRight;
    _detailLabel.textColor = [MFColor mf_fontGrayStyleColor];
    [self.contentView addSubview:_detailLabel];
    
    __weak typeof(self) weakSelf = self;
    [_titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.centerY.equalTo(strongSelf.contentView);
        make.left.equalTo(strongSelf.contentView).offset(15);
    }];
    
    UIView *leftView = self.contentView;
    if (_showImageView) {
        leftView = _titleImageView;
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            __strong typeof(self) strongSelf = weakSelf;
            make.left.equalTo(leftView).offset(8).priorityHigh();
            make.centerY.equalTo(strongSelf.contentView);
            make.right.mas_equalTo(-60);
            make.height.mas_equalTo(20);
        }];
    }else{
        [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            __strong typeof(self) strongSelf = weakSelf;
            make.left.equalTo(leftView).offset(15);
            make.centerY.equalTo(strongSelf.contentView);
            make.right.mas_equalTo(-60);
            make.height.mas_equalTo(20);
        }];
    }
    
    [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.left.equalTo(strongSelf.titleLabel);
        make.centerY.equalTo(strongSelf.contentView);
        make.right.mas_equalTo(-60);
        make.height.mas_equalTo(20);
    }];
    
    [_indicateImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.size.mas_equalTo(CGSizeMake(13, 13));
        make.centerY.equalTo(strongSelf.contentView);
        make.right.equalTo(strongSelf.contentView).offset(-12.5);
    }];
    
    [_detailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.centerY.equalTo(strongSelf.contentView);
        make.left.equalTo(strongSelf.contentView).offset(8);
        make.right.mas_equalTo(strongSelf.indicateImageView.mas_left);
        make.height.mas_equalTo(20);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    _showIndicateView = YES;
    _showImageView = NO;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    __weak typeof(self) weakSelf = self;
    UIView *leftView = self.contentView;
    //显示前面图片
    if (_showImageView) {
        [_titleImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
            __strong typeof(self) strongSelf = weakSelf;
            make.width.equalTo(strongSelf.contentView.mas_height).multipliedBy(0.74);
            make.height.equalTo(strongSelf.contentView.mas_height).multipliedBy(0.74);
            make.centerY.equalTo(strongSelf.contentView);
            make.left.equalTo(strongSelf.contentView).offset(10);
        }];
        
        leftView = self.titleImageView;
        
        if (_subTitleLabel.text&&_subTitleLabel.text.length > 0) {
            [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                __strong typeof(self) strongSelf = weakSelf;
                make.left.equalTo(leftView.mas_right).offset(8);
                make.centerY.equalTo(strongSelf.contentView).offset(-9);
                make.right.mas_equalTo(-60);
                make.height.mas_equalTo(20);
            }];
            [_subTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                __strong typeof(self) strongSelf = weakSelf;
                make.left.equalTo(strongSelf.titleLabel);
                make.centerY.equalTo(strongSelf.contentView).offset(9);
                make.right.mas_equalTo(-60);
                make.height.mas_equalTo(20);
            }];
        }else{
            [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                __strong typeof(self) strongSelf = weakSelf;
                make.left.equalTo(leftView.mas_right).offset(8);
                make.centerY.equalTo(strongSelf.contentView);
                make.right.mas_equalTo(-60);
                make.height.mas_equalTo(20);
            }];
        }
    }
    //不显示前面的图片
    else{
        if (_subTitleLabel.text&&_subTitleLabel.text.length > 0) {
            [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                __strong typeof(self) strongSelf = weakSelf;
                make.left.equalTo(strongSelf.contentView).offset(10);
                make.centerY.equalTo(strongSelf.contentView).offset(-9);
                make.right.mas_equalTo(-60);
                make.height.mas_equalTo(20);
            }];
            [_subTitleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                __strong typeof(self) strongSelf = weakSelf;
                make.left.equalTo(strongSelf.titleLabel);
                make.centerY.equalTo(strongSelf.contentView).offset(9);
                make.right.mas_equalTo(-60);
                make.height.mas_equalTo(20);
            }];
        }else{
            [_titleLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                __strong typeof(self) strongSelf = weakSelf;
                make.left.equalTo(strongSelf.contentView).offset(15);
                make.centerY.equalTo(strongSelf.contentView);
                make.right.mas_equalTo(-60);
                make.height.mas_equalTo(20);
            }];
        }
    }
    
    
    if (!_showIndicateView) {
        _indicateImageView.hidden = YES;
        __weak typeof(self) weakSelf = self;
        [_detailLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            __strong typeof(self) strongSelf = weakSelf;
            make.centerY.equalTo(strongSelf.contentView);
            make.right.equalTo(strongSelf.indicateImageView.mas_right).offset(0);
            make.left.equalTo(strongSelf.contentView).offset(8);
            make.height.mas_equalTo(20);
        }];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setShowIndicateView:(BOOL)showIndicateView
{
    _showIndicateView = showIndicateView;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)setShowImageView:(BOOL)showImageView
{
    _showImageView = showImageView;
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)setTitleImageFrame:(CGRect)frame
{

}

@end
