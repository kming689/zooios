//
//  MFSearchResultViewController.m
//  Pods
//
//  Created by huanghy on 2017/9/15.
//
//

#import "MFSearchResultViewController.h"

@interface MFSearchResultViewController ()

@end

@implementation MFSearchResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - MFSearchResultDelegate
-(void)searchTextDidChange:(NSString *) searchText
{
    
}

-(void)searchButtonDidTapped:(NSString *) searchText
{
    
}

-(void)searchCancelButtonDidTapped
{
    
}

-(BOOL)shouldShowSearchResultControllerBeforePresentation
{
    return NO;
}

-(BOOL)shouldHideSearchResultControllerWhenNoSearch
{
    return YES;
}

-(void (^)())animationForDismiss
{
    return ^() {
        self.view.alpha = 0;
    };
}

-(void (^)())animationForPresentation
{
    self.view.alpha = 1;
    return nil;
}

@end

