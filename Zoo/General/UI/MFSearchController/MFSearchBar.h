//
//  MFSearchBar.h
//  Pods
//
//  Created by huanghy on 2017/9/15.
//
//

#import <UIKit/UIKit.h>

//占位符对齐类型 -- ios11+
typedef NS_ENUM(NSInteger, MFSearchBarPlaceholderHorizontal){
    MFSearchBarPlaceholderHorizontalCenter,
    MFSearchBarPlaceholderHorizontalLeft
};

@interface MFSearchBar : UISearchBar

/**
 默认风格的search bar
 
 @return MFSearchBar
 */
+(instancetype) defaultSearchBar;

/**
 占位符对齐类型 -- ios11+
 */
@property(nonatomic, assign) MFSearchBarPlaceholderHorizontal placeholderHorizontal;

/**
 输入框
 
 @return UITextField
 */
-(UITextField *) textField;


/**
 取消按钮
 
 @return UIButton
 */
-(UIButton *) cancelButton;

/**
 左图标视图
 
 @return UIImageView
 */
-(UIImageView *) leftIconImageView;

/**
 输入框占位文本
 
 @return UILabel
 */
-(UILabel *) textFieldPlaceholderLabel;

/**
 注销第一响应
 */
-(void) resignFirstResponderWithCancelButtonRemainEnabled;

@end

