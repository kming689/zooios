//
//  MFSearchViewController.m
//  Pods
//
//  Created by huanghy on 2017/9/15.
//
//

#import "MFSearchViewController.h"

@interface MFSearchViewController ()<UISearchBarDelegate>
@property(nonatomic, strong) UIView *searchBackgroundView;
@property(nonatomic, strong) UIView *fromNavigationBarView;
@property(nonatomic, strong) UIView *toNavigationBarView;
@end

@implementation MFSearchViewController
{
    UISearchBar *_fromSearchBar;
    CGRect _fromSearchBarFrame;
    
    UIView *_searchResultView;
    BOOL _willShowSearchResultController;
    BOOL _shouldHideSearchResultWhenNoSearch;
    BOOL _navigationBarHidden;
    BOOL _shouldAllowSearchBarEditing;
    
    CGFloat _searchBarLeftIconImageViewOriginX;
    CGFloat _searchBarPlaceholderLabelOriginX;
}

-(instancetype)initWithSearchResultController:(UIViewController<MFSearchResultDelegate>*) searchResultController
{
    self = [super init];
    if (self) {
        _searchResultController = searchResultController;
        
        self.view.backgroundColor = [UIColor clearColor];
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
        
        _searchBackgroundView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _searchBackgroundView.backgroundColor = [UIColor clearColor];
        _searchBackgroundView.alpha = 0;
        [self.view addSubview:_searchBackgroundView];
        
        UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blur];
        effectView.frame = [UIScreen mainScreen].bounds;
        [_searchBackgroundView addSubview:effectView];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cancelTapHandler:)];
        tap.numberOfTapsRequired = 1;
        tap.numberOfTouchesRequired = 1;
        [_searchBackgroundView addGestureRecognizer:tap];
        
        self.toNavigationBarView = [[UIView alloc] init];
        [self.view addSubview:self.toNavigationBarView];
        
        self.searchBar = [MFSearchBar defaultSearchBar];
        self.searchBar.delegate = self;
        [self.toNavigationBarView addSubview:self.searchBar];
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _shouldAllowSearchBarEditing = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (!self.presentedViewController) {
        _navigationBarHidden = self.navigationController.navigationBarHidden;
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    _shouldAllowSearchBarEditing = !self.searchBar.isFirstResponder;
}

#pragma mark - Actions
- (void)cancelTapHandler:(UITapGestureRecognizer *)tap {
    [self hide];
}

- (void)dismissSearchController {
    [self hide];
}

- (void)dismissKeyboard {
    if ([self.searchBar isFirstResponder]) {
        [self.searchBar resignFirstResponder];
        UIButton *searchBtn = [self.searchBar cancelButton];
        searchBtn.enabled = YES;
    }
}

- (void)cancelTapped:(UIButton *)btn {
    [self hide];
}

#pragma mark - 显示
- (void)showInViewController:(UIViewController *)controller fromSearchBar:(UISearchBar *) fromSearchBar {
    if (!controller.navigationController) return;
    
    _willShowSearchResultController = YES;
    if (self.searchResultController && [self.searchResultController respondsToSelector:@selector(shouldShowSearchResultControllerBeforePresentation)]) {
        _willShowSearchResultController = [self.searchResultController shouldShowSearchResultControllerBeforePresentation];
    }
    
    _shouldHideSearchResultWhenNoSearch = NO;
    if (self.searchResultController && [self.searchResultController respondsToSelector:@selector(shouldHideSearchResultControllerWhenNoSearch)]) {
        _shouldHideSearchResultWhenNoSearch = [self.searchResultController shouldHideSearchResultControllerWhenNoSearch];
    }
    
    _fromSearchBar = fromSearchBar;
    
    _fromSearchBarFrame = [fromSearchBar convertRect:fromSearchBar.bounds toView:[UIApplication sharedApplication].delegate.window];
    self.toNavigationBarView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, CGRectGetMaxY(_fromSearchBarFrame));
    self.toNavigationBarView.backgroundColor = fromSearchBar.barTintColor;
    
    CGRect frame = fromSearchBar.frame;
    //    frame.origin.x = 0;
    frame.origin.y = CGRectGetHeight(self.toNavigationBarView.frame) - CGRectGetHeight(fromSearchBar.frame);
    self.searchBar.frame = frame;
    self.searchBar.placeholder = fromSearchBar.placeholder;
    
    self.fromNavigationBarView = [controller.navigationController.navigationBar resizableSnapshotViewFromRect:CGRectMake(0, -20, [UIScreen mainScreen].bounds.size.width, 64) afterScreenUpdates:NO withCapInsets:UIEdgeInsetsZero];
    [self.view addSubview:self.fromNavigationBarView];
    
    _searchResultView = nil;
    if (_willShowSearchResultController) {
        [self addSearchResultController];
    }
    
    [controller addChildViewController:self];
    [controller.view addSubview:self.view];
    [self show];
}

- (void)show {
    _active = YES;
    
    if ([self.delegate respondsToSelector:@selector(willPresentSearchController:)])
        [self.delegate willPresentSearchController:self];
    
    [self.searchBar layoutIfNeeded];
    [self.searchBar becomeFirstResponder];
    [self.searchBar setShowsCancelButton:YES animated:YES];
    [self.searchBar.textField setTintColor:[UIColor clearColor]];
    
    void (^animationBlock)();
    if (self.searchResultController && [self.searchResultController respondsToSelector:@selector(animationForPresentation)]) {
        animationBlock = [self.searchResultController animationForPresentation];
    }
    
    [UIView animateWithDuration:SEARCH_CONTROLLER_SHOW_ANIMATION_DURATION
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionLayoutSubviews
                     animations:^{
                         CGRect frame = self.fromNavigationBarView.frame;
                         frame.origin.y = -frame.size.height;
                         self.fromNavigationBarView.frame = frame;
                         
                         if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0f) {
                             CGRect rect = _searchBar.leftIconImageView.frame;
                             _searchBarLeftIconImageViewOriginX = rect.origin.x;
                             rect.origin.x = 8;
                             _searchBar.leftIconImageView.frame = rect;
                             
                             CGRect rect2 = _searchBar.textFieldPlaceholderLabel.frame;
                             _searchBarPlaceholderLabelOriginX = rect2.origin.x;
                             rect2.origin.x = 8 + rect.size.width + _searchBar.searchTextPositionAdjustment.horizontal;
                             _searchBar.textFieldPlaceholderLabel.frame = rect2;
                         }
                     }
                     completion:^(BOOL finished) {
                         if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0f) {
                             self.searchBar.placeholderHorizontal = MFSearchBarPlaceholderHorizontalLeft;
                         }
                         [self.searchBar.textField setTintColor:self.searchBar.tintColor];
                     }];
    [UIView animateWithDuration:SEARCH_CONTROLLER_SHOW_ANIMATION_DURATION
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         CGRect frame = self.toNavigationBarView.frame;
                         frame.origin.y = -frame.size.height + 64;
                         self.toNavigationBarView.frame = frame;
                         
                         frame = _searchBar.frame;
                         frame.size.width = [UIScreen mainScreen].bounds.size.width;
                         _searchBar.frame = frame;
                         
                         if (_searchResultView) {
                             frame = _searchResultView.frame;
                             frame.origin.y = CGRectGetMaxY(self.toNavigationBarView.frame);
                             _searchResultView.frame = frame;
                         }
                         
                         self.searchBackgroundView.alpha = 1;
                         
                         if (animationBlock)
                             animationBlock();
                     }
                     completion:^(BOOL finished) {
                         if (!_willShowSearchResultController)
                             [self addSearchResultController];
                         if ([self.delegate respondsToSelector:@selector(didPresentSearchController:)])
                             [self.delegate didPresentSearchController:self];
                     }];
}

- (void)addSearchResultController {
    [self addChildViewController:self.searchResultController];
    CGRect frame = self.view.frame;
    frame.origin.y = CGRectGetMaxY(self.toNavigationBarView.frame);
    frame.size.height -= 64;
    _searchResultView = self.searchResultController.view;
    _searchResultView.frame = frame;
    [self.view insertSubview:self.searchResultController.view aboveSubview:self.searchBackgroundView];
    [self.searchResultController didMoveToParentViewController:self];
    
    self.searchResultController.view.hidden = _shouldHideSearchResultWhenNoSearch;
}

#pragma mark - 隐藏
- (void)hide{
    if ([self.delegate respondsToSelector:@selector(willDismissSearchController:)])
        [self.delegate willDismissSearchController:self];
    self.searchBar.text = nil;
    
    [self dismissKeyboard];
    [self.searchBar setShowsCancelButton:NO animated:YES];
    [self.searchBar.textField setTintColor:[UIColor clearColor]];
    
    void (^animationBlock)();
    if (self.searchResultController && [self.searchResultController respondsToSelector:@selector(animationForDismiss)]) {
        animationBlock = [self.searchResultController animationForDismiss];
    }
    
    UIColor *originViewBackgroundColor = _searchResultView.backgroundColor;
    [self.navigationController setNavigationBarHidden:_navigationBarHidden animated:YES];
    [UIView animateWithDuration:SEARCH_CONTROLLER_HIDE_ANIMATION_DURATION
                          delay:0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionLayoutSubviews
                     animations:^{
                         CGRect frame = self.fromNavigationBarView.frame;
                         frame.origin.y = 0;
                         self.fromNavigationBarView.frame = frame;
                         
                         frame = self.toNavigationBarView.frame;
                         frame.origin.y = 0;
                         self.toNavigationBarView.frame = frame;
                         
                         frame = _searchBar.frame;
                         frame.size.width = CGRectGetWidth(_fromSearchBarFrame);
                         _searchBar.frame = frame;
                         
                         self.searchBackgroundView.alpha = 0;
                         
                         if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0f) {
                             CGRect rect = _searchBar.leftIconImageView.frame;
                             rect.origin.x = _searchBarLeftIconImageViewOriginX;
                             _searchBar.leftIconImageView.frame = rect;
                             
                             CGRect rect2 = _searchBar.textFieldPlaceholderLabel.frame;
                             rect2.origin.x = _searchBarPlaceholderLabelOriginX;
                             _searchBar.textFieldPlaceholderLabel.frame = rect2;
                         }
                         
                         if (animationBlock)
                             animationBlock();
                     }
                     completion:^(BOOL finished) {
                         [UIView animateWithDuration:SEARCH_CONTROLLER_HIDE_ANIMATION_DURATION animations:^{
                             _searchResultView.backgroundColor = [UIColor clearColor];
                             if ([self.delegate respondsToSelector:@selector(didDismissSearchController:)]) {
                                 [self.delegate didDismissSearchController:self];
                             }
                         } completion:^(BOOL finished) {
                             if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0f) {
                                 self.searchBar.placeholderHorizontal = MFSearchBarPlaceholderHorizontalCenter;
                             }
                             [self.searchBar.textField setTintColor:self.searchBar.tintColor];
                             _searchResultView.backgroundColor = originViewBackgroundColor;
                             [self removeSearchResultController];
                             
                             [self removeFromParentViewController];
                             [self.view removeFromSuperview];
                             [self.fromNavigationBarView removeFromSuperview];
                             [self.navigationController ? self.navigationController : self dismissViewControllerAnimated:NO completion:^(){
                                 [self.fromNavigationBarView removeFromSuperview];
                                 
                             }];
                             _active = NO;
                         }];
                     }];
    
}

- (void)removeSearchResultController {
    [self.searchResultController willMoveToParentViewController:nil];
    [self.searchResultController.view removeFromSuperview];
    [self.searchResultController removeFromParentViewController];
    [self.searchResultController didMoveToParentViewController:nil];
    _searchResultView = nil;
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    if (_shouldAllowSearchBarEditing) {
        return YES;
    }else {
        _shouldAllowSearchBarEditing = YES;
        UIButton *searchBtn = [self.searchBar cancelButton];
        searchBtn.enabled = YES;
        return NO;
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        self.searchResultController.view.hidden = _shouldHideSearchResultWhenNoSearch;
        [self.searchResultController searchTextDidChange:nil];
    }else {
        self.searchResultController.view.hidden = NO;
        [self.searchResultController searchTextDidChange:searchText];
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    if (self.searchResultController && [self.searchResultController respondsToSelector:@selector(searchCancelButtonDidTapped)]) {
        [self.searchResultController searchCancelButtonDidTapped];
    }
    [self hide];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self dismissKeyboard];
    
    NSString *searchText = searchBar.text;
    if (searchText.length == 0) {
        self.searchResultController.view.hidden = _shouldHideSearchResultWhenNoSearch;
        [self.searchResultController searchButtonDidTapped:searchText];
    }else {
        self.searchResultController.view.hidden = NO;
        [self.searchResultController searchButtonDidTapped:searchText];
        
    }
}


@end

