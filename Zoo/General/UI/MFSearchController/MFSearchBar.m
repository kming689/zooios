//
//  MFSearchBar.m
//  Pods
//
//  Created by huanghy on 2017/9/15.
//
//

#import "MFSearchBar.h"

#define kSearchBarTintColor [UIColor colorWithRed:64/255.0f green:64/255.0f blue:64/255.0f alpha:1]
#define kSearchBarBackgroundColor [UIColor colorWithRed:239/255.0f green:239/255.0f blue:244/255.0f alpha:1]
#define kSearchBarDefaultHeight 44
#ifndef kNavigationHeight
#define kNavigationHeight ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0 ? 64 : 44)
#endif

@implementation MFSearchBar
{
    BOOL _isFirstLayoutFinish;
}

+(void)initialize
{
    if (self == [MFSearchBar class]) {
        
        [[UIBarButtonItem appearanceWhenContainedIn: [MFSearchBar class], nil] setTintColor:kSearchBarTintColor];
        [[UIBarButtonItem appearanceWhenContainedIn: [MFSearchBar class], nil] setTitle:@"取消"];
    }
}

+(instancetype)defaultSearchBar
{
    MFSearchBar *searchBar = [[MFSearchBar alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, kSearchBarDefaultHeight)];
    searchBar.placeholder = @"搜索";
    searchBar.showsCancelButton = NO;
    searchBar.barStyle = UISearchBarStyleDefault;
    searchBar.backgroundColor = kSearchBarBackgroundColor;
    searchBar.backgroundImage = [UIImage new];
    
    searchBar.barTintColor = kSearchBarBackgroundColor;
    searchBar.tintColor = kSearchBarTintColor;
    
    searchBar.keyboardType = UIKeyboardTypeDefault;
    searchBar.returnKeyType = UIReturnKeySearch;
    searchBar.enablesReturnKeyAutomatically = YES;
    
    UITextField *textField = [searchBar textField];
    textField.backgroundColor = [UIColor whiteColor];
    textField.textColor = kSearchBarTintColor;
    textField.font = [UIFont systemFontOfSize:13];
    
    searchBar.searchTextPositionAdjustment = UIOffsetMake(5, 0);
    
    return searchBar;
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0f) {
        CGRect rect = self.textField.frame;
        if (rect.size.height != 28) {
            rect.size.height = 28;
            rect.origin.y = (self.bounds.size.height - 28) / 2;
            self.textField.frame = rect;
        }
        
        if (!_isFirstLayoutFinish) {
            _isFirstLayoutFinish = YES;
            [self adjustmentPlaceholderHorizontal];
        }
    }
}

-(void)configCancelButton
{
    UIButton *cancelButton = [self cancelButton];
    if (cancelButton) {
        UIColor *color = [cancelButton titleColorForState:UIControlStateNormal];
        [cancelButton setTitleColor:color forState:UIControlStateDisabled];
    }
}

-(UITextField *)textField
{
    UITextField *searchTextField = nil;
    for (UIView* subview in self.subviews[0].subviews) {
        if ([subview isKindOfClass:[UITextField class]]) {
            searchTextField = (UITextField*)subview;
            break;
        }
    }
    return searchTextField;
}

-(UIButton *)cancelButton
{
    UIButton *btn;
    
    NSArray<UIView *> *subviews = self.subviews[0].subviews;
    for(UIView *view in subviews) {
        if([view isKindOfClass:[NSClassFromString(@"UINavigationButton") class]]) {
            btn = (UIButton *)view;
            break;
        }
    }
    return btn;
}

-(UIImageView *)leftIconImageView
{
    UIImageView *imageView = nil;
    
    UITextField *textField = self.textField;
    for (UIView *view in textField.subviews) {
        if ([view isKindOfClass:[UIImageView class]] && ![view isKindOfClass:NSClassFromString(@"_UISearchBarSearchFieldBackgroundView")]) {
            imageView = (UIImageView *)view;
            break;
        }
    }
    return imageView;
}

-(UILabel *)textFieldPlaceholderLabel
{
    UILabel *label = nil;
    
    UITextField *textField = self.textField;
    for (UIView *view in textField.subviews) {
        if ([view isKindOfClass:NSClassFromString(@"UISearchBarTextFieldLabel")]) {
            label = (UILabel *)view;
            break;
        }
    }
    return label;
}

-(void)resignFirstResponderWithCancelButtonRemainEnabled
{
    [self resignFirstResponder];
    
    UIButton *cancelButton = [self cancelButton];
    [cancelButton setEnabled:YES];
}

-(void)setShowsCancelButton:(BOOL)showsCancelButton animated:(BOOL)animated {
    [super setShowsCancelButton:showsCancelButton animated:animated];
    [self configCancelButton];
}

-(void)setShowsCancelButton:(BOOL)showsCancelButton {
    [self setShowsCancelButton:showsCancelButton animated:NO];
}

-(void)setPlaceholder:(NSString *)placeholder
{
    [super setPlaceholder:placeholder];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 11.0f && _isFirstLayoutFinish) {
        [self adjustmentPlaceholderHorizontal];
    }
}

-(void)setPlaceholderHorizontal:(MFSearchBarPlaceholderHorizontal)placeholderHorizontal
{
    _placeholderHorizontal = placeholderHorizontal;
    [self adjustmentPlaceholderHorizontal];
}

-(void)adjustmentPlaceholderHorizontal
{
    if (_placeholderHorizontal == MFSearchBarPlaceholderHorizontalCenter) {
        [self placeholderHorizontalCenter];
    }else{
        [self placeholderHorizontalLeft];
    }
}

-(void)placeholderHorizontalCenter
{
    CGSize iconSize = CGSizeMake(14, 14);
    CGSize size = [self.placeholder boundingRectWithSize:CGSizeMake(MAXFLOAT, MAXFLOAT) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.textField.font} context:nil].size;
    CGFloat width = iconSize.width + size.width + self.searchTextPositionAdjustment.horizontal;
    CGFloat horizontal = (self.textField.bounds.size.width - width) / 2 - self.textField.frame.origin.x;
    [self setPositionAdjustment:UIOffsetMake(horizontal, 0) forSearchBarIcon:UISearchBarIconSearch];
}

-(void)placeholderHorizontalLeft
{
    [self setPositionAdjustment:UIOffsetMake(0, 0) forSearchBarIcon:UISearchBarIconSearch];
}
@end

