//
//  MFSearchViewController.h
//  Pods
//
//  Created by huanghy on 2017/9/15.
//
//

#import <UIKit/UIKit.h>
#import "MFSearchBar.h"

@class MFSearchViewController;

#define SEARCH_CONTROLLER_HIDE_ANIMATION_DURATION 0.2
#define SEARCH_CONTROLLER_SHOW_ANIMATION_DURATION 0.2

@protocol MFSearchResultDelegate <NSObject>

-(void)searchTextDidChange:(NSString *) searchText;

-(void)searchButtonDidTapped:(NSString *) searchText;

-(void)searchCancelButtonDidTapped;

@optional

//是否在SearchController动画前显示搜索结果Controller，默认为YES
-(BOOL)shouldShowSearchResultControllerBeforePresentation;

//当没有执行搜索时，是否隐藏搜索结果Controller，默认为NO
//注意：不是指没有搜索结果，而是指取消当前搜索，比如用户清空搜索字符串
-(BOOL)shouldHideSearchResultControllerWhenNoSearch;

//搜索结果控制器，需要和搜索转场动画同时执行的动画，比如内容淡入
-(void (^)())animationForPresentation;

//搜索结果控制器，需要和搜索转场动画同时执行的动画，比如内容淡出
-(void (^)())animationForDismiss;

@end

@protocol MFSearchViewControllerDelegate <NSObject>

@optional
-(void)willDismissSearchController:(MFSearchViewController *) searchController;

-(void)didDismissSearchController:(MFSearchViewController *) searchController;

-(void)willPresentSearchController:(MFSearchViewController *) searchController;

-(void)didPresentSearchController:(MFSearchViewController *) searchController;

@end

@interface MFSearchViewController : UIViewController

@property(nonatomic, assign) BOOL active;

@property(nonatomic, strong, readonly) UIViewController<MFSearchResultDelegate> *searchResultController;

@property(nonatomic, weak) id<MFSearchViewControllerDelegate> delegate;

@property(nonatomic, strong) MFSearchBar *searchBar;

-(instancetype)initWithSearchResultController:(UIViewController<MFSearchResultDelegate>*) searchResultController;

-(void)showInViewController:(UIViewController *) controller fromSearchBar:(UISearchBar *) fromSearchBar;

-(void)dismissSearchController;

-(void)dismissKeyboard;

@end

