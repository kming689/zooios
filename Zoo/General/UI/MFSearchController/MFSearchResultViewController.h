//
//  MFSearchResultViewController.h
//  Pods
//
//  Created by huanghy on 2017/9/15.
//
//

#import <UIKit/UIKit.h>
#import "MFSearchViewController.h"

@interface MFSearchResultViewController : UIViewController<MFSearchResultDelegate>


@end

