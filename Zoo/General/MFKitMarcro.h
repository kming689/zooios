//
//  MFKitMarcro.h
//  MFKit
//
//  Created by 方子扬 on 2017/3/23.
//  Copyright © 2017年 YiMu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sys/time.h>
#import <pthread.h>
#import <ReactiveCocoa/RACEXTScope.h>

#ifndef MFKitMarcro_h
#define MFKitMarcro_h


#ifndef _IOS7_
    #define _IOS7_ [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#endif

#ifndef _iOS8_
    #define _iOS8_ [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0
#endif

#ifndef _iOS9_
    #define _iOS9_ [[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0
#endif

#ifndef _iOS10_
    #define _iOS10_ [[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0
#endif

#ifndef kScreenWidth
    #define kScreenWidth [UIScreen mainScreen].bounds.size.width
#endif

#ifndef kScreenHeight
    #define kScreenHeight [UIScreen mainScreen].bounds.size.height
#endif

#ifndef kStatusBarHeight
    #define kStatusBarHeight (_IOS7_ ? 20 : 0)
#endif

#ifndef kNavigationHeight
    #define kNavigationHeight [[UIApplication sharedApplication] statusBarFrame].size.height + self.navigationController.navigationBar.frame.size.height
#endif

#ifndef kRGBColor
    #define kRGBColor(R,G,B,a) [UIColor colorWithRed:(R)/255.0 green:(G)/255.0 blue:(B)/255.0 alpha:(a)]
#endif


/**
 Whether in main queue/thread.
 */
static inline bool dispatch_is_main_queue() {
    return pthread_main_np() != 0;
}

/**
 Submits a block for asynchronous execution on a main queue and returns immediately.
 */
static inline void dispatch_async_on_main_queue(void (^block)()) {
    if (pthread_main_np()) {
        block();
    } else {
        dispatch_async(dispatch_get_main_queue(), block);
    }
}

/**
 Submits a block for execution on a main queue and waits until the block completes.
 */
static inline void dispatch_sync_on_main_queue(void (^block)(void)) {
    if (pthread_main_np()) {
        block();
    } else {
        dispatch_sync(dispatch_get_main_queue(), block);
    }
}

/**
 回调block
 
 @param result 结果
 @param resultObject 结果数据
 @param error 错误信息
 */
typedef void(^mf_networkComplateHandlerBlock)(BOOL result,id resultObject,NSError *error);

/**
 回调block
 
 @param result 结果
 @param error 错误信息
 */
typedef void(^mf_complateHandlerBlock)(BOOL result,NSError *error);

#endif /* MFKitMarcro_h */
