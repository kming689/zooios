//
//  NSObject+Conver.m
//  Pods
//
//  Created by 方子扬 on 2017/6/24.
//
//

#import "NSObject+Conver.h"
#import <YYModel/YYModel.h>

@implementation NSObject (Conver)

+(id)converObjectProperty:(id)obj toClass:(Class)cls
{
    return [obj converObjectPropertyToClass:cls];
}


-(id)converObjectPropertyToClass:(Class)cls
{
    NSString *json = [self yy_modelToJSONString];
    return [cls yy_modelWithJSON:json];
}


@end
