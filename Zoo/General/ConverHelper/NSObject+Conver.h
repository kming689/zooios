//
//  NSObject+Conver.h
//  Pods
//
//  Created by 方子扬 on 2017/6/24.
//
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSObject (Conver)


/**
 转换对象的属性到某个类里面

 @param obj 转换对象
 @param cls 转换的类
 @return 转换后到类对象
 */
+(id) converObjectProperty:(id) obj toClass:(Class) cls;



/**
 转换对象的属性到某个类里面

 @param cls 转换的类
 @return 转换后到类对象
 */
-(id) converObjectPropertyToClass:(Class) cls;


@end

NS_ASSUME_NONNULL_END
