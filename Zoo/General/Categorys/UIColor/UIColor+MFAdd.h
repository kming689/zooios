//
//  UIColor+MFAdd.h
//  MFKit
//
//  Created by 方子扬 on 2017/3/23.
//  Copyright © 2017年 YiMu. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (MFAdd)


/**
 16进制转换颜色

 @param hexString 16进制值
 @return UIColor
 */
+(UIColor *) colorWithHexString:(NSString *) hexString;

@end
