//
//  NSNumber+MFAdd.h
//  MFKit
//
//  Created by 方子扬 on 2017/3/23.
//  Copyright © 2017年 YiMu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (MFAdd)

/**
 Creates and returns an NSNumber object from a string.
 Valid format: @"12", @"12.345", @" -0xFF", @" .23e99 "...
 
 @param string  The string described an number.
 
 @return an NSNumber when parse succeed, or nil if an error occurs.
 */
+ (NSNumber *)numberWithString:(NSString *)string;

@end
