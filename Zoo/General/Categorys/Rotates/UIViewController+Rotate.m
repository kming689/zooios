//
//  UIViewController+Rotate.m
//  Pods
//
//  Created by huanghy on 2017/9/5.
//
//

#import "UIViewController+Rotate.h"

@implementation UIViewController (Rotate)

-(BOOL)shouldAutorotate
{
    return NO;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
