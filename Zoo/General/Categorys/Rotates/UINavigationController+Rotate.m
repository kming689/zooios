//
//  UINavigationController+Rotate.m
//  Pods
//
//  Created by huanghy on 2017/9/5.
//
//

#import "UINavigationController+Rotate.h"

@implementation UINavigationController (Rotate)

-(BOOL)shouldAutorotate
{
    return self.topViewController.shouldAutorotate;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return self.topViewController.supportedInterfaceOrientations;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
