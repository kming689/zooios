//
//  UITabBarController+Rotate.m
//  Pods
//
//  Created by huanghy on 2017/9/5.
//
//

#import "UITabBarController+Rotate.h"

@implementation UITabBarController (Rotate)

-(BOOL)shouldAutorotate
{
    return self.selectedViewController.shouldAutorotate;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return self.selectedViewController.supportedInterfaceOrientations;
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
