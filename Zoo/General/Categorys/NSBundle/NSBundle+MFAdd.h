//
//  NSBundle+MFAdd.h
//  MFKit
//
//  Created by 方子扬 on 2017/3/22.
//  Copyright © 2017年 YiMu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (MFAdd)


/**
 当前app版本

 @return NSString
 */
+(NSString *) appVersionString;


/**
 当前app build版本

 @return NSString
 */
+(NSString *) appBuildVersionString;


/**
 app bundle name

 @return NSString
 */
+(NSString *) appBundleName;


/**
 当前app build版本号是否早于传入的版本号，也可以理解为少于传入的版本号

 @param version 版本号
 @return yes:当前build号少于传入版本号  no:当前版本号大于传入版本号
 */
+(BOOL) appBuildVersionEarlierVersion:(NSString *) version;



/**
 一组NSNumber对象,用于搜索图片等最佳的搜索顺序
 iPhone3GS:@[@1,@2,@3] iPhone5:@[@2,@3,@1]  iPhone6 Plus:@[@3,@2,@1]

 @return NSArray<NSNumber *>
 */
+(NSArray<NSNumber *> *)preferredScales;



/**
 返回指定bundle里指定名称与后缀的文件路径，并且会搜索与当前屏幕的比例文件，例如（@2x）

 @param name 文件名
 @param ext 后缀
 @param bundlePath bundle路径
 @return NSString
 */
+(NSString *)pathForScaledResource:(NSString *)name
                            ofType:(NSString *)ext
                       inDirectory:(NSString *)bundlePath;


/**
 返回指定名称与后缀的文件路径，并且会搜索与当前屏幕的比例文件，例如（@2x）
 
 @param name 文件名
 @param ext 后缀
 @return NSString
 */
-(NSString *)pathForScaledResource:(NSString *)name
                            ofType:(NSString *)ext;


@end
