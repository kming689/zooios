//
//  NSBundle+MFAdd.m
//  MFKit
//
//  Created by 方子扬 on 2017/3/22.
//  Copyright © 2017年 YiMu. All rights reserved.
//

#import "NSBundle+MFAdd.h"
#import <UIKit/UIKit.h>
#import "NSString+MFAdd.h"

@implementation NSBundle (MFAdd)

/**
 当前app版本
 
 @return NSString
 */
+(NSString *) appVersionString
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}


/**
 当前app build版本
 
 @return NSString
 */
+(NSString *) appBuildVersionString
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
}

/**
 app bundle name
 
 @return NSString
 */
+(NSString *) appBundleName
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleName"];
}


/**
 当前app build版本号是否早于传入的版本号，也可以理解为少于传入的版本号
 
 @param version 版本号
 @return yes:当前build号少于传入版本号  no:当前版本号大于传入版本号
 */
+(BOOL) appBuildVersionEarlierVersion:(NSString *) version
{
    NSString *buildVersion = [self appBuildVersionString];
    
    if ([buildVersion compare:version options:NSNumericSearch] == NSOrderedAscending){
        return YES;
    }else{
        return NO;
    }
}


/**
 一组NSNumber对象,用于搜索图片等最佳的搜索顺序
 iPhone3GS:@[@1,@2,@3] iPhone5:@[@2,@3,@1]  iPhone6 Plus:@[@3,@2,@1]
 
 @return NSArray<NSNumber *>
 */
+(NSArray<NSNumber *> *)preferredScales
{
    static NSArray *scales;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CGFloat screenScale = [UIScreen mainScreen].scale;
        if (screenScale <= 1) {
            scales = @[@1,@2,@3];
        } else if (screenScale <= 2) {
            scales = @[@2,@3,@1];
        } else {
            scales = @[@3,@2,@1];
        }
    });
    return scales;
}


/**
 返回指定bundle里指定名称与后缀的文件路径，并且会搜索与当前屏幕的比例文件，例如（@2x）
 
 @param name 文件名
 @param ext 后缀
 @param bundlePath bundle路径
 @return NSString
 */
+(NSString *)pathForScaledResource:(NSString *)name
                            ofType:(NSString *)ext
                       inDirectory:(NSString *)bundlePath
{
    if (name.length == 0) return nil;
    if ([name hasSuffix:@"/"]) return [self pathForResource:name ofType:ext inDirectory:bundlePath];
    
    NSString *path = nil;
    NSArray *scales = [self preferredScales];
    for (int s = 0; s < scales.count; s++) {
        CGFloat scale = ((NSNumber *)scales[s]).floatValue;
        NSString *scaledName = ext.length ? [name stringByAppendingNameScale:scale]
        : [name stringByAppendingPathScale:scale];
        path = [self pathForResource:scaledName ofType:ext inDirectory:bundlePath];
        if (path) break;
    }
    
    return path;
}


/**
 返回指定名称与后缀的文件路径，并且会搜索与当前屏幕的比例文件，例如（@2x）
 
 @param name 文件名
 @param ext 后缀
 @return NSString
 */
-(NSString *)pathForScaledResource:(NSString *)name
                            ofType:(NSString *)ext
{
    if (name.length == 0) return nil;
    if ([name hasSuffix:@"/"]) return [self pathForResource:name ofType:ext];
    
    NSString *path = nil;
    NSArray *scales = [NSBundle preferredScales];
    for (int s = 0; s < scales.count; s++) {
        CGFloat scale = ((NSNumber *)scales[s]).floatValue;
        NSString *scaledName = ext.length ? [name stringByAppendingNameScale:scale]
        : [name stringByAppendingPathScale:scale];
        path = [self pathForResource:scaledName ofType:ext];
        if (path) break;
    }
    
    return path;
}

@end
