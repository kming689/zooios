//
//  AVURLAsset+MFAdd.m
//  MFKit
//
//  Created by 方子扬 on 2017/3/22.
//  Copyright © 2017年 YiMu. All rights reserved.
//

#import "AVURLAsset+MFAdd.h"
#import <UIKit/UIKit.h>

@implementation AVURLAsset (MFAdd)

/**
 取视频的第N帧图片
 
 @param url 视频地址(本地\网络)
 @param time 第N帧
 @return 图片
 */
+(UIImage *) imageForVideoURL:(NSURL *) url atTime:(NSTimeInterval) time
{
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    return [asset imageForVideoAtTime:time];
}


/**
 取视频的第N帧图片
 
 @param time 第N帧
 @return 图片
 */
-(UIImage *) imageForVideoAtTime:(NSTimeInterval) time
{
    AVAssetImageGenerator *gen = [[AVAssetImageGenerator alloc] initWithAsset:self];
    gen.appliesPreferredTrackTransform = YES;
    gen.requestedTimeToleranceAfter = kCMTimeZero;
    gen.requestedTimeToleranceBefore = kCMTimeZero;
    CMTime ctTime = CMTimeMakeWithSeconds(time, 600);
    NSError *error = nil;
    CMTime actualTime;
    CGImageRef image = [gen copyCGImageAtTime:ctTime actualTime:&actualTime error:&error];
    UIImage *img = [[UIImage alloc] initWithCGImage:image];
    return img;
}


/**
 获取视频的时长格式化字符串
 
 @param url 视频地址(本地\网络)
 @return 返回视频时长，如:09:45
 */
+(NSString *) durationStringForVideoURL:(NSURL *) url
{
    NSDictionary *opts = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO]
                                                     forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:opts];
    if (asset) {
       return [asset durationStringForVideo];
    }else{
        return nil;
    }
}


/**
 获取视频的时长格式化字符串
 
 @return 返回视频时长，如:09:45
 */
-(NSString *) durationStringForVideo
{
    NSInteger duration = [self durationForVideo];
    NSInteger seconds = duration % 60;
    NSInteger minutes = (duration / 60) % 60;
    NSInteger hours = duration / 3600;
    
    NSMutableString *str = [NSMutableString string];
    if (hours > 0) {
        [str appendFormat:@"%zd:",hours];
    }
    if (minutes > 9) {
        [str appendFormat:@"%zd:",minutes];
    }else{
        if (hours > 0) {
            [str appendFormat:@"0%zd:",minutes];
        }else{
            [str appendFormat:@"%zd:",minutes];
        }
    }
    if (seconds > 9) {
        [str appendFormat:@"%zd",seconds];
    }else{
        [str appendFormat:@"0%zd",seconds];
    }
    return str;
}


/**
 获取视频时长
 
 @param url 视频地址(本地\网络)
 @return 返回总时长
 */
+(NSInteger) durationForVideoURL:(NSURL *) url
{
    NSDictionary *opts = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO]
                                                     forKey:AVURLAssetPreferPreciseDurationAndTimingKey];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:opts];
    if (asset) {
        return [asset durationForVideo];
    }else{
        return 0;
    }
}


/**
 获取视频时长
 
 @return 返回总时长
 */
-(NSInteger) durationForVideo
{
    return (NSInteger)ceilf(self.duration.value/self.duration.timescale);
}
@end
