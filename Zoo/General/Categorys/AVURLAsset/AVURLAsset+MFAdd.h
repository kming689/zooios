//
//  AVURLAsset+MFAdd.h
//  MFKit
//
//  Created by 方子扬 on 2017/3/22.
//  Copyright © 2017年 YiMu. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>

@interface AVURLAsset (MFAdd)



/**
 取视频的第N帧图片

 @param url 视频地址(本地\网络)
 @param time 第N帧
 @return 图片
 */
+(UIImage *) imageForVideoURL:(NSURL *) url atTime:(NSTimeInterval) time;


/**
 取视频的第N帧图片

 @param time 第N帧
 @return 图片
 */
-(UIImage *) imageForVideoAtTime:(NSTimeInterval) time;


/**
 获取视频的时长格式化字符串

 @param url 视频地址(本地\网络)
 @return 返回视频时长，如:09:45
 */
+(NSString *) durationStringForVideoURL:(NSURL *) url;


/**
 获取视频的时长格式化字符串
 
 @return 返回视频时长，如:09:45
 */
-(NSString *) durationStringForVideo;


/**
 获取视频时长

 @param url 视频地址(本地\网络)
 @return 返回总时长
 */
+(NSInteger) durationForVideoURL:(NSURL *) url;


/**
 获取视频时长

 @return 返回总时长
 */
-(NSInteger) durationForVideo;
@end
