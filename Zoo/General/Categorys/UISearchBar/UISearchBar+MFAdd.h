//
//  UISearchBar+MFAdd.h
//  MFKit
//
//  Created by huanghy on 2017/9/17.
//

#import <UIKit/UIKit.h>

@interface UISearchBar (MFAdd)


/**
 输入框

 @return UITextField
 */
-(UITextField *) searchTextField;

@end
