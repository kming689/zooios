//
//  UISearchBar+MFAdd.m
//  MFKit
//
//  Created by huanghy on 2017/9/17.
//

#import "UISearchBar+MFAdd.h"

@implementation UISearchBar (MFAdd)

-(UITextField *)searchTextField
{
    UITextField *searchTextField = nil;
    for (UIView* subview in self.subviews[0].subviews) {
        if ([subview isKindOfClass:[UITextField class]]) {
            searchTextField = (UITextField*)subview;
            break;
        }
    }
    return searchTextField;
}

@end
