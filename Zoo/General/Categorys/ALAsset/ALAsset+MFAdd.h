//
//  ALAsset+MFAdd.h
//  MFKit
//
//  Created by 方子扬 on 2017/3/22.
//  Copyright © 2017年 YiMu. All rights reserved.
//

#import <AssetsLibrary/AssetsLibrary.h>

typedef NS_ENUM(NSInteger, MFVideoCompressQuailty){
    MFVideoCompressQuailtyLow,
    
    MFVideoCompressQuailtyMedium,
    
    MFVideoCompressQuailtyHighest,
    
    MFVideoCompressQuailty640x480,
    
    MFVideoCompressQuailty960x540,
    
    MFVideoCompressQuailty1280x720,
    
    MFVideoCompressQuailty1920x1080,
    
    MFVideoCompressQuailty3840x2160
};


@interface ALAsset (MFAdd)

/**
 视频压缩
 
 @param quailty 压缩质量
 @param outputURL 压缩后存储的全路径（包括文件名）
 @param completedHandler 结果回调
 */
-(void) videoCompressWithQuailty:(MFVideoCompressQuailty) quailty outputURL:(NSURL *) outputURL completedHandler:(void (^)(BOOL outputCompleted, NSError *error)) completedHandler;


@end
