//
//  ALAsset+MFAdd.m
//  MFKit
//
//  Created by 方子扬 on 2017/3/22.
//  Copyright © 2017年 YiMu. All rights reserved.
//

#import "ALAsset+MFAdd.h"
#import <AVFoundation/AVFoundation.h>

@implementation ALAsset (MFAdd)

/**
 视频压缩
 
 @param quailty 压缩质量
 @param outputURL 压缩后存储的全路径（包括文件名）
 @param completedHandler 结果回调
 */
-(void) videoCompressWithQuailty:(MFVideoCompressQuailty) quailty outputURL:(NSURL *) outputURL completedHandler:(void (^)(BOOL outputCompleted, NSError *error)) completedHandler
{
    NSError *error = nil;
    if (![[self valueForProperty:ALAssetPropertyType] isEqualToString:ALAssetTypeVideo]) {
        error = [NSError errorWithDomain:@"MFKit" code:-1 userInfo:@{NSLocalizedDescriptionKey:@"source is not video"}];
        if (completedHandler) {
            completedHandler(NO,error);
        }
        return;
    }
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:[outputURL path]]) {
        [[NSFileManager defaultManager] removeItemAtURL:outputURL error:&error];
    }else{
        NSURL *dir = nil;
        if (outputURL.pathExtension.length > 0) {
            dir = [outputURL URLByDeletingLastPathComponent];
        }else{
            dir = outputURL;
        }
        [[NSFileManager defaultManager] createDirectoryAtURL:dir withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    if (error != nil) {
        if (completedHandler) {
            completedHandler(NO, error);
        }
    }else{
        NSString *preset = nil;
        switch (quailty) {
            case MFVideoCompressQuailtyLow:
                preset = AVAssetExportPresetLowQuality;
                break;
            case MFVideoCompressQuailtyMedium:
                preset = AVAssetExportPresetMediumQuality;
                break;
            case MFVideoCompressQuailtyHighest:
                preset = AVAssetExportPresetHighestQuality;
                break;
            case MFVideoCompressQuailty640x480:
                preset = AVAssetExportPreset640x480;
                break;
            case MFVideoCompressQuailty960x540:
                preset = AVAssetExportPreset960x540;
                break;
            case MFVideoCompressQuailty1280x720:
                preset = AVAssetExportPreset1280x720;
                break;
            case MFVideoCompressQuailty1920x1080:
                preset = AVAssetExportPreset1920x1080;
                break;
            case MFVideoCompressQuailty3840x2160:
                preset = AVAssetExportPreset3840x2160;
                break;
            default:
                preset = AVAssetExportPresetLowQuality;
                break;
        }
        
        NSURL *inputURL = [self valueForProperty:ALAssetPropertyAssetURL];
        AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
        AVAssetExportSession *session = [[AVAssetExportSession alloc] initWithAsset:asset presetName:preset];
        session.outputURL = outputURL;
        session.outputFileType = AVFileTypeQuickTimeMovie;
        session.shouldOptimizeForNetworkUse = YES;
        [session exportAsynchronouslyWithCompletionHandler:^{
            if (completedHandler) {
                completedHandler(session.status==AVAssetExportSessionStatusCompleted,nil);
            }
        }];
    }
}

@end
