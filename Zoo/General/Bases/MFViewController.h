//
//  MFViewController.h
//  Pods
//
//  Created by tanfameng on 2017/6/20.
//
//
/**
 该类提供的方法，都是不能在子类直接调用，而是需要覆盖重写的方法
 */

#import <UIKit/UIKit.h>

@interface MFViewController : UIViewController

#pragma mark -- 配置导航栏
/**
 设置导航栏，包括颜色，透明度，子类重载该方法实现特殊需求,该方法会调用下面的三个方法，所以，子类不能同时重写该方法跟下面方法的任何一个,只能重写这个或者下面的三个
 */
- (void)configNavigationBar;

#pragma mark -- 初始化界面
/**
 子类应该重写该方法，并将界面初始化代码放在该方法中
 */
- (void)configView;

/**
 刷新界面,默认实现为空
 */
- (void)refreshAllSubView;

#pragma mark --绑定viewModel
/**
 如果模块采用MVVM+RAC架构开发，重写该方法，并在方法中做RAC绑定
 */
- (void)bindViewModel;

#pragma makr - 登录成功

/**
 登录成功通知事件

 @param notification 通知
 */
- (void) didReceivedLoginSuccessNotification:(NSNotification *) notification;
    
@end
