//
//  MFViewModel.h
//  Pods
//
//  Created by tanfameng on 2017/6/20.
//
//

#import "MFObject.h"

@interface MFViewModel : MFObject

//如果使用MVVM+RAC架构，重写该方法，并在方法中做model跟viewModel的绑定
- (void)bindModel;

@end
