//
//  MFViewController.m
//  Pods
//
//  Created by tanfameng on 2017/6/20.
//
//

#import "MFViewController.h"
#import "MFColor.h"

@interface MFViewController ()

@end

@implementation MFViewController

- (instancetype)init {
    self = [super init];
    if (self) {

    }
    return self;
}

-(void)dealloc
{

}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self configNavigationBar];
    [self configView];
    [self bindViewModel];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshAllSubView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)configNavigationBar
{

}

- (void)configView
{
    self.view.backgroundColor = [MFColor mf_controllerViewBackgroundColor];
}

- (void)refreshAllSubView
{

}

- (void)bindViewModel
{

}

-(void)didReceivedLoginSuccessNotification:(NSNotification *)notification
{
    
}

@end
