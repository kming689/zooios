//
//  MFProxy.h
//  Pods
//
//  Created by 方子扬 on 2017/6/20.
//
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Proxy代理基类
 */
@interface MFProxy : NSProxy


/**
 对象初始化配置
 */
- (void) setup;

/**
 注册接口以及接口实现类
 
 @param protocol 接口类名
 @param protocolImpl 实现类对象
 */
- (void) registerProtocol:(Protocol *) protocol protocolImpl:(id) protocolImpl;

@end

NS_ASSUME_NONNULL_END
