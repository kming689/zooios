//
//  MFNavigationController.m
//  Pods
//
//  Created by tanfameng on 2017/6/20.
//
//

#import "MFNavigationController.h"
#import <objc/runtime.h>

static char *kSaveOriginDelegateKey;

@interface UINavigationController (MFShouldPop)
- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPopItem:(UINavigationItem *)item;
@end

#pragma clang diagnostic push
#pragma clang diagnostic ignored"-Wincomplete-implementation"
@implementation UINavigationController (MFShouldPop)
@end
#pragma clang diagnostic pop


@interface MFNavigationController ()

@end

@implementation MFNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    objc_setAssociatedObject(self, kSaveOriginDelegateKey, self.interactivePopGestureRecognizer.delegate, OBJC_ASSOCIATION_ASSIGN);
    self.interactivePopGestureRecognizer.delegate = (id<UIGestureRecognizerDelegate>)self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if (self.viewControllers.count != 0) {
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}

#pragma mark - 监听返回按钮事件
/**
 *  重载父类的方法
 *  是否要pop
 */
- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPopItem:(UINavigationItem *)item
{
    UIViewController *vc = self.topViewController;
    
    if (item != vc.navigationItem) { // 这个条件成立的话，则是代码写的pop语句
        // return [super navigationBar:navigationBar shouldPopItem:item];
        return YES; // 本来应该像上一句一样返回默认实现的，但是如果返回NO，crash，所以直接返回YES。
    }
    
    if ([vc conformsToProtocol:@protocol(MFNavigationControllerShouldPop)]) { // 看看vc有没有意愿
        if (![(id<MFNavigationControllerShouldPop>)vc mf_navigationController:self shouldPopItemWhenBackBarButtonItemClick:item]) { // 返回NO,表示不希望pop
            return NO;
        }
    }
    // 否则返回默认实现
    return [super navigationBar:navigationBar shouldPopItem:item];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer == self.interactivePopGestureRecognizer) {
        UIViewController *vc = [self topViewController];
        if ([vc conformsToProtocol:@protocol(MFNavigationControllerShouldPop)]) {
            if (![(id<MFNavigationControllerShouldPop>)vc mf_navigationControllerShouldStartInteractivePopGestureRecognizer:self]) {
                return NO;
            }
        }
        id<UIGestureRecognizerDelegate> originDelegate = objc_getAssociatedObject(self, kSaveOriginDelegateKey);
        return [originDelegate gestureRecognizerShouldBegin:gestureRecognizer];
    }
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (gestureRecognizer == self.interactivePopGestureRecognizer) {
        id<UIGestureRecognizerDelegate> originDelegate = objc_getAssociatedObject(self, kSaveOriginDelegateKey);
        return [originDelegate gestureRecognizer:gestureRecognizer shouldReceiveTouch:touch];
    }
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if (gestureRecognizer == self.interactivePopGestureRecognizer) {
        id<UIGestureRecognizerDelegate> originDelegate = objc_getAssociatedObject(self, kSaveOriginDelegateKey);
        return [originDelegate gestureRecognizer:gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:otherGestureRecognizer];
    }
    return YES;
}
@end
