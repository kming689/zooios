//
//  MFView.h
//  Pods
//
//  Created by tanfameng on 2017/6/20.
//
//

#import <UIKit/UIKit.h>

@interface MFView : UIView

/**
 在该方法内部刷新当前view
 */
- (void)refreshAllSubView;

@end
