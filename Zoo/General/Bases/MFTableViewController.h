//
//  MFTableViewController.h
//  Pods
//
//  Created by tanfameng on 2017/6/20.
//
//

#import <UIKit/UIKit.h>

@interface MFTableViewController : UITableViewController

#pragma makr - 登录成功

/**
 登录成功通知事件
 
 @param notification 通知
 */
- (void) didReceivedLoginSuccessNotification:(NSNotification *) notification;

@end
