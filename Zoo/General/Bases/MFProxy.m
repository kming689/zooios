//
//  MFProxy.m
//  Pods
//
//  Created by 方子扬 on 2017/6/20.
//
//

#import "MFProxy.h"
#import <objc/runtime.h>

@interface MFProxy ()

@property(nonatomic, strong) NSMutableDictionary *protocolImplMap;

@end

@implementation MFProxy

-(void)setup
{
    
}

-(void)registerProtocol:(Protocol *)protocol protocolImpl:(id)protocolImpl
{
    unsigned int numberOfMethods = 0;
    
    //Get all methods in protocol
    struct objc_method_description *methods = protocol_copyMethodDescriptionList(protocol, YES, YES, &numberOfMethods);
    
    //Register protocol methods
    for (unsigned int i = 0; i < numberOfMethods; i++) {
        struct objc_method_description method = methods[i];
        [self.protocolImplMap setValue:protocolImpl forKey:NSStringFromSelector(method.name)];
    }
    
    methods = protocol_copyMethodDescriptionList(protocol, NO, YES, &numberOfMethods);
    
    for (unsigned int i = 0; i < numberOfMethods; i++) {
        struct objc_method_description method = methods[i];
        [self.protocolImplMap setValue:protocolImpl forKey:NSStringFromSelector(method.name)];
    }
}

#pragma mark - property
-(NSMutableDictionary *)protocolImplMap
{
    if (_protocolImplMap) {
        return _protocolImplMap;
    }
    
    _protocolImplMap = [[NSMutableDictionary alloc] init];
    return _protocolImplMap;
}

#pragma mark - override method
-(NSMethodSignature *)methodSignatureForSelector:(SEL)sel
{
    NSString *methodName = NSStringFromSelector(sel);
    id protocolImpl = [self.protocolImplMap valueForKey:methodName];
    
    if (protocolImpl != nil && [protocolImpl respondsToSelector:sel]) {
        return [protocolImpl methodSignatureForSelector:sel];
    }else{
        return [super methodSignatureForSelector:sel];
    }
}

-(void)forwardInvocation:(NSInvocation *)invocation
{
    NSString *methodName = NSStringFromSelector(invocation.selector);
    id protocolImpl = [self.protocolImplMap valueForKey:methodName];
    
    if (protocolImpl != nil && [protocolImpl respondsToSelector:invocation.selector]) {
        [invocation invokeWithTarget:protocolImpl];
    }else{
        [super forwardInvocation:invocation];
    }
}

@end
