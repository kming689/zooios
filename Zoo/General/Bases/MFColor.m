//
//  MFColor.m
//  Pods
//
//  Created by tanfameng on 2017/6/20.
//
//

#import "MFColor.h"

@implementation MFColor

+(NSString *) mf_hexFromUIColor:(UIColor *) color {
    
    CGFloat r, g, b, a;
    [color getRed:&r green:&g blue:&b alpha:&a];
    int rgb = (int) (r * 255.0f)<<16 | (int) (g * 255.0f)<<8 | (int) (b * 255.0f)<<0;
    return [NSString stringWithFormat:@"#%06x", rgb];
}

+(UIColor *)mf_colorWithHexString:(NSString *)color
{
    NSString *cString = [[color stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    // String should be 6 or 8 characters
    if ([cString length] < 6) {
        return [UIColor clearColor];
    }
    
    // strip 0X if it appears
    if ([cString hasPrefix:@"0X"])
        cString = [cString substringFromIndex:2];
    if ([cString hasPrefix:@"#"])
        cString = [cString substringFromIndex:1];
    if ([cString length] != 6)
        return [UIColor clearColor];
    
    // Separate into r, g, b substrings
    NSRange range;
    range.location = 0;
    range.length = 2;
    
    //r
    NSString *rString = [cString substringWithRange:range];
    
    //g
    range.location = 2;
    NSString *gString = [cString substringWithRange:range];
    
    //b
    range.location = 4;
    NSString *bString = [cString substringWithRange:range];
    
    // Scan values
    unsigned int r, g, b;
    [[NSScanner scannerWithString:rString] scanHexInt:&r];
    [[NSScanner scannerWithString:gString] scanHexInt:&g];
    [[NSScanner scannerWithString:bString] scanHexInt:&b];
    
    return [UIColor colorWithRed:((float) r / 255.0f) green:((float) g / 255.0f) blue:((float) b / 255.0f) alpha:1.0f];
}

+(UIColor *)mf_mainColor
{
    return [self mf_colorWithHexString:@"#00aceb"];
}

+(UIColor *)mf_navigationBarTitleColor
{
    return [self mf_colorWithHexString:@"#404040"];
}

+(UIColor *)mf_controllerViewBackgroundColor
{
    return [self mf_colorWithHexString:@"#F1F2F1"];
}

+(UIColor *)mf_lineColor
{
    return [[UIColor blackColor] colorWithAlphaComponent:0.10];
}

+(UIColor *) mf_fontWhiteStyleColor
{
    return [self mf_colorWithHexString:@"#FFFFFF"];
}

+(UIColor *) mf_fontSkyBlueStyleColor
{
    return [self mf_colorWithHexString:@"#00aceb"];
}

+(UIColor *) mf_fontBlueStyleColor
{
    return [self mf_colorWithHexString:@"#286ee0"];
}

+(UIColor *) mf_fontBlackStyleColor
{
    return [self mf_colorWithHexString:@"#000000"];
}

+(UIColor *) mf_fontGrayStyleColor
{
    return [self mf_colorWithHexString:@"#B7B7B7"];
}

+(UIColor *) mf_fontLightGrayStyleColor
{
    return [self mf_colorWithHexString:@"#DFDFDF"];
}

+(UIColor *) mf_buttonTitleSkyBlueStyleNormalColor
{
    return [self mf_colorWithHexString:@"#00aceb"];
}

+(UIColor *) mf_buttonTitleSkyBlueStyleHighlightedColor
{
    return [self mf_colorWithHexString:@"#44C0FF"];
}

+(UIColor *) mf_buttonTitleBlueStyleNormalColor
{
    return [self mf_colorWithHexString:@"#4798F6"];
}

+(UIColor *) mf_buttonTitleBlueStyleHighlightedColor
{
    return [self mf_colorWithHexString:@"#44C0FF"];
}

+(UIColor *) mf_buttonTitleGrayStyleNormalColor
{
    return [self mf_colorWithHexString:@"#B7B7B7"];
}

+(UIColor *) mf_buttonTitleGrayStyleHighlightedColor
{
    return [self mf_colorWithHexString:@"#DFDFDF"];
}
@end
