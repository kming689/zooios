//
//  MFColor.h
//  Pods
//
//  Created by tanfameng on 2017/6/20.
//
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MFColor : UIColor


/**
 UIColor转换16进制

 @param color 颜色
 @return 16进制颜色值
 */
+(NSString *) mf_hexFromUIColor: (UIColor *) color;

/**
 16进制颜色值转换UIColor

 @param color 16进制颜色值
 @return UIColor
 */
+(UIColor *) mf_colorWithHexString:(NSString *)color;



/**
 主体（天蓝色）颜色，navigationBar color

 @return UIColor
 */
+(UIColor *) mf_mainColor;


/**
 导航栏标题颜色

 @return UIColor
 */
+(UIColor *) mf_navigationBarTitleColor;


/**
 Controller底View的颜色

 @return UIColor
 */
+(UIColor *) mf_controllerViewBackgroundColor;

/**
 线条颜色

 @return UIColor
 */
+(UIColor *) mf_lineColor;


/**
 字体白色风格颜色

 @return UIColor
 */
+(UIColor *) mf_fontWhiteStyleColor;

/**
 字体天蓝色风格颜色
 
 @return UIColor
 */
+(UIColor *) mf_fontSkyBlueStyleColor;

/**
 字体蓝色风格颜色

 @return UIColor
 */
+(UIColor *) mf_fontBlueStyleColor;


/**
 字体黑色风格颜色

 @return UIColor
 */
+(UIColor *) mf_fontBlackStyleColor;


/**
 字体灰色风格颜色

 @return UIColor
 */
+(UIColor *) mf_fontGrayStyleColor;

/**
 字体亮灰色风格颜色
 
 @return UIColor
 */
+(UIColor *) mf_fontLightGrayStyleColor;

/**
 按钮天蓝色标题风格

 @return UIColor
 */
+(UIColor *) mf_buttonTitleSkyBlueStyleNormalColor;

/**
 按钮天蓝色标题高亮风格
 
 @return UIColor
 */
+(UIColor *) mf_buttonTitleSkyBlueStyleHighlightedColor;

/**
 按钮蓝色标题风格
 
 @return UIColor
 */
+(UIColor *) mf_buttonTitleBlueStyleNormalColor;

/**
 按钮蓝色标题高亮风格
 
 @return UIColor
 */
+(UIColor *) mf_buttonTitleBlueStyleHighlightedColor;

/**
 按钮灰色标题风格
 
 @return UIColor
 */
+(UIColor *) mf_buttonTitleGrayStyleNormalColor;

/**
 按钮灰色标题高亮风格
 
 @return UIColor
 */
+(UIColor *) mf_buttonTitleGrayStyleHighlightedColor;

@end

NS_ASSUME_NONNULL_END
