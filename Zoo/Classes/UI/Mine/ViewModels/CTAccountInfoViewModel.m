//
//  CTAccountInfoViewModel.m
//  CTYun
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAccountInfoViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <objc/runtime.h>
#import "CTAuthenItemBaseModel.h"
#import "CTAuthenInputItem.h"
#import "CTAuthenSelectItem.h"
#import "CTAuthenSelectOption.h"

#import "MFNetRequestBusiness.h"
#import "CTPersonalAccountInfo.h"

@implementation CTAccountInfoViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark -- override method
- (void)bindModel
{
    [super bindModel];
}

#pragma mark -- property
- (RACCommand *)dataCommand
{
    if (!_dataCommand) {
        @weakify(self)
        _dataCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //获取模板
                NSInteger type = 1;
                if (input) {
                    type = [input integerValue];
                }
                [self getAccountInfoWithAccoutType:(NSInteger)type success:^(id responseObject) {
                    [subscriber sendNext:responseObject];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                    
                }];
            }];
            
            return dataSignal;
        }];
    }
    return _dataCommand;
}

#pragma mark -- data

- (NSDictionary *)getAuthenDefaultParams
{
    NSMutableDictionary *globalParams = [[NSMutableDictionary alloc] init];
    
//    if (_accountInfo.accountType == eCTAccountTypeCompany) {
//        NSMutableDictionary *baseInfoDic = [[NSMutableDictionary alloc] init];
//        [globalParams setObject:baseInfoDic forKey:@"companyInfo"];
//        NSMutableDictionary *legalInfoDic = [[NSMutableDictionary alloc] init];
//        [globalParams setObject:legalInfoDic forKey:@"legalPersonInfo"];
//        CTCompanyAccountInfo *companyAccount = (CTCompanyAccountInfo *)_accountInfo;
//        //公司基本信息
//        if (companyAccount.name&&companyAccount.name.length > 0) {
//            [baseInfoDic setObject:companyAccount.name forKey:@"name"];
//        }
//        [baseInfoDic setObject:@(companyAccount.companyType) forKey:@"companyType"];
//        if (companyAccount.address&&companyAccount.address.length > 0) {
//            [baseInfoDic setObject:companyAccount.address forKey:@"address"];
//        }
//        if (companyAccount.postNo&&companyAccount.postNo.length > 0) {
//            [baseInfoDic setObject:companyAccount.postNo forKey:@"postNo"];
//        }
//        if (companyAccount.telephone&&companyAccount.telephone.length > 0) {
//            [baseInfoDic setObject:companyAccount.telephone forKey:@"telephone"];
//        }
//        [baseInfoDic setObject:@(companyAccount.licenseType) forKey:@"licenseType"];
//        if (companyAccount.licenseNo&&companyAccount.licenseNo.length > 0) {
//            [baseInfoDic setObject:companyAccount.licenseNo forKey:@"licenseNo"];
//        }
//        if (companyAccount.licenseFilePath&&companyAccount.licenseFilePath.length > 0) {
//            [baseInfoDic setObject:companyAccount.licenseFilePath forKey:@"licenseFilePath"];
//        }
//        //法人信息
//        if (companyAccount.legalPerson) {
//            CTLegalPerson *legalPerson = companyAccount.legalPerson;
//            if (legalPerson.name&&legalPerson.name.length > 0) {
//                [legalInfoDic setObject:legalPerson.name forKey:@"name"];
//            }
//            if (legalPerson.email&&legalPerson.email.length > 0) {
//                [legalInfoDic setObject:legalPerson.email forKey:@"email"];
//            }
//            if (legalPerson.mobilePhone&&legalPerson.mobilePhone.length > 0) {
//                [legalInfoDic setObject:legalPerson.mobilePhone forKey:@"mobilePhone"];
//            }
//            if (legalPerson.cardNo&&legalPerson.cardNo.length > 0) {
//                [legalInfoDic setObject:legalPerson.cardNo forKey:@"cardNo"];
//            }
//        }
//    }else if (_accountInfo.accountType == eCTAccountTypePersonal) {
//        NSMutableDictionary *baseInfoDic = [[NSMutableDictionary alloc] init];
//        [globalParams setObject:baseInfoDic forKey:@"personInfo"];
//        NSMutableDictionary *cardInfoDic = [[NSMutableDictionary alloc] init];
//        [globalParams setObject:cardInfoDic forKey:@"personCardInfo"];
//
//        CTPersonalAccountInfo *personalAccount = (CTPersonalAccountInfo *)_accountInfo;
//        //个人基础信息
//        if (personalAccount.name&&personalAccount.name.length > 0) {
//            [baseInfoDic setObject:personalAccount.name forKey:@"name"];
//        }
//        if (personalAccount.address&&personalAccount.address.length > 0) {
//            [baseInfoDic setObject:personalAccount.address forKey:@"address"];
//        }
//        if (personalAccount.postNo&&personalAccount.postNo.length > 0) {
//            [baseInfoDic setObject:personalAccount.postNo forKey:@"postNo"];
//        }
//        if (personalAccount.telephone&&personalAccount.telephone.length > 0) {
//            [baseInfoDic setObject:personalAccount.telephone forKey:@"telephone"];
//        }
//        if (personalAccount.mobilephone&&personalAccount.mobilephone.length > 0) {
//            [baseInfoDic setObject:personalAccount.mobilephone forKey:@"mobilephone"];
//        }
//        //个人card信息
//        [cardInfoDic setObject:@(personalAccount.cardType) forKey:@"cardType"];
//        if (personalAccount.cardNo&&personalAccount.cardNo.length > 0) {
//            [cardInfoDic setObject:personalAccount.cardNo forKey:@"cardNo"];
//        }
//    }
    
    return globalParams;
}

- (NSArray *)getCompanyCellItems
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    ///------第一组,账户信息
    NSMutableDictionary *companyClassDic = [[NSMutableDictionary alloc] init];
    [array addObject:companyClassDic];
    [companyClassDic setObject:@"账户信息" forKey:@"title"];
    NSMutableArray *accountInfos = [[NSMutableArray alloc] init];
    [companyClassDic setObject:accountInfos forKey:@"data"];
    //账户类型
    CTAuthenSelectItem *classItem = [[CTAuthenSelectItem alloc] init];
    classItem.title = @"账户类型";
    classItem.indicatorTip = @"公司";
    classItem.canEdit = NO;
    [accountInfos addObject:classItem];
    //认证信息
    CTAuthenInputItem *authenItem = [[CTAuthenInputItem alloc] init];
    authenItem.title = @"认证信息";
    authenItem.indicatorTip = [self getTipWithAuthenState:_accountInfo.auditStatus];
    authenItem.submitKey = @"auditStatus";
    
    [accountInfos addObject:authenItem];
    
    ///------第二组,基本信息
    NSMutableDictionary *baseInfoDic = [[NSMutableDictionary alloc] init];
    [array addObject:baseInfoDic];
    [baseInfoDic setObject:@"基本信息" forKey:@"title"];
    NSMutableArray *baseInfos = [[NSMutableArray alloc] init];
    [baseInfoDic setObject:baseInfos forKey:@"data"];
    //公司名称
    CTAuthenInputItem *nameItem = [[CTAuthenInputItem alloc] init];
    nameItem.title = @"公司名称";
    if (_accountInfo.name&&_accountInfo.name.length > 0) {
        nameItem.indicatorTip = _accountInfo.name;
        nameItem.submitValue = _accountInfo.name;
    }else{
        nameItem.indicatorTip = @"请填写";
    }
    nameItem.submitKey = @"name";
    [baseInfos addObject:nameItem];
    //固定电话
    CTAuthenInputItem *fixPhoneItem = [[CTAuthenInputItem alloc] init];
    fixPhoneItem.title = @"固定电话";
    if (_accountInfo.telephone&&_accountInfo.telephone.length > 0) {
        fixPhoneItem.indicatorTip = _accountInfo.telephone;
        fixPhoneItem.submitValue = _accountInfo.telephone;
    }else{
        fixPhoneItem.indicatorTip = @"请填写";
    }
    fixPhoneItem.submitKey = @"telephone";
    fixPhoneItem.regex = @"(^(\\d{2,4}[-_－—]?)?\\d{3,8}([-_－—]?\\d{3,8})?([-_－—]?\\d{1,7})?$)|(^0?1[35]\\d{9}$)";
    [baseInfos addObject:fixPhoneItem];
    //联系地址
    CTAuthenInputItem *addressItem = [[CTAuthenInputItem alloc] init];
    addressItem.title = @"联系地址";
    if (_accountInfo.address&&_accountInfo.address.length > 0) {
        addressItem.indicatorTip = _accountInfo.address;
        addressItem.submitValue = _accountInfo.address;
    }else{
        addressItem.indicatorTip = @"请填写";
    }
    addressItem.submitKey = @"address";
    [baseInfos addObject:addressItem];
    //邮政编码
    CTAuthenInputItem *postcodeItem = [[CTAuthenInputItem alloc] init];
    postcodeItem.title = @"邮政编码";
    if (_accountInfo.postNo&&_accountInfo.postNo.length > 0) {
        postcodeItem.indicatorTip = _accountInfo.postNo;
        postcodeItem.submitValue = _accountInfo.postNo;
    }else{
        postcodeItem.indicatorTip = @"请填写";
    }
    postcodeItem.submitKey = @"postNo";
    postcodeItem.regex = @"[1-9]\\d{5}(?!\\d)";
    [baseInfos addObject:postcodeItem];
    
    //第三组，安全信息
    NSMutableDictionary *secretInfoDic = [[NSMutableDictionary alloc] init];
    [array addObject:secretInfoDic];
    [secretInfoDic setObject:@"安全信息" forKey:@"title"];
    NSMutableArray *secretInfos = [[NSMutableArray alloc] init];
    [secretInfoDic setObject:secretInfos forKey:@"data"];
    
    //绑定手机
    CTAuthenInputItem *mobilePhoneItem = [[CTAuthenInputItem alloc] init];
    mobilePhoneItem.title = @"绑定手机";
    if (_accountInfo.mobilephone&&_accountInfo.mobilephone.length > 0) {
        mobilePhoneItem.indicatorTip = _accountInfo.mobilephone;
        mobilePhoneItem.submitValue = _accountInfo.mobilephone;
    }else{
        mobilePhoneItem.indicatorTip = @"请填写";
    }
    mobilePhoneItem.submitKey = @"mobilephone";
    mobilePhoneItem.regex = @"^1[0-9]\\d{9}$";
    [secretInfos addObject:mobilePhoneItem];
    
    //绑定邮箱
    CTAuthenInputItem *emailItem = [[CTAuthenInputItem alloc] init];
    emailItem.title = @"绑定邮箱";
    if (_accountInfo.email&&_accountInfo.email.length > 0) {
        emailItem.indicatorTip = _accountInfo.email;
        emailItem.submitValue = _accountInfo.email;
    }else{
        emailItem.indicatorTip = @"请填写";
    }
    emailItem.submitKey = @"email";
    emailItem.regex = @"[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?";
    [secretInfos addObject:emailItem];
    
    return array;
}

- (NSArray *)getPersonalCellItems
{
    CTPersonalAccountInfo *personAccount = (CTPersonalAccountInfo *)_accountInfo;
    
    NSMutableArray *array = [[NSMutableArray alloc] init];
    
    ///------第一组,账户信息
    NSMutableDictionary *companyClassDic = [[NSMutableDictionary alloc] init];
    [array addObject:companyClassDic];
    [companyClassDic setObject:@"账户信息" forKey:@"title"];
    NSMutableArray *accountInfos = [[NSMutableArray alloc] init];
    [companyClassDic setObject:accountInfos forKey:@"data"];
    //用户类型
    CTAuthenSelectItem *classItem = [[CTAuthenSelectItem alloc] init];
    classItem.title = @"用户类型";
    classItem.indicatorTip = @"个人客户";
    classItem.canEdit = NO;
    [accountInfos addObject:classItem];
    //认证信息
    CTAuthenInputItem *authenItem = [[CTAuthenInputItem alloc] init];
    authenItem.title = @"认证信息";
    authenItem.submitKey = @"auditStatus";
    authenItem.indicatorTip = [self getTipWithAuthenState:_accountInfo.auditStatus];
    [accountInfos addObject:authenItem];
    
    ///------第二组,基本信息
    NSMutableDictionary *baseInfoDic = [[NSMutableDictionary alloc] init];
    [array addObject:baseInfoDic];
    [baseInfoDic setObject:@"基本信息" forKey:@"title"];
    NSMutableArray *baseInfos = [[NSMutableArray alloc] init];
    [baseInfoDic setObject:baseInfos forKey:@"data"];
    //姓名
    CTAuthenInputItem *nameItem = [[CTAuthenInputItem alloc] init];
    nameItem.title = @"姓名";
    if (_accountInfo.name&&_accountInfo.name.length > 0) {
        nameItem.indicatorTip = _accountInfo.name;
        nameItem.submitValue = _accountInfo.name;
    }else{
        nameItem.indicatorTip = @"请填写";
    }
    nameItem.submitKey = @"name";
    [baseInfos addObject:nameItem];
    //性别
    CTAuthenSelectItem *sexItem = [[CTAuthenSelectItem alloc] init];
    sexItem.title = @"性别";
    sexItem.submitKey = @"sex";
    if (personAccount.sex) {
        sexItem.indicatorTip = @"女";
    }else{
        sexItem.indicatorTip = @"男";
    }
    NSMutableArray *sexOptions = [[NSMutableArray alloc] init];
    CTAuthenSelectOption *option1 = [[CTAuthenSelectOption alloc] init];
    option1.name = @"男";
    option1.value = @"0";
    CTAuthenSelectOption *option2 = [[CTAuthenSelectOption alloc] init];
    option2.name = @"女";
    option2.value = @"1";
    [sexOptions addObject:option1];
    [sexOptions addObject:option2];
    sexItem.optionValues = sexOptions;
    [baseInfos addObject:sexItem];
    //固定电话
    CTAuthenInputItem *fixPhoneItem = [[CTAuthenInputItem alloc] init];
    fixPhoneItem.title = @"固定电话";
    if (_accountInfo.telephone&&_accountInfo.telephone.length > 0) {
        fixPhoneItem.indicatorTip = _accountInfo.telephone;
        fixPhoneItem.submitValue = _accountInfo.telephone;
    }else{
        fixPhoneItem.indicatorTip = @"请填写";
    }
    fixPhoneItem.submitKey = @"telephone";
    fixPhoneItem.regex = @"(^(\\d{2,4}[-_－—]?)?\\d{3,8}([-_－—]?\\d{3,8})?([-_－—]?\\d{1,7})?$)|(^0?1[35]\\d{9}$)";
    [baseInfos addObject:fixPhoneItem];
    //联系地址
    CTAuthenInputItem *addressItem = [[CTAuthenInputItem alloc] init];
    addressItem.title = @"联系地址";
    if (_accountInfo.address&&_accountInfo.address.length > 0) {
        addressItem.indicatorTip = _accountInfo.address;
        addressItem.submitValue = _accountInfo.address;
    }else{
        addressItem.indicatorTip = @"请填写";
    }
    addressItem.submitKey = @"address";
    [baseInfos addObject:addressItem];
    //邮政编码
    CTAuthenInputItem *postcodeItem = [[CTAuthenInputItem alloc] init];
    postcodeItem.title = @"邮政编码";
    if (_accountInfo.postNo&&_accountInfo.postNo.length > 0) {
        postcodeItem.indicatorTip = _accountInfo.postNo;
        postcodeItem.submitValue = _accountInfo.postNo;
    }else{
        postcodeItem.indicatorTip = @"请填写";
    }
    postcodeItem.submitKey = @"postNo";
    postcodeItem.regex = @"[1-9]\\d{5}(?!\\d)";
    [baseInfos addObject:postcodeItem];
    
    //第三组，安全信息
    NSMutableDictionary *secretInfoDic = [[NSMutableDictionary alloc] init];
    [array addObject:secretInfoDic];
    [secretInfoDic setObject:@"安全信息" forKey:@"title"];
    NSMutableArray *secretInfos = [[NSMutableArray alloc] init];
    [secretInfoDic setObject:secretInfos forKey:@"data"];
    
    //绑定手机
    CTAuthenInputItem *mobilePhoneItem = [[CTAuthenInputItem alloc] init];
    mobilePhoneItem.title = @"绑定手机";
    if (_accountInfo.mobilephone&&_accountInfo.mobilephone.length > 0) {
        mobilePhoneItem.indicatorTip = _accountInfo.mobilephone;
        mobilePhoneItem.submitValue = _accountInfo.mobilephone;
    }else{
        mobilePhoneItem.indicatorTip = @"请填写";
    }
    mobilePhoneItem.submitKey = @"mobilephone";
    mobilePhoneItem.regex = @"^1[0-9]\\d{9}$";
    [secretInfos addObject:mobilePhoneItem];

    //绑定邮箱
    CTAuthenInputItem *emailItem = [[CTAuthenInputItem alloc] init];
    emailItem.title = @"绑定邮箱";
    if (_accountInfo.email&&_accountInfo.email.length > 0) {
        emailItem.indicatorTip = _accountInfo.email;
        emailItem.submitValue = _accountInfo.email;
    }else{
        emailItem.indicatorTip = @"请填写";
    }
    emailItem.submitKey = @"email";
    emailItem.regex = @"[\\w!#$%&'*+/=?^_`{|}~-]+(?:\\.[\\w!#$%&'*+/=?^_`{|}~-]+)*@(?:[\\w](?:[\\w-]*[\\w])?\\.)+[\\w](?:[\\w-]*[\\w])?";
    [secretInfos addObject:emailItem];
    
    return array;
}

#pragma mark -- 网络请求
- (void)getAccountInfoWithAccoutType:(NSInteger)type success:(void (^)(id responseObject))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
//    [proxy requestAccountInfoSuccess:^(CTAccountInfomation *accountInfo) {
//        _accountInfo = accountInfo;
//        if (completion) {
//            if (_accountInfo.accountType == eCTAccountTypeCompany) {
//                completion([self getCompanyCellItems]);
//            }else if (_accountInfo.accountType == eCTAccountTypePersonal) {
//                completion([self getPersonalCellItems]);
//            }
//        }
//    } failure:^(NSError *error) {
//        if (failure) {
//            failure(error);
//        }
//    }];
}

- (void)updateAccountInfo:(NSDictionary *)params success:(void (^)(BOOL))completion failure:(void (^)(NSError *))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    
//    if (_accountInfo.accountType == eCTAccountTypePersonal) {
//        [proxy updatePersonalAccountInfo:params accountId:_accountInfo.accountId userId:_accountInfo.userId success:^(BOOL success) {
//            if (completion) {
//                completion(success);
//            }
//        } failure:^(NSError * _Nonnull error) {
//            if (failure) {
//                failure(error);
//            }
//        }];
//    }else if (_accountInfo.accountType == eCTAccountTypeCompany) {
//        [proxy updateCompanyAccountInfo:params accountId:_accountInfo.accountId userId:_accountInfo.userId success:^(BOOL success) {
//            if (completion) {
//                completion(success);
//            }
//        } failure:^(NSError * _Nonnull error) {
//            if (failure) {
//                failure(error);
//            }
//        }];
//    }
}

#pragma mark -- 处理数据
- (void)dealDataWithInfo:(CTAccountInfomation *)accountInfo
{
    
}

- (NSString *)getTipWithAuthenState:(eCTAuthenticationState)state
{
    if (state == eCTAuthenticationStateNone) {
        return @"未认证";
    }else if (state == eCTAuthenticationStateBeing) {
        return @"认证中";
    }else if (state == eCTAuthenticationStateFail) {
        return @"认证失败";
    }else if (state == eCTAuthenticationStateDone) {
        return @"已认证";
    }
    return @"";
}

@end
