//
//  CTAuthenViewModel.m
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <YYModel/YYModel.h>

#import "CTAuthenItemBaseModel.h"
#import "CTAuthenInputItem.h"
#import "CTAuthenSelectItem.h"
#import "CTAuthenUploadCardItem.h"
#import "CTAuthenUploadFileItem.h"
#import "CTAuthenDescItem.h"
#import "CTAuthenButtonItem.h"
#import "CTAuthenTemplate.h"
#import "CTAuthenGroup.h"
#import "CTAuthenSelectOption.h"

#import "CTAccountInfomation.h"
#import "CTPersonalAccountInfo.h"

#import "MFNetRequestBusiness.h"

@interface CTAuthenViewModel()

@end

@implementation CTAuthenViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark -- override method
- (void)bindModel
{
    [super bindModel];
}

#pragma mark -- property
- (RACCommand *)dataCommand
{
    if (!_dataCommand) {
        @weakify(self)
        _dataCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //获取模板
                [self getDataSourceTemplateSuccess:^(id responseObject) {
                    [subscriber sendNext:responseObject];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求

                }];
            }];
            
            return dataSignal;
        }];
    }
    return _dataCommand;
}

- (RACCommand *)submitCommand
{
    if (!_submitCommand) {
        @weakify(self)
        _submitCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *submitSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //提交信息
                [self submitDataSourceSuccess:^(BOOL success) {
                    [subscriber sendNext:[NSNumber numberWithBool:success]];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                    
                }];
            }];
            
            return submitSignal;
        }];
    }
    return _submitCommand;
}

#pragma mark -- public


#pragma mark -- 请求数据
- (void)getDataSourceTemplateSuccess:(void (^)(id responseObject))completion failure:(void (^)(NSError *error))failure
{
    //判断是否已经完成认证，如果已经完成认证，不从网络获取模板
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
//    [proxy requestAuthenTemplateWithAccountType:_accountInfo.accountType auditStatus:_accountInfo.auditStatus success:^(id authenTemplate) {
//        if (completion) {
//            completion([self dealDataWithTemplate:authenTemplate]);
//        }
//    } failure:^(NSError *error) {
//        if (failure) {
//            failure(error);
//        }
//    }];
}

- (void)submitDataSourceSuccess:(void (^)(BOOL success))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    
//    if (_accountInfo.accountType == eCTAccountTypePersonal) {
//        [proxy auditPersonalAccountWithInfo:[self generateParams] accountId:_accountInfo.accountId userId:_accountInfo.userId success:^(BOOL success) {
//            if (completion) {
//                completion(success);
//            }
//        } failure:^(NSError * _Nonnull error) {
//            if (failure) {
//                failure(error);
//            }
//        }];
//    }else if (_accountInfo.accountType == eCTAccountTypeCompany) {
//        [proxy auditCompanyAccountWithInfo:[self generateParams] accountId:_accountInfo.accountId userId:_accountInfo.userId success:^(BOOL success) {
//            if (completion) {
//                completion(success);
//            }
//        } failure:^(NSError * _Nonnull error) {
//            if (failure) {
//                failure(error);
//            }
//        }];
//    }
}

- (void)uploadFile:(id)fileData imageKey:(NSString *)imageKey success:(void (^)(id responseObject))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    __block NSString *fileName = [[NSProcessInfo processInfo] globallyUniqueString];
    [params setObject:imageKey forKey:@"uploadKey"];

}

#pragma mark -- private

- (NSMutableDictionary *)generateParams
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    for (CTAuthenGroup *group in self.dataSource.structures) {
        NSMutableDictionary *groupDic = [[NSMutableDictionary alloc] init];
        [params setObject:groupDic forKey:group.submitKey];
        for (CTAuthenItemBaseModel *item in group.itemCells) {
            if (item.submitValue&&item.submitValue.length > 0) {
                [groupDic setObject:item.submitValue forKey:item.submitKey];
            }
        }
    }
    
    return params;
}

- (NSDictionary *)getAuthenDefaultParams
{
    NSMutableDictionary *globalParams = [[NSMutableDictionary alloc] init];
    
//    if (_accountInfo.accountType == eCTAccountTypeCompany) {
//        NSMutableDictionary *baseInfoDic = [[NSMutableDictionary alloc] init];
//        [globalParams setObject:baseInfoDic forKey:@"companyInfo"];
//        NSMutableDictionary *legalInfoDic = [[NSMutableDictionary alloc] init];
//        [globalParams setObject:legalInfoDic forKey:@"legalPersonInfo"];
//        CTCompanyAccountInfo *companyAccount = (CTCompanyAccountInfo *)_accountInfo;
//        //公司基本信息
//        if (companyAccount.name&&companyAccount.name.length > 0) {
//            [baseInfoDic setObject:companyAccount.name forKey:@"name"];
//        }
//        [baseInfoDic setObject:@(companyAccount.companyType) forKey:@"companyType"];
//        if (companyAccount.address&&companyAccount.address.length > 0) {
//            [baseInfoDic setObject:companyAccount.address forKey:@"address"];
//        }
//        if (companyAccount.postNo&&companyAccount.postNo.length > 0) {
//            [baseInfoDic setObject:companyAccount.postNo forKey:@"postNo"];
//        }
//        if (companyAccount.telephone&&companyAccount.telephone.length > 0) {
//            [baseInfoDic setObject:companyAccount.telephone forKey:@"telephone"];
//        }
//        [baseInfoDic setObject:@(companyAccount.licenseType) forKey:@"licenseType"];
//        if (companyAccount.licenseNo&&companyAccount.licenseNo.length > 0) {
//            [baseInfoDic setObject:companyAccount.licenseNo forKey:@"licenseNo"];
//        }
//        if (companyAccount.licenseFilePath&&companyAccount.licenseFilePath.length > 0) {
//            [baseInfoDic setObject:companyAccount.licenseFilePath forKey:@"licenseFilePath"];
//        }
//        //法人信息
//        if (companyAccount.legalPerson) {
//            CTLegalPerson *legalPerson = companyAccount.legalPerson;
//            if (legalPerson.name&&legalPerson.name.length > 0) {
//                [legalInfoDic setObject:legalPerson.name forKey:@"name"];
//            }
//            if (legalPerson.email&&legalPerson.email.length > 0) {
//                [legalInfoDic setObject:legalPerson.email forKey:@"email"];
//            }
//            if (legalPerson.mobilePhone&&legalPerson.mobilePhone.length > 0) {
//                [legalInfoDic setObject:legalPerson.mobilePhone forKey:@"mobilePhone"];
//            }
//            if (legalPerson.cardNo&&legalPerson.cardNo.length > 0) {
//                [legalInfoDic setObject:legalPerson.cardNo forKey:@"cardNo"];
//            }
//        }
//    }else if (_accountInfo.accountType == eCTAccountTypePersonal) {
//        NSMutableDictionary *baseInfoDic = [[NSMutableDictionary alloc] init];
//        [globalParams setObject:baseInfoDic forKey:@"personInfo"];
//        NSMutableDictionary *cardInfoDic = [[NSMutableDictionary alloc] init];
//        [globalParams setObject:cardInfoDic forKey:@"personCardInfo"];
//        
//        CTPersonalAccountInfo *personalAccount = (CTPersonalAccountInfo *)_accountInfo;
//        //个人基础信息
//        if (personalAccount.name&&personalAccount.name.length > 0) {
//            [baseInfoDic setObject:personalAccount.name forKey:@"name"];
//        }
//        if (personalAccount.address&&personalAccount.address.length > 0) {
//            [baseInfoDic setObject:personalAccount.address forKey:@"address"];
//        }
//        if (personalAccount.postNo&&personalAccount.postNo.length > 0) {
//            [baseInfoDic setObject:personalAccount.postNo forKey:@"postNo"];
//        }
//        if (personalAccount.telephone&&personalAccount.telephone.length > 0) {
//            [baseInfoDic setObject:personalAccount.telephone forKey:@"telephone"];
//        }
//        if (personalAccount.mobilephone&&personalAccount.mobilephone.length > 0) {
//            [baseInfoDic setObject:personalAccount.mobilephone forKey:@"mobilephone"];
//        }
//        //个人card信息
//        [cardInfoDic setObject:@(personalAccount.cardType) forKey:@"cardType"];
//        if (personalAccount.cardNo&&personalAccount.cardNo.length > 0) {
//            [cardInfoDic setObject:personalAccount.cardNo forKey:@"cardNo"];
//        }
//    }
    
    return globalParams;
}

- (CTAuthenTemplate *)dealDataWithTemplate:(id)authenTemplate
{
    NSDictionary *dataDic = authenTemplate;
    CTAuthenTemplate *templateData = [[CTAuthenTemplate alloc] init];
    templateData.templateType = [dataDic objectForKey:@"templateType"];
    templateData.introduction = [dataDic objectForKey:@"introduce"];
    
    NSDictionary *_defaultParams = self.defaultParams;
    if (!_defaultParams) {
        _defaultParams = [self getAuthenDefaultParams];
    }
    
    NSArray *structuresArray = [dataDic objectForKey:@"structures"];
    NSInteger index = 0;
    for (NSDictionary *groupDic in structuresArray) {
        CTAuthenGroup *authenGroup = [[CTAuthenGroup alloc] init];
        authenGroup.title = [groupDic objectForKey:@"title"];
        authenGroup.submitKey = [groupDic objectForKey:@"key"];
        
        NSDictionary *defalutGroupParams = [_defaultParams objectForKey:authenGroup.submitKey];
        
        NSArray *itemsArray = [groupDic objectForKey:@"items"];
        for (NSDictionary *itemDic in itemsArray) {
            NSString *itemType = [itemDic objectForKey:@"itemType"];
            CTAuthenItemBaseModel *baseItem = nil;
            if ([itemType isEqualToString:@"input"]) {
                CTAuthenInputItem *item = [CTAuthenInputItem yy_modelWithDictionary:itemDic];
                [authenGroup.itemCells addObject:item];
                baseItem = item;
            }else if ([itemType isEqualToString:@"selector"]) {
                CTAuthenSelectItem *item = [CTAuthenSelectItem yy_modelWithDictionary:itemDic];
                [authenGroup.itemCells addObject:item];
                baseItem = item;
            }else if ([itemType isEqualToString:@"image"]) {
                CTAuthenUploadCardItem *item = [CTAuthenUploadCardItem yy_modelWithDictionary:itemDic];
                [authenGroup.itemCells addObject:item];
                baseItem = item;
            }else if ([itemType isEqualToString:@"showText"]) {
                CTAuthenInputItem *item = [CTAuthenInputItem yy_modelWithDictionary:itemDic];
                item.canEdit = NO;
                [authenGroup.itemCells addObject:item];
                baseItem = item;
            }else if ([itemType isEqualToString:@"upload"]) {
                CTAuthenUploadFileItem *item = [CTAuthenUploadFileItem yy_modelWithDictionary:itemDic];
                [authenGroup.itemCells addObject:item];
                baseItem = item;
            }
            //检查是否有初始值
            if (baseItem) {
                //如果该item对应的值已经初始化过了
                if ([defalutGroupParams objectForKey:baseItem.submitKey]) {
                    //如果是选择类型的，先找出值
                    if ([baseItem.itemType isEqualToString:@"selector"]) {
                        CTAuthenSelectItem *selectItem = (CTAuthenSelectItem *)baseItem;
                        //遍历该item的选项，比对key，option的key就是参数的值
                        for (CTAuthenSelectOption *option in selectItem.optionValues) {
                            if ([option.value isEqualToString:[NSString stringWithFormat:@"%@",[defalutGroupParams objectForKey:baseItem.submitKey]]]) {
                                baseItem.indicatorTip = option.name;
                                baseItem.submitValue = option.value;
                            }
                        }
                    }else{
                        baseItem.indicatorTip = [NSString stringWithFormat:@"%@",[defalutGroupParams objectForKey:baseItem.submitKey]];
                        baseItem.submitValue = [NSString stringWithFormat:@"%@",[defalutGroupParams objectForKey:baseItem.submitKey]];
                    }
                }else{
                    //如果是认证完成状态，取消掉所有hint显示
                    if (_accountInfo.auditStatus != eCTAuthenticationStateNone) {
                        baseItem.indicatorTip = @"未填写相关信息";
                    }
                }
            }
        }
        
        if (index == structuresArray.count-1) {
            //描述
            CTAuthenDescItem *descItem = [[CTAuthenDescItem alloc] init];
            descItem.authenDescription = [dataDic objectForKey:@"introduce"];
            [authenGroup.itemCells addObject:descItem];
            
            if (_accountInfo.auditStatus == eCTAuthenticationStateNone) {
                //提交按钮
                CTAuthenButtonItem *btnItem = [[CTAuthenButtonItem alloc] init];
                btnItem.title = @"提交";
                [authenGroup.itemCells addObject:btnItem];
            }
        }
        
        [templateData.structures addObject:authenGroup];
        
        index++;
    }
    
    return templateData;
}

@end
