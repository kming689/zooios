//
//  CTAuthenViewModel.h
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFViewModel.h"
#import "MFMarco.h"

@class RACCommand;
@class RACSignal;
@class CTAuthenTemplate;
@class CTAccountInfomation;

@interface CTAuthenViewModel : MFViewModel

@property (nonatomic, strong) CTAccountInfomation *accountInfo;

@property (nonatomic, strong) NSDictionary *defaultParams;

@property (nonatomic, strong) CTAuthenTemplate *dataSource;//数据源

@property (nonatomic, strong) RACCommand *dataCommand;//获取模板数据

@property (nonatomic, strong) RACCommand *submitCommand;//提交

- (void)uploadFile:(id)fileData imageKey:(NSString *)imageKey success:(void (^)(id responseObject))completion failure:(void (^)(NSError *error))failure;

@end
