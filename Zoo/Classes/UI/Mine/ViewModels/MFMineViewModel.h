//
//  MFMineViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/26.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"

@class RACSubject;
@class RACCommand;
@class RACSignal;

@interface MFMineViewModel : MFViewModel

@property (nonatomic, strong) RACCommand *dataCommand;

@property (nonatomic, strong) RACSignal *dataSignal;

@property (nonatomic, strong) RACSubject *refreshDelegate;//刷新界面的回调

@end
