//
//  CTAuthenUploadFileItem.h
//  CTYun
//
//  Created by tanfameng on 2018/3/10.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenItemBaseModel.h"

@interface CTAuthenUploadFileItem : CTAuthenItemBaseModel

@property (nonatomic, strong) UIImage *image;

@end
