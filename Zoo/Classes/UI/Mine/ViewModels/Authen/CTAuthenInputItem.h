//
//  CTAuthenInputItem.h
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

/*
 输入类型，可以用textField，或者textView
 */

#import "CTAuthenItemBaseModel.h"

@interface CTAuthenInputItem : CTAuthenItemBaseModel

@end
