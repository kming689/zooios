//
//  CTAuthenGroup.h
//  CTYun
//
//  Created by tanfameng on 2018/3/7.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFObject.h"

@class CTAuthenItemBaseModel;

@interface CTAuthenGroup : MFObject

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *submitKey;

@property (nonatomic, strong) NSMutableArray<CTAuthenItemBaseModel *> *itemCells;

@end
