//
//  CTAuthenUploadFileItem.m
//  CTYun
//
//  Created by tanfameng on 2018/3/10.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenUploadFileItem.h"
#import <YYModel/YYModel.h>

@implementation CTAuthenUploadFileItem

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"itemType" : @"itemType",
             @"indicatorTip" : @"hint",
             @"title" : @"title",
             @"canEdit" : @"canEdit",
             @"regex" : @"regex",
             @"submitKey" : @"key"};
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
