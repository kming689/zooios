//
//  CTAuthenSelectItem.h
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenItemBaseModel.h"

@class CTAuthenSelectOption;

@interface CTAuthenSelectItem : CTAuthenItemBaseModel

@property (nonatomic, strong) NSArray<CTAuthenSelectOption *> *optionValues;//对于选择类型的选项

@end
