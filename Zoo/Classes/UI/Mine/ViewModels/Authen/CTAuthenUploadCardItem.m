//
//  CTAuthenUploadCardItem.m
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenUploadCardItem.h"
#import <YYModel/YYModel.h>

@implementation CTAuthenUploadCardItem

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"itemType" : @"itemType",
             @"indicatorTip" : @"hint",
             @"title" : @"title",
             @"canEdit" : @"canEdit",
             @"submitKey" : @"key"};
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (CGFloat)cellHeight
{
    return 200;
}

@end
