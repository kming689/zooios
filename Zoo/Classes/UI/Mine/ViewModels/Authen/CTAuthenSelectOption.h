//
//  CTAuthenSelectOption.h
//  CTYun
//
//  Created by tanfameng on 2018/3/7.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFObject.h"

@interface CTAuthenSelectOption : MFObject

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *value;

@end
