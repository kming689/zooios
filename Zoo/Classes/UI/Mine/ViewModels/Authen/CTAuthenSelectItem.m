//
//  CTAuthenSelectItem.m
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenSelectItem.h"
#import "CTAuthenSelectOption.h"
#import <YYModel/YYModel.h>

@implementation CTAuthenSelectItem

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"itemType" : @"itemType",
             @"indicatorTip" : @"hint",
             @"title" : @"title",
             @"optionValues" : @"options",
             @"canEdit" : @"canEdit",
             @"submitKey" : @"key"};
}

+ (NSDictionary *)modelContainerPropertyGenericClass {
    return @{@"optionValues":[CTAuthenSelectOption class]};
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
