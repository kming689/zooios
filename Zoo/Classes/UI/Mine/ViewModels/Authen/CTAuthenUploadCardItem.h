//
//  CTAuthenUploadCardItem.h
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenItemBaseModel.h"

@interface CTAuthenUploadCardItem : CTAuthenItemBaseModel

@property (nonatomic, strong) UIImage *image;

@end
