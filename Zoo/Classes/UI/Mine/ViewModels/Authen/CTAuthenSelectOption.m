//
//  CTAuthenSelectOption.m
//  CTYun
//
//  Created by tanfameng on 2018/3/7.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenSelectOption.h"
#import <YYModel/YYModel.h>

@implementation CTAuthenSelectOption

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"name" : @"value",
             @"value" : @"key"};
}

@end
