//
//  CTAuthenTemplate.h
//  CTYun
//
//  Created by tanfameng on 2018/3/7.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFObject.h"

@class CTAuthenGroup;

@interface CTAuthenTemplate : MFObject

@property (nonatomic, strong) NSString *templateType;

@property (nonatomic, strong) NSString *introduction;

@property (nonatomic, strong) NSMutableArray<CTAuthenGroup *> *structures;

@end
