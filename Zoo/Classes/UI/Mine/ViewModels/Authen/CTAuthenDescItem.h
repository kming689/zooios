//
//  CTAuthenDescItem.h
//  CTYun
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenItemBaseModel.h"

@interface CTAuthenDescItem : CTAuthenItemBaseModel

@property (nonatomic, strong) NSString *authenDescription;//简介

@end
