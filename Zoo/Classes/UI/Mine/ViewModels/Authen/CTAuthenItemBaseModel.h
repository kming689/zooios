//
//  CTAuthenItemBaseModel.h
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFObject.h"

@interface CTAuthenItemBaseModel : MFObject

@property (nonatomic, strong) NSString *icon;

@property (nonatomic, strong) NSString *itemType;//item类型，决定了该item怎么显示

@property (nonatomic, strong) NSString *actionType;//动作类型，决定了点击该item的下一步动作

@property (nonatomic, strong) NSString *title;//标题

@property (nonatomic, strong) NSString *submitKey;//提交数据的时候，对应的key

@property (nonatomic, strong) NSString *indicatorTip;//右边指示字符

@property (nonatomic, assign) BOOL allowNull;//是否允许为空

@property (nonatomic, assign) BOOL canEdit;//是否可编辑

@property (nonatomic, strong) NSString *submitValue;

@property (nonatomic, strong) NSString *regex;//正则表达式，检测输入字符的格式

- (CGFloat)cellHeight;

- (NSString *)regex;

@end
