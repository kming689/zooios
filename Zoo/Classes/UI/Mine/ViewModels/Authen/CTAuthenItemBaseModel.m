//
//  CTAuthenItemBaseModel.m
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenItemBaseModel.h"
#import <YYModel/YYModel.h>
#import <Base64/MF_Base64Additions.h>

@implementation CTAuthenItemBaseModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _allowNull = NO;
        _canEdit = YES;
    }
    return self;
}

- (NSString *)regex
{
    if ([NSString stringFromBase64String:_regex]) {
        NSString *string = [NSString stringFromBase64String:_regex];
        return string;
    }
    return _regex;
}

- (CGFloat)cellHeight
{
    return 44;
}

@end
