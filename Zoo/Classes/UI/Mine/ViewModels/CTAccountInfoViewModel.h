//
//  CTAccountInfoViewModel.h
//  CTYun
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFViewModel.h"
#import "MFMarco.h"

@class RACCommand;
@class CTAccountInfomation;

@interface CTAccountInfoViewModel : MFViewModel

@property (nonatomic, strong) CTAccountInfomation *accountInfo;

@property (nonatomic, strong) NSArray *dataSource;//数据源

@property (nonatomic, strong) RACCommand *dataCommand;

- (void)updateAccountInfo:(NSDictionary *)params success:(void (^)(BOOL success))completion failure:(void (^)(NSError *error))failure;

- (NSDictionary *)getAuthenDefaultParams;

@end
