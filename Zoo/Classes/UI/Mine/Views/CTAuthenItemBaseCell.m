//
//  CTAuthenItemBaseCell.m
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenItemBaseCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "UIImage+MFAdd.h"
#import "CTAuthenItemBaseModel.h"

@interface CTAuthenItemBaseCell()

@property (nonatomic, strong) MFImageView *indicatorView;

@end

@implementation CTAuthenItemBaseCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.textColor = [MFColor mf_colorWithHexString:@"666666"];
    _titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    [self.contentView addSubview:_titleLabel];
    
    _indicatorLabel = [[MFLabel alloc] init];
    _indicatorLabel.textColor = [MFColor mf_colorWithHexString:@"999999"];
    _indicatorLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    _indicatorLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_indicatorLabel];
    
    _indicatorView = [[MFImageView alloc] init];
    _indicatorView.image = [UIImage imageNamed:@"mine_cell_left.png" bundleClass:[self class]];
    [self.contentView addSubview:_indicatorView];
    
    @weakify(self);
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(15);
        make.centerY.equalTo(self.contentView);
    }];
    [_indicatorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.contentView).offset(-35);
        make.centerY.equalTo(self.contentView);
    }];
    [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.contentView).offset(-13);
        make.centerY.equalTo(self.contentView);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    self.accessoryType = UITableViewCellAccessoryNone;
    _titleLabel.textColor = [MFColor mf_colorWithHexString:@"666666"];
    _indicatorLabel.textColor = [MFColor mf_colorWithHexString:@"999999"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithItem:(CTAuthenItemBaseModel *)item
{
    self.titleLabel.text = item.title;
    self.indicatorLabel.text = item.indicatorTip;
    
    @weakify(self);
    if (item.canEdit) {
        [_indicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.right.equalTo(self.contentView).offset(-13);
            make.centerY.equalTo(self.contentView);
        }];
        [_indicatorLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.right.equalTo(self.contentView).offset(-30);
            make.centerY.equalTo(self.contentView);
        }];
    }else{
        [_indicatorView mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.width.mas_equalTo(0);
            make.centerY.equalTo(self.contentView);
        }];
        [_indicatorLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
            @strongify(self);
            make.right.equalTo(self.contentView).offset(-15);
            make.centerY.equalTo(self.contentView);
        }];
    }
}

@end
