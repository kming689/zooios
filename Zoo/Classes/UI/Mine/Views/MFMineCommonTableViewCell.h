//
//  MFMineCommonTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTableViewCell.h"
#import "MFKitBases.h"

@interface MFMineCommonTableViewCell : MFTableViewCell

@property (nonatomic, strong) MFLabel *titleLabel;

@property (nonatomic, strong) MFLabel *tipLabel;

@property (nonatomic, strong) MFLabel *contentLabel;

@end
