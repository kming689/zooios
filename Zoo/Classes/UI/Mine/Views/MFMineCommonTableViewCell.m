//
//  MFMineCommonTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFMineCommonTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation MFMineCommonTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _titleLabel.font = [UIFont fontWithName:@"HiraginoSans-W3" size:14];
    [self.contentView addSubview:_titleLabel];
    
    _tipLabel = [[MFLabel alloc] init];
    _tipLabel.textColor = [MFColor mf_colorWithHexString:@"44c0ff"];
    _tipLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _tipLabel.font = [UIFont fontWithName:@"HiraginoSans-W3" size:10];
    [self.contentView addSubview:_tipLabel];
    
    _contentLabel = [[MFLabel alloc] init];
    _contentLabel.textColor = [MFColor mf_colorWithHexString:@"4A4A4A"];
    _contentLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _contentLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:14];
    [self.contentView addSubview:_contentLabel];
    
    @weakify(self);
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.textLabel.mas_left);
        make.centerY.equalTo(self.contentView);
    }];
    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.mas_equalTo(90);
        make.centerY.equalTo(self.contentView);
    }];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.contentView).offset(-40);
        make.centerY.equalTo(self.contentView);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
