//
//  CTAuthenBtnTableViewCell.h
//  CTYun
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFKitBases.h"

@interface CTAuthenBtnTableViewCell : MFTableViewCell

@property (nonatomic, strong, readonly) MFButton *button;

@end
