//
//  CTCardItemTableViewCell.m
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTCardItemTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "UIImage+MFAdd.h"
#import "CTAuthenUploadCardItem.h"

@interface CTCardItemTableViewCell()

@end

@implementation CTCardItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.textColor = [MFColor mf_colorWithHexString:@"666666"];
    _titleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    _titleLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:_titleLabel];
    
    _indicatorLabel = [[MFLabel alloc] init];
    _indicatorLabel.textColor = [MFColor mf_colorWithHexString:@"999999"];
    _indicatorLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    _indicatorLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_indicatorLabel];
    
    _cardImageView = [[MFImageView alloc] init];
    _cardImageView.image = [UIImage imageNamed:@"authen_card.png" bundleClass:[self class]];
    _cardImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_cardImageView];
    
    _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _activityView.hidesWhenStopped = YES;
    [self.contentView addSubview:_activityView];
    
    @weakify(self);
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.contentView).offset(10);
    }];
    [_indicatorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.bottom.equalTo(self.contentView).offset(-22);
        make.centerX.equalTo(self.contentView);
    }];
    [_cardImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
    [_activityView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.contentView);
        make.centerY.equalTo(self.contentView);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithItem:(CTAuthenItemBaseModel *)item;
{
    _titleLabel.text = item.title;
    _indicatorLabel.text = item.indicatorTip;
}

@end
