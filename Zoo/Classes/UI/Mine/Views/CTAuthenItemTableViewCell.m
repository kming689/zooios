//
//  CTAuthenItemTableViewCell.m
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenItemTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

#import "CTAuthenItemBaseModel.h"

@interface CTAuthenItemTableViewCell()

@end

@implementation CTAuthenItemTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithItem:(CTAuthenItemBaseModel *)item
{
    [super configCellWithItem:item];
    
}

@end
