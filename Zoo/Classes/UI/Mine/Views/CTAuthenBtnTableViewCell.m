//
//  CTAuthenBtnTableViewCell.m
//  CTYun
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenBtnTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface CTAuthenBtnTableViewCell()

@property (nonatomic, strong, readwrite) MFButton *button;

@end

@implementation CTAuthenBtnTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    MFButton *submitButton = [MFButton buttonWithType:UIButtonTypeCustom];
    self.button = submitButton;
    UIImage *logoutButtonImage = [UIImage imageNamed:@"setting_outLogin"];
    [submitButton setBackgroundImage:[logoutButtonImage stretchableImageWithLeftCapWidth:logoutButtonImage.size.width / 2 topCapHeight:logoutButtonImage.size.height /2] forState:UIControlStateNormal];
    //[submitButton setTitle:@"提交" forState:UIControlStateNormal];
    submitButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.contentView addSubview:submitButton];
    @weakify(self);
    [submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(30);
        make.bottom.equalTo(self.contentView).offset(-50);
        make.height.mas_equalTo(45).priorityHigh();
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
