//
//  CTAuthenDescTableViewCell.h
//  CTYun
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFKitBases.h"

@interface CTAuthenDescTableViewCell : MFTableViewCell

@property (nonatomic, strong, readonly) MFLabel *descriptionLabel;

@end
