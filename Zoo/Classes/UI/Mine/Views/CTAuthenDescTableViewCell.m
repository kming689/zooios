//
//  CTAuthenDescTableViewCell.m
//  CTYun
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenDescTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface CTAuthenDescTableViewCell()

@property (nonatomic, strong, readwrite) MFLabel *descriptionLabel;

@end

@implementation CTAuthenDescTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _descriptionLabel = [[MFLabel alloc] init];
    _descriptionLabel.numberOfLines = 0;
    _descriptionLabel.textColor = [MFColor mf_colorWithHexString:@"999999"];
    _descriptionLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    _descriptionLabel.textAlignment = NSTextAlignmentLeft;
    [self.contentView addSubview:_descriptionLabel];
    
    @weakify(self);
    [_descriptionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-15);
        make.top.equalTo(self.contentView).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
