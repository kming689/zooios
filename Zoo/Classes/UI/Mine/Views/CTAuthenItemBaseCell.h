//
//  CTAuthenItemBaseCell.h
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFKitBases.h"

@class CTAuthenItemBaseModel;

@interface CTAuthenItemBaseCell : MFTableViewCell

@property (nonatomic, strong) MFLabel *titleLabel;

@property (nonatomic, strong) MFLabel *indicatorLabel;

- (void)configCellWithItem:(CTAuthenItemBaseModel *)item;

@end
