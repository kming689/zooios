//
//  CTAccountInfoViewController.m
//  CTYun
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAccountInfoViewController.h"
#import "CTAccountInfoViewModel.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFActionSheet.h"
#import "MFAlertView.h"
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import <SVProgressHUD/SVProgressHUD.h>

#import "CTAuthenItemBaseModel.h"
#import "CTAuthenInputItem.h"
#import "CTAuthenSelectItem.h"
#import "CTAuthenSelectOption.h"

#import "CTAuthenItemBaseCell.h"
#import "CTAuthenItemTableViewCell.h"

#import "CTAutheningViewController.h"
#import "CTInputTextViewController.h"

#import "CTAccountInfomation.h"

#define NAV_HEIGHT [[UIApplication sharedApplication] statusBarFrame].size.height + self.navigationController.navigationBar.frame.size.height

static const NSString *kCTAccountItemTableViewCellIdentifer = @"kCTAccountItemTableViewCellIdentifer";

@interface CTAccountInfoViewController ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, strong) CTAccountInfoViewModel *viewModel;

@property (nonatomic, strong) MFTableView *tableView;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation CTAccountInfoViewController
{

}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.title = @"账户信息";
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self registerNotification];
    [self.indicatorView startAnimating];
    self.tableView.hidden = YES;
    RACSignal *signal = [self.viewModel.dataCommand execute:nil];
    @weakify(self);
    [signal subscribeNext:^(id x) {
        self.viewModel.dataSource = x;
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
    }];
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
    }];
}

#pragma mark -- 注册通知
- (void)registerNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAccountInfoChanged:) name:@"kCTAccountInfoChanged" object:nil];
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];

}

-(void)configView
{
    [super configView];
    
    [self configTableView];
    
    @weakify(self);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.and.top.mas_equalTo(0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_indicatorView];
    [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(NAV_HEIGHT + 30);
    }];
    
    self.navigationController.navigationBar.translucent = NO;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.automaticallyAdjustsScrollViewInsets = YES;
}

-(void)bindViewModel
{
    [super bindViewModel];
}

#pragma mark -- 懒加载属性
- (CTAccountInfoViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[CTAccountInfoViewModel alloc] init];
    }
    return _viewModel;
}

#pragma mark -- UI设置
- (void)configTableView
{
    _tableView = [[MFTableView alloc] init];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    //根据认证状态确定是否需要提交按钮,按钮放在footerView上面
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    footerView.backgroundColor = [MFColor clearColor];
    _tableView.tableFooterView = footerView;
    
    _tableView.backgroundColor = [MFColor clearColor];
    _tableView.separatorColor = [MFColor mf_lineColor];
    _tableView.sectionIndexColor = [MFColor mf_fontGrayStyleColor];
    _tableView.sectionIndexBackgroundColor =[UIColor clearColor];
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CTAuthenItemTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kCTAccountItemTableViewCellIdentifer copy]];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.emptyDataSetSource = self;
    _tableView.emptyDataSetDelegate = self;
    
    //调整分割线边距
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        self.tableView.separatorInset = UIEdgeInsetsZero;
    }
    //2.调整(iOS8以上)view边距(或者在cell中设置preservesSuperviewLayoutMargins,二者等效)
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
    [self.view addSubview:self.tableView];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSArray *dataArray = self.viewModel.dataSource;
    return dataArray.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSDictionary *dict = self.viewModel.dataSource[section];
    NSString *title = dict[@"title"];
    
    MFView *headerView = [[MFView alloc] init];
    headerView.translatesAutoresizingMaskIntoConstraints = YES;
    headerView.backgroundColor = [MFColor mf_colorWithHexString:@"F3F3F3"];
    headerView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 22);
    MFLabel *tipLabel = [[MFLabel alloc] initWithFrame:CGRectMake(15, 6, tableView.bounds.size.width - 20, 30)];
    tipLabel.textColor = [MFColor mf_colorWithHexString:@"999999"];
    tipLabel.font = [UIFont systemFontOfSize:13.0];
    tipLabel.text = title;
    [headerView addSubview:tipLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 36;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSDictionary *dict = self.viewModel.dataSource[section];
    NSArray *array = [dict objectForKey:@"data"];
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = self.viewModel.dataSource[indexPath.section];
    NSArray *array = [dict objectForKey:@"data"];
    CTAuthenItemBaseModel *item = [array objectAtIndex:indexPath.row];
    CTAuthenItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kCTAccountItemTableViewCellIdentifer copy] forIndexPath:indexPath];
    //调整分割线边距
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
    
    [cell configCellWithItem:item];
    if ([item.indicatorTip containsString:@"未认证"]) {
        cell.indicatorLabel.textColor = [MFColor mf_colorWithHexString:@"FF693B"];
    }else if ([item.indicatorTip containsString:@"认证中"]) {
        cell.indicatorLabel.textColor = [MFColor mf_colorWithHexString:@"208EF5"];
    }
    
    return cell;
}

#pragma mark -- UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = self.viewModel.dataSource[indexPath.section];
    NSArray *array = [dict objectForKey:@"data"];
    CTAuthenItemBaseModel *item = [array objectAtIndex:indexPath.row];
    return item.cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CTAuthenItemBaseCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    NSDictionary *dict = self.viewModel.dataSource[indexPath.section];
    NSArray *array = [dict objectForKey:@"data"];
    CTAuthenItemBaseModel *item = [array objectAtIndex:indexPath.row];
    
    //判断是否可编辑
    if (!item.canEdit) {
        return;
    }
    
    //输入类型
    if ([item isKindOfClass:[CTAuthenInputItem class]]) {
        CTAuthenInputItem *inpuItem = (CTAuthenInputItem *)item;
        //是否是认证行，如果是，跳转到下一个页面，其余的，跳转到输入数据页面
        if ([inpuItem.title containsString:@"认证"]) {
            if (self.viewModel.accountInfo.auditStatus == eCTAuthenticationStateBeing) {
                CTAutheningViewController *autheningVC = [[CTAutheningViewController alloc] init];
                [self.navigationController pushViewController:autheningVC animated:YES];
                return;
            }
            //进入认证界面之前，先生成初始化参数，包含了部分已经有的信息，进入下一级页面后，默认填充这些数据
            CTAuthenViewController *controller = [[CTAuthenViewController alloc] initWithAuthenState:self.viewModel.accountInfo.auditStatus dataSource:self.viewModel.accountInfo defalutParams:[self.viewModel getAuthenDefaultParams]];
            [self.navigationController pushViewController:controller animated:YES];
        }else{            
            CTInputTextViewController *inputVC = [[CTInputTextViewController alloc] initWithValue:inpuItem.submitValue title:inpuItem.title limitLength:0 regexString:item.regex submitChangeBlock:^(NSString *newValue,UIViewController *controller) {
    
                [controller.navigationController popViewControllerAnimated:YES];
                
                NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
                [params setObject:newValue forKey:item.submitKey];
                
                [self.viewModel updateAccountInfo:params success:^(BOOL success) {
                    inpuItem.submitValue = newValue;
                    inpuItem.indicatorTip = newValue;
                    cell.indicatorLabel.text = newValue;
                } failure:^(NSError *error) {
                    
                }];
            }];
            [self.navigationController pushViewController:inputVC animated:YES];
        }
    }else if ([item isKindOfClass:[CTAuthenSelectItem class]]) {
        CTAuthenSelectItem *selectItem = (CTAuthenSelectItem *)item;
        MFPopupCompletionBlock completeBlock = ^(MFPopupView *popupView, BOOL finished){
            
        };
        //@weakify(self);
        MFPopupItemHandler block = ^(NSInteger index){
            //@strongify(self);
            CTAuthenSelectOption *option = [selectItem.optionValues objectAtIndex:index];
            
            NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
            [params setObject:option.value forKey:item.submitKey];
            
            [self.viewModel updateAccountInfo:params success:^(BOOL success) {
                selectItem.submitValue = option.value;
                selectItem.indicatorTip = option.name;
                cell.indicatorLabel.text = option.name;
            } failure:^(NSError *error) {
                
            }];
        };
        NSMutableArray *items = [[NSMutableArray alloc] init];
        for (CTAuthenSelectOption *option in selectItem.optionValues) {
            MFPopupItem *menuItem = MFItemMake(option.name, MFItemTypeNormal, block);
            [items addObject:menuItem];
        }
        MFActionSheet *sheetView = [[MFActionSheet alloc] initWithTitle:nil
                                                                  items:items];
        sheetView.attachedView.mf_dimBackgroundBlurEnabled = NO;
        [sheetView showWithBlock:completeBlock];
    }
}

#pragma mark -- actions

- (void)onAccountInfoChanged:(id)notifacation
{
    RACSignal *signal = [self.viewModel.dataCommand execute:nil];
    @weakify(self);
    [signal subscribeNext:^(id x) {
        self.viewModel.dataSource = x;
        [self.tableView reloadData];
    }];
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.tableView reloadData];
    }];
}

#pragma mark - DZNEmptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"数据请求失败";
    UIFont *font = [UIFont boldSystemFontOfSize:14.0];
    UIColor *textColor = [MFColor mf_colorWithHexString:@"#494c53"];
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    
    if (!text) {
        return nil;
    }
    
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"请检查您的网络设置";
    UIFont *font = [UIFont systemFontOfSize:13.0];
    UIColor *textColor = [MFColor mf_colorWithHexString:@"#7a7d83"];
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    if (!text) {
        return nil;
    }
    
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    if (paragraph) [attributes setObject:paragraph forKey:NSParagraphStyleAttributeName];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
    return attributedString;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    UIImage *image = [UIImage imageNamed:@"" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    return image;
}

- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D: CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0) ];
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    return animation;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    NSString *text = @"点击重试";
    UIFont *font = [UIFont boldSystemFontOfSize:13.0];
    UIColor *textColor = [MFColor mf_colorWithHexString:(state == UIControlStateNormal) ? @"#05adff" : @"#6bceff"];
    
    if (!text) {
        return nil;
    }
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - DZNEmptyDataSetDelegate Methods

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button
{
    self.tableView.hidden = YES;
    [self.indicatorView startAnimating];
    RACSignal *signal = [self.viewModel.dataCommand execute:nil];
    @weakify(self);
    [signal subscribeNext:^(id x) {
        self.viewModel.dataSource = x;
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
    }];
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
    }];
}

@end
