//
//  CTAutheningViewController.m
//  CTYun
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAutheningViewController.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "UIImage+MFAdd.h"

#define NAV_HEIGHT [[UIApplication sharedApplication] statusBarFrame].size.height + self.navigationController.navigationBar.frame.size.height

@interface CTAutheningViewController ()

@property (nonatomic, strong) MFView *leftDashView;

@property (nonatomic, strong) MFView *rightDashView;

@end

@implementation CTAutheningViewController

-(NSString *)nibName
{
    return NSStringFromClass([self class]);
}

-(NSBundle *)nibBundle
{
    return [NSBundle bundleForClass:[self class]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
//        NSArray *familyNames = [UIFont familyNames];
//        for( NSString *familyName in familyNames ){
//            NSLog(@"Family: %@",familyName);
//            NSArray *fontNames = [UIFont fontNamesForFamilyName:familyName];
//            for( NSString *fontName in fontNames ){
//                NSLog(@"Font: %@",fontName);
//            }
//        }
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    [[self class] drawDashLine:self.leftDashView lineLength:2 lineSpacing:2 lineColor:[MFColor mf_colorWithHexString:@"50B4F4"]];
    [[self class] drawDashLine:self.rightDashView lineLength:2 lineSpacing:2 lineColor:[MFColor mf_colorWithHexString:@"50B4F4"]];
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
    self.title = @"认证审核中";
}

-(void)configView
{
    [super configView];

    //认证开始
    MFImageView *startImageView = [[MFImageView alloc] init];
    startImageView.contentMode = UIViewContentModeScaleAspectFit;
    startImageView.image = [UIImage imageNamed:@"start_authen.png" bundleClass:[self class]];
    [self.view addSubview:startImageView];
    
    MFImageView *middleImageView = [[MFImageView alloc] init];
    middleImageView.contentMode = UIViewContentModeScaleAspectFit;
    middleImageView.image = [UIImage imageNamed:@"middle_authen.png" bundleClass:[self class]];
    [self.view addSubview:middleImageView];
    
    MFImageView *endImageView = [[MFImageView alloc] init];
    endImageView.contentMode = UIViewContentModeScaleAspectFit;
    endImageView.image = [UIImage imageNamed:@"end_authen.png" bundleClass:[self class]];
    [self.view addSubview:endImageView];
    
    MFView *leftDashView = [[MFView alloc] init];
    self.leftDashView = leftDashView;
    leftDashView.backgroundColor = [MFColor clearColor];
    [self.view addSubview:leftDashView];
    
    MFView *rightDashView = [[MFView alloc] init];
    self.rightDashView = rightDashView;
    rightDashView.backgroundColor = [MFColor clearColor];
    [self.view addSubview:rightDashView];
    
    MFLabel *startLabel = [[MFLabel alloc] init];
    startLabel.textAlignment = NSTextAlignmentCenter;
    startLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    startLabel.textColor = [MFColor mf_colorWithHexString:@"333333"];
    startLabel.text = @"提交认证";
    [self.view addSubview:startLabel];
    
    MFLabel *middleLabel = [[MFLabel alloc] init];
    middleLabel.textAlignment = NSTextAlignmentCenter;
    middleLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:15];
    middleLabel.textColor = [MFColor mf_colorWithHexString:@"333333"];
    middleLabel.text = @"认证中";
    [self.view addSubview:middleLabel];
    
    MFLabel *endLabel = [[MFLabel alloc] init];
    endLabel.textAlignment = NSTextAlignmentCenter;
    endLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:14];
    endLabel.textColor = [MFColor mf_colorWithHexString:@"999999"];
    endLabel.text = @"认证完成";
    [self.view addSubview:endLabel];
    
    MFLabel *tipLabel = [[MFLabel alloc] init];
    tipLabel.numberOfLines = 2;
    tipLabel.textAlignment = NSTextAlignmentLeft;
    tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:13];
    tipLabel.textColor = [MFColor mf_colorWithHexString:@"999999"];
    tipLabel.text = @"温馨提示\n您的认证申请正在审核中,请耐心等待,谢谢";
    [self.view addSubview:tipLabel];
    
    @weakify(self);
    [middleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(36);
    }];
    [startImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.view).offset(30);
        make.centerY.equalTo(middleImageView);
    }];
    [endImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.view).offset(-30);
        make.centerY.equalTo(middleImageView);
    }];
    [leftDashView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(startImageView.mas_right);
        make.right.equalTo(middleImageView.mas_left);
        make.centerY.equalTo(middleImageView);
        make.height.mas_equalTo(1);
    }];
    [rightDashView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(middleImageView.mas_right);
        make.right.equalTo(endImageView.mas_left);
        make.centerY.equalTo(middleImageView);
        make.height.mas_equalTo(1);
    }];
    [startLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(startImageView);
        make.top.equalTo(startImageView.mas_bottom).offset(20);
    }];
    [middleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(middleImageView);
        make.top.equalTo(middleImageView.mas_bottom).offset(20);
    }];
    [endLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(endImageView);
        make.centerY.equalTo(middleLabel);
    }];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(startLabel);
        make.top.equalTo(startLabel.mas_bottom).offset(55);
    }];
}

-(void)bindViewModel
{
    [super bindViewModel];
}

/**
 ** lineView:       需要绘制成虚线的view
 ** lineLength:     虚线的宽度
 ** lineSpacing:    虚线的间距
 ** lineColor:      虚线的颜色
 **/
+ (void)drawDashLine:(UIView *)lineView lineLength:(int)lineLength lineSpacing:(int)lineSpacing lineColor:(UIColor *)lineColor
{
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    [shapeLayer setBounds:lineView.bounds];
    [shapeLayer setPosition:CGPointMake(CGRectGetWidth(lineView.bounds) / 2, CGRectGetHeight(lineView.bounds))];
    [shapeLayer setFillColor:[UIColor clearColor].CGColor];
    //  设置虚线颜色为blackColor
    [shapeLayer setStrokeColor:lineColor.CGColor];
    //  设置虚线宽度
    [shapeLayer setLineWidth:CGRectGetHeight(lineView.frame)];
    [shapeLayer setLineJoin:kCALineJoinRound];
    //  设置线宽，线间距
    [shapeLayer setLineDashPattern:[NSArray arrayWithObjects:[NSNumber numberWithInt:lineLength], [NSNumber numberWithInt:lineSpacing], nil]];
    //  设置路径
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 0, 0);
    CGPathAddLineToPoint(path, NULL,CGRectGetWidth(lineView.bounds), 0);
    [shapeLayer setPath:path];
    CGPathRelease(path);
    //  把绘制好的虚线添加上来
    [lineView.layer addSublayer:shapeLayer];
}

@end
