//
//  CTSubmitedAuthenViewController.m
//  CTYun
//
//  Created by tanfameng on 2018/3/7.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTSubmitedAuthenViewController.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <ReactiveCocoa/UIControl+RACSignalSupport.h>
#import "UIImage+MFAdd.h"

#define NAV_HEIGHT [[UIApplication sharedApplication] statusBarFrame].size.height + self.navigationController.navigationBar.frame.size.height

@interface CTSubmitedAuthenViewController ()

@end

@implementation CTSubmitedAuthenViewController

-(NSString *)nibName
{
    return NSStringFromClass([self class]);
}

-(NSBundle *)nibBundle
{
    return [NSBundle bundleForClass:[self class]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
    self.title = @"实名认证";
}

-(void)configView
{
    [super configView];
    
    //提交完成图片
    MFImageView *imageView = [[MFImageView alloc] init];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = [UIImage imageNamed:@"submited" bundleClass:[self class]];
    [self.view addSubview:imageView];
    
    MFLabel *tipLabel = [[MFLabel alloc] init];
    tipLabel.numberOfLines = 2;
    tipLabel.textAlignment = NSTextAlignmentCenter;
    tipLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:17];
    tipLabel.textColor = [MFColor mf_colorWithHexString:@"666666"];
    tipLabel.text = @"您已提交实名认证,我们将尽快审核";
    [self.view addSubview:tipLabel];
    
    MFButton *submitButton = [MFButton buttonWithType:UIButtonTypeCustom];
    UIImage *logoutButtonImage = [UIImage imageNamed:@"i_know"];
    [submitButton setBackgroundImage:[logoutButtonImage stretchableImageWithLeftCapWidth:logoutButtonImage.size.width / 2 topCapHeight:logoutButtonImage.size.height /2] forState:UIControlStateNormal];
    [submitButton setTitle:@"我知道了" forState:UIControlStateNormal];
    submitButton.titleLabel.font = [UIFont systemFontOfSize:16];
    [self.view addSubview:submitButton];
    @weakify(self);
    [[submitButton
      rac_signalForControlEvents:UIControlEventTouchUpInside]
     subscribeNext:^(id x) {
         @strongify(self)
         NSArray *controllers = [self.navigationController viewControllers];
         NSInteger index = [controllers indexOfObject:self];
         UIViewController *vc = [controllers objectAtIndex:index-2];
         [self.navigationController popToViewController:vc animated:YES];
     }];
    
    [imageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(80);
    }];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(imageView.mas_bottom).offset(20);
    }];
    [submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view);
    }];
}

-(void)bindViewModel
{
    [super bindViewModel];
}

@end
