//
//  MFMineViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFMineViewController.h"
#import "MFMineViewModel.h"
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <BlocksKit/UIView+BlocksKit.h>
#import <BlocksKit/UIControl+BlocksKit.h>
#import "MFKitBases.h"
#import "MFKitMarcro.h"
#import "UIImage+MFAdd.h"
#import "WRNavigationBar.h"
#import "MFMineCommonTableViewCell.h"

#import "MFLoginViewController.h"
#import "MFTransitionNavigationController.h"

#import "MFAccountManager.h"
#import "MFAccount.h"
#import "MFAccountInfo.h"

#define NAVBAR_COLORCHANGE_POINT (-IMAGE_HEIGHT + NAV_HEIGHT*2)
#define NAV_HEIGHT [[UIApplication sharedApplication] statusBarFrame].size.height + self.navigationController.navigationBar.frame.size.height
#define IMAGE_HEIGHT 120
#define SCROLL_DOWN_LIMIT 180
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define LIMIT_OFFSET_Y -(IMAGE_HEIGHT + SCROLL_DOWN_LIMIT)

static const NSString *kMFMineCommonTableViewCellIdentifer = @"kMFMineCommonTableViewCellIdentifer";

@interface MFMineViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) MFImageView *headView;
@property (nonatomic, strong) MFImageView *avatarView;
@property (nonatomic, strong) MFLabel *nameLabel;
@property (nonatomic, strong) MFLabel *numberLabel;
@property (nonatomic, strong) MFButton *loginButton;
@property (nonatomic, strong) MFTableView *tableView;
@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;
@property (nonatomic, strong) MFMineViewModel *viewModel;

@end

@implementation MFMineViewController
{
    NSMutableArray *_titleGroups;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self initData];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
//    [self.indicatorView startAnimating];
//    self.tableView.hidden = YES;
    RACSignal *signal = [self.viewModel.dataCommand execute:[NSNumber numberWithBool:NO]];
    @weakify(self);
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
    
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [UIColor wr_setDefaultStatusBarStyle:UIStatusBarStyleLightContent];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [UIColor wr_setDefaultStatusBarStyle:UIStatusBarStyleDefault];
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
}

#pragma mark - override methods
- (void)configNavigationBar
{
    [super configNavigationBar];
    [self wr_setNavBarBackgroundAlpha:0];
    [self wr_setNavBarShadowImageHidden:YES];
}

- (void)configView
{
    [super configView];
    
    self.tableView.contentInset = UIEdgeInsetsMake(IMAGE_HEIGHT - NAV_HEIGHT, 0, 0, 0);
    [self.view addSubview:self.tableView];
    [self.tableView addSubview:self.headView];
    MFImageView *avatarBGView = [[MFImageView alloc] init];
    avatarBGView.userInteractionEnabled = YES;
    avatarBGView.contentMode = UIViewContentModeScaleAspectFit;
    avatarBGView.image = [UIImage imageNamed:@"account_avatar_bg"];
    _avatarView = [[MFImageView alloc] init];
    _avatarView.userInteractionEnabled = YES;
    _avatarView.contentMode = UIViewContentModeScaleAspectFit;
    _nameLabel = [[MFLabel alloc] init];
    _nameLabel.font = [UIFont systemFontOfSize:13.0];
    _nameLabel.textColor = [MFColor whiteColor];
    _nameLabel.textAlignment = NSTextAlignmentLeft;
    _nameLabel.text = @"";
    _nameLabel.hidden = YES;
    _numberLabel = [[MFLabel alloc] init];
    _numberLabel.font = [UIFont systemFontOfSize:10.0];
    _numberLabel.textColor = [MFColor whiteColor];
    _numberLabel.textAlignment = NSTextAlignmentLeft;
    _numberLabel.text = @"";
    _nameLabel.hidden = YES;
    _loginButton = [MFButton buttonWithType:UIButtonTypeCustom];
    [_loginButton setTitle:@"登录/注册" forState:UIControlStateNormal];
    _loginButton.titleLabel.font = [UIFont systemFontOfSize:14];
    [_loginButton setTitleColor:[MFColor whiteColor] forState:UIControlStateNormal];
    [self.headView addSubview:avatarBGView];
    [self.headView addSubview:_loginButton];
    [avatarBGView addSubview:_avatarView];
    [self.headView addSubview:_nameLabel];
    [self.headView addSubview:_numberLabel];
    @weakify(self);
    [_avatarView bk_whenTapped:^{
        @strongify(self);
        MFAccountManager *accountMgr = [MFAccountManager shareInstance];
        if (accountMgr.isLogin) {
            
        }else{
            [self showLoginViewController];
        }
    }];
    [_loginButton bk_addEventHandler:^(id sender) {
        @strongify(self);
        [self showLoginViewController];
    } forControlEvents:UIControlEventTouchUpInside];
    
    MFLabel *renzhengLabel = [[MFLabel alloc] init];
    renzhengLabel.userInteractionEnabled = YES;
    renzhengLabel.font = [UIFont fontWithName:@"PingFangHK-Regular" size:8];
    renzhengLabel.textColor = [MFColor whiteColor];
    renzhengLabel.text = @"完成实名认证>";
    [self.headView addSubview:renzhengLabel];
    [renzhengLabel bk_whenTapped:^{
        @strongify(self);
        [self onRealNameAuthentication];
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.and.top.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(0);
    }];
    [avatarBGView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.headView).offset(21);
        make.bottom.equalTo(self.headView).offset(-30);
        make.size.mas_equalTo(CGSizeMake(90, 90));
    }];
    [_avatarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.and.centerY.equalTo(avatarBGView);
        make.size.mas_equalTo(CGSizeMake(70, 70));
    }];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        //@strongify(self);
        make.left.equalTo(avatarBGView.mas_right).offset(20);
        make.bottom.equalTo(avatarBGView.mas_centerY).offset(0);
        make.size.mas_equalTo(CGSizeMake(200, 20));
    }];
    [_numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.nameLabel);
        make.top.equalTo(self.nameLabel.mas_bottom);
        make.size.mas_equalTo(CGSizeMake(200, 20));
    }];
    [renzhengLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.bottom.equalTo(self.headView).offset(-20);
        make.right.equalTo(self.headView).offset(-32);
    }];
    [_loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerY.equalTo(self.avatarView);
        make.left.equalTo(avatarBGView.mas_right).offset(10);
    }];
    //网络指示器提示
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_indicatorView];
    [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(kNavigationHeight + 30);
    }];
    
    [self refreshAllSubView];
}

- (void)refreshAllSubView
{
    [super refreshAllSubView];
    
    MFAccountManager *mgr = [MFAccountManager shareInstance];
    MFAccount *account = mgr.account;
    
    [_avatarView sd_setImageWithURL:[NSURL URLWithString:account.icon] placeholderImage:[UIImage imageNamed:@"account_avatar"]];
    
    if (mgr.isLogin) {
        [self onAccountLogined];
    }
    
    [self.tableView reloadData];
}

- (void)bindViewModel
{
    [super bindViewModel];
    
    @weakify(self)
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"kMFAccountLogined" object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        @strongify(self);
        [self onAccountLogined];
    }];
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"kMFAccountExit" object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        @strongify(self);
        [self onAccountExit];
    }];
    
    //添加数据刷新回调
    RACSubject *tapSubject = [[RACSubject alloc] init];
    [tapSubject subscribeNext:^(id x) {
        @strongify(self);
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
        [self refreshAllSubView];
    }];
    self.viewModel.refreshDelegate = tapSubject;
}

#pragma mark -- UI设置

#pragma mark -- 数据初始化
- (void)initData
{
    _titleGroups = [[NSMutableArray alloc] init];
    //第一组
    NSMutableArray *firstArray = [[NSMutableArray alloc] init];
    [firstArray addObject:@"资产"];
    [firstArray addObject:@"交易"];
    //[_titleGroups addObject:firstArray];
    //第二组
    NSMutableArray *secondArray = [[NSMutableArray alloc] init];
    [secondArray addObject:@"加客服微信"];
    [secondArray addObject:@"常见问题"];
    [secondArray addObject:@"推荐『ZOO』给朋友"];
    //[_titleGroups addObject:secondArray];
}

#pragma mark -- 懒加载属性
- (MFImageView *)headView
{
    if (!_headView) {
        _headView = [[MFImageView alloc] init];
        _headView.translatesAutoresizingMaskIntoConstraints = YES;
        _headView.userInteractionEnabled = YES;
        _headView.backgroundColor = [MFColor clearColor];
        _headView.frame = CGRectMake(0, -IMAGE_HEIGHT, kScreenWidth, IMAGE_HEIGHT);
        _headView.image = [UIImage imageNamed:@"mine_head"];
        _headView.clipsToBounds = YES;
        _headView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _headView;
}

- (MFTableView *)tableView
{
    if (!_tableView) {
        _tableView = [[MFTableView alloc] init];
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
        footerView.backgroundColor = [MFColor clearColor];
        _tableView.tableFooterView = footerView;
        _tableView.backgroundColor = [MFColor clearColor];
        _tableView.separatorColor = [MFColor mf_lineColor];
        _tableView.sectionIndexColor = [MFColor mf_fontGrayStyleColor];
        _tableView.sectionIndexBackgroundColor =[UIColor clearColor];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFMineCommonTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFMineCommonTableViewCellIdentifer copy]];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //调整分割线边距
        if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
            self.tableView.separatorInset = UIEdgeInsetsZero;
        }
        //2.调整(iOS8以上)view边距(或者在cell中设置preservesSuperviewLayoutMargins,二者等效)
        if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
            self.tableView.layoutMargins = UIEdgeInsetsZero;
        }
    }
    return _tableView;
}

- (MFViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFMineViewModel alloc] init];
    }
    return _viewModel;
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _titleGroups.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return [MFView new];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    return 30;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = [_titleGroups objectAtIndex:section];
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        MFMineCommonTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFMineCommonTableViewCellIdentifer copy] forIndexPath:indexPath];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cell.textLabel.font = [UIFont fontWithName:@"HiraginoSans-W3" size:14.0];
        //调整分割线边距
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        NSArray *titleArray = [_titleGroups objectAtIndex:indexPath.section];
        NSString *title = [titleArray objectAtIndex:indexPath.row];
        cell.textLabel.text = title;
        
        if ([title isEqualToString:@"加客服微信"]) {
            cell.imageView.image = [UIImage imageNamed:@"add_wechat"];
        }else if ([title isEqualToString:@"常见问题"]) {
            cell.imageView.image = [UIImage imageNamed:@"problem"];
        }else if ([title containsString:@"推荐"]) {
            cell.imageView.image = [UIImage imageNamed:@"recommend"];
        }
        
        MFAccountManager *accountMgr = [MFAccountManager shareInstance];
        if ([title containsString:@"资产"]) {
            if (!accountMgr.account) {
                cell.tipLabel.text = @"需要登录";
            }else{
                cell.contentLabel.text = accountMgr.account.accountInfo.fund;
            }
        }
        if ([title containsString:@"交易"]) {
            cell.tipLabel.text = @"绑定交易所";
        }
        return cell;
    }else{
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"cell_%li_%li",(long)indexPath.section,(long)indexPath.row]];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:[NSString stringWithFormat:@"cell_%li_%li",(long)indexPath.section,(long)indexPath.row]];
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
            cell.textLabel.font = [UIFont fontWithName:@"HiraginoSans-W3" size:14.0];
        }
        
        NSArray *titleArray = [_titleGroups objectAtIndex:indexPath.section];
        NSString *title = [titleArray objectAtIndex:indexPath.row];
        cell.textLabel.text = title;
        if ([title isEqualToString:@"加客服微信"]) {
            cell.imageView.image = [UIImage imageNamed:@"add_wechat"];
        }else if ([title isEqualToString:@"常见问题"]) {
            cell.imageView.image = [UIImage imageNamed:@"problem"];
        }else if ([title containsString:@"推荐"]) {
            cell.imageView.image = [UIImage imageNamed:@"recommend"];
        }
        
        return cell;
    }
}

#pragma mark -- UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSArray *groupArray = [_titleGroups objectAtIndex:indexPath.section];
    NSString *title = [groupArray objectAtIndex:indexPath.row];
    if ([title isEqualToString:@"退出登录"]) {
        [self onAccountExit];
    }
}

#pragma mark -- private
- (UIImage *)imageWithImageSimple:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(CGSizeMake(newSize.width*2, newSize.height*2));
    [image drawInRect:CGRectMake (0, 0, newSize.width*2, newSize.height*2)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

#pragma mark -- scrollView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetY = scrollView.contentOffset.y;
    
    if (offsetY > NAVBAR_COLORCHANGE_POINT)
    {
        CGFloat alpha = (offsetY - NAVBAR_COLORCHANGE_POINT) / NAV_HEIGHT;
        [self wr_setNavBarBackgroundAlpha:alpha];
    }
    else
    {
        [self wr_setNavBarBackgroundAlpha:0];
    }
    
    //限制下拉的距离
    if(offsetY < LIMIT_OFFSET_Y) {
        [scrollView setContentOffset:CGPointMake(0, LIMIT_OFFSET_Y)];
    }
    
    // 改变图片框的大小 (上滑的时候不改变)
    // 这里不能使用offsetY，因为当（offsetY < LIMIT_OFFSET_Y）的时候，y = LIMIT_OFFSET_Y 不等于 offsetY
    CGFloat newOffsetY = scrollView.contentOffset.y;
    if (newOffsetY < -IMAGE_HEIGHT)
    {
        self.headView.frame = CGRectMake(0, newOffsetY, kScreenWidth, -newOffsetY);
    }
}

#pragma mark -- actions
//实名认证
- (void)onRealNameAuthentication
{
    
}

//登录
- (void)showLoginViewController
{
    MFLoginViewController *controller = [[MFLoginViewController alloc] init];
    controller.successLogined = ^(BOOL success, UIViewController *controller) {
        [controller.navigationController dismissViewControllerAnimated:YES completion:^{
            
        }];
    };
    MFTransitionNavigationController *nav = [[MFTransitionNavigationController alloc] initWithRootViewController:controller];
    [self.navigationController presentViewController:nav animated:YES completion:^{
        
    }];
}

- (void)onAccountLogined
{
    MFAccountManager *mgr = [MFAccountManager shareInstance];
    MFAccount *account = mgr.account;
    [_avatarView sd_setImageWithURL:[NSURL URLWithString:account.icon] placeholderImage:[UIImage imageNamed:@"account_avatar"]];
    
    self.nameLabel.hidden = NO;
    self.numberLabel.hidden = NO;
    self.loginButton.hidden = YES;
    self.nameLabel.text = account.userName;
    self.numberLabel.text = account.userId;
    
    //登录成功，添加退出登录按钮
    if (_titleGroups.count == 0) {
        NSMutableArray *thirdArray = [[NSMutableArray alloc] init];
        [thirdArray addObject:@"退出登录"];
        [_titleGroups addObject:thirdArray];
    }
    [self.tableView reloadData];
}

- (void)onAccountExit
{
    self.nameLabel.hidden = YES;
    self.numberLabel.hidden = YES;
    self.loginButton.hidden = NO;
    
    MFAccountManager *mgr = [MFAccountManager shareInstance];
    mgr.account = nil;
    
    if (_titleGroups.count == 2) {
        [_titleGroups removeLastObject];
    }
    [self.tableView reloadData];
    
    //清空保存在本地的，除了账号意外的所有信息
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults removeObjectForKey:@"kMFAccount"];
//    [userDefaults removeObjectForKey:@"refresh_token"];
//    [userDefaults removeObjectForKey:@"access_token"];
//    [userDefaults removeObjectForKey:@"token_expires"];
    [userDefaults removeObjectForKey:@"passWord"];
    [userDefaults synchronize];
}

@end
