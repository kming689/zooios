//
//  CTInputTextViewController.h
//  CTYun
//
//  Created by 方子扬 on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFKitBases.h"

@interface CTInputTextViewController : MFViewController


-(instancetype) initWithValue:(NSString *) value
                        title:(NSString *) title
                  limitLength:(NSInteger) limitLength
                  regexString:(NSString *) regexString
            submitChangeBlock:(void(^)(NSString *newValue,UIViewController *viewController)) submitChangeBlock;

@end
