//
//  CTInputTextViewController.m
//  CTYun
//
//  Created by 方子扬 on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTInputTextViewController.h"
#import "UITextView+YLTextView.h"
#import "NSString+MFAdd.h"
#import <SVProgressHUD/SVProgressHUD.h>

@interface CTInputTextViewController ()
@property(nonatomic, weak) IBOutlet UITextView *textView;
@property(nonatomic, copy) NSString *value;
@property(nonatomic, copy) NSString *valueTitle;
@property(nonatomic, assign) NSInteger limitLength;
@property(nonatomic, copy) NSString *regexString;
@property(nonatomic, copy) void(^submitChange)(NSString *,UIViewController *);
@end

@implementation CTInputTextViewController

-(NSString *)nibName
{
    return NSStringFromClass([CTInputTextViewController class]);
}

-(NSBundle *)nibBundle
{
    return [NSBundle mainBundle];
}

-(instancetype)initWithValue:(NSString *)value title:(NSString *)title limitLength:(NSInteger)limitLength regexString:(NSString *) regexString submitChangeBlock:(void (^)(NSString *,UIViewController *))submitChangeBlock
{
    self = [super init];
    if (self) {
        self.value = value;
        self.valueTitle = title;
        self.limitLength = limitLength;
        self.regexString = regexString;
        self.submitChange = submitChangeBlock;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = [NSString stringWithFormat:@"修改%@",self.valueTitle];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - config view
-(void)configView
{
    [super configView];
    
    if (self.limitLength > 0) {
        self.textView.limitLength = @(self.limitLength);
    }
    
    if (self.value.length > 0) {
        self.textView.text = self.value;
    }else{
       self.textView.placeholder = [NSString stringWithFormat:@"请输入%@",self.valueTitle];
    }
    
    UIBarButtonItem *okButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"完成" style:UIBarButtonItemStylePlain target:self action:@selector(okBarButtonItemAction:)];
    [self.navigationItem setRightBarButtonItem:okButtonItem];
    
    [self.textView becomeFirstResponder];
}

#pragma mark - action
-(IBAction) okBarButtonItemAction:(id)sender
{
    if (self.regexString.length > 0) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", self.regexString];
        BOOL isValid = [predicate evaluateWithObject:self.textView.text];
        if (!isValid) {
            [SVProgressHUD showErrorWithStatus:[NSString stringWithFormat:@"%@格式错误",self.valueTitle]];
            return;
        }
    }
    
    if (_submitChange) {
        _submitChange(self.textView.text,self);
    }
}

@end
