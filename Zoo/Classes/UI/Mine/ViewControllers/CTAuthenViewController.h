//
//  CTAuthenViewController.h
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFKitBases.h"
#import "MFMarco.h"

@class CTAccountInfomation;

@interface CTAuthenViewController : MFViewController

/**
 初始化一个实名认证的试图控制器

 @param authenState 认证状态
 @param dataSource 初始化数据源
 @return 实例对象
 */
- (instancetype)initWithAuthenState:(eCTAuthenticationState)authenState dataSource:(CTAccountInfomation *)dataSource defalutParams:(NSDictionary *)defaultParams;

@end
