//
//  CTAuthenViewController.m
//  CTYun
//
//  Created by tanfameng on 2018/3/2.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAuthenViewController.h"
#import "CTAuthenViewModel.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFKitBases.h"
#import "MFActionSheet.h"
#import "MFKitMarcro.h"
#import "MFAlertView.h"
#import <TZImagePickerController/TZImagePickerController.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import "UIImage+MFAdd.h"

#import "CTAuthenItemBaseModel.h"
#import "CTAuthenInputItem.h"
#import "CTAuthenSelectItem.h"
#import "CTAuthenUploadCardItem.h"
#import "CTAuthenUploadFileItem.h"
#import "CTAuthenDescItem.h"
#import "CTAuthenButtonItem.h"
#import "CTAuthenTemplate.h"
#import "CTAuthenGroup.h"
#import "CTAuthenSelectOption.h"

#import "CTAuthenItemBaseCell.h"
#import "CTAuthenItemTableViewCell.h"
#import "CTCardItemTableViewCell.h"
#import "CTAuthenDescTableViewCell.h"
#import "CTAuthenBtnTableViewCell.h"

#import "CTInputTextViewController.h"
#import "CTSubmitedAuthenViewController.h"

static const NSString *kCTAuthenItemTableViewCellIdentifer = @"kCTAuthenItemTableViewCellIdentifer";
static const NSString *kMFAuthenCardItemTableViewCellIdentifer = @"kMFAuthenCardItemTableViewCellIdentifer";
static const NSString *kMFAuthenDescItemTableViewCellIdentifer = @"kMFAuthenDescItemTableViewCellIdentifer";
static const NSString *kMFAuthenbtnItemTableViewCellIdentifer = @"kMFAuthenbtnItemTableViewCellIdentifer";

@interface CTAuthenViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) CTAuthenViewModel *viewModel;

@property (nonatomic, strong) MFTableView *tableView;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation CTAuthenViewController

- (instancetype)init
{
    self = [super init];
    if (self) {
        //初始化数据源，用于界面显示
        self.title = @"实名认证";
    }
    return self;
}

- (instancetype)initWithAuthenState:(eCTAuthenticationState)authenState dataSource:(CTAccountInfomation *)dataSource defalutParams:(NSDictionary *)defaultParams
{
    self = [super init];
    if (self) {
        self.title = @"实名认证";
        self.viewModel.accountInfo = dataSource;
        self.viewModel.defaultParams = defaultParams;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.indicatorView startAnimating];
    self.tableView.hidden = YES;
    RACSignal *signal = [self.viewModel.dataCommand execute:nil];
    @weakify(self);
    [signal subscribeNext:^(id x) {
        self.viewModel.dataSource = x;
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
    }];
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
}

-(void)configView
{
    [super configView];
    
    [self configTableView];
    
    @weakify(self);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.and.top.mas_equalTo(0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_indicatorView];
    [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(30);
    }];
}

-(void)bindViewModel
{
    [super bindViewModel];
}

#pragma mark -- 懒加载属性
- (CTAuthenViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[CTAuthenViewModel alloc] init];
    }
    return _viewModel;
}

#pragma mark -- UI设置
- (void)configTableView
{
    _tableView = [[MFTableView alloc] init];
    _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    //根据认证状态确定是否需要提交按钮,按钮放在footerView上面
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    footerView.backgroundColor = [MFColor clearColor];
    _tableView.tableFooterView = footerView;
    
    _tableView.backgroundColor = [MFColor clearColor];
    _tableView.separatorColor = [MFColor mf_lineColor];
    _tableView.sectionIndexColor = [MFColor mf_fontGrayStyleColor];
    _tableView.sectionIndexBackgroundColor =[UIColor clearColor];
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CTAuthenItemTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kCTAuthenItemTableViewCellIdentifer copy]];
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CTCardItemTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFAuthenCardItemTableViewCellIdentifer copy]];
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CTAuthenDescTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFAuthenDescItemTableViewCellIdentifer copy]];
    [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([CTAuthenBtnTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFAuthenbtnItemTableViewCellIdentifer copy]];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    //调整分割线边距
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        self.tableView.separatorInset = UIEdgeInsetsZero;
    }
    //2.调整(iOS8以上)view边距(或者在cell中设置preservesSuperviewLayoutMargins,二者等效)
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        self.tableView.layoutMargins = UIEdgeInsetsZero;
    }
    [self.view addSubview:self.tableView];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.viewModel.dataSource.structures.count;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    CTAuthenGroup *authenGroup = self.viewModel.dataSource.structures[section];
    NSString *title = authenGroup.title;
    
    MFView *headerView = [[MFView alloc] init];
    headerView.translatesAutoresizingMaskIntoConstraints = YES;
    headerView.backgroundColor = [MFColor mf_colorWithHexString:@"F3F3F3"];
    headerView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 22);
    MFLabel *tipLabel = [[MFLabel alloc] initWithFrame:CGRectMake(15, 6, tableView.bounds.size.width - 20, 30)];
    tipLabel.textColor = [MFColor mf_colorWithHexString:@"999999"];
    tipLabel.font = [UIFont systemFontOfSize:13.0];
    tipLabel.text = title;
    [headerView addSubview:tipLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 36;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    CTAuthenGroup *authenGroup = self.viewModel.dataSource.structures[section];
    NSArray *array = authenGroup.itemCells;
    return array.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTAuthenGroup *authenGroup = self.viewModel.dataSource.structures[indexPath.section];
    NSArray *array = authenGroup.itemCells;
    CTAuthenItemBaseModel *item = [array objectAtIndex:indexPath.row];
    if ([item isKindOfClass:[CTAuthenUploadCardItem class]]) {
        CTCardItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFAuthenCardItemTableViewCellIdentifer copy] forIndexPath:indexPath];
        //调整分割线边距
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, kScreenWidth);
        [cell configCellWithItem:item];
        return cell;
    }
    else if ([item isKindOfClass:[CTAuthenUploadFileItem class]]) {
        CTAuthenItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kCTAuthenItemTableViewCellIdentifer copy] forIndexPath:indexPath];
        //调整分割线边距
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        [cell configCellWithItem:item];
        
        return cell;
    }
    else if ([item isKindOfClass:[CTAuthenDescItem class]]) {
        CTAuthenDescItem *descItem = (CTAuthenDescItem *)item;
        CTAuthenDescTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFAuthenDescItemTableViewCellIdentifer copy] forIndexPath:indexPath];
        //调整分割线边距
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, kScreenWidth);
        cell.contentView.backgroundColor = [MFColor mf_colorWithHexString:@"F3F3F3"];
        cell.descriptionLabel.text = descItem.authenDescription;
        
        return cell;
    }
    else if ([item isKindOfClass:[CTAuthenButtonItem class]]) {
        CTAuthenButtonItem *btnItem = (CTAuthenButtonItem *)item;
        CTAuthenBtnTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFAuthenbtnItemTableViewCellIdentifer copy] forIndexPath:indexPath];
        //调整分割线边距
        cell.separatorInset = UIEdgeInsetsMake(0, 0, 0, kScreenWidth);
        cell.contentView.backgroundColor = [MFColor mf_colorWithHexString:@"F3F3F3"];
        [cell.button setTitle:btnItem.title forState:UIControlStateNormal];

        [cell.button removeTarget:self action:@selector(onSubmitButtonDone:) forControlEvents:UIControlEventTouchUpInside];
        [cell.button addTarget:self action:@selector(onSubmitButtonDone:) forControlEvents:UIControlEventTouchUpInside];
        return cell;
    }
    else {
        CTAuthenItemTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kCTAuthenItemTableViewCellIdentifer copy] forIndexPath:indexPath];
        //调整分割线边距
        if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
            [cell setLayoutMargins:UIEdgeInsetsZero];
        }
        
        [cell configCellWithItem:item];
        
        return cell;
    }
}

#pragma mark -- UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CTAuthenGroup *authenGroup = self.viewModel.dataSource.structures[indexPath.section];
    NSArray *array = authenGroup.itemCells;
    CTAuthenItemBaseModel *item = [array objectAtIndex:indexPath.row];
    return item.cellHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    CTAuthenItemBaseCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    
    CTAuthenGroup *authenGroup = self.viewModel.dataSource.structures[indexPath.section];
    NSArray *array = authenGroup.itemCells;
    CTAuthenItemBaseModel *item = [array objectAtIndex:indexPath.row];
    
    //判断是否可编辑
    if (!item.canEdit) {
        return;
    }
    
    //输入类型
    if ([item isKindOfClass:[CTAuthenInputItem class]]) {
        CTAuthenInputItem *inpuItem = (CTAuthenInputItem *)item;
        CTInputTextViewController *inputVC = [[CTInputTextViewController alloc] initWithValue:inpuItem.submitValue title:inpuItem.title limitLength:0 regexString:item.regex submitChangeBlock:^(NSString *newValue,UIViewController *controller) {
            inpuItem.submitValue = newValue;
            inpuItem.indicatorTip = newValue;
            cell.indicatorLabel.text = newValue;
            [controller.navigationController popViewControllerAnimated:YES];
        }];
        [self.navigationController pushViewController:inputVC animated:YES];
    }
    //选择类型
    else if ([item isKindOfClass:[CTAuthenSelectItem class]]) {
        CTAuthenSelectItem *selectItem = (CTAuthenSelectItem *)item;
        MFPopupCompletionBlock completeBlock = ^(MFPopupView *popupView, BOOL finished){
            
        };
        MFPopupItemHandler block = ^(NSInteger index){
            //@strongify(self);
            CTAuthenSelectOption *option = [selectItem.optionValues objectAtIndex:index];
            selectItem.submitValue = option.value;
            selectItem.indicatorTip = option.name;
            cell.indicatorLabel.text = option.name;
        };
        NSMutableArray *items = [[NSMutableArray alloc] init];
        for (CTAuthenSelectOption *option in selectItem.optionValues) {
            MFPopupItem *menuItem = MFItemMake(option.name, MFItemTypeNormal, block);
            [items addObject:menuItem];
        }
        MFActionSheet *sheetView = [[MFActionSheet alloc] initWithTitle:nil
                                                                  items:items];
        sheetView.attachedView.mf_dimBackgroundBlurEnabled = NO;
        [sheetView showWithBlock:completeBlock];
    }
    //上传证件类型
    else if ([item isKindOfClass:[CTAuthenUploadCardItem class]]) {
        CTAuthenUploadCardItem *cardItem = (CTAuthenUploadCardItem *)item;
        [self pushImagePickerControllerWithCell:cell Item:cardItem];
    }
    else if ([item isKindOfClass:[CTAuthenUploadFileItem class]]) {
        CTAuthenUploadFileItem *fileItem = (CTAuthenUploadFileItem *)item;
        [self pushImagePickerControllerWithCell:cell Item:fileItem];
    }
}

#pragma mark -- actions
- (void)pushImagePickerControllerWithCell:(MFTableViewCell *)cell Item:(CTAuthenItemBaseModel *)item
{
    TZImagePickerController *pickerController = [[TZImagePickerController alloc] initWithMaxImagesCount:10 columnNumber:4 delegate:nil pushPhotoPickerVc:YES];
    pickerController.autoDismiss = NO;
    pickerController.allowTakePicture = NO; // 在内部显示拍照按钮
    pickerController.maxImagesCount = 1;
    pickerController.selectedAssets = nil;//[NSMutableArray arrayWithArray:_assets];
    
    // 2. 设置是否可以选择视频/图片/原图
    pickerController.allowPickingVideo = NO;
    pickerController.allowPickingImage = YES;
    pickerController.allowPickingOriginalPhoto = NO;
    pickerController.allowPickingGif = NO;
    pickerController.allowPickingMultipleVideo = NO; // 是否可以多选视频
    
    // 3. 照片排列按修改时间升序
    pickerController.sortAscendingByModificationDate = YES;
    //    pickerController.isStatusBarDefault = YES;
    
    // 你可以通过block或者代理，来得到用户选择的照片.
    //@weakify(self);
    @weakify(pickerController);
    [pickerController setDidFinishPickingPhotosHandle:^(NSArray<UIImage *> *photos, NSArray *assets, BOOL isSelectOriginalPhoto) {
        //@strongify(self);
        @strongify(pickerController);
        [pickerController dismissViewControllerAnimated:YES completion:nil];
        if (photos.count > 0) {
            if ([cell isKindOfClass:[CTCardItemTableViewCell class]]) {
                CTCardItemTableViewCell *cardCell = (CTCardItemTableViewCell *)cell;
                CTAuthenUploadCardItem *cardItem = (CTAuthenUploadCardItem *)item;
                
                cardCell.cardImageView.image = [photos lastObject];
                cardItem.image = [photos lastObject];
                
                NSData *imageData = UIImagePNGRepresentation(cardItem.image);
                
                [self uploadCardData:imageData cell:cardCell item:cardItem];
            }else{
                CTAuthenItemTableViewCell *itemCell = (CTAuthenItemTableViewCell *)cell;
                CTAuthenUploadFileItem *fileItem = (CTAuthenUploadFileItem *)item;
                
                fileItem.image = [photos lastObject];

                NSData *imageData = UIImagePNGRepresentation(fileItem.image);
                
                [self uploadFileData:imageData cell:itemCell item:fileItem];
            }
        }
    }];
    [pickerController setImagePickerControllerDidCancelHandle:^{
        @strongify(pickerController);
        [pickerController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [self presentViewController:pickerController animated:YES completion:^{
        
    }];
}

- (void)onSubmitButtonDone:(id)sender
{
    RACSignal *signal = [self.viewModel.submitCommand execute:nil];
    @weakify(self);
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
    [signal subscribeNext:^(id x) {
        @strongify(self);
        CTSubmitedAuthenViewController *controller = [[CTSubmitedAuthenViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kCTAccountInfoChanged" object:nil];
    }];
}

- (void)uploadCardData:(NSData *)data cell:(CTCardItemTableViewCell *)cell item:(CTAuthenUploadCardItem *)cardItem
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    NSString *imageKey = [NSString stringWithFormat:@"pic%li",(long)indexPath.row+1];
    
    [cell.activityView startAnimating];
    [self.viewModel uploadFile:data imageKey:imageKey success:^(id responseObject) {
        [cell.activityView stopAnimating];
        //成功，设置submitValue
        UIImage *image = [UIImage imageWithData:data];
        cell.cardImageView.image = image;
        cardItem.submitValue = responseObject;
    } failure:^(NSError *error) {
        [cell.activityView stopAnimating];
        //上传失败，回复最初始的图片设置
        cell.cardImageView.image = [UIImage imageNamed:@"authen_card.png" bundleClass:[self class]];
        cardItem.submitValue = nil;
        
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

- (void)uploadFileData:(NSData *)data cell:(CTAuthenItemTableViewCell *)cell item:(CTAuthenUploadFileItem *)fileItem
{
    NSString *imageKey = [[NSProcessInfo processInfo] globallyUniqueString];
    
    [self.viewModel uploadFile:data imageKey:imageKey success:^(id responseObject) {
        //成功，设置submitValue
        fileItem.submitValue = responseObject;
        fileItem.indicatorTip = @"已上传";
        cell.indicatorLabel.text = @"已上传";
    } failure:^(NSError *error) {
        fileItem.submitValue = nil;
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

@end
