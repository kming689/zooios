//
//  MFPriceListViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPriceListViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <YYModel/YYModel.h>
#import "MFNetRequestBusiness.h"
#import "MFCurrency.h"

@interface MFPriceListViewModel()<SRWebSocketDelegate>

@property (nonatomic, strong) RACSubject *pushSubject;

@property (nonatomic, strong) SRWebSocket *webSocket;

@end

@implementation MFPriceListViewModel

- (id)init
{
    self = [super init];
    if (self) {
        //数据初始化
        _priceList = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];

    @weakify(self);
    _pushSubject = [RACSubject subject];
    [[[_pushSubject throttle:0.1] deliverOnMainThread] subscribeNext:^(id x) {
        @strongify(self);
        [self dealDataWithPush:x];
    }];
}

#pragma mark -- 懒加载属性
- (RACSignal *)dataSignal
{
    @weakify(self);
    RACSignal *coldSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
       //请求数据
        NSURLSessionTask *task = [self requestPriceListWithBourse:self.bourseId page:1 success:^(NSArray<MFCurrency *> *currencyList) {
            [subscriber sendNext:currencyList];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
            [task cancel];
        }];
    }];
    RACSubject *subject = [RACSubject subject];
    RACMulticastConnection *multicastConnection = [coldSignal multicast:subject];
    RACSignal *hotSignal = multicastConnection.autoconnect;
    return hotSignal;
}

- (RACCommand *)dataCommand
{
    if (!_dataCommand) {
        @weakify(self)
        _dataCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //获取行情列表
                NSInteger page = 1;
                if (input) {
                    page = ((NSNumber *)input).integerValue;
                }
                NSURLSessionTask *task = [self requestPriceListWithBourse:self.bourseId page:page success:^(NSArray<MFCurrency *> *currencyList) {
                    [subscriber sendNext:currencyList];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                    [task cancel];
                }];
            }];
            
            return dataSignal;
        }];
    }
    
    return _dataCommand;
}

#pragma mark -- 业务方法
- (NSURLSessionTask *)requestPriceListWithBourse:(NSString *)BourseId page:(NSInteger)page success:(void (^)(NSArray<MFCurrency *> *currencyList))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    
    if (_priceType == MFPriceTypeCustom) {
        return [business requestCustomCurrencyListWithBourse:BourseId page:page pageSize:20 success:^(NSArray<MFCurrency *> *priceList) {
            if (completion) {
                completion(priceList);
            }
        } failure:^(NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
    }else{
        return [business requestCurrencyListWithBourse:BourseId page:page pageSize:20  success:^(NSArray<MFCurrency *> *priceList) {
            if (completion) {
                completion(priceList);
            }
        } failure:^(NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
    }
}

- (NSURLSessionTask *)changeFollowStatusWithCurrency:(MFCurrency *)currency success:(void (^)(id response))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    
    //添加自选
    if (!currency.isFollowed) {
        return [business addCustomPriceWithBourse:currency.bourseId currency:currency.symbol success:^(id response) {
            currency.isFollowed = !currency.isFollowed;
            if (completion) {
                completion(response);
            }
        } failure:^(NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
    }
    //取消自选
    else{
        return [business deleteCustomPriceWithBourse:currency.bourseId currency:currency.symbol success:^(id response) {
            currency.isFollowed = !currency.isFollowed;
            if (completion) {
                completion(response);
            }
        } failure:^(NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
    }
}

- (void)launchWebSocket
{
    if (!self.webSocket) {
        SRWebSocket *webSocket = [[SRWebSocket alloc] initWithURL:[NSURL URLWithString:@"ws://ifw.zoo.one:8080"]];
        self.webSocket = webSocket;
        webSocket.delegate = self;
    }
    [self.webSocket open];
}

- (void)stopWebSocket
{
    [self.webSocket close];
}

#pragma mark -- SRWebSocketDelegate
- (void)webSocketDidOpen:(SRWebSocket *)webSocket
{
    NSLog(@"开启websocket");
    self.isConnected = YES;
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveMessage:(id)message
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        NSData *jsonData = [message dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:jsonData options:kNilOptions error:NULL];
        NSArray *pushArray = [jsonDic objectForKey:@"data"];
        for (NSDictionary *symbolDic in pushArray) {
            MFCurrency *currency = [MFCurrency yy_modelWithDictionary:symbolDic];
            //本地计算涨跌幅
            NSNumber *openNumber = [symbolDic objectForKey:@"open"];
            NSNumber *closeNumber = [symbolDic objectForKey:@"close"];
            double open = openNumber.doubleValue;
            double close = closeNumber.doubleValue;
            double percent = (close - open)/open;
            NSString *percentString = [NSString stringWithFormat:@"%2.2f%%",percent*100];
            currency.changePercent = percentString;
            //价格跟法币价格
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dealDataWithPush:currency];
            });
            //[_pushSubject sendNext:currency];
        }
    });
}

- (void)webSocket:(SRWebSocket *)webSocket didFailWithError:(NSError *)error
{
    NSLog(@"websocket错误");
}

- (void)webSocket:(SRWebSocket *)webSocket didReceivePong:(NSData *)pongPayload
{
    
}

- (void)webSocket:(SRWebSocket *)webSocket didCloseWithCode:(NSInteger)code reason:(NSString *)reason wasClean:(BOOL)wasClean
{
    self.isConnected = NO;
    self.webSocket = nil;
}

#pragma mark -- 返回数据处理
- (void)dealDataWithPush:(MFCurrency *)currency
{
    if ([self.priceList containsObject:currency]) {
        NSInteger index = [self.priceList indexOfObject:currency];
        MFCurrency *originCurrency = [self.priceList objectAtIndex:index];
        [originCurrency cloneFromCurrency:currency];
        //[self.priceList replaceObjectAtIndex:index withObject:currency];

        //[self.refreshDelegate sendNext:nil];
    }else{
        //[self.priceList addObject:currency];
    }
}

#pragma mark -- public
- (void)dealDataWithLoadMore:(NSArray<MFCurrency *> *)infos
{
    if (_priceType == MFPriceTypeCustom) {
        for (MFCurrency *info in infos) {
            info.isFollowed = YES;
        }
    }
    
    [self.priceList addObjectsFromArray:infos];
}

- (void)dealDataWithLoadNew:(NSArray<MFCurrency *> *)infos
{
    //如果是自选的，先修改自选状态，然后替换整个数据源
    if (_priceType == MFPriceTypeCustom) {
        for (MFCurrency *info in infos) {
            info.isFollowed = YES;
        }
        [self.priceList removeAllObjects];
        [self.priceList addObjectsFromArray:infos];
    }
    //如果是所有的，已经带自选状态了，把下拉刷新获取的新数据添加到数据源的头部
    else{
        int count = (int)infos.count;
        for (int i = count-1; i >=0 ; i--) {
            MFCurrency *info = [infos objectAtIndex:i];
            if (![self.priceList containsObject:info]) {
                [self.priceList insertObject:info atIndex:0];
            }
        }
    }
}

@end
