//
//  MFPageViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPageViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFNetRequestBusiness.h"
#import "MFBourse.h"

@implementation MFPageViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];
}

#pragma mark -- 懒加载属性
- (RACSignal *)dataSignal
{
    @weakify(self);
    RACSignal *coldSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        //请求数据
        NSURLSessionTask *task = [self requestBourseListWithType:self.priceType success:^(NSArray<NSString *> *priceList) {
            [subscriber sendNext:priceList];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
            [task cancel];
        }];
    }];
    RACSubject *subject = [RACSubject subject];
    RACMulticastConnection *multicastConnection = [coldSignal multicast:subject];
    RACSignal *hotSignal = multicastConnection.autoconnect;
    return hotSignal;
}

#pragma mark -- 业务方法
- (NSURLSessionTask *)requestBourseListWithType:(MFPriceType)type success:(void (^)(NSArray<MFBourse *> *bourseList))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    return [business requestBourseListWithOptionType:nil success:^(NSArray<MFBourse *> *bourseList) {
        if (completion) {
            completion(bourseList);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -- 返回数据处理
- (void)dealData:(NSArray *)data
{
    _bourseList = [NSMutableArray arrayWithArray:data];
    [_refreshDelegate sendNext:data];
}

@end
