//
//  MFPriceListViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"
#import <SocketRocket/SocketRocket.h>
#import "MFMarco.h"

@class RACSubject;
@class RACSignal;
@class RACCommand;
@class MFCurrency;

@interface MFPriceListViewModel : MFViewModel

@property (nonatomic, strong) NSString *bourseId;

@property (nonatomic, assign) MFPriceType priceType;

@property (nonatomic, strong) NSMutableArray<MFCurrency *> *priceList;

@property (nonatomic, strong) RACCommand *dataCommand;//参数为交易所ID

@property (nonatomic, strong) RACSignal *dataSignal;

@property (nonatomic, strong) RACSubject *refreshDelegate;//刷新界面的回调

@property (nonatomic, assign) BOOL isConnected;

- (void)dealDataWithLoadMore:(NSArray<MFCurrency *> *)infos;

- (void)dealDataWithLoadNew:(NSArray<MFCurrency *> *)infos;

- (void)launchWebSocket;

- (void)stopWebSocket;

- (NSURLSessionTask *)changeFollowStatusWithCurrency:(MFCurrency *)currency success:(void (^)(id response))completion failure:(void (^)(NSError *error))failure;

@end
