//
//  MFPageViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"
#import "MFMarco.h"

@class RACSubject;
@class RACSignal;
@class MFBourse;

@interface MFPageViewModel : MFViewModel

@property (nonatomic, assign) MFPriceType priceType;

@property (nonatomic, strong) NSMutableArray<MFBourse *> *bourseList;

@property (nonatomic, strong) RACSignal *dataSignal;

@property (nonatomic, strong) RACSubject *refreshDelegate;//刷新界面的回调

@end
