//
//  MFPriceDetailViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/10.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"

@class RACSubject;
@class RACSignal;
@class RACCommand;
@class MFCurrencyDetail;
@class MFCurrency;
@class MFChartModel;

@interface MFPriceDetailViewModel : MFViewModel

@property (nonatomic, strong) NSString *bourseId;

@property (nonatomic, strong) NSString *currencyId;

@property (nonatomic, strong) MFCurrency *currency;

@property (nonatomic, strong) MFCurrencyDetail *currencyDetail;

@property (nonatomic, strong) RACCommand *dataCommand;

@property (nonatomic, strong) RACCommand *changeStateCommand;

@property (nonatomic, strong) RACSignal *dataSignal;

@property (nonatomic, strong) RACSubject *refreshDelegate;//刷新界面的回调

- (void)initDetailWithCurrency:(MFCurrency *)currency;

//获取图表趋势图数据
- (void)requestPriceDetailChartDataTimeType:(NSString *)timeType success:(void (^)(MFChartModel *chartModel))completion failure:(void (^)(NSError *error))failure;

- (NSURLSessionTask *)requestNewsWithPriceSuccess:(void (^)(NSArray *newsList))completion failure:(void (^)(NSError *error))failure;

@end
