//
//  MFPriceDetailViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/10.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPriceDetailViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <YYModel/YYModel.h>
#import "NSDate+MFAdd.h"
#import "MFNetRequestBusiness.h"
#import "MFCurrencyDetail.h"
#import "MFChartModel.h"

@implementation MFPriceDetailViewModel

- (id)init
{
    self = [super init];
    if (self) {
        //数据初始化
    }
    return self;
}

- (void)initDetailWithCurrency:(MFCurrency *)currency
{
    NSString *jsonString = [currency yy_modelToJSONString];
    MFCurrencyDetail *detail = [MFCurrencyDetail yy_modelWithJSON:jsonString];
    [detail.chartTitles addObjectsFromArray:@[@"24小时",@"1周",@"1个月",@"3个月",@"6个月",@"全部"]];
    self.currencyDetail = detail;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];
    @weakify(self)
    [[[self.dataCommand executionSignals] switchToLatest] subscribeNext:^(id x) {
        @strongify(self)
        [self dealData:x];
    }];
}

#pragma mark -- 懒加载属性
- (RACSignal *)dataSignal
{
    @weakify(self);
    RACSignal *coldSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        //请求数据
        NSURLSessionTask *task = [self requestPriceDetailSuccess:^(MFCurrencyDetail *currencyDetail) {
            [subscriber sendNext:currencyDetail];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
            [task cancel];
        }];
    }];
    RACSubject *subject = [RACSubject subject];
    RACMulticastConnection *multicastConnection = [coldSignal multicast:subject];
    RACSignal *hotSignal = multicastConnection.autoconnect;
    return hotSignal;
}

- (RACCommand *)dataCommand
{
    if (!_dataCommand) {
        @weakify(self)
        _dataCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //获取行情列表
                NSURLSessionTask *task = [self requestPriceDetailSuccess:^(MFCurrencyDetail *currencyDetail) {
                    [subscriber sendNext:currencyDetail];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                    [task cancel];
                }];
            }];
            
            return dataSignal;
        }];
    }
    
    return _dataCommand;
}

- (RACCommand *)changeStateCommand
{
    if (!_changeStateCommand) {
        @weakify(self)
        _changeStateCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //改变币对自选状态
                NSURLSessionTask *task = [self changeFollowStatusWithSuccess:^(id response) {
                    [subscriber sendNext:response];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                    [task cancel];
                }];
            }];
            
            return dataSignal;
        }];
    }
    
    return _changeStateCommand;
}

#pragma mark -- 业务方法
- (NSURLSessionTask *)requestPriceDetailSuccess:(void (^)(MFCurrencyDetail *currencyDetail))completion failure:(void (^)(NSError *error))failure
{
    if (completion) {
        completion(_currencyDetail);
    }
    return nil;
    
//    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
//    return [business requestPriceDetailWithBourse:_bourseId currency:_currencyId success:^(MFCurrencyDetail *currencyDetail) {
//        if (completion) {
//            completion(currencyDetail);
//        }
//    } failure:^(NSError *error) {
//        if (failure) {
//            failure(error);
//        }
//    }];
}

- (void)requestPriceDetailChartDataTimeType:(NSString *)timeType success:(void (^)(MFChartModel *chartModel))completion failure:(void (^)(NSError *error))failure
{
    __block MFChartModel *globalChartModel = nil;
    __block NSError *globalError = nil;
    __block id globalResponse = nil;
    __block id baseResponse = nil;
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    [business requestPriceDetailChartDataWithBourse:_bourseId currency:_currencyId timeInterval:[self getTimeParamWithType:timeType] success:^(MFChartModel *chartModel) {
        chartModel.title = timeType;
        globalChartModel = chartModel;
        dispatch_group_leave(group);
    } failure:^(NSError *error) {
        globalError = error;
        dispatch_group_leave(group);
    }];
    dispatch_group_enter(group);
    [business requestPriceDetailChartCountDataWithBourse:_bourseId currency:_currencyId timeInterval:[self getTimeParamWithType:timeType] success:^(id response) {
        dispatch_group_leave(group);
        globalResponse = response;
    } failure:^(NSError *error) {
        globalError = error;
        dispatch_group_leave(group);
    }];
    dispatch_group_enter(group);
    [business requestCurrencyBaseInfoWithKeyCurrency:_currencyDetail.currencyName success:^(id response) {
        baseResponse = response;
        dispatch_group_leave(group);
    } failure:^(NSError *error) {
        globalError = error;
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if (globalError) {
            if (failure) {
                failure(globalError);
            }
        }else{
            NSDictionary *summary = [globalResponse objectForKey:@"summary"];
            NSNumber *max = [summary objectForKey:@"max_price"];
            NSNumber *min = [summary objectForKey:@"min_price"];
            NSNumber *amount = [summary objectForKey:@"amount"];
            if (![max isEqual:[NSNull null]]) {
                double maxValue = max.doubleValue;
                globalChartModel.maxValue = [NSString stringWithFormat:@"%5.5f",maxValue];
            }
            if (![min isEqual:[NSNull null]]) {
                double minValue = min.doubleValue;
                globalChartModel.minValue = [NSString stringWithFormat:@"%5.5f",minValue];
            }
            if (![amount isEqual:[NSNull null]]) {
                long long amountValue = amount.longLongValue;
                globalChartModel.amount = [NSString stringWithFormat:@"%lli",amountValue];
            }
            
            NSDictionary *baseDic = baseResponse;
            self.currencyDetail.introduction = [baseDic objectForKey:@"content_raw"];
            self.currencyDetail.bourseIntroduction = [baseDic objectForKey:@"markets"];
            if (completion) {
                completion(globalChartModel);
            }
        }
    });
}

- (NSURLSessionTask *)requestNewsWithPriceSuccess:(void (^)(NSArray *))completion failure:(void (^)(NSError *))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    
    return [business requestNewsWithKeyWord:self.currencyDetail.currencyName success:^(NSArray *newsList) {
        [self.currencyDetail.newsList addObjectsFromArray:newsList];
        if (completion) {
            completion(newsList);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)changeFollowStatusWithSuccess:(void (^)(id response))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    MFCurrency *currency = self.currencyDetail;
    //添加自选
    if (!currency.isFollowed) {
        return [business addCustomPriceWithBourse:currency.bourseId currency:currency.symbol success:^(id response) {
            currency.isFollowed = !currency.isFollowed;
            if (completion) {
                completion(response);
            }
        } failure:^(NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
    }
    //取消自选
    else{
        return [business deleteCustomPriceWithBourse:currency.bourseId currency:currency.symbol success:^(id response) {
            currency.isFollowed = !currency.isFollowed;
            if (completion) {
                completion(response);
            }
        } failure:^(NSError *error) {
            if (failure) {
                failure(error);
            }
        }];
    }
}

#pragma mark -- 返回数据处理
- (void)dealData:(id)data
{
    self.currencyDetail = data;
    [_refreshDelegate sendNext:data];
}

#pragma mark -- private
- (NSString *)getTimeParamWithType:(NSString *)type
{
    NSDate *now = [NSDate date];
    
    if ([type isEqualToString:@"24小时"]) {
        NSDate *startDate = [now dateAfterDay:-1];
//        NSDateFormatter *formatter = [NSDateFormatter new];
//        formatter.locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
//        formatter.dateFormat = @"yyyy,MM,dd,HH";
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at@ymdh:%@-%@",startDateString,endDateString];
        return lastString;
    }
    else if ([type isEqualToString:@"1周"]) {
        NSDate *startDate = [now dateAfterDay:-7];
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at@ymdh:%@-%@",startDateString,endDateString];
        return lastString;
    }
    else if ([type isEqualToString:@"1个月"]) {
        NSDate *startDate = [now dateAfterMonth:-1];
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at@ymdh:%@-%@",startDateString,endDateString];
        return lastString;
    }
    else if ([type isEqualToString:@"3个月"]) {
        NSDate *startDate = [now dateAfterDay:-3];
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at@ymdh:%@-%@",startDateString,endDateString];
        return lastString;
    }
    else if ([type isEqualToString:@"6个月"]) {
        NSDate *startDate = [now dateAfterDay:-6];
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at@ymdh:%@-%@",startDateString,endDateString];
        return lastString;
    }
    return nil;
}

- (NSString *)getCountTimeParamWithType:(NSString *)type
{
    NSDate *now = [NSDate date];
    
    if ([type isEqualToString:@"24小时"]) {
        NSDate *startDate = [now dateAfterDay:-1];
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at:%@-%@",startDateString,endDateString];
        return lastString;
    }
    else if ([type isEqualToString:@"1周"]) {
        NSDate *startDate = [now dateAfterDay:-1];
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at:%@-%@",startDateString,endDateString];
        return lastString;
    }
    else if ([type isEqualToString:@"1个月"]) {
        NSDate *startDate = [now dateAfterDay:-1];
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at:%@-%@",startDateString,endDateString];
        return lastString;
    }
    else if ([type isEqualToString:@"3个月"]) {
        NSDate *startDate = [now dateAfterDay:-1];
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at:%@-%@",startDateString,endDateString];
        return lastString;
    }
    else if ([type isEqualToString:@"6个月"]) {
        NSDate *startDate = [now dateAfterDay:-1];
        NSString *startDateString = [startDate stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *endDateString = [now stringWithFormat:@"yyyy,MM,dd,HH"];
        NSString *lastString = [NSString stringWithFormat:@"published_at:%@-%@",startDateString,endDateString];
        return lastString;
    }
    return nil;
}

@end
