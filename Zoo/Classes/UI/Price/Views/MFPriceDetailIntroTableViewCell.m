//
//  MFPriceDetailIntroTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPriceDetailIntroTableViewCell.h"
#import "MFKitBases.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFCurrencyDetail.h"

@interface MFPriceDetailIntroTableViewCell()

@property (nonatomic, strong) MFLabel *currencyNameLabel;

@property (nonatomic, strong) MFLabel *currencyIntroLabel;

@property (nonatomic, strong) MFLabel *bourseNameLabel;

@property (nonatomic, strong) MFLabel *bourseIntroLabel;

@end

@implementation MFPriceDetailIntroTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _currencyNameLabel = [[MFLabel alloc] init];
    _currencyNameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _currencyNameLabel.font = [UIFont systemFontOfSize:14.0];
    [self.contentView addSubview:_currencyNameLabel];
    
    _currencyIntroLabel = [[MFLabel alloc] init];
    _currencyIntroLabel.numberOfLines = 0;
    _currencyIntroLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _currencyIntroLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _currencyIntroLabel.font = [UIFont systemFontOfSize:13.0];
    [self.contentView addSubview:_currencyIntroLabel];
    
    _bourseNameLabel = [[MFLabel alloc] init];
    _bourseNameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _bourseNameLabel.font = [UIFont systemFontOfSize:14.0];
    [self.contentView addSubview:_bourseNameLabel];
    
    _bourseIntroLabel = [[MFLabel alloc] init];
    _bourseIntroLabel.numberOfLines = 0;
    _bourseIntroLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _bourseIntroLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _bourseIntroLabel.font = [UIFont systemFontOfSize:13.0];
    [self.contentView addSubview:_bourseIntroLabel];
    
    @weakify(self);
    [_currencyNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.contentView).offset(16);
    }];
    [_currencyIntroLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.equalTo(self.currencyNameLabel.mas_bottom).offset(10);
    }];
    [_currencyNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.contentView).offset(16);
    }];
    [_bourseNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.currencyIntroLabel.mas_bottom).offset(10);
    }];
    [_bourseIntroLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.equalTo(self.bourseNameLabel.mas_bottom).offset(5);
        make.bottom.equalTo(self.contentView.mas_bottom).offset(-10);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellViewIntro:(MFCurrencyDetail *)intro
{
    _currencyNameLabel.text = intro.currencyName;
    _currencyIntroLabel.text = intro.introduction;
    _bourseNameLabel.text = @"上市交易所";
    _bourseIntroLabel.text = intro.bourseIntroduction;
}

@end
