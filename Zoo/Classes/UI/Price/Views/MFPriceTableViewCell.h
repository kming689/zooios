//
//  MFPriceTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTableViewCell.h"

@class MFCurrency;

@interface MFPriceTableViewCell : MFTableViewCell

@property (nonatomic, strong) void(^cellRightButtonBeClicked)(NSInteger index);

- (void)configCellWithCurrency:(MFCurrency *)currency;

@end
