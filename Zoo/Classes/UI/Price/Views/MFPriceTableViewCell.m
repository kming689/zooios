//
//  MFPriceTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPriceTableViewCell.h"
#import "MFKitBases.h"
#import "MFKitMarcro.h"
#import "UIImage+MFAdd.h"
#import "UIColor+MFAdd.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <BlocksKit/UIControl+BlocksKit.h>

#import "MFCurrency.h"

@interface MFPriceTableViewCell()

@property (nonatomic, strong) MFCurrency *currency;

@property (nonatomic, strong) MFLabel *nameLabel;//代币的名称

@property (nonatomic, strong) MFLabel *conversionNameLabel;//代币换算的通用货币的名称

@property (nonatomic, strong) MFLabel *conversionPriceLabel;//代币换算成通用货币的价格

@property (nonatomic, strong) MFLabel *cashPriceLabel;//代币换算成通法币的价格

@property (nonatomic, strong) MFButton *rightButton;//涨跌幅显示的按钮，可点击

@property (nonatomic, strong) MFView *bottonLineView;

@end

@implementation MFPriceTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _nameLabel = [[MFLabel alloc] init];
    _nameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _nameLabel.font = [UIFont fontWithName:@"HiraginoSans-W6" size:12.0];
    [self.contentView addSubview:_nameLabel];
    
    _conversionNameLabel = [[MFLabel alloc] init];
    _conversionNameLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _conversionNameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _conversionNameLabel.font = [UIFont fontWithName:@"PingFangHK-Regular" size:10];
    [self.contentView addSubview:_conversionNameLabel];
    
    _conversionPriceLabel = [[MFLabel alloc] init];
    _conversionPriceLabel.textAlignment = NSTextAlignmentRight;
    _conversionPriceLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _conversionPriceLabel.font = [UIFont fontWithName:@"ProximaNova Bold" size:14];
    [self.contentView addSubview:_conversionPriceLabel];
    
    _cashPriceLabel = [[MFLabel alloc] init];
    _cashPriceLabel.textAlignment = NSTextAlignmentRight;
    _cashPriceLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _cashPriceLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _cashPriceLabel.font = [UIFont fontWithName:@"HiraginoSans-W3" size:10];
    [self.contentView addSubview:_cashPriceLabel];
    
    MFButton *butDone = [MFButton buttonWithType:UIButtonTypeCustom];
    butDone.layer.cornerRadius = 4.0f;
    butDone.layer.masksToBounds = YES;
    [butDone setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#FF0008"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateNormal];
    [butDone setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#F55E5F"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateHighlighted];
    [butDone setTitleColor:[MFColor colorWithHexString:@"#8ee0ff"] forState:UIControlStateHighlighted];
    [butDone setTitleColor:[MFColor colorWithHexString:@"#8ee0ff"] forState:UIControlStateHighlighted];
    butDone.titleEdgeInsets = UIEdgeInsetsMake(0, -kScreenHeight, 0, 0);
    butDone.titleLabel.font = [UIFont fontWithName:@"Courier" size:12];
    [self.contentView addSubview:butDone];
    _rightButton = butDone;
    __weak typeof(self) weakSelf = self;
    [_rightButton bk_addEventHandler:^(id sender) {
        __strong typeof(self) strongSelf = weakSelf;
        if (strongSelf.cellRightButtonBeClicked) {
            strongSelf.cellRightButtonBeClicked(1);
        }
    } forControlEvents:UIControlEventTouchUpInside];
    
    _bottonLineView = [[MFView alloc] init];
    _bottonLineView.backgroundColor = [MFColor mf_lineColor];
    _bottonLineView.layer.opacity = 0.5;
    [self.contentView addSubview:_bottonLineView];
    
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.left.equalTo(strongSelf.contentView).offset(15);
        make.centerY.equalTo(strongSelf.contentView);
    }];
    [_conversionNameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.left.equalTo(strongSelf.nameLabel.mas_right).offset(8);
        make.centerY.equalTo(strongSelf.nameLabel);
    }];
    [_rightButton mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.right.equalTo(strongSelf.contentView).offset(-15);
        make.centerY.equalTo(strongSelf.contentView);
        make.height.mas_equalTo(25);
        make.width.mas_equalTo(63);
    }];
    [_conversionPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.right.equalTo(strongSelf.rightButton.mas_left).offset(-30);
        make.bottom.mas_equalTo(strongSelf.mas_centerY);
    }];
    [_cashPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.top.equalTo(strongSelf.conversionPriceLabel.mas_bottom).offset(5);
        make.right.equalTo(strongSelf.conversionPriceLabel);
    }];
    [_bottonLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        __strong typeof(self) strongSelf = weakSelf;
        make.left.and.right.and.bottom.equalTo(strongSelf.contentView);
        make.height.mas_equalTo(3);
    }];
    
    //rac绑定
    RAC(self.nameLabel, text) = RACObserve(self, currency.currencyName);
    @weakify(self);
    [RACObserve(self, currency.conversionName) subscribeNext:^(NSString *conversionName) {
        @strongify(self);
        self.conversionNameLabel.text = [NSString stringWithFormat:@"/%@",conversionName];
    }];
    [RACObserve(self, currency.conversionPrice) subscribeNext:^(NSString *conversionPrice) {
        @strongify(self);
        NSString *priceString = [NSString stringWithFormat:@"%@ %@",conversionPrice,self.currency.conversionName];
        NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:priceString];
        [attrStr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:@"Courier-Bold" size:14]
                        range:NSMakeRange(0, conversionPrice.length)];
        [attrStr addAttribute:NSForegroundColorAttributeName
                        value:[MFColor mf_navigationBarTitleColor]
                        range:NSMakeRange(0, conversionPrice.length)];
        [attrStr addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:@"PingFangHK-Regular" size:10]
                        range:NSMakeRange(priceString.length - self.currency.conversionName.length, self.currency.conversionName.length)];
        [attrStr addAttribute:NSForegroundColorAttributeName
                        value:[MFColor mf_fontGrayStyleColor]
                        range:NSMakeRange(priceString.length - self.currency.conversionName.length, self.currency.conversionName.length)];
        self.conversionPriceLabel.attributedText = attrStr;
    }];
    [RACObserve(self, currency.currencyCashPrice) subscribeNext:^(NSString *currencyCashPrice) {
        @strongify(self);
        NSString *unit = self.currency.priceUnit.lowercaseString;
        if ([unit isEqualToString:@"cny"]) {
            self.cashPriceLabel.text = [NSString stringWithFormat:@"≈ ￥ %@ %@",currencyCashPrice,self.currency.priceUnit];
        }else{
            self.cashPriceLabel.text = [NSString stringWithFormat:@"≈ %@ %@",currencyCashPrice,self.currency.priceUnit];
        }
    }];
    [RACObserve(self, currency.changePercent) subscribeNext:^(NSString *changePercent) {
        @strongify(self);
        if ([changePercent hasPrefix:@"-"]) {
            [self.rightButton setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#FF0008"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateNormal];
            [self.rightButton setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#F55E5F"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateHighlighted];
        }else{
            [self.rightButton setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#00DBA9"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateNormal];
        }
        [self.rightButton setTitle:changePercent forState:UIControlStateNormal];
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    self.nameLabel.text = @"";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithCurrency:(MFCurrency *)currency
{
    self.currency = currency;
    
//    self.nameLabel.text = currency.currencyName;
//    self.conversionNameLabel.text = [NSString stringWithFormat:@"/%@",currency.conversionName];
//    
//    NSString *priceString = [NSString stringWithFormat:@"%@ %@",currency.conversionPrice,currency.conversionName];
//    NSMutableAttributedString *attrStr = [[NSMutableAttributedString alloc] initWithString:priceString];
//    [attrStr addAttribute:NSFontAttributeName
//                    value:[UIFont fontWithName:@"Courier-Bold" size:14]
//                    range:NSMakeRange(0, currency.conversionPrice.length)];
//    [attrStr addAttribute:NSForegroundColorAttributeName
//                    value:[MFColor mf_navigationBarTitleColor]
//                    range:NSMakeRange(0, currency.conversionPrice.length)];
//    [attrStr addAttribute:NSFontAttributeName
//                    value:[UIFont fontWithName:@"PingFangHK-Regular" size:10]
//                    range:NSMakeRange(priceString.length - currency.conversionName.length, currency.conversionName.length)];
//    [attrStr addAttribute:NSForegroundColorAttributeName
//                    value:[MFColor mf_fontGrayStyleColor]
//                    range:NSMakeRange(priceString.length - currency.conversionName.length, currency.conversionName.length)];
//    self.conversionPriceLabel.attributedText = attrStr;
//    
//    self.cashPriceLabel.text = [NSString stringWithFormat:@"%@ %@",currency.currencyCashPrice,currency.priceUnit];
//    
//    if ([currency.changePercent hasPrefix:@"-"]) {
//        [self.rightButton setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#FF0008"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateNormal];
//        [self.rightButton setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#F55E5F"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateHighlighted];
//    }else{
//        [self.rightButton setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#00DBA9"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateNormal];
//    }
//    [self.rightButton setTitle:currency.changePercent forState:UIControlStateNormal];
}

@end
