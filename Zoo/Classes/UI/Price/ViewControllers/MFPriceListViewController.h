//
//  MFPriceListViewController.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewController.h"
#import "MFMarco.h"

@interface MFPriceListViewController : MFViewController

- (instancetype)initWithBourseId:(NSString *)bourseId priceType:(MFPriceType)priceType;

@end
