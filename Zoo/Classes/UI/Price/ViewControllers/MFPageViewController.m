//
//  MFPageViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPageViewController.h"
#import "MFPriceListViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFKitBases.h"
#import "MFBourse.h"

#define MENU_HEIGHT 35

#define NAV_HEIGHT [[UIApplication sharedApplication] statusBarFrame].size.height + self.navigationController.navigationBar.frame.size.height

@interface MFPageViewController ()

@property (nonatomic, strong) MFPageViewModel *viewModel;

@end

@implementation MFPageViewController

- (instancetype)initWithPriceType:(MFPriceType)priceType
{
    self = [super init];
    if (self) {
        self.viewModel.priceType = priceType;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [MFColor mf_colorWithHexString:@"F3F3F3"];
    RACSignal *signal = self.viewModel.dataSignal;
    @weakify(self);
    [signal subscribeNext:^(id x) {
        @strongify(self);
        self.viewModel.bourseList = [NSMutableArray arrayWithArray:x];
        [self reloadData];
    }];
    [signal subscribeError:^(NSError *error) {
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

#pragma mark -- 懒加载属性
- (MFPageViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFPageViewModel alloc] init];
    }
    return _viewModel;
}


#pragma mark -- dataSource
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.viewModel.bourseList.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    MFBourse *bourse = [self.viewModel.bourseList objectAtIndex:index];
    return bourse.bourseName;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    MFBourse *bourse = [self.viewModel.bourseList objectAtIndex:index];
    return [[MFPriceListViewController alloc] initWithBourseId:bourse.backend priceType:self.viewModel.priceType];
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width + 20;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    if (self.menuViewPosition == WMMenuViewPositionBottom) {
        menuView.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1.0];
        return CGRectMake(0, self.view.frame.size.height - MENU_HEIGHT, self.view.frame.size.width, MENU_HEIGHT);
    }
    CGFloat leftMargin = self.showOnNavigationBar ? 50 : 0;
    CGRect rect = CGRectMake(leftMargin, 0, self.view.frame.size.width - 2*leftMargin, MENU_HEIGHT);
    return rect;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    if (self.menuViewPosition == WMMenuViewPositionBottom) {
        return CGRectMake(0, NAV_HEIGHT, self.view.frame.size.width, self.view.frame.size.height - NAV_HEIGHT - MENU_HEIGHT);
    }
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}

@end
