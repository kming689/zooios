//
//  MFPriceDetailViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/10.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPriceDetailViewController.h"
#import "MFPriceDetailViewModel.h"
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <BlocksKit/UIControl+BlocksKit.h>
#import "MFKitBases.h"
#import "MFKitMarcro.h"
#import "UIColor+MFAdd.h"
#import "UIImage+MFAdd.h"
#import "MWNormalLineChartView.h"
#import "MFChartModel.h"
#import "MFCurrencyDetail.h"
#import "MFFileCategorySegmentedView.h"

#import "MFPriceDetailIntroTableViewCell.h"
#import "MFPriceDetailNewsTableViewCell.h"
#import "MFPriceDetailRemarkTableViewCell.h"

#import "MFInfomation.h"
#import "MFNewsDetailViewController.h"

static const NSString *kMFPriceDetailIntroTableViewCellIdentifer = @"kMFPriceDetailIntroTableViewCellIdentifer";
static const NSString *kMFPriceDetailNewsTableViewCellIdentifer = @"kMFPriceDetailNewsTableViewCellIdentifer";
static const NSString *kMFPriceDetailRemarkTableViewCellIdentifer = @"kMFPriceDetailRemarkTableViewCellIdentifer";

@interface MFPriceDetailViewController ()<MWNormalLineChartViewDelegate,ChartViewDelegate,MFFileSegmentedControlDelegate,UITableViewDelegate,UITableViewDataSource,IChartAxisValueFormatter>

@property (nonatomic, strong) MFPriceDetailViewModel *viewModel;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@property (nonatomic, strong) UIActivityIndicatorView *bottomIndicatorView;

@property (nonatomic, strong) MFScrollView *scrollBgView;

@property (nonatomic, strong) MFView *contentBackgroudView;

@property (nonatomic, strong) MFView *topBgView;

@property (nonatomic, strong) MFView *chartBgView;

@property (nonatomic, strong) MFView *bottomBgView;

@property (nonatomic, strong) MFFileCategorySegmentedView *chartTitleSegmentView;

@property (nonatomic, strong) MFView *chartTitleValueView;//用于显示手指点击折线的时候选中的值

@property (nonatomic, strong) MFLabel *chartTitleLabel;

@property (nonatomic, strong) MWNormalLineChartView *chartView;

@property (nonatomic, strong) MFView *chartInfoView;

@property (nonatomic, strong) MFFileCategorySegmentedView *bottomTitleSegmentView;

@property (nonatomic, strong) MFTableView *bottomTableView;

@property (nonatomic, strong) MFChartModel *currentChartModel;

@end

@implementation MFPriceDetailViewController
{
    NSInteger _currentBottomType;
    NSTimer *_timer;
}

- (instancetype)initWithCurrency:(MFCurrency *)currency
{
    self = [super init];
    if (self) {
        self.viewModel.bourseId = currency.bourseId;
        self.viewModel.currencyId = currency.symbol;
        self.viewModel.currency = currency;
        [self.viewModel initDetailWithCurrency:currency];
    }
    return self;
}

- (void)dealloc
{
    if (_timer&&_timer.isValid) {
        [_timer invalidate];
    }
    _timer = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.indicatorView startAnimating];
    RACSignal *signal = [self.viewModel.dataCommand execute:[NSNumber numberWithBool:NO]];
    @weakify(self);
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.indicatorView stopAnimating];
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [SVProgressHUD dismiss];
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
}

-(void)configView
{
    [super configView];
    
    @weakify(self);
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_indicatorView];
    [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(kNavigationHeight + 30);
    }];
}

-(void)bindViewModel
{
    [super bindViewModel];
    
    //添加数据刷新回调
    @weakify(self)
    RACSubject *tapSubject = [[RACSubject alloc] init];
    [tapSubject subscribeNext:^(id x) {
        @strongify(self)
        [self.indicatorView stopAnimating];
        //根据网络返回的数据更新界面
        [self setViewWithData];
    }];
    self.viewModel.refreshDelegate = tapSubject;
}

#pragma mark -- 懒加载属性
- (MFPriceDetailViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFPriceDetailViewModel alloc] init];
    }
    return _viewModel;
}

- (MFScrollView *)scrollBgView
{
    if (!_scrollBgView) {
        _scrollBgView = [[MFScrollView alloc] init];
        CGRect rectStatus = [[UIApplication sharedApplication] statusBarFrame];
        CGRect rectNav = self.navigationController.navigationBar.frame;
        _scrollBgView.contentInset = UIEdgeInsetsMake(rectNav.size.height + rectStatus.size.height, 0, 0, 0);
    }
    return _scrollBgView;
}

- (MFView *)contentBackgroudView
{
    if (!_contentBackgroudView) {
        _contentBackgroudView = [[MFView alloc] init];
        _contentBackgroudView.backgroundColor = [MFColor mf_colorWithHexString:@"F6F7F6"];
    }
    return _contentBackgroudView;
}

- (MFView *)topBgView
{
    if (!_topBgView) {
        _topBgView = [[MFView alloc] init];
        _topBgView.backgroundColor = [MFColor whiteColor];
    }
    return _topBgView;
}

- (MFView *)bottomBgView
{
    if (!_bottomBgView) {
        _bottomBgView = [[MFView alloc] init];
        _bottomBgView.backgroundColor = [MFColor whiteColor];
    }
    return _bottomBgView;
}

- (MFView *)chartBgView
{
    if (!_chartBgView) {
        _chartBgView = [[MFView alloc] init];
        _chartBgView.backgroundColor = [MFColor whiteColor];
    }
    return _chartBgView;
}

#pragma mark UI设置

- (void)setViewWithData
{
    @weakify(self);
    [self.view addSubview:self.scrollBgView];
    [_scrollBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.edges.equalTo(self.view);
    }];
    [self.scrollBgView addSubview:self.contentBackgroudView];
    [_contentBackgroudView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.edges.equalTo(self.scrollBgView);
        make.width.equalTo(self.scrollBgView);
    }];
    [self.contentBackgroudView addSubview:self.topBgView];
    [_topBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.equalTo(self.contentBackgroudView);
        make.top.equalTo(self.contentBackgroudView).offset(5);
        make.height.mas_equalTo(65);
    }];
    [self.contentBackgroudView addSubview:self.chartBgView];
    [_chartBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.equalTo(self.contentBackgroudView);
        make.top.equalTo(self.topBgView.mas_bottom).offset(5);
        make.height.mas_equalTo(200);
    }];
    [self.contentBackgroudView addSubview:self.bottomBgView];
    [_bottomBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.equalTo(self.contentBackgroudView);
        make.top.equalTo(self.chartBgView.mas_bottom).offset(5);
        make.bottom.equalTo(self.view.mas_bottom);
    }];
    
    [_contentBackgroudView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.bottom.equalTo(self.bottomBgView.mas_bottom).with.offset(0);
    }];
    
    //设置标题
    MFCurrencyDetail *currencyDetail = self.viewModel.currencyDetail;
    if (currencyDetail.currencyChinaName) {
        self.title = [NSString stringWithFormat:@"%@ %@",currencyDetail.currencyName,currencyDetail.currencyChinaName];
    }else{
        self.title = currencyDetail.currencyName;
    }
    
    [self configTopView];
    [self configChartView];
    [self configBottomView];
}

- (void)configTopView
{
    MFCurrencyDetail *currencyDetail = self.viewModel.currencyDetail;
    //名字
    MFLabel *nameLabel = [[MFLabel alloc] init];
    nameLabel.font = [UIFont fontWithName:@"HiraginoSans-W6" size:10];
    if (currencyDetail.currencyChinaName) {
        nameLabel.text = [NSString stringWithFormat:@"%@ %@",currencyDetail.currencyName,currencyDetail.currencyChinaName];
    }else{
        nameLabel.text = currencyDetail.currencyName;
    }
    [self.topBgView addSubview:nameLabel];
    @weakify(self);
    [nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.topBgView).offset(20);
        make.top.equalTo(self.topBgView).offset(10);
    }];
    //交易所
    MFLabel *bourseLabel = [[MFLabel alloc] init];
    bourseLabel.font = [UIFont fontWithName:@"HiraginoSans-W3" size:10];
    bourseLabel.text = currencyDetail.bourseName;
    [self.topBgView addSubview:bourseLabel];
    [bourseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(nameLabel.mas_right).offset(20);
        make.centerY.equalTo(nameLabel);
    }];
    //价值
    MFLabel *cashPriceLabel = [[MFLabel alloc] init];
    cashPriceLabel.font = [UIFont fontWithName:@"HiraginoSans-W6" size:16];
    cashPriceLabel.text = currencyDetail.currencyCashPrice;
    [self.topBgView addSubview:cashPriceLabel];
    [cashPriceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.topBgView).offset(20);
        make.top.equalTo(nameLabel.mas_bottom).offset(15);
    }];
    //单位
    MFLabel *unitLabel = [[MFLabel alloc] init];
    unitLabel.font = [UIFont fontWithName:@"HiraginoSans-W6" size:10];
    unitLabel.text = currencyDetail.priceUnit;
    [self.topBgView addSubview:unitLabel];
    [unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(cashPriceLabel.mas_right).offset(10);
        make.bottom.equalTo(cashPriceLabel.mas_bottom).offset(-2);
    }];
    //涨跌幅
    MFLabel *changeLabel = [[MFLabel alloc] init];
    changeLabel.font = [UIFont systemFontOfSize:17.0];
    changeLabel.text = currencyDetail.changePercent;
    [self.topBgView addSubview:changeLabel];
    [changeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.topBgView.mas_centerX).offset(20);
        make.bottom.equalTo(cashPriceLabel.mas_bottom);
    }];
    
    //加自选按钮
    MFButton *butDone = [MFButton buttonWithType:UIButtonTypeCustom];
    butDone.layer.cornerRadius = 2.0f;
    butDone.layer.masksToBounds = YES;
    [butDone setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#00DBA9"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateNormal];
//    [butDone setImage:[UIImage imageWithColor:[MFColor colorWithHexString:@"#F55E5F"] size:CGSizeMake(kScreenHeight, kScreenHeight)] forState:UIControlStateHighlighted];
    [butDone setTitleColor:[MFColor colorWithHexString:@"#8ee0ff"] forState:UIControlStateHighlighted];
    [butDone setTitleColor:[MFColor colorWithHexString:@"#8ee0ff"] forState:UIControlStateHighlighted];
    butDone.titleEdgeInsets = UIEdgeInsetsMake(0, -kScreenHeight, 0, 0);
    butDone.titleLabel.font = [UIFont fontWithName:@"PingFangHK-Light" size:10];
    //[butDone setContentEdgeInsets:UIEdgeInsetsMake(0, 5, 0, 5)];
    if (currencyDetail.isFollowed) {
        [butDone setTitle:@"取消自选" forState:UIControlStateNormal];
    }else{
        [butDone setTitle:@"加自选" forState:UIControlStateNormal];
    }
    [self.topBgView addSubview:butDone];
    [butDone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.topBgView).offset(-20);
        make.top.equalTo(nameLabel);
        make.width.mas_equalTo(55);
        make.height.mas_equalTo(20);
    }];
    [butDone bk_addEventHandler:^(id sender) {
        @strongify(self);
        [self onAddCustom:sender];
    } forControlEvents:UIControlEventTouchUpInside];
    
    RAC(cashPriceLabel, text) = RACObserve(self.viewModel.currency, currencyCashPrice);
    RAC(changeLabel, text) = RACObserve(self.viewModel.currency, changePercent);
}

- (void)configChartView
{
    [self configSegmentView];

    [self configChartDataView];
}

- (void)configSegmentView
{
    NSArray *titles = self.viewModel.currencyDetail.chartTitles;
    
    self.chartTitleSegmentView = [[MFFileCategorySegmentedView alloc] initWithTitls:titles delegate:self];
    self.chartTitleSegmentView.translatesAutoresizingMaskIntoConstraints = NO;
    self.chartTitleSegmentView.indicatorBarColor = [MFColor colorWithHexString:@"#00DBA9"];
    self.chartTitleSegmentView.segmentedTitleSelectedColor = [MFColor colorWithHexString:@"#00DBA9"];
    self.chartTitleSegmentView.segmentedTitleFont = [UIFont systemFontOfSize:10.0];
    self.chartTitleSegmentView.backgroundColor = [UIColor whiteColor];
    self.chartTitleSegmentView.index = 0;
    self.chartTitleSegmentView.offsetX = 5;
    self.chartTitleSegmentView.indicatorBarHeight = 0;
    [self.chartBgView addSubview:self.chartTitleSegmentView];
    @weakify(self);
    [self.chartTitleSegmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.chartBgView).offset(10);
        make.top.equalTo(self.chartBgView);
        make.width.equalTo(self.chartBgView.mas_width);
        make.height.mas_equalTo(30);
    }];
    
    [self segmentedControl:_chartTitleSegmentView didSelectIndex:0];
    
    self.chartTitleValueView = [[MFView alloc] init];
    self.chartTitleValueView.hidden = YES;
    self.chartTitleValueView.backgroundColor = [MFColor whiteColor];
    [self.chartTitleSegmentView addSubview:self.chartTitleValueView];
    [self.chartTitleValueView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.edges.equalTo(self.chartTitleSegmentView);
    }];
    self.chartTitleLabel = [[MFLabel alloc] init];
    self.chartTitleLabel.font = [UIFont systemFontOfSize:14.0];
    [self.chartTitleValueView addSubview:self.chartTitleLabel];
    [self.chartTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.and.centerY.equalTo(self.chartTitleValueView);
    }];
}

- (void)configChartDataView
{
    
    MWNormalLineChartView *chartView = [[MWNormalLineChartView alloc] initWithFrame:CGRectZero ChartData:nil Delegate:self];
    _chartView = chartView;
    //横屏模式下让垂直方向scrollview的手势优先，具体方法见gestrue代理函数
    if ([UIApplication sharedApplication].statusBarOrientation != UIInterfaceOrientationPortrait) {
        
    }
    //    chartView.gestrueDelegate = self;
    //    chartView.tag = 50;
    
    NSMutableDictionary *userInfo = [[NSMutableDictionary alloc] init];
    [userInfo setValue:[NSNumber numberWithInt:0] forKey:@"index"];
    chartView.userInfo = userInfo;
    
    [chartView loadChartView];
    
    [self.chartBgView addSubview:chartView];
    
    @weakify(self);
    [chartView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.chartBgView).offset(-20);
        make.right.equalTo(self.chartBgView).offset(10);
        make.top.equalTo(self.chartTitleSegmentView.mas_bottom);
        make.bottom.equalTo(self.chartBgView).offset(-50);
    }];
    
    [self configChartInfoView];
}

- (void)configChartInfoView
{
    //MFCurrencyDetail *currencyDetail = self.viewModel.currencyDetail;
    
    self.chartInfoView = [[MFView alloc] init];
    [self.chartBgView addSubview:self.chartInfoView];
    @weakify(self);
    [self.chartInfoView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.and.bottom.equalTo(self.chartBgView);
        make.top.equalTo(self.chartView.mas_bottom);
    }];
    
    NSArray *tipArray = @[@"最高",@"最低",@"成交量"];
    NSInteger index = 0;
    for (NSString *tip in tipArray) {
        CGFloat by = index*0.33;
        if (by == 0) {
            by = 0.00001;
        }
        MFLabel *tipLabel = [[MFLabel alloc] init];
        tipLabel.font = [UIFont systemFontOfSize:10.0];
        tipLabel.textColor = [MFColor mf_colorWithHexString:@"4A4A4A"];
        tipLabel.text = tip;
        [self.chartInfoView addSubview:tipLabel];
        [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.chartInfoView.mas_right).multipliedBy(by).offset(25);
            make.top.equalTo(self.chartInfoView).offset(15);
        }];
        
        MFLabel *valueLabel = [[MFLabel alloc] init];
        valueLabel.font = [UIFont systemFontOfSize:10.0];
        valueLabel.textColor = [MFColor mf_colorWithHexString:@"4A4A4A"];
        valueLabel.text = @" ";
        [self.chartInfoView addSubview:valueLabel];
        [valueLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.chartInfoView.mas_right).multipliedBy(by).offset(25);
            make.top.equalTo(tipLabel.mas_bottom).offset(3);
            make.height.mas_equalTo(15);
        }];
        
        MFLabel *unitLabel = [[MFLabel alloc] init];
        unitLabel.font = [UIFont fontWithName:@"PingFangHK-Regular" size:8];
        unitLabel.textColor = [MFColor mf_fontGrayStyleColor];
        unitLabel.text = @"";
        [self.chartInfoView addSubview:unitLabel];
        [unitLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(valueLabel.mas_right).offset(5);
            make.centerY.equalTo(valueLabel);
        }];
        
        if ([tip isEqualToString:@"最高"]) {
            unitLabel.text = @"CNY";
            RAC(valueLabel,text) = RACObserve(self, currentChartModel.maxValue);
        }
        if ([tip isEqualToString:@"最低"]) {
            unitLabel.text = @"CNY";
            RAC(valueLabel,text) = RACObserve(self, currentChartModel.minValue);
        }
        if ([tip isEqualToString:@"成交量"]) {
            unitLabel.text = @" ";
            RAC(valueLabel,text) = RACObserve(self, currentChartModel.amount);
        }
        
        index++;
    }
}

- (void)configBottomView
{
    NSArray *titles = @[@"简介",@"新闻"];
    
    _bottomTitleSegmentView = [[MFFileCategorySegmentedView alloc] initWithTitls:titles delegate:self];
    _bottomTitleSegmentView.translatesAutoresizingMaskIntoConstraints = NO;
    _bottomTitleSegmentView.indicatorBarColor = [MFColor colorWithHexString:@"#00DBA9"];
    _bottomTitleSegmentView.segmentedTitleSelectedColor = [MFColor colorWithHexString:@"#00DBA9"];
    _bottomTitleSegmentView.segmentedTitleFont = [UIFont systemFontOfSize:13.0];
    _bottomTitleSegmentView.backgroundColor = [UIColor whiteColor];
    _bottomTitleSegmentView.index = 0;
    _bottomTitleSegmentView.indicatorBarHeight = 4;
    [self.bottomBgView addSubview:_bottomTitleSegmentView];
    @weakify(self);
    [_bottomTitleSegmentView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.bottomBgView).offset(10);
        make.top.equalTo(self.bottomBgView);
        make.width.mas_equalTo(self.bottomBgView.mas_width).multipliedBy(0.60);
        make.height.mas_equalTo(30);
    }];
    
    MFView *lineView = [[MFView alloc] init];
    lineView.backgroundColor = [MFColor mf_lineColor];
    [self.bottomBgView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.equalTo(self.bottomBgView);
        make.top.equalTo(self.bottomTitleSegmentView.mas_bottom);
        make.height.mas_equalTo(0.5);
    }];
    
    _bottomIndicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _bottomIndicatorView.hidesWhenStopped = YES;
    [self.bottomBgView addSubview:_bottomIndicatorView];
    [_bottomIndicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.bottomBgView);
        make.top.equalTo(lineView.mas_bottom).offset(30);
    }];
    
    _bottomTableView = [[MFTableView alloc] init];
    _bottomTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
    footerView.backgroundColor = [MFColor clearColor];
    _bottomTableView.tableFooterView = footerView;
    _bottomTableView.backgroundColor = [MFColor clearColor];
    _bottomTableView.separatorColor = [MFColor mf_lineColor];
    _bottomTableView.sectionIndexColor = [MFColor mf_fontGrayStyleColor];
    _bottomTableView.sectionIndexBackgroundColor =[UIColor clearColor];
    [_bottomTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFPriceDetailIntroTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFPriceDetailIntroTableViewCellIdentifer copy]];
    [_bottomTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFPriceDetailNewsTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFPriceDetailNewsTableViewCellIdentifer copy]];
    [_bottomTableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFPriceDetailRemarkTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFPriceDetailRemarkTableViewCellIdentifer copy]];
    _bottomTableView.delegate = self;
    _bottomTableView.dataSource = self;
    [self.bottomBgView addSubview:_bottomTableView];
    [_bottomTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.equalTo(self.bottomBgView);
        make.top.equalTo(lineView.mas_bottom);
        make.bottom.equalTo(self.bottomBgView);
    }];
}

#pragma mark -- actions
- (void)onAddCustom:(MFButton *)sender
{
    [SVProgressHUD show];
    RACSignal *signal = [self.viewModel.changeStateCommand execute:nil];
    [[signal doNext:^(id x) {
        sender.enabled = NO;
    }] subscribeNext:^(id x) {
        sender.enabled = YES;
        [SVProgressHUD showSuccessWithStatus:@"添加成功"];
    }];
    [signal subscribeError:^(NSError *error) {
        sender.enabled = YES;
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    self.chartTitleValueView.hidden = YES;
}

#pragma mark -- MFFileSegmentedControlDelegate
-(void)segmentedControl:(MFFileCategorySegmentedView *)segmentedControl didSelectIndex:(NSUInteger)index
{
    //切换他图表
    if (segmentedControl == _chartTitleSegmentView) {
        NSString *title = [self.viewModel.currencyDetail.chartTitles objectAtIndex:index];
        for (MFChartModel *chartModel in self.viewModel.currencyDetail.chartModels) {
            if ([chartModel.title isEqualToString:title]) {
                self.currentChartModel = chartModel;
                [_chartView reLoadWithData:chartModel];
                return;
            }
        }
        @weakify(self);
        [SVProgressHUD show];
        [self.viewModel requestPriceDetailChartDataTimeType:title success:^(MFChartModel *chartModel) {
            [SVProgressHUD dismiss];
            @strongify(self);
            self.currentChartModel = chartModel;
            if ([self.viewModel.currencyDetail.chartModels containsObject:chartModel]) {
                NSInteger index = [self.viewModel.currencyDetail.chartModels indexOfObject:chartModel];
                //MFChartModel *originModel = [self.viewModel.currencyDetail.chartModels objectAtIndex:index];
                [self.viewModel.currencyDetail.chartModels replaceObjectAtIndex:index withObject:chartModel];
            }else{
                [self.viewModel.currencyDetail.chartModels addObject:chartModel];
            }
            [_chartView reLoadWithData:chartModel];
            [_bottomTableView reloadData];
        } failure:^(NSError *error) {
            [SVProgressHUD setMinimumDismissTimeInterval:1.5];
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        }];
    }
    //切换简介，新闻，评论
    else{
        _currentBottomType = index;
        //如果是新闻
        if (index == 1) {
            
            if (self.viewModel.currencyDetail.newsList.count > 0) {
                _bottomTableView.hidden = NO;
                [_bottomIndicatorView stopAnimating];
                [self.bottomTableView reloadData];
            }else{
                _bottomTableView.hidden = YES;
                [_bottomIndicatorView startAnimating];
                @weakify(self);
                [self.viewModel requestNewsWithPriceSuccess:^(NSArray *newsList) {
                    @strongify(self);
                    if (self->_currentBottomType == 1) {
                        _bottomTableView.hidden = NO;
                        [_bottomIndicatorView stopAnimating];
                        [self.bottomTableView reloadData];
                    }
                } failure:^(NSError *error) {
                    @strongify(self);
                    if (self->_currentBottomType == 1) {
                        _bottomTableView.hidden = NO;
                        [_bottomIndicatorView stopAnimating];
                        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
                        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
                    }
                }];
            }
        }else{
            _bottomTableView.hidden = NO;
            [_bottomIndicatorView stopAnimating];
            [_bottomTableView reloadData];
        }
    }
}

#pragma mark -- MWChartViewDelegate
- (NSString *)getMarkerData:(NSInteger)xIndex yIndex:(NSInteger)yIndex chartView:(UIView *)chartView
{
    return nil;
}

- (NSString *)getLegend:(NSInteger)lineIndex chartView:(UIView *)chartView
{
    return @"";
}

- (UIColor *)getLineColor:(NSInteger)lineIndex chartView:(UIView *)chartView
{
    return [UIColor colorWithHexString:@"D93A47"];
}

- (BOOL)isDrawCublicLine:(NSInteger)lineIndex chartView:(UIView *)chartView
{
    return YES;
}

#pragma mark -- ChartViewDelegate
- (void)chartValueSelected:(ChartViewBase *)chartView entry:(ChartDataEntry *)entry highlight:(ChartHighlight *)highlight
{
    if (_currentChartModel&&_currentChartModel.xValue.count > entry.x) {
        self.chartTitleValueView.hidden = NO;
        NSString *xValue = [_currentChartModel.xValue objectAtIndex:entry.x];
        NSDate *date = [NSDate dateWithString:xValue format:@"yyyy-MM-dd'T'HH:mm"];
        NSString *lastXvalue = [date stringWithFormat:@"yyyy-MM-dd HH:mm"];
        self.chartTitleLabel.text = lastXvalue;
        [self startTimer];
    }else{
        self.chartTitleValueView.hidden = YES;
    }
}

- (void)chartValueNothingSelected:(ChartViewBase *)chartView
{
    self.chartTitleValueView.hidden = YES;
}

- (NSString *)stringForValue:(double)value axis:(ChartAxisBase *)axis {
    if (_currentChartModel&&_currentChartModel.xValue.count > value) {
        NSString *xValue = [_currentChartModel.xValue objectAtIndex:value];
        NSDate *date = [NSDate dateWithString:xValue format:@"yyyy-MM-dd'T'HH:mm"];
        NSString *lastXvalue = [date stringWithFormat:@"HH:mm"];
        return lastXvalue;
    }
    return [NSString stringWithFormat:@"%i",(int)value];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_currentBottomType == 0) {
        return 1;
    }else{
        return self.viewModel.currencyDetail.newsList.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_currentBottomType == 0) {
        MFPriceDetailIntroTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFPriceDetailIntroTableViewCellIdentifer copy] forIndexPath:indexPath];
        //配置内容
        [cell configCellViewIntro:self.viewModel.currencyDetail];
        
        return cell;
    }else{
        
        MFInfomation *newsInfo = [self.viewModel.currencyDetail.newsList objectAtIndex:indexPath.row];
        
        MFPriceDetailNewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFPriceDetailNewsTableViewCellIdentifer copy] forIndexPath:indexPath];
        //配置内容
        [cell configCellWithNews:newsInfo];
        
        return cell;
    }
}

#pragma mark -- UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (_currentBottomType == 1) {
        MFInfomation *newsInfo = [self.viewModel.currencyDetail.newsList objectAtIndex:indexPath.row];
        MFNewsDetailViewController *detailVC = [[MFNewsDetailViewController alloc] initWithUrl:newsInfo.contentUrl];
        detailVC.title = @"新闻详情";
        [self.navigationController pushViewController:detailVC animated:YES];
    }
}

- (void)startTimer
{
    if (_timer && _timer.isValid) {
        [_timer invalidate];
    }
    
    NSDate *beginDate = [NSDate date];
    _timer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(timerSelector:) userInfo:@{@"beginDate":beginDate} repeats:NO];
}

- (void)timerSelector:(NSTimer *)timer
{
    self.chartTitleValueView.hidden = YES;
}

@end
