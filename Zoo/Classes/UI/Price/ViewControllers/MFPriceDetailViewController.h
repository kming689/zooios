//
//  MFPriceDetailViewController.h
//  Zoo
//
//  Created by tanfameng on 2018/2/10.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewController.h"

@class MFCurrency;

@interface MFPriceDetailViewController : MFViewController

- (instancetype)initWithCurrency:(MFCurrency *)currency;

@end
