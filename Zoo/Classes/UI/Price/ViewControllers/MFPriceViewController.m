//
//  MFPriceViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPriceViewController.h"
#import "MFPriceViewModel.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFPageViewController.h"
#import "MFPriceListViewController.h"
#import "MFLoginViewController.h"
#import "MFTransitionNavigationController.h"
#import "MFColor.h"

#import "MFAccountManager.h"

@interface MFPriceViewController ()

@property (nonatomic, strong) MFPriceViewModel *viewModel;

@property (nonatomic, strong) UISegmentedControl *segmentedControl;

@property (nonatomic, strong) MFViewController *currentVC;

@property (nonatomic, strong) NSMutableArray *controllers;

@property (nonatomic, assign) NSInteger selectedSegmentIndex;

@end

@implementation MFPriceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"行情";
    _controllers = [[NSMutableArray alloc] init];
    [self initControllers];
    
    //根据userDefaults里面存的token来判断初始状态是显示自选还是所有
    MFAccountManager *mgr = [MFAccountManager shareInstance];
    if (mgr.isLogin) {
        _segmentedControl.selectedSegmentIndex = 0;
    }else{
        _segmentedControl.selectedSegmentIndex = 1;
    }

    [self changeViewController:[_controllers objectAtIndex:_segmentedControl.selectedSegmentIndex]];
    
    @weakify(self);
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"kMFPriceTypeChanged" object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        @strongify(self);
        self.segmentedControl.selectedSegmentIndex = 1;
        [self changeViewController:[_controllers objectAtIndex:_segmentedControl.selectedSegmentIndex]];
    }];
}

#pragma mark -- 数据初始化
- (void)initControllers
{
//    MFPageViewController *custom = [[MFPageViewController alloc] initWithPriceType:MFPriceTypeCustom];
//    custom.scrollEnable = NO;
//    custom.titleColorNormal = [MFColor mf_navigationBarTitleColor];
//    custom.titleColorSelected = [MFColor mf_colorWithHexString:@"00DAAA"];
//    custom.titleSizeSelected = 14;
//    custom.titleSizeNormal = 14;
//    custom.menuViewStyle = WMMenuViewStyleLine;
//    custom.automaticallyCalculatesItemWidths = YES;
//    [self customizePageController:custom];
    MFPriceListViewController *custom = [[MFPriceListViewController alloc] init];
    MFPageViewController *all = [[MFPageViewController alloc] initWithPriceType:MFPriceTypeAll];
    all.scrollEnable = NO;
    all.titleColorNormal = [MFColor mf_navigationBarTitleColor];
    all.titleColorSelected = [MFColor mf_colorWithHexString:@"00DAAA"];
    all.titleSizeSelected = 14;
    all.titleSizeNormal = 14;
    all.menuViewStyle = WMMenuViewStyleLine;
    all.automaticallyCalculatesItemWidths = YES;
    [self customizePageController:all];
    [_controllers addObject:custom];
    [_controllers addObject:all];
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
    UISegmentedControl *titleSegment = self.segmentedControl;
    self.navigationItem.titleView = titleSegment;
    titleSegment.selectedSegmentIndex = 0;
}

-(void)configView
{
    [super configView];
}

-(void)bindViewModel
{
    [super bindViewModel];

    @weakify(self);
    [[[[NSNotificationCenter defaultCenter] rac_addObserverForName:@"kMFAccountExit" object:nil] takeUntil:[self rac_willDeallocSignal]] subscribeNext:^(id x) {
        @strongify(self);
        [self changeViewController:[_controllers objectAtIndex:_segmentedControl.selectedSegmentIndex]];
    }];
}

#pragma mark -- 懒加载属性
- (MFPriceViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFPriceViewModel alloc] init];
    }
    return _viewModel;
}

#pragma mark -- UI设置

- (UISegmentedControl *)segmentedControl{
    if (!_segmentedControl) {
        
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"自选",@"行情"]];
        _segmentedControl.tintColor = [MFColor mf_colorWithHexString:@"969796"];
        _segmentedControl.frame = CGRectMake(0, 0, 160, 30);
        [_segmentedControl addTarget:self action:@selector(segmentedControlValueChange:) forControlEvents:UIControlEventValueChanged];
    }
    return _segmentedControl;
}

- (void)changeViewController:(MFViewController *)vc
{
    if (_currentVC) {
        [_currentVC.view removeFromSuperview];
        [_currentVC removeFromParentViewController];
    }
    
    [vc willMoveToParentViewController:self];
    [self addChildViewController:vc];
    [self.view addSubview:vc.view];
    CGRect originBounds = self.view.bounds;
    CGFloat yoffset = 0;
    if (self.navigationController) {
        CGRect rectStatus = [[UIApplication sharedApplication] statusBarFrame];
        CGRect rectNav = self.navigationController.navigationBar.frame;
        yoffset = rectStatus.size.height + rectNav.size.height;
        originBounds.origin.y = yoffset;
    }
    originBounds.size.height = originBounds.size.height - yoffset - 49;
    vc.view.frame = originBounds;
    [vc didMoveToParentViewController:self];
    
    _currentVC = vc;
}
#pragma mark -- actions
-(void) segmentedControlValueChange:(id)sender
{
    UISegmentedControl *seg = (UISegmentedControl *)sender;
    MFAccountManager *accountMgr = [MFAccountManager shareInstance];
    //如果没登录
    if (seg.selectedSegmentIndex == 0&&!accountMgr.isLogin) {
        seg.selectedSegmentIndex = 1;
        MFLoginViewController *controller = [[MFLoginViewController alloc] init];
        controller.successLogined = ^(BOOL success, UIViewController *controller) {
            
            if (success) {
                seg.selectedSegmentIndex = 0;
                _selectedSegmentIndex = seg.selectedSegmentIndex;
                if (_controllers.count > seg.selectedSegmentIndex) {
                    MFViewController *vc = [_controllers objectAtIndex:seg.selectedSegmentIndex];
                    [self changeViewController:vc];
                }
            }
            
            [controller.navigationController dismissViewControllerAnimated:YES completion:^{
                
            }];
        };
        MFTransitionNavigationController *nav = [[MFTransitionNavigationController alloc] initWithRootViewController:controller];
        [self.navigationController presentViewController:nav animated:YES completion:^{
            
        }];
    }else{
        _selectedSegmentIndex = seg.selectedSegmentIndex;
        if (_controllers.count > seg.selectedSegmentIndex) {
            MFViewController *vc = [_controllers objectAtIndex:seg.selectedSegmentIndex];
            [self changeViewController:vc];
        }
    }
}

- (void)customizePageController:(WMPageController *)vc {
    switch (vc.menuViewStyle) {
        case WMMenuViewStyleSegmented:
        case WMMenuViewStyleFlood: {
            vc.titleColorSelected = [UIColor whiteColor];
            vc.titleColorNormal = [UIColor colorWithRed:168.0/255.0 green:20.0/255.0 blue:4/255.0 alpha:1];
            vc.progressColor = [UIColor colorWithRed:168.0/255.0 green:20.0/255.0 blue:4/255.0 alpha:1];
            vc.showOnNavigationBar = YES;
            vc.menuViewLayoutMode = WMMenuViewLayoutModeCenter;
            vc.titleSizeSelected = 15;
        }
            break;
        case WMMenuViewStyleTriangle: {
            vc.progressWidth = 6;
            vc.progressHeight = 4;
            vc.titleSizeSelected = 15;
        }
            break;
        case WMMenuViewStyleDefault: {
            vc.titleSizeSelected = 16;
        }
            break;
        default:
            break;
    }
}

@end
