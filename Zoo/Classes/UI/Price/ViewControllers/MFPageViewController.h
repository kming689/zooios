//
//  MFPageViewController.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import <WMPageController/WMPageController.h>
#import "MFPageViewModel.h"

typedef NS_ENUM(NSUInteger, WMMenuViewPosition) {
    WMMenuViewPositionDefault,
    WMMenuViewPositionBottom,
};

@interface MFPageViewController : WMPageController

@property (nonatomic, assign) WMMenuViewPosition menuViewPosition;

- (instancetype)initWithPriceType:(MFPriceType)priceType;

@end
