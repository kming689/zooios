//
//  MFPriceListViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPriceListViewController.h"
#import "MFPriceListViewModel.h"
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <MJRefresh/MJRefresh.h>
#import <DZNEmptyDataSet/UIScrollView+EmptyDataSet.h>
#import "MFKitBases.h"
#import "MFKitMarcro.h"
#import "MFPriceTableViewCell.h"
#import "MFPriceDetailViewController.h"
#import "MFRouterNavigation.h"

#import "MFCurrency.h"

static const NSString *kMFPriceTableViewCellIdentifer = @"kMFPriceTableViewCellIdentifer";

@interface MFPriceListViewController ()<UITableViewDelegate,UITableViewDataSource,DZNEmptyDataSetSource,DZNEmptyDataSetDelegate>

@property (nonatomic, strong) MFPriceListViewModel *viewModel;

@property (nonatomic, strong) MFTableView *tableView;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation MFPriceListViewController
{
    NSInteger _currentPage;
    BOOL _isPushed;
}

- (instancetype)initWithBourseId:(NSString *)bourseId priceType:(MFPriceType)priceType
{
    self = [super init];
    if (self) {
        self.viewModel.bourseId = bourseId;
        self.viewModel.priceType = priceType;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.indicatorView startAnimating];
    self.tableView.hidden = YES;
    RACSignal *signal = [self.viewModel.dataCommand execute:[NSNumber numberWithInteger:_currentPage]];
    @weakify(self);
    [signal subscribeNext:^(id x) {
        @strongify(self);
        [self.viewModel dealDataWithLoadMore:x];
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
        if (self.viewModel.priceList.count < 20) {
            self.tableView.mj_footer.hidden = YES;
        }else{
            self.tableView.mj_footer.hidden = NO;
        }
    }];
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
    
    //[self.viewModel launchWebSocket];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    if (!_isPushed) {
        [self.viewModel stopWebSocket];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    _isPushed = NO;
    if (!self.viewModel.isConnected) {
        [self.viewModel launchWebSocket];
    }
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
}

-(void)configView
{
    [super configView];
    
    [self configTableView];
    
    [self setupRefresh];
    
    @weakify(self);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.and.top.mas_equalTo(0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_indicatorView];
    [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(30);
    }];
}

-(void)bindViewModel
{
    [super bindViewModel];
    
    //添加数据刷新回调
    @weakify(self)
    RACSubject *tapSubject = [[RACSubject alloc] init];
    [tapSubject subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView reloadData];
    }];
    self.viewModel.refreshDelegate = tapSubject;
}

#pragma mark -- 懒加载属性
- (MFPriceListViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFPriceListViewModel alloc] init];
    }
    return _viewModel;
}

- (MFTableView *)tableView
{
    if (!_tableView) {
        _tableView = [[MFTableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
        footerView.backgroundColor = [MFColor clearColor];
        _tableView.tableFooterView = footerView;
        _tableView.backgroundColor = [MFColor clearColor];
        _tableView.separatorColor = [MFColor mf_lineColor];
        _tableView.sectionIndexColor = [MFColor mf_fontGrayStyleColor];
        _tableView.sectionIndexBackgroundColor =[UIColor clearColor];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFPriceTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFPriceTableViewCellIdentifer copy]];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.emptyDataSetSource = self;
        _tableView.emptyDataSetDelegate = self;
    }
    return _tableView;
}

#pragma mark -- UI设置
- (void)configTableView
{
    [self.view addSubview:self.tableView];
}

- (void)setupRefresh {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshClick:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    //上拉刷新
    @weakify(self);
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        //加载更多数据
        self->_currentPage += 1;
        RACSignal *dataSignal = [self.viewModel.dataCommand execute:[NSNumber numberWithInteger:_currentPage]];
        [dataSignal subscribeNext:^(id x) {
            @strongify(self);
            [self.viewModel dealDataWithLoadMore:x];
            [self.tableView reloadData];
            [self.indicatorView stopAnimating];
            self.tableView.hidden = NO;
        }];
        [dataSignal subscribeCompleted:^{
            // 结束刷新
            [self.tableView.mj_footer endRefreshing];
        }];
        [dataSignal subscribeError:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            // 结束刷新
            [self.tableView.mj_footer endRefreshing];
        }];
    }];
    self.tableView.mj_footer.hidden = YES;
}

#pragma mark -- actions
// 下拉刷新触发，在此获取数据
- (void)refreshClick:(UIRefreshControl *)refreshControl {
    
    RACSignal *dataSignal = [self.viewModel.dataCommand execute:[NSNumber numberWithBool:NO]];
    [dataSignal subscribeCompleted:^{

    }];
    [dataSignal subscribeNext:^(id x) {
        [refreshControl endRefreshing];
        [self.viewModel dealDataWithLoadNew:x];
        [self.tableView reloadData];
    }];
    [dataSignal subscribeError:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        [refreshControl endRefreshing];
    }];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    MFView *headerView = [[MFView alloc] init];
    headerView.translatesAutoresizingMaskIntoConstraints = YES;
    headerView.backgroundColor = [MFColor whiteColor];
    headerView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 22);
    
    MFLabel *tipLabel = [[MFLabel alloc] init];
    tipLabel.textColor = [MFColor mf_fontGrayStyleColor];
    tipLabel.font = [UIFont systemFontOfSize:13.0];
    tipLabel.text = @"名称";
    [headerView addSubview:tipLabel];
    [tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(headerView).offset(15);
        make.centerY.equalTo(headerView);
    }];
    
    MFLabel *priceLabel = [[MFLabel alloc] init];
    priceLabel.textColor = [MFColor mf_fontGrayStyleColor];
    priceLabel.font = [UIFont systemFontOfSize:13.0];
    priceLabel.text = @"最新价";
    [headerView addSubview:priceLabel];
    [priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headerView).offset(-130);
        make.centerY.equalTo(headerView);
    }];
    
    MFLabel *changeLabel = [[MFLabel alloc] init];
    changeLabel.textColor = [MFColor mf_fontGrayStyleColor];
    changeLabel.font = [UIFont systemFontOfSize:13.0];
    changeLabel.text = @"涨跌幅";
    [headerView addSubview:changeLabel];
    [changeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(headerView).offset(-16);
        make.centerY.equalTo(headerView);
    }];
    
    MFView *lineView = [[MFView alloc] init];
    lineView.backgroundColor = [MFColor mf_lineColor];
    [headerView addSubview:lineView];
    [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.equalTo(headerView);
        make.height.mas_equalTo(0.5);
    }];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 25;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel.priceList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MFPriceTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFPriceTableViewCellIdentifer copy] forIndexPath:indexPath];
    //配置内容
    MFCurrency *currency = [self.viewModel.priceList objectAtIndex:indexPath.row];
    cell.cellRightButtonBeClicked = ^(NSInteger index) {
        NSLog(@"====== %li",(long)index);
    };
    [cell configCellWithCurrency:currency];
    
    return cell;
}

#pragma mark -- UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49;
}

- (NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MFCurrency *currency = [self.viewModel.priceList objectAtIndex:indexPath.row];
    NSString *actionTitle = @"";
    NSString *tipMessage = @"";
    if (self.viewModel.priceType == MFPriceTypeAll) {
        if (currency.isFollowed) {
            actionTitle = @" 取消自选 ";
            tipMessage = @"取消自选成功";
        }else{
            actionTitle = @" 添加自选 ";
            tipMessage = @"添加自选成功";
        }
    }else{
        actionTitle = @" 取消自选 ";
        tipMessage = @"取消自选成功";
    }
    
    @weakify(self);
    UITableViewRowAction *deleteAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:actionTitle handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        @strongify(self);
        [self.viewModel changeFollowStatusWithCurrency:currency success:^(id response) {
            if (self.viewModel.priceType == MFPriceTypeCustom) {
                [self.viewModel.priceList removeObjectAtIndex:indexPath.row];
                [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:YES];
            }else{
                [SVProgressHUD showSuccessWithStatus:tipMessage];
            }
        } failure:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        }];
    }];
    return @[deleteAction];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    _isPushed = YES;
    MFCurrency *currency = [self.viewModel.priceList objectAtIndex:indexPath.row];
    
    MFPriceDetailViewController *vc = [[MFPriceDetailViewController alloc] initWithCurrency:currency];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - DZNEmptyDataSet
- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"数据请求失败";
    if (self.viewModel.priceType == MFPriceTypeCustom) {
        text = @"还未添加自选";
    }
    UIFont *font = [UIFont boldSystemFontOfSize:14.0];
    UIColor *textColor = [MFColor mf_colorWithHexString:@"#494c53"];
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    
    if (!text) {
        return nil;
    }
    
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"请检查您的网络设置";
    if (self.viewModel.priceType == MFPriceTypeCustom) {
        text = @"";
    }
    UIFont *font = [UIFont systemFontOfSize:13.0];
    UIColor *textColor = [MFColor mf_colorWithHexString:@"#7a7d83"];
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    
    NSMutableParagraphStyle *paragraph = [NSMutableParagraphStyle new];
    paragraph.lineBreakMode = NSLineBreakByWordWrapping;
    paragraph.alignment = NSTextAlignmentCenter;
    
    if (!text) {
        return nil;
    }
    
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    if (paragraph) [attributes setObject:paragraph forKey:NSParagraphStyleAttributeName];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
    return attributedString;
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    UIImage *image = [UIImage imageNamed:@"" inBundle:[NSBundle bundleForClass:[self class]] compatibleWithTraitCollection:nil];
    return image;
}

- (CAAnimation *)imageAnimationForEmptyDataSet:(UIScrollView *)scrollView
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
    animation.toValue = [NSValue valueWithCATransform3D: CATransform3DMakeRotation(M_PI_2, 0.0, 0.0, 1.0) ];
    animation.duration = 0.25;
    animation.cumulative = YES;
    animation.repeatCount = MAXFLOAT;
    return animation;
}

- (NSAttributedString *)buttonTitleForEmptyDataSet:(UIScrollView *)scrollView forState:(UIControlState)state
{
    NSString *text = @"点击重试";
    if (self.viewModel.priceType == MFPriceTypeCustom) {
        text = @"添加自选";
    }
    UIFont *font = [UIFont boldSystemFontOfSize:13.0];
    UIColor *textColor = [MFColor mf_colorWithHexString:(state == UIControlStateNormal) ? @"#05adff" : @"#6bceff"];
    
    if (!text) {
        return nil;
    }
    
    NSMutableDictionary *attributes = [NSMutableDictionary new];
    if (font) [attributes setObject:font forKey:NSFontAttributeName];
    if (textColor) [attributes setObject:textColor forKey:NSForegroundColorAttributeName];
    
    return [[NSAttributedString alloc] initWithString:text attributes:attributes];
}

#pragma mark - DZNEmptyDataSetDelegate Methods

- (BOOL)emptyDataSetShouldDisplay:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return YES;
}

- (void)emptyDataSet:(UIScrollView *)scrollView didTapButton:(UIButton *)button
{
    if (self.viewModel.priceType == MFPriceTypeCustom) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"kMFPriceTypeChanged" object:nil];
    }else{
        [self.indicatorView startAnimating];
        self.tableView.hidden = YES;
        RACSignal *signal = [self.viewModel.dataCommand execute:[NSNumber numberWithInteger:_currentPage]];
        @weakify(self);
        [signal subscribeNext:^(id x) {
            @strongify(self);
            [self.viewModel dealDataWithLoadMore:x];
            [self.tableView reloadData];
            [self.indicatorView stopAnimating];
            self.tableView.hidden = NO;
        }];
        [signal subscribeError:^(NSError *error) {
            @strongify(self);
            [self.tableView reloadData];
            [self.indicatorView stopAnimating];
            self.tableView.hidden = NO;
            [SVProgressHUD setMinimumDismissTimeInterval:1.5];
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        }];
        
        //[self.viewModel launchWebSocket];
    }
}

@end
