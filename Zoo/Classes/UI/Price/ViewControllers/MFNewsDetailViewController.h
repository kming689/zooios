//
//  MFNewsDetailViewController.h
//  Zoo
//
//  Created by tanfameng on 2018/3/20.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFKitBases.h"//"MFViewController.h"""

@interface MFNewsDetailViewController : MFViewController

- (instancetype)initWithUrl:(NSString *)url;

- (instancetype)initWithHtml:(NSString *)html;

@end
