//
//  MFLoginViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFLoginViewController.h"
#import "MFLoginViewModel.h"
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <BlocksKit/UIControl+BlocksKit.h>
#import <YPNavigationBarTransition/YPNavigationBarTransition.h>
#import "MFKitBases.h"
#import "MFKitMarcro.h"
#import "UIImage+MFAdd.h"
#import "UIColor+MFAdd.h"
#import "WRNavigationBar.h"

#import "MFAccountManager.h"
#import "MFAccount.h"
#import "MFAccountInfo.h"

#define RETRY_WAITING_TIME 60

@interface MFLoginViewController ()<YPNavigationBarConfigureStyle>

@property (nonatomic, strong) MFLoginViewModel *viewModel;

@property (nonatomic, strong) MFTextField *nameTextField;

@property (nonatomic, strong) MFTextField *passTextField;

@property (nonatomic, strong) MFButton *loginButton;

@property (nonatomic, strong) MFButton *findButton;

@property(nonatomic, strong) NSTimer *timer;

@end

@implementation MFLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ((!self.navigationController && self.presentingViewController) || (self.navigationController && [self.navigationController.viewControllers indexOfObject:self] == 0 && self.navigationController.presentingViewController)) {
        UIBarButtonItem *closeButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"login_back"] style:UIBarButtonItemStylePlain target:self action:@selector(onBackButtonDone:)];
        [self.navigationItem setLeftBarButtonItem:closeButtonItem];
    }
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
    [self wr_setNavBarBackgroundAlpha:0];
    [self wr_setNavBarShadowImageHidden:YES];
}

-(void)configView
{
    [super configView];
    self.view.backgroundColor = [MFColor whiteColor];
    //logo
    MFImageView *logoBgView = [[MFImageView alloc] init];
    logoBgView.image = [UIImage imageNamed:@"logoBgImage" bundleClass:[self class]];
    [self.view addSubview:logoBgView];
    MFImageView *logoImageView = [[MFImageView alloc] init];
    logoImageView.image = [UIImage imageNamed:@"logo" bundleClass:[self class]];
    [logoBgView addSubview:logoImageView];
    
    //宣传词
    MFLabel *introLabel = [[MFLabel alloc] init];
    introLabel.textColor = [MFColor mf_colorWithHexString:@"8E8C9B"];
    introLabel.font = [UIFont fontWithName:@"PingFangSC-Regular" size:16];
    NSString *text = @"数字货币投资平台";
    NSDictionary *dic = @{NSKernAttributeName:@2.f
                          };
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:text attributes:dic];
    introLabel.attributedText = attributedString;
    [self.view addSubview:introLabel];
    
    //读取上次登录的用户名跟密码，默认填充
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSString *userName = [userDefaults objectForKey:@"userName"];
    //NSString *passWord = [userDefaults objectForKey:@"passWord"];
    
    //账号
    MFImageView *nameView = [MFImageView new];
    nameView.userInteractionEnabled = YES;
    nameView.image = [UIImage imageNamed:@"inputBgImage"];
    [self.view addSubview:nameView];
    _nameTextField = [[MFTextField alloc] init];
    _nameTextField.font = [UIFont fontWithName:@"PingFangSC-Medium" size:14];
    _nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    NSString *namePlace = @"用户名";
    NSMutableAttributedString *namePlaceholder = [[NSMutableAttributedString alloc] initWithString:namePlace];
    [namePlaceholder addAttribute:NSForegroundColorAttributeName
                        value:[MFColor mf_fontGrayStyleColor]
                        range:NSMakeRange(0, namePlace.length)];
    [namePlaceholder addAttribute:NSFontAttributeName
                        value:[UIFont fontWithName:@"PingFangSC-Medium" size:13]
                        range:NSMakeRange(0, namePlace.length)];
    _nameTextField.attributedPlaceholder = namePlaceholder;
    if (userName) {
        _nameTextField.text = userName;
    }
    [nameView addSubview:_nameTextField];
    
    //密码
    MFImageView *passView = [MFImageView new];
    passView.userInteractionEnabled = YES;
    passView.image = [UIImage imageNamed:@"inputBgImage"];
    [self.view addSubview:passView];
    _passTextField = [[MFTextField alloc] init];
    _passTextField.secureTextEntry = YES;
    _passTextField.font = [UIFont fontWithName:@"PingFangSC-Medium" size:14];
    _passTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    NSString *passPlace = @"输入验证码";
    NSMutableAttributedString *passPlaceholder = [[NSMutableAttributedString alloc] initWithString:passPlace];
    [passPlaceholder addAttribute:NSForegroundColorAttributeName
                            value:[MFColor mf_fontGrayStyleColor]
                            range:NSMakeRange(0, passPlace.length)];
    [passPlaceholder addAttribute:NSFontAttributeName
                            value:[UIFont fontWithName:@"PingFangSC-Medium" size:13]
                            range:NSMakeRange(0, passPlace.length)];
    _passTextField.attributedPlaceholder = passPlaceholder;
//    if (passWord) {
//        _passTextField.text = passWord;
//    }
    [passView addSubview:_passTextField];
    
    //注册按钮
    MFButton *registerBtn = [MFButton buttonWithType:UIButtonTypeSystem];
    registerBtn.tintColor = [MFColor mf_colorWithHexString:@"4A4A4A"];
    registerBtn.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    registerBtn.titleLabel.font = [UIFont fontWithName:@"PingFangHK-Light" size:13];
    [registerBtn setTitle:@"" forState:UIControlStateNormal];
    [self.view addSubview:registerBtn];
    @weakify(self);
    [registerBtn bk_addEventHandler:^(id sender) {
        @strongify(self);
        [self onRegister];
    } forControlEvents:UIControlEventTouchUpInside];
    
    //找回密码
    MFButton *findBtn = [MFButton buttonWithType:UIButtonTypeSystem];
    _findButton = findBtn;
    findBtn.layer.cornerRadius = 13;
    findBtn.layer.masksToBounds = YES;
    UIImage *getCodeImage = [UIImage imageWithColor:[MFColor mf_colorWithHexString:@"00DAAA"] size:CGSizeMake(100, 30)];
    [findBtn setBackgroundImage:getCodeImage forState:UIControlStateNormal];
    [findBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    findBtn.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:11];
    [findBtn setTitleColor:[MFColor whiteColor] forState:UIControlStateNormal];
    [passView addSubview:findBtn];
    
    //登录按钮
    MFButton *butDone = [MFButton buttonWithType:UIButtonTypeCustom];
    [butDone setBackgroundImage:[UIImage imageNamed:@"loginBtnImage"] forState:UIControlStateNormal];
    butDone.titleLabel.font = [UIFont fontWithName:@"PingFangSC-Medium" size:18];
    [butDone setTitle:@"登 录" forState:UIControlStateNormal];
    _loginButton = butDone;
    [self.view addSubview:butDone];
    
    [logoBgView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(kNavigationHeight + 30);
    }];
    [logoImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.and.centerY.equalTo(logoBgView);
    }];
    [introLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.view.mas_bottom).multipliedBy(0.3);
        make.centerX.equalTo(self.view);
    }];
    [nameView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(introLabel.mas_bottom).offset(30);
        make.left.equalTo(self.view).offset(46);
        make.right.equalTo(self.view).offset(-46);
        make.height.mas_equalTo(44);
    }];
    [_nameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.equalTo(nameView);
        make.left.equalTo(nameView).offset(16);
        make.right.equalTo(nameView).offset(-10);
    }];
    [passView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(nameView.mas_bottom).offset(10);
        make.left.and.right.and.height.equalTo(nameView);
    }];
    [_passTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.and.bottom.equalTo(passView);
        make.left.equalTo(passView).offset(16);
        make.right.equalTo(findBtn.mas_left).offset(-5);
    }];
    [registerBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(passView.mas_bottom).offset(20);
        make.left.equalTo(passView);
        make.size.mas_equalTo(CGSizeMake(10, 20));
    }];
    [findBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(passView);
        make.right.equalTo(passView).offset(-5);
        make.size.mas_equalTo(CGSizeMake(80, 26));
    }];
    [_loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(nameView);
        make.top.equalTo(passView.mas_bottom).offset(30);
        make.height.mas_equalTo(44);
    }];
}

-(void)bindViewModel
{
    [super bindViewModel];
    @weakify(self);
    RAC(self.viewModel, userName) = self.nameTextField.rac_textSignal;
    RAC(self.viewModel, passWord) = self.passTextField.rac_textSignal;
    
    [self.viewModel.validCodeSignal subscribeNext:^(NSNumber *x) {
        @strongify(self);
        self.findButton.enabled = x.boolValue;
    }];
    //获取验证码
    self.findButton.rac_command = self.viewModel.getCodeCommand;
    //错误
    [[RACSignal
      merge:@[self.viewModel.getCodeCommand.errors]]
     subscribeNext:^(NSError *error) {
         [SVProgressHUD setMaximumDismissTimeInterval:1.5];
         [SVProgressHUD showErrorWithStatus:error.localizedDescription];
     }];
    //成功
    [[[self.viewModel.getCodeCommand executionSignals] switchToLatest] subscribeNext:^(id x) {
        @strongify(self)
        //自动换行
        [self.passTextField becomeFirstResponder];
    }];
    
    [[RACObserve(self.viewModel, countDownTime) filter:^BOOL(NSString *value) {
        return value.length > 0;
    }] subscribeNext:^(NSString *value) {
        @strongify(self);
        [self.findButton setTitle:value forState:UIControlStateNormal];
    }];
    
    
    [[[RACSignal
       merge:@[self.viewModel.loginCommand.executing]]
      doNext:^(id x) {
          @strongify(self);
          [self.view endEditing:YES];
      }]
     subscribeNext:^(NSNumber *executing) {
         if (executing.boolValue) {
             [SVProgressHUD showWithStatus:@"正在登录"];
         } else {
             //[SVProgressHUD dismiss];
         }
     }];
    
    [[RACSignal
      merge:@[self.viewModel.loginCommand.errors]]
     subscribeNext:^(NSError *error) {
         self.loginButton.enabled = YES;
         [SVProgressHUD setMaximumDismissTimeInterval:1.5];
         [SVProgressHUD showErrorWithStatus:error.localizedDescription];
     }];
    
    RAC(self.loginButton,enabled) = self.viewModel.validLoginSignal;
    [[[self.loginButton rac_signalForControlEvents:UIControlEventTouchUpInside] doNext:^(id x) {
        @strongify(self);
        self.loginButton.enabled = NO;
    }] subscribeNext:^(id x) {
        @strongify(self);
        RACSignal *signal = [self.viewModel.loginCommand execute:nil];
        [signal subscribeNext:^(id x) {
            //保存用户名跟密码
            [SVProgressHUD dismiss];
            self.loginButton.enabled = YES;
            NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
            [userDefaults setObject:_nameTextField.text forKey:@"userName"];
            [userDefaults setObject:_passTextField.text forKey:@"passWord"];
            [userDefaults synchronize];
            if (self.successLogined) {
                self.successLogined(YES, self);
            }
        }];
    }];
}

#pragma mark -- 懒加载属性
- (MFLoginViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFLoginViewModel alloc] init];
    }
    return _viewModel;
}

#pragma mark - YPNavigationBarConfigureStyle
-(YPNavigationBarConfigurations)yp_navigtionBarConfiguration
{
    YPNavigationBarConfigurations conf = YPNavigationBarConfigurationsDefault;
    conf |= YPNavigationBarBackgroundStyleTransparent;
    conf |= YPNavigationBarBackgroundStyleNone;
    return conf;
}

-(UIColor *)yp_navigationBarTintColor
{
    return [UIColor whiteColor];
}

#pragma mark -- actions
- (void)onRegister
{

}

- (void)onBackButtonDone:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{

    }];
}

@end
