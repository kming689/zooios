//
//  MFLoginViewController.h
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewController.h"

@interface MFLoginViewController : MFViewController

@property(nonatomic, copy) void(^successLogined)(BOOL success,UIViewController *controller);

@end
