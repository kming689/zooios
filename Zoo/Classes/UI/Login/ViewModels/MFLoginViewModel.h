//
//  MFLoginViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"

@class RACSubject;
@class RACCommand;
@class RACSignal;

@interface MFLoginViewModel : MFViewModel

@property (nonatomic, strong) NSString *userName;

@property (nonatomic, strong) NSString *passWord;

@property (nonatomic, strong, readonly) RACSignal *validLoginSignal;

@property (nonatomic, strong, readonly) RACSignal *validCodeSignal;

@property (nonatomic, strong, readonly) RACCommand *loginCommand;

@property (nonatomic, strong, readonly) RACCommand *getCodeCommand;

@property (nonatomic, strong) NSString *countDownTime;//倒计时秒数


@end
