//
//  MFLoginViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFLoginViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFNetRequestBusiness.h"
#import "NSDate+MFAdd.h"
#import "NSString+MFAdd.h"

#import "MFAccountManager.h"
#import "MFAccount.h"

#define RETRY_WAITING_TIME 60

@interface MFLoginViewModel()

@property (nonatomic, strong, readwrite) RACSignal *validLoginSignal;

@property (nonatomic, strong, readwrite) RACSignal *validCodeSignal;

@property (nonatomic, strong, readwrite) RACCommand *loginCommand;

@property (nonatomic, strong, readwrite) RACCommand *getCodeCommand;

@property(nonatomic, assign) NSInteger validateCodeRetryTime;

@property(nonatomic, strong) NSTimer *timer;

@end

@implementation MFLoginViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.countDownTime = @"获取验证码";
    }
    return self;
}

-(void)dealloc
{
    if (_timer && _timer.isValid) {
        [_timer invalidate];
    }
    _timer = nil;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];
    @weakify(self);
    //获取验证码按钮是否有效
    self.validCodeSignal = [[[RACObserve(self, userName) takeUntil:[self rac_willDeallocSignal]] map:^id(NSString *value) {
        return @([value verifyPhoneNumber]);
    }] distinctUntilChanged];
    //提交按钮是否有效，合并两项检测，要获取验证码按钮有效，且输入了验证码，提交按钮才有效
    self.validLoginSignal = [[RACSignal combineLatest:@[_validCodeSignal, RACObserve(self, passWord)]
                                            reduce:^(NSNumber *codeValid, NSString *codeNumber) {
                                                return @(codeValid.boolValue && codeNumber.length > 0);
                                            }] distinctUntilChanged];
    
    self.loginCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
        
        RACSignal *loginSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            @strongify(self)
            //登录
            NSURLSessionTask *task = [self loginSuccess:^(BOOL success) {
                [subscriber sendNext:[NSNumber numberWithBool:YES]];
                [subscriber sendCompleted];
            } failure:^(NSError *error) {
                [subscriber sendError:error];
            }];
            
            return [RACDisposable disposableWithBlock:^{
                //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                [task cancel];
            }];
        }];
        return loginSignal;
    }];
}

#pragma mark -- 懒加载属性

- (RACCommand *)getCodeCommand
{
    if (_getCodeCommand) {
        return _getCodeCommand;
    }
    
    RACSignal *retrySignal = [[RACObserve(self, validateCodeRetryTime) takeUntil:[self rac_willDeallocSignal]] map:^id(NSNumber *value) {
        return @([value integerValue] == 0);
    }];
    
    RACSignal *enabledSignal = [RACSignal combineLatest:@[[self validCodeSignal],retrySignal] reduce:^id(NSNumber *phoneNumber,NSNumber *codeRetry){
        return @([phoneNumber boolValue] && [codeRetry boolValue]);
    }];
    
    @weakify(self);
    _getCodeCommand = [[RACCommand alloc] initWithEnabled:enabledSignal signalBlock:^RACSignal *(id input) {
        return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            @strongify(self);
            //开始获取
            self.countDownTime = @"正在获取...";
            
            //请求获取
            NSURLSessionTask *task = [self getCodeSuccess:^(NSString *token) {
                [self startCodeRetryWaiting];
                [subscriber sendNext:token];
                [subscriber sendCompleted];
            } failure:^(NSError *error) {
                self.countDownTime = @"获取验证码";
                self.validateCodeRetryTime = 0;
                [subscriber sendError:error];
                [subscriber sendError:error];
            }];
            return [RACDisposable disposableWithBlock:^{
                //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
            }];
        }];
    }];
    return _getCodeCommand;
}

#pragma mark -- 业务方法
//登录
- (NSURLSessionTask *)loginSuccess:(void (^)(BOOL success))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    return [business registerWithUserName:self.userName passWord:self.passWord success:^(id response) {
        if (completion) {
            completion(YES);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

//获取验证码
- (NSURLSessionTask *)getCodeSuccess:(void (^)(NSString *token))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    return [business getRegisterCodeWithMobile:self.userName success:^(NSString *code) {
        if (completion) {
            completion(code);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -- 返回数据处理
- (void)dealData:(NSArray *)data
{

}

#pragma mark -- private
-(void) startCodeRetryWaiting
{
    self.validateCodeRetryTime = RETRY_WAITING_TIME;
    self.countDownTime = [NSString stringWithFormat:@"%zds",self.validateCodeRetryTime];
    
    if (_timer && _timer.isValid) {
        [_timer invalidate];
    }
    
    NSDate *beginDate = [NSDate date];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerSelector:) userInfo:@{@"beginDate":beginDate} repeats:YES];
}

-(void) timerSelector:(NSTimer *) timer
{
    NSDate *beginDate = [[timer userInfo] objectForKey:@"beginDate"];
    NSDate *date = [NSDate date];
    NSTimeInterval time = [date timeIntervalSinceDate:beginDate];
    if (time >= RETRY_WAITING_TIME) {
        [self.timer invalidate];
        self.validateCodeRetryTime = 0;
        self.countDownTime = @"获取验证码";
    }else{
        self.countDownTime = [NSString stringWithFormat:@"%.0fs",RETRY_WAITING_TIME - time];
    }
}

@end
