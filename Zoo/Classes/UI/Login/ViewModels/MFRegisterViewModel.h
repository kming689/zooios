//
//  MFRegisterViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"

@class RACSubject;
@class RACCommand;
@class RACSignal;

@interface MFRegisterViewModel : MFViewModel

@property (nonatomic, strong) NSString *userName;

@property (nonatomic, strong) NSString *code;

@property (nonatomic, strong, readonly) RACSignal *validLoginSignal;

@property (nonatomic, strong, readonly) RACCommand *registerCommand;

@end
