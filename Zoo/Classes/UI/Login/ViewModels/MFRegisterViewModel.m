//
//  MFRegisterViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFRegisterViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFNetRequestBusiness.h"

@interface MFRegisterViewModel()

@property (nonatomic, strong, readwrite) RACSignal *validLoginSignal;

@property (nonatomic, strong, readwrite) RACCommand *registerCommand;

@end

@implementation MFRegisterViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];
    
    self.validLoginSignal = [[RACSignal
                              combineLatest:@[RACObserve(self, userName), RACObserve(self, code)]
                              reduce:^(NSString *username, NSString *code) {
                                  return @(username.length > 0 && code.length > 0);
                              }]
                             distinctUntilChanged];
    
    @weakify(self);
    self.registerCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
        
        RACSignal *loginSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
            @strongify(self)
            //登录
            NSURLSessionTask *task = [self loginSuccess:^(NSString *token) {
                [subscriber sendNext:token];
                [subscriber sendCompleted];
            } failure:^(NSError *error) {
                [subscriber sendError:error];
            }];
            
            return [RACDisposable disposableWithBlock:^{
                //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                [task cancel];
            }];
        }];
        return loginSignal;
    }];
}

#pragma mark -- 懒加载属性

#pragma mark -- 业务方法
- (NSURLSessionTask *)loginSuccess:(void (^)(NSString *token))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    return [business loginWithUserName:self.userName passWord:self.code success:^(NSString *token) {
        if (completion) {
            completion(token);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -- 返回数据处理
- (void)dealData:(NSArray *)data
{
    
}

@end
