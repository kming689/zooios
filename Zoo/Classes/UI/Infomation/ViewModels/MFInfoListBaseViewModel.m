//
//  MFInfoListBaseViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFInfoListBaseViewModel.h"
#import "MFInfomation.h"

@implementation MFInfoListBaseViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _infoList = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];

}

#pragma mark -- public
- (void)dealDataWithLoadMore:(NSArray<MFInfomation *> *)infos
{
    [self.infoList addObjectsFromArray:infos];
}

- (void)dealDataWithLoadNew:(NSArray<MFInfomation *> *)infos
{
    int count = (int)infos.count;
    for (int i = count-1; i >=0 ; i--) {
        MFInfomation *info = [infos objectAtIndex:i];
        if (![self.infoList containsObject:info]) {
            [self.infoList insertObject:info atIndex:0];
        }
    }
}

@end
