//
//  MFInfoPageViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"

@class RACSignal;
@class RACSubject;
@class MFInfoType;

@interface MFInfoPageViewModel : MFViewModel

@property (nonatomic, strong) NSArray<MFInfoType *> *infoTypes;

@property (nonatomic, strong) RACSignal *dataSignal;

@property (nonatomic, strong) RACSubject *refreshDelegate;//刷新界面的回调

@end
