//
//  MFMediaInfoViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFInfoListBaseViewModel.h"

@class RACSubject;
@class RACCommand;
@class RACSignal;
@class MFMediaInfo;

@interface MFMediaInfoViewModel : MFInfoListBaseViewModel

@property (nonatomic, strong) RACCommand *dataCommand;

@property (nonatomic, strong) RACSignal *dataSignal;

@property (nonatomic, strong) RACSubject *refreshDelegate;//刷新界面的回调

@end
