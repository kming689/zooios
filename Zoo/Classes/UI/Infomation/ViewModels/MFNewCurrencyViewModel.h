//
//  MFNewCurrencyViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"

@class RACSubject;
@class RACCommand;
@class RACSignal;
@class MFNewCurrency;

@interface MFNewCurrencyViewModel : MFViewModel

@property (nonatomic ,strong) NSMutableArray<MFNewCurrency *> *currencyList;

@property (nonatomic, strong) RACCommand *dataCommand;

@property (nonatomic, strong) RACSignal *dataSignal;

- (void)dealDataWithLoadMore:(NSArray<MFNewCurrency *> *)infos;

- (void)dealDataWithLoadNew:(NSArray<MFNewCurrency *> *)infos;

@end
