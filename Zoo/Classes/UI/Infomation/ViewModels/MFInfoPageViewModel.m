//
//  MFInfoPageViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFInfoPageViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFInfoType.h"
#import "MFNetRequestBusiness.h"

@implementation MFInfoPageViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];
}

#pragma mark -- 懒加载属性
- (RACSignal *)dataSignal
{
    @weakify(self);
    RACSignal *coldSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        //请求数据
        [self requestInfoTypeSuccess:^(NSArray<MFInfoType *> *infoTypes) {
            [subscriber sendNext:infoTypes];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求

        }];
    }];
    RACSubject *subject = [RACSubject subject];
    RACMulticastConnection *multicastConnection = [coldSignal multicast:subject];
    RACSignal *hotSignal = multicastConnection.autoconnect;
    return hotSignal;
}

#pragma mark -- 业务方法
- (NSURLSessionTask *)requestInfoTypeSuccess:(void (^)(NSArray<MFInfoType *> *infoTypes))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    return [business requestInfoTypeSuccess:^(NSArray<MFInfoType *> *infoTypes) {
        if (completion) {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            MFInfoType *bourseType = [[MFInfoType alloc] init];
            bourseType.typeName = @"交易所";
            MFInfoType *newCurrencyType = [[MFInfoType alloc] init];
            newCurrencyType.typeName = @"新币";
            [array addObjectsFromArray:infoTypes];
            [array addObject:bourseType];
            [array addObject:newCurrencyType];
            completion(array);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -- 返回数据处理
- (void)dealData:(NSArray *)data
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    MFInfoType *bourseType = [[MFInfoType alloc] init];
    bourseType.typeName = @"交易所";
    MFInfoType *newCurrencyType = [[MFInfoType alloc] init];
    newCurrencyType.typeName = @"新币";
    [array addObjectsFromArray:data];
    [array addObject:bourseType];
    [array addObject:newCurrencyType];
    _infoTypes = array;
    [_refreshDelegate sendNext:array];
}

@end
