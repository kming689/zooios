//
//  MFBourseDetailViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"

@class RACSubject;
@class RACCommand;
@class RACSignal;
@class MFBourseDetail;

@interface MFBourseDetailViewModel : MFViewModel

@property (nonatomic, strong) MFBourseDetail *bourseDetail;

@property (nonatomic, strong) NSString *bourseId;//传入参数，交易所ID

@property (nonatomic, strong) RACCommand *dataCommand;//参数为交易所ID

@property (nonatomic, strong) RACSignal *dataSignal;

@property (nonatomic, strong) RACSubject *refreshDelegate;//刷新界面的回调

@end
