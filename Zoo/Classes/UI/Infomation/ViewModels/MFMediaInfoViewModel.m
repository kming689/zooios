//
//  MFMediaInfoViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFMediaInfoViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFNetRequestBusiness.h"

#import "MFMediaInfo.h"

@implementation MFMediaInfoViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];

}

#pragma mark -- 懒加载属性
- (RACSignal *)dataSignal
{
    @weakify(self);
    RACSignal *coldSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        //请求数据
        NSURLSessionTask *task = [self requestInfoListWithPage:1 success:^(NSArray<MFMediaInfo *> *infoList) {
            [subscriber sendNext:infoList];
            [subscriber sendCompleted];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
            [task cancel];
        }];
    }];
    RACSubject *subject = [RACSubject subject];
    RACMulticastConnection *multicastConnection = [coldSignal multicast:subject];
    RACSignal *hotSignal = multicastConnection.autoconnect;
    return hotSignal;
}

- (RACCommand *)dataCommand
{
    if (!_dataCommand) {
        @weakify(self)
        _dataCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //获取媒体信息列表
                NSInteger page = 1;
                if (input) {
                    page = ((NSNumber *)input).integerValue;
                }
                NSURLSessionTask *task = [self requestInfoListWithPage:page success:^(NSArray<MFMediaInfo *> *infoList) {
                    [subscriber sendNext:infoList];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                    [task cancel];
                }];
            }];
            
            return dataSignal;
        }];
    }
    
    return _dataCommand;
}

#pragma mark -- 业务方法
- (NSURLSessionTask *)requestInfoListWithPage:(NSInteger)page success:(void (^)(NSArray<MFMediaInfo *> *infoList))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    return [business requestMediaInfoListWithInfoTypeId:self.infoTypeId page:page pageSize:10 success:^(NSArray<MFMediaInfo *> *infoList) {
        if (completion) {
            completion(infoList);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -- 返回数据处理
- (void)dealData:(NSArray *)data
{

}

@end
