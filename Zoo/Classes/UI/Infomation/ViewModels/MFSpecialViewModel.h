//
//  MFSpecialViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFInfoListBaseViewModel.h"

@class RACSubject;
@class RACCommand;
@class RACSignal;

@interface MFSpecialViewModel : MFInfoListBaseViewModel

@property (nonatomic, strong) RACCommand *dataCommand;

@property (nonatomic, strong) RACSignal *dataSignal;

@end
