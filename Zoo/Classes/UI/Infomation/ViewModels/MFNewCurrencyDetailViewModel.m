//
//  MFNewCurrencyDetailViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFNewCurrencyDetailViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFNetRequestBusiness.h"
#import "MFNewCurrencyDetail.h"

@implementation MFNewCurrencyDetailViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {

    }
    return self;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];
    @weakify(self)
    [[[self.dataCommand executionSignals] switchToLatest] subscribeNext:^(id x) {
        @strongify(self)
        [self dealData:x];
    }];
}

#pragma mark -- 懒加载属性
- (RACSignal *)dataSignal
{
    @weakify(self);
    RACSignal *coldSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        //请求数据
        NSURLSessionTask *task = [self requestCurrencyDetailSuccess:^(MFNewCurrencyDetail *currencyDetail) {
            [subscriber sendNext:currencyDetail];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
            [task cancel];
        }];
    }];
    RACSubject *subject = [RACSubject subject];
    RACMulticastConnection *multicastConnection = [coldSignal multicast:subject];
    RACSignal *hotSignal = multicastConnection.autoconnect;
    return hotSignal;
}

- (RACCommand *)dataCommand
{
    if (!_dataCommand) {
        @weakify(self)
        _dataCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //获取新币详情
                NSURLSessionTask *task = [self requestCurrencyDetailSuccess:^(MFNewCurrencyDetail *currencyDetail) {
                    [subscriber sendNext:currencyDetail];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                    [task cancel];
                }];
            }];
            
            return dataSignal;
        }];
    }
    
    return _dataCommand;
}

#pragma mark -- 业务方法
- (NSURLSessionTask *)requestCurrencyDetailSuccess:(void (^)(MFNewCurrencyDetail *currencyDetail))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    return [business requestNewCurrencyDetailWithId:self.currencyId success:^(MFNewCurrencyDetail *currencyDetail) {
        if (completion) {
            completion(currencyDetail);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -- 返回数据处理
- (void)dealData:(id)data
{
    _currencyDetail = data;
    [_refreshDelegate sendNext:data];
}

@end
