//
//  MFNewCurrencyDetailViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"

@class RACSubject;
@class RACCommand;
@class RACSignal;
@class MFNewCurrencyDetail;

@interface MFNewCurrencyDetailViewModel : MFViewModel

@property (nonatomic, strong) NSString *currencyId;//参数，由外部传递

@property (nonatomic, strong) MFNewCurrencyDetail *currencyDetail;

@property (nonatomic, strong) RACCommand *dataCommand;

@property (nonatomic, strong) RACSignal *dataSignal;

@property (nonatomic, strong) RACSubject *refreshDelegate;//刷新界面的回调

@end
