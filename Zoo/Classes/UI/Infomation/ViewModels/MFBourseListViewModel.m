//
//  MFBourseListViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseListViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFNetRequestBusiness.h"
#import "MFBourse.h"

@implementation MFBourseListViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _bourseList = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];
    @weakify(self)
    [[[self.dataCommand executionSignals] switchToLatest] subscribeNext:^(id x) {
        @strongify(self)
        [self dealData:x];
    }];
}

#pragma mark -- 懒加载属性
- (RACSignal *)dataSignal
{
    @weakify(self);
    RACSignal *coldSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        //请求数据
        NSURLSessionTask *task = [self requestBourseListSuccess:^(NSArray<MFBourse *> *bourseList) {
            [subscriber sendNext:bourseList];
        } failure:^(NSError *error) {
            [subscriber sendNext:error];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
            [task cancel];
        }];
    }];
    RACSubject *subject = [RACSubject subject];
    RACMulticastConnection *multicastConnection = [coldSignal multicast:subject];
    RACSignal *hotSignal = multicastConnection.autoconnect;
    return hotSignal;
}

- (RACCommand *)dataCommand
{
    if (!_dataCommand) {
        @weakify(self)
        _dataCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //获取交易所列表
                NSURLSessionTask *task = [self requestBourseListSuccess:^(NSArray<MFBourse *> *bourseList) {
                    [subscriber sendNext:bourseList];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendNext:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                    [task cancel];
                }];
            }];
            
            return dataSignal;
        }];
    }
    
    return _dataCommand;
}

#pragma mark -- 业务方法
- (NSURLSessionTask *)requestBourseListSuccess:(void (^)(NSArray<MFBourse *> *bourseList))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    return [business requestBourseListWithOptionType:nil success:^(NSArray<MFBourse *> *bourseList) {
        if (completion) {
            completion(bourseList);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -- 返回数据处理
- (void)dealData:(NSArray *)data
{
    _bourseList = [NSMutableArray arrayWithArray:data];
    [_refreshDelegate sendNext:data];
}

@end
