//
//  MFNewCurrencyViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFNewCurrencyViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFNetRequestBusiness.h"
#import "MFNewCurrency.h"

@implementation MFNewCurrencyViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _currencyList = [[NSMutableArray alloc] init];
    }
    return self;
}

#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];
}

#pragma mark -- 懒加载属性
- (RACSignal *)dataSignal
{
    @weakify(self);
    RACSignal *coldSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        //请求数据
        NSURLSessionTask *task = [self requestCurrencyListPage:1 pageSize:10 success:^(NSArray<MFNewCurrency *> *currencyList) {
            [subscriber sendNext:currencyList];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
            [task cancel];
        }];
    }];
    RACSubject *subject = [RACSubject subject];
    RACMulticastConnection *multicastConnection = [coldSignal multicast:subject];
    RACSignal *hotSignal = multicastConnection.autoconnect;
    return hotSignal;
}

- (RACCommand *)dataCommand
{
    if (!_dataCommand) {
        @weakify(self)
        _dataCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //获取新币列表
                NSInteger page = 1;
                if (input) {
                    page = ((NSNumber *)input).integerValue;
                }
                NSURLSessionTask *task = [self requestCurrencyListPage:page pageSize:10 success:^(NSArray<MFNewCurrency *> *currencyList) {
                    [subscriber sendNext:currencyList];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                    [task cancel];
                }];
            }];
            
            return dataSignal;
        }];
    }
    
    return _dataCommand;
}

#pragma mark -- 业务方法
- (NSURLSessionTask *)requestCurrencyListPage:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFNewCurrency *> *currencyList))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    return [business requestNewCurrencyListPage:page pageSize:pageSize success:^(NSArray<MFNewCurrency *> *currencyList) {
        if (completion) {
            completion(currencyList);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

#pragma mark -- 返回数据处理
- (void)dealData:(NSArray *)data
{

}

#pragma mark -- public
- (void)dealDataWithLoadMore:(NSArray<MFNewCurrency *> *)infos
{
    [self.currencyList addObjectsFromArray:infos];
}

- (void)dealDataWithLoadNew:(NSArray<MFNewCurrency *> *)infos
{
    int count = (int)infos.count;
    for (int i = count-1; i >=0 ; i--) {
        MFNewCurrency *info = [infos objectAtIndex:i];
        if (![self.currencyList containsObject:info]) {
            [self.currencyList insertObject:info atIndex:0];
        }
    }
}

@end
