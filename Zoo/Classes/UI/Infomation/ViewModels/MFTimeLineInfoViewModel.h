//
//  MFTimeLineInfoViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFInfoListBaseViewModel.h"

@class RACSubject;
@class RACCommand;
@class RACSignal;
@class MFInfomation;

@interface MFTimeLineInfoViewModel : MFInfoListBaseViewModel

@property (nonatomic, strong) RACCommand *dataCommand;//参数是页码

@property (nonatomic, strong) RACSignal *dataSignal;

@end
