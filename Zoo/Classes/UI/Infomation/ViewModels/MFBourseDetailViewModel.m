//
//  MFBourseDetailViewModel.m
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseDetailViewModel.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFNetRequestBusiness.h"
#import "MFBourseDetail.h"

@implementation MFBourseDetailViewModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}
#pragma mark -- override Method
- (void)bindModel
{
    [super bindModel];
    @weakify(self)
    [[[self.dataCommand executionSignals] switchToLatest] subscribeNext:^(id x) {
        @strongify(self)
        [self dealData:x];
    }];
}

#pragma mark -- 懒加载属性
- (RACSignal *)dataSignal
{
    @weakify(self);
    RACSignal *coldSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self);
        //请求数据
        [self requestBourseDetailSuccess:^(MFBourseDetail *bourseDetail) {
            [subscriber sendNext:bourseDetail];
        } failure:^(NSError *error) {
            [subscriber sendError:error];
        }];
        
        return [RACDisposable disposableWithBlock:^{
            //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求

        }];
    }];
    RACSubject *subject = [RACSubject subject];
    RACMulticastConnection *multicastConnection = [coldSignal multicast:subject];
    RACSignal *hotSignal = multicastConnection.autoconnect;
    return hotSignal;
}

- (RACCommand *)dataCommand
{
    if (!_dataCommand) {
        @weakify(self)
        _dataCommand = [[RACCommand alloc] initWithEnabled:nil signalBlock:^RACSignal *(id input) {
            
            RACSignal *dataSignal = [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
                @strongify(self)
                //获取交易所列表
                [self requestBourseDetailSuccess:^(MFBourseDetail *bourseDetail) {
                    [subscriber sendNext:bourseDetail];
                    [subscriber sendCompleted];
                } failure:^(NSError *error) {
                    [subscriber sendError:error];
                }];
                
                return [RACDisposable disposableWithBlock:^{
                    //该block中做一些清理工作，比如取消网络请求，或者取消数据库请求
                }];
            }];
            
            return dataSignal;
        }];
    }
    
    return _dataCommand;
}

#pragma mark -- 业务方法
- (void)requestBourseDetailSuccess:(void (^)(MFBourseDetail *bourseDetail))completion failure:(void (^)(NSError *error))failure
{
    MFNetRequestBusiness *business = [MFNetRequestBusiness shareInstance];
    __block NSArray *currencyClasses = nil;
    __block MFBourseDetail *globalBourseDetail = nil;
    __block NSError *globalError = nil;
    //分两个接口获取，基础信息跟支持货币
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    [business requestBourseDetailWithBourseId:self.bourseId success:^(MFBourseDetail *bourseDetail) {
        globalBourseDetail = bourseDetail;
        dispatch_group_leave(group);
    } failure:^(NSError *error) {
        globalError = error;
        dispatch_group_leave(group);
    }];
    dispatch_group_enter(group);
    [business requestCurrencyClassSymbolsListWithBourse:self.bourseId page:1 pageSize:10 success:^(NSArray<MFCurrency *> *currencyList) {
        currencyClasses = currencyList;
        dispatch_group_leave(group);
    } failure:^(NSError *error) {
        globalError = error;
        dispatch_group_leave(group);
    }];
    
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        if (currencyClasses.count > 0) {
            [globalBourseDetail.currencyClasses addObjectsFromArray:currencyClasses];
        }
        if (globalError) {
            if (failure) {
                failure(globalError);
            }
        }else{
            if (completion) {
                completion(globalBourseDetail);
            }
        }
    });
}

#pragma mark -- 返回数据处理
- (void)dealData:(MFBourseDetail *)data
{
    _bourseDetail = data;
    [_refreshDelegate sendNext:data];
}

@end
