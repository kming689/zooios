//
//  MFInfoListBaseViewModel.h
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewModel.h"

@class RACSubject;
@class MFInfomation;

@interface MFInfoListBaseViewModel : MFViewModel

@property (nonatomic, strong) NSString *infoTypeId;

@property (nonatomic, strong) NSMutableArray<MFInfomation *> *infoList;

@property (nonatomic, strong) RACSubject *refreshDelegate;//刷新界面的回调

- (void)dealDataWithLoadMore:(NSArray<MFInfomation *> *)infos;

- (void)dealDataWithLoadNew:(NSArray<MFInfomation *> *)infos;

@end
