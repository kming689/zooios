//
//  MFInfoPageViewController.h
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import <WMPageController/WMPageController.h>

typedef NS_ENUM(NSUInteger, MFInfoMenuViewPosition) {
    MFInfoMenuViewPositionDefault,
    MFInfoMenuViewPositionBottom,
};

@interface MFInfoPageViewController : WMPageController

@property (nonatomic, assign) MFInfoMenuViewPosition menuViewPosition;

@end
