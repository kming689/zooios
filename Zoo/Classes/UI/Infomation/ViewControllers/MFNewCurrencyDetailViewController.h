//
//  MFNewCurrencyDetailViewController.h
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewController.h"

@interface MFNewCurrencyDetailViewController : MFViewController

- (instancetype)initWithCurrencyId:(NSString *)currencyId;

@end
