//
//  MFSpecialViewController.h
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewController.h"

@interface MFSpecialViewController : MFViewController

- (instancetype)initWithInfoTypeId:(NSString *)infoTypeId;

@end
