//
//  MFNewCurrencyDetailViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFNewCurrencyDetailViewController.h"
#import "MFNewCurrencyDetailViewModel.h"
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFKitBases.h"
#import "MFKitMarcro.h"

#import "MFNewCurrencyDetail.h"

#import "MFCurrencyHeadTableViewCell.h"
#import "MFBourseContentTableViewCell.h"
#import "MFBaseInfoTableViewCell.h"
#import "MFTeamTableViewCell.h"

static const NSString *kMFCurrencyHeadTableViewCellIdentifer = @"kMFCurrencyHeadTableViewCellIdentifer";
static const NSString *kMFCurrencyContentTableViewCellIdentifer = @"kMFCurrencyContentTableViewCellIdentifer";
static const NSString *kMFBaseInfoTableViewCellIdentifer = @"kMFBaseInfoTableViewCellIdentifer";
static const NSString *kMFCurrencyTeamTableViewCellIdentifer = @"kMFCurrencyTeamTableViewCellIdentifer";

@interface MFNewCurrencyDetailViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) MFNewCurrencyDetailViewModel *viewModel;

@property (nonatomic, strong) MFTableView *tableView;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation MFNewCurrencyDetailViewController

- (instancetype)initWithCurrencyId:(NSString *)currencyId
{
    self = [super init];
    if (self) {
        self.viewModel.currencyId = currencyId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.indicatorView startAnimating];
    self.tableView.hidden = YES;
    RACSignal *signal = [self.viewModel.dataCommand execute:[NSNumber numberWithBool:NO]];
    @weakify(self);
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
}

-(void)configView
{
    [super configView];
    
    [self configTableView];
    
    [self setupRefresh];
    
    @weakify(self);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.and.top.equalTo(self.view).offset(0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_indicatorView];
    [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(kNavigationHeight + 30);
    }];
}

-(void)bindViewModel
{
    [super bindViewModel];
    
    //添加数据刷新回调
    @weakify(self)
    RACSubject *tapSubject = [[RACSubject alloc] init];
    [tapSubject subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
    }];
    self.viewModel.refreshDelegate = tapSubject;
}

#pragma mark -- 懒加载属性
- (MFNewCurrencyDetailViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFNewCurrencyDetailViewModel alloc] init];
    }
    return _viewModel;
}

- (MFTableView *)tableView
{
    if (!_tableView) {
        _tableView = [[MFTableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
        footerView.backgroundColor = [MFColor clearColor];
        _tableView.tableFooterView = footerView;
        _tableView.backgroundColor = [MFColor clearColor];
        _tableView.separatorColor = [MFColor mf_lineColor];
        _tableView.sectionIndexColor = [MFColor mf_fontGrayStyleColor];
        _tableView.sectionIndexBackgroundColor =[UIColor clearColor];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFCurrencyHeadTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFCurrencyHeadTableViewCellIdentifer copy]];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFBourseContentTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFCurrencyContentTableViewCellIdentifer copy]];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFBaseInfoTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFBaseInfoTableViewCellIdentifer copy]];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFTeamTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFCurrencyTeamTableViewCellIdentifer copy]];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

#pragma mark -- UI设置
- (void)configTableView
{
    [self.view addSubview:self.tableView];
}

- (void)setupRefresh {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshClick:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

#pragma mark -- actions
// 下拉刷新触发，在此获取数据
- (void)refreshClick:(UIRefreshControl *)refreshControl {
    
    RACSignal *dataSignal = [self.viewModel.dataCommand execute:[NSNumber numberWithBool:NO]];
    [dataSignal subscribeCompleted:^{
        [refreshControl endRefreshing];
    }];
    [dataSignal subscribeError:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        [refreshControl endRefreshing];
    }];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //基础信息
    if (indexPath.row == 0) {
        MFCurrencyHeadTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFCurrencyHeadTableViewCellIdentifer copy] forIndexPath:indexPath];
        //配置内容
        MFNewCurrencyDetail *detail = self.viewModel.currencyDetail;
        [cell configCellWithDetailInfo:detail];
        
        return cell;
    }
    //介绍
    else if (indexPath.row == 1) {
        MFBourseContentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFCurrencyContentTableViewCellIdentifer copy] forIndexPath:indexPath];
        //配置内容
        MFNewCurrencyDetail *detail = self.viewModel.currencyDetail;
        [cell configCellWithTitle:@"介绍" content:detail.introduction];
        
        return cell;
    }
    //基本信息
    else if (indexPath.row == 2) {
        MFBaseInfoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFBaseInfoTableViewCellIdentifer copy] forIndexPath:indexPath];
        //配置内容
        MFNewCurrencyDetail *detail = self.viewModel.currencyDetail;
        [cell configCellWithBaseInfos:detail.baseInfos title:@"基本信息"];
        return cell;
    }
    //团队
    else{
        MFTeamTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFCurrencyTeamTableViewCellIdentifer copy] forIndexPath:indexPath];
        MFNewCurrencyDetail *detail = self.viewModel.currencyDetail;
        [cell configCellWithDetailInfo:detail];
        return cell;
    }
}

#pragma mark -- UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 65;
    }
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
