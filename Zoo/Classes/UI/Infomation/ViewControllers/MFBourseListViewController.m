//
//  MFBourseListViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseListViewController.h"
#import "MFBourseListViewModel.h"
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFKitBases.h"
#import "MFKitMarcro.h"
#import "MFBourseTableViewCell.h"
#import "MFBourseDetailViewController.h"

#import "MFBourse.h"

static const NSString *kMFBourseTableViewCellIdentifer = @"kMFBourseTableViewCellIdentifer";

@interface MFBourseListViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) MFBourseListViewModel *viewModel;

@property (nonatomic, strong) MFTableView *tableView;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation MFBourseListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.indicatorView startAnimating];
    self.tableView.hidden = YES;
    RACSignal *signal = [self.viewModel.dataCommand execute:[NSNumber numberWithBool:NO]];
    @weakify(self);
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
}

-(void)configView
{
    [super configView];
    
    [self configTableView];
    
    [self setupRefresh];
    
    @weakify(self);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.and.top.mas_equalTo(0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_indicatorView];
    [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(30);
    }];
}

-(void)bindViewModel
{
    [super bindViewModel];
    
    //添加数据刷新回调
    @weakify(self)
    RACSubject *tapSubject = [[RACSubject alloc] init];
    [tapSubject subscribeNext:^(id x) {
        @strongify(self)
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
    }];
    self.viewModel.refreshDelegate = tapSubject;
}

#pragma mark -- 懒加载属性
- (MFBourseListViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFBourseListViewModel alloc] init];
    }
    return _viewModel;
}

- (MFTableView *)tableView
{
    if (!_tableView) {
        _tableView = [[MFTableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
        footerView.backgroundColor = [MFColor clearColor];
        _tableView.tableFooterView = footerView;
        _tableView.backgroundColor = [MFColor clearColor];
        _tableView.separatorColor = [MFColor mf_lineColor];
        _tableView.sectionIndexColor = [MFColor mf_fontGrayStyleColor];
        _tableView.sectionIndexBackgroundColor =[UIColor clearColor];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFBourseTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFBourseTableViewCellIdentifer copy]];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

#pragma mark -- UI设置
- (void)configTableView
{
    [self.view addSubview:self.tableView];
}

- (void)setupRefresh {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshClick:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
}

#pragma mark -- actions
// 下拉刷新触发，在此获取数据
- (void)refreshClick:(UIRefreshControl *)refreshControl {
    
    RACSignal *dataSignal = [self.viewModel.dataCommand execute:[NSNumber numberWithBool:NO]];
    [dataSignal subscribeCompleted:^{
        [refreshControl endRefreshing];
    }];
    [dataSignal subscribeError:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        [refreshControl endRefreshing];
    }];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.viewModel.bourseList count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MFBourseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFBourseTableViewCellIdentifer copy] forIndexPath:indexPath];
    //配置内容
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    MFBourse *bourse = [self.viewModel.bourseList objectAtIndex:indexPath.row];
    [cell configCellWithBourseInfo:bourse];
    
    return cell;
}

#pragma mark -- UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MFBourse *bourse = [self.viewModel.bourseList objectAtIndex:indexPath.row];
    MFBourseDetailViewController *vc = [[MFBourseDetailViewController alloc] initWithBourseId:bourse.bourseId];
    vc.title = bourse.bourseName;
    [self.navigationController pushViewController:vc animated:YES];
}

@end
