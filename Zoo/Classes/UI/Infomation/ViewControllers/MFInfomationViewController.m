//
//  MFInfomationViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFInfomationViewController.h"
#import "MFInfoPageViewController.h"
#import "MFColor.h"

@interface MFInfomationViewController ()

@end

@implementation MFInfomationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"币读";
    
    MFInfoPageViewController *vc = [[MFInfoPageViewController alloc] init];
    vc.scrollEnable = NO;
    vc.titleColorNormal = [MFColor mf_navigationBarTitleColor];
    vc.titleColorSelected = [MFColor mf_colorWithHexString:@"00DAAA"];
    vc.titleSizeNormal = 14;
    vc.titleSizeSelected = 14;
    vc.menuViewStyle = WMMenuViewStyleLine;
    vc.automaticallyCalculatesItemWidths = YES;
    
    [vc willMoveToParentViewController:self];
    [self addChildViewController:vc];
    [self.view addSubview:vc.view];
    CGRect originBounds = self.view.bounds;
    CGFloat yoffset = 0;
    if (self.navigationController) {
        CGRect rectStatus = [[UIApplication sharedApplication] statusBarFrame];
        CGRect rectNav = self.navigationController.navigationBar.frame;
        yoffset = rectStatus.size.height + rectNav.size.height;
        originBounds.origin.y = yoffset;
    }
    originBounds.size.height = originBounds.size.height - yoffset - 49;
    vc.view.frame = originBounds;
    [vc didMoveToParentViewController:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
