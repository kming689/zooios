//
//  MFInfoPageViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFInfoPageViewController.h"
#import "MFInfoPageViewModel.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFKitBases.h"

#import "MFTimeLineInfoViewController.h"
#import "MFMediaInfoViewController.h"
#import "MFSpecialViewController.h"
#import "MFBourseListViewController.h"
#import "MFNewCurrencyViewController.h"

#import "MFInfoType.h"

#define MENU_HEIGHT 35

#define NAV_HEIGHT [[UIApplication sharedApplication] statusBarFrame].size.height + self.navigationController.navigationBar.frame.size.height

@interface MFInfoPageViewController ()

@property (nonatomic, strong) MFInfoPageViewModel *viewModel;

@end

@implementation MFInfoPageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [MFColor mf_controllerViewBackgroundColor];
    RACSignal *signal = self.viewModel.dataSignal;
    @weakify(self);
    [signal subscribeNext:^(id x) {
        @strongify(self);
        self.viewModel.infoTypes = x;
        [self reloadData];
    }];
    [signal subscribeError:^(NSError *error) {
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

#pragma mark -- 懒加载属性
- (MFInfoPageViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFInfoPageViewModel alloc] init];
    }
    return _viewModel;
}


#pragma mark -- dataSource
- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.viewModel.infoTypes.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    MFInfoType *infoType = [self.viewModel.infoTypes objectAtIndex:index];
    return infoType.typeName;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    MFInfoType *infoType = [self.viewModel.infoTypes objectAtIndex:index];
    NSString *title = infoType.typeName;
    if ([title isEqualToString:@"快讯"]) {
        return [[MFTimeLineInfoViewController alloc] initWithInfoTypeId:infoType.typeId];
    }else if ([title isEqualToString:@"媒体"]) {
        return [[MFMediaInfoViewController alloc] initWithInfoTypeId:infoType.typeId];
    }else if ([title isEqualToString:@"专栏"]) {
        return [[MFSpecialViewController alloc] initWithInfoTypeId:infoType.typeId];
    }else if ([title isEqualToString:@"交易所"]) {
        return [[MFBourseListViewController alloc] init];
    }else if ([title isEqualToString:@"新币"]) {
        return [[MFNewCurrencyViewController alloc] init];
    }else{
        return [[UIViewController alloc] init];
    }
}

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = [super menuView:menu widthForItemAtIndex:index];
    return width + 20;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    if (self.menuViewPosition == MFInfoMenuViewPositionBottom) {
        menuView.backgroundColor = [UIColor colorWithWhite:0.95 alpha:1.0];
        return CGRectMake(0, self.view.frame.size.height - MENU_HEIGHT, self.view.frame.size.width, MENU_HEIGHT);
    }
    CGFloat leftMargin = self.showOnNavigationBar ? 50 : 0;
    CGRect rect = CGRectMake(leftMargin, 0, self.view.frame.size.width - 2*leftMargin, MENU_HEIGHT);
    return rect;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    if (self.menuViewPosition == MFInfoMenuViewPositionBottom) {
        return CGRectMake(0, NAV_HEIGHT, self.view.frame.size.width, self.view.frame.size.height - NAV_HEIGHT - MENU_HEIGHT);
    }
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}

@end
