//
//  MFMediaInfoViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFMediaInfoViewController.h"
#import "MFMediaInfoViewModel.h"
#import <Masonry/Masonry.h>
#import <SVProgressHUD/SVProgressHUD.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <MJRefresh/MJRefresh.h>
#import "MFKitBases.h"
#import "MFKitMarcro.h"
#import "MFMediaTableViewCell.h"
#import "MFNewsDetailViewController.h"
#import "MFInfomation.h"

static const NSString *kMFMediaTableViewCellIdentifer = @"kMFMediaTableViewCellIdentifer";

@interface MFMediaInfoViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) MFMediaInfoViewModel *viewModel;

@property (nonatomic, strong) MFTableView *tableView;

@property (nonatomic, strong) UIActivityIndicatorView *indicatorView;

@end

@implementation MFMediaInfoViewController
{
    NSInteger _currentPage;
}

- (instancetype)initWithInfoTypeId:(NSString *)infoTypeId
{
    self = [super init];
    if (self) {
        self.viewModel.infoTypeId = infoTypeId;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _currentPage = 1;
    [self.indicatorView startAnimating];
    self.tableView.hidden = YES;
    RACSignal *signal = [self.viewModel.dataCommand execute:[NSNumber numberWithBool:_currentPage]];
    @weakify(self);
    [signal subscribeNext:^(id x) {
        @strongify(self);
        [self.viewModel dealDataWithLoadMore:x];
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
        if (self.viewModel.infoList.count < 10) {
            self.tableView.mj_footer.hidden = YES;
        }else{
            self.tableView.mj_footer.hidden = NO;
        }
    }];
    [signal subscribeError:^(NSError *error) {
        @strongify(self);
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
        [SVProgressHUD setMinimumDismissTimeInterval:1.5];
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
    }];
}

#pragma mark - override methods
-(void)configNavigationBar
{
    [super configNavigationBar];
    
}

-(void)configView
{
    [super configView];
    
    [self configTableView];
    
    [self setupRefresh];
    
    @weakify(self);
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.and.top.mas_equalTo(0);
        make.bottom.equalTo(self.view).offset(0);
    }];
    
    _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _indicatorView.hidesWhenStopped = YES;
    [self.view addSubview:_indicatorView];
    [_indicatorView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(30);
    }];
}

-(void)bindViewModel
{
    [super bindViewModel];
    
    //添加数据刷新回调
}

#pragma mark -- 懒加载属性
- (MFMediaInfoViewModel *)viewModel
{
    if (!_viewModel) {
        _viewModel = [[MFMediaInfoViewModel alloc] init];
    }
    return _viewModel;
}

- (MFTableView *)tableView
{
    if (!_tableView) {
        _tableView = [[MFTableView alloc] init];
        _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        UIView *footerView = [[UIView alloc] initWithFrame:CGRectZero];
        footerView.backgroundColor = [MFColor clearColor];
        _tableView.tableFooterView = footerView;
        _tableView.backgroundColor = [MFColor clearColor];
        _tableView.separatorColor = [MFColor mf_lineColor];
        _tableView.sectionIndexColor = [MFColor mf_fontGrayStyleColor];
        _tableView.sectionIndexBackgroundColor =[UIColor clearColor];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MFMediaTableViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellReuseIdentifier:[kMFMediaTableViewCellIdentifer copy]];
        _tableView.delegate = self;
        _tableView.dataSource = self;
    }
    return _tableView;
}

#pragma mark -- UI设置
- (void)configTableView
{
    [self.view addSubview:self.tableView];
}

- (void)setupRefresh {
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshClick:) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:refreshControl];
    
    //上拉刷新
    @weakify(self);
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self);
        //加载更多数据
        self->_currentPage += 1;
        RACSignal *dataSignal = [self.viewModel.dataCommand execute:[NSNumber numberWithInteger:_currentPage]];
        [dataSignal subscribeNext:^(NSArray *x) {
            @strongify(self);
            [self.viewModel dealDataWithLoadMore:x];
            [self.tableView reloadData];
            [self.indicatorView stopAnimating];
            self.tableView.hidden = NO;
            if (x.count == 0) {
                [(MJRefreshAutoNormalFooter *)self.tableView.mj_footer endRefreshingWithNoMoreData];
            }
        }];
        [dataSignal subscribeCompleted:^{
            // 结束刷新
            [self.tableView.mj_footer endRefreshing];
        }];
        [dataSignal subscribeError:^(NSError *error) {
            [SVProgressHUD showErrorWithStatus:error.localizedDescription];
            // 结束刷新
            [self.tableView.mj_footer endRefreshing];
        }];
    }];
    // 默认先隐藏footer
    self.tableView.mj_footer.hidden = YES;
    [(MJRefreshAutoNormalFooter *)self.tableView.mj_footer setTitle:@"点击或上拉加载更多" forState:MJRefreshStateIdle];
    [(MJRefreshAutoNormalFooter *)self.tableView.mj_footer setTitle:@"没有更多数据了" forState:MJRefreshStateNoMoreData];
}

#pragma mark -- actions
// 下拉刷新触发，在此获取数据
- (void)refreshClick:(UIRefreshControl *)refreshControl {
    
    RACSignal *dataSignal = [self.viewModel.dataCommand execute:[NSNumber numberWithInteger:1]];
    @weakify(self);
    [dataSignal subscribeNext:^(id x) {
        @strongify(self);
        [self.viewModel dealDataWithLoadNew:x];
        [self.tableView reloadData];
        [self.indicatorView stopAnimating];
        self.tableView.hidden = NO;
    }];
    [dataSignal subscribeCompleted:^{
        [refreshControl endRefreshing];
    }];
    [dataSignal subscribeError:^(NSError *error) {
        [SVProgressHUD showErrorWithStatus:error.localizedDescription];
        [refreshControl endRefreshing];
    }];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.infoList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MFMediaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[kMFMediaTableViewCellIdentifer copy] forIndexPath:indexPath];
    //配置内容
    MFMediaInfo *mediaInfo = (MFMediaInfo *)[self.viewModel.infoList objectAtIndex:indexPath.row];
    [cell configCellWithMediaInfo:mediaInfo];
    
    return cell;
}

#pragma mark -- UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MFInfomation *newsInfo = [self.viewModel.infoList objectAtIndex:indexPath.row];
    MFNewsDetailViewController *detailVC = [[MFNewsDetailViewController alloc] initWithUrl:newsInfo.contentUrl];
    detailVC.title = @"媒体详情";
    [self.navigationController pushViewController:detailVC animated:YES];
}

@end
