//
//  MFBourseDetailViewController.h
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFViewController.h"

@interface MFBourseDetailViewController : MFViewController

- (instancetype)initWithBourseId:(NSString *)bourseId;

@end
