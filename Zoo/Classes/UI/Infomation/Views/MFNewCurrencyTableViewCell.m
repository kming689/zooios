//
//  MFNewCurrencyTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFNewCurrencyTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "MFKitBases.h"
#import "MFNewCurrency.h"

@interface MFNewCurrencyTableViewCell()

@property (nonatomic, strong) MFImageView *avatarImageView;

@property (nonatomic, strong) MFLabel *titleLabel;

@property (nonatomic, strong) MFLabel *infoLabel;

@property (nonatomic, strong) MFLabel *startTipLabel;

@property (nonatomic, strong) MFLabel *dateLabel;

@property (nonatomic, strong) MFView *lineView;

@property (nonatomic, strong) MFLabel *attentionLabel;

@property (nonatomic, strong) MFView *seperateView;

@end

@implementation MFNewCurrencyTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    MFImageView *imageView = [[MFImageView alloc] init];
    _avatarImageView = imageView;
//    _avatarImageView.layer.cornerRadius = 15;
//    _avatarImageView.layer.masksToBounds = YES;
    _avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:imageView];
    
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.numberOfLines = 2;
    _titleLabel.textColor = [MFColor mf_navigationBarTitleColor];
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _titleLabel.font = [UIFont systemFontOfSize:13.0];
    [self.contentView addSubview:_titleLabel];
    
    _infoLabel = [[MFLabel alloc] init];
    _infoLabel.numberOfLines = 2;
    _infoLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _infoLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _infoLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_infoLabel];
    
    _startTipLabel = [[MFLabel alloc] init];
    _startTipLabel.numberOfLines = 2;
    _startTipLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _startTipLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _startTipLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_startTipLabel];
    
    _dateLabel = [[MFLabel alloc] init];
    _dateLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _dateLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _dateLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_dateLabel];
    
    _lineView = [[MFView alloc] init];
    _lineView.backgroundColor = [MFColor mf_lineColor];
    [self.contentView addSubview:_lineView];
    
    MFLabel *attentionTipLabel = [[MFLabel alloc] init];
    attentionTipLabel.textColor = [MFColor mf_fontGrayStyleColor];
    attentionTipLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    attentionTipLabel.font = [UIFont systemFontOfSize:12.0];
    attentionTipLabel.text = @"关注";
    [self.contentView addSubview:attentionTipLabel];
    
    _attentionLabel = [[MFLabel alloc] init];
    _attentionLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _attentionLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _attentionLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_attentionLabel];
    
    _seperateView = [[MFView alloc] init];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
    [self.contentView addSubview:_seperateView];
    
    @weakify(self);
    [_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.avatarImageView);
        make.left.equalTo(self.avatarImageView.mas_right).offset(10);
    }];
    [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.left.equalTo(self.titleLabel);
        make.right.equalTo(self.contentView).offset(-15);
    }];
    [_startTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.infoLabel.mas_bottom).offset(10);
        make.left.equalTo(self.titleLabel);
    }];
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.startTipLabel.mas_right).offset(20);
        make.centerY.equalTo(self.startTipLabel);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.startTipLabel.mas_bottom).offset(10);
        make.left.and.right.equalTo(self.contentView);
        make.height.mas_equalTo(0.5);
    }];
    
    [attentionTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.avatarImageView);
        make.top.equalTo(self.lineView.mas_bottom).offset(10);
    }];
    [_attentionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        //@strongify(self);
        make.left.equalTo(attentionTipLabel.mas_right).offset(20);
        make.centerY.equalTo(attentionTipLabel);
    }];
    [_seperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(attentionTipLabel.mas_bottom).offset(10);
        make.left.and.right.and.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(6);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
    _lineView.backgroundColor = [MFColor mf_lineColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
    _lineView.backgroundColor = [MFColor mf_lineColor];
}

- (void)configCellWithInfo:(MFNewCurrency *)info
{
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:info.icon] placeholderImage:[UIImage imageNamed:@"currency"]];
    self.titleLabel.text = info.currencyName;
    self.infoLabel.text = info.abstract;
    self.startTipLabel.text = info.startStatus;
    self.dateLabel.text = info.publishTime;
    self.attentionLabel.text = info.attention;
}

@end
