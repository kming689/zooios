//
//  MFBourseContentTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseContentTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFKitBases.h"

@interface MFBourseContentTableViewCell()

@property (nonatomic, strong) MFLabel *titleLabel;

@property (nonatomic, strong) MFLabel *contentLabel;

@property (nonatomic, strong) MFView *lineView;

@property (nonatomic, strong) MFView *seperateView;

@end

@implementation MFBourseContentTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _seperateView = [[MFView alloc] init];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
    [self.contentView addSubview:_seperateView];
    
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.textColor = [MFColor mf_navigationBarTitleColor];
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _titleLabel.font = [UIFont systemFontOfSize:13.0];
    [self.contentView addSubview:_titleLabel];
    
    _contentLabel = [[MFLabel alloc] init];
    _contentLabel.numberOfLines = 0;
    _contentLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _contentLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _contentLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_contentLabel];
    
    _lineView = [[MFView alloc] init];
    _lineView.backgroundColor = [MFColor mf_lineColor];
    [self.contentView addSubview:_lineView];
    
    @weakify(self);
    [_seperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.and.left.and.right.equalTo(self.contentView);
        make.height.mas_equalTo(6);
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.seperateView.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.equalTo(self.contentView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.height.mas_equalTo(0.5);
    }];
    [_contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.titleLabel);
        make.right.equalTo(self.contentView).offset(-16);
        make.top.equalTo(self.lineView.mas_bottom).offset(10);
        make.bottom.equalTo(self.contentView).offset(-10);
        //make.height.mas_equalTo(80).priorityHigh();
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    _lineView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    _lineView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)configCellWithTitle:(NSString *)title content:(NSString *)content
{
    self.titleLabel.text = title;
    self.contentLabel.text = content;
}

@end
