//
//  MFTeamTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTeamTableViewCell.h"
#import "MFKitBases.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "MFNewCurrencyDetail.h"

@interface MFTeamTableViewCell()

@property (nonatomic, strong) MFLabel *titleLabel;

@property (nonatomic, strong) MFView *lineView;

@property (nonatomic, strong) MFView *seperateView;

@property (nonatomic, strong) MFImageView *titleImageView;

@property (nonatomic, strong) MFLabel *nameLabel;

@property (nonatomic, strong) MFLabel *subTitleLabel;

@end

@implementation MFTeamTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _seperateView = [[MFView alloc] init];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
    [self.contentView addSubview:_seperateView];
    
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.textColor = [MFColor mf_navigationBarTitleColor];
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _titleLabel.font = [UIFont systemFontOfSize:13.0];
    _titleLabel.text = @"团队";
    [self.contentView addSubview:_titleLabel];
    
    _lineView = [[MFView alloc] init];
    _lineView.backgroundColor = [MFColor mf_lineColor];
    [self.contentView addSubview:_lineView];
    
    @weakify(self);
    [_seperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.and.left.and.right.equalTo(self.contentView);
        make.height.mas_equalTo(6);
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.seperateView.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.equalTo(self.contentView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.height.mas_equalTo(0.5);
    }];
    
    _titleImageView = [[MFImageView alloc] init];
    _titleImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_titleImageView];
    
    _nameLabel = [[MFLabel alloc] init];
    _nameLabel.textColor = [MFColor mf_navigationBarTitleColor];
    _nameLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _nameLabel.font = [UIFont systemFontOfSize:14.0];
    [self.contentView addSubview:_nameLabel];
    
    _subTitleLabel = [[MFLabel alloc] init];
    _subTitleLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _subTitleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _subTitleLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_subTitleLabel];
    
    [_titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.lineView.mas_bottom).offset(15);
        make.size.mas_equalTo(CGSizeMake(40, 40));
        make.bottom.equalTo(self.contentView).offset(-15);
    }];
    [_nameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.titleImageView.mas_right).offset(15);
        make.top.equalTo(self.titleImageView);
    }];
    [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.nameLabel);
        make.bottom.equalTo(self.titleImageView);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)configCellWithDetailInfo:(MFNewCurrencyDetail *)info
{
    self.nameLabel.text = info.teamName;
    self.subTitleLabel.text = info.teamIntro;
    [self.titleImageView sd_setImageWithURL:[NSURL URLWithString:info.teamIcon] placeholderImage:[UIImage imageNamed:@"team"]];
}

@end
