//
//  MFTeamTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTableViewCell.h"

@class MFNewCurrencyDetail;

@interface MFTeamTableViewCell : MFTableViewCell

- (void)configCellWithDetailInfo:(MFNewCurrencyDetail *)info;

@end
