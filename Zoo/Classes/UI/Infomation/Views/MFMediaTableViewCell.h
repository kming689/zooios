//
//  MFMediaTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTableViewCell.h"

@class MFMediaInfo;

@interface MFMediaTableViewCell : MFTableViewCell

- (void)configCellWithMediaInfo:(MFMediaInfo *)info;

@end
