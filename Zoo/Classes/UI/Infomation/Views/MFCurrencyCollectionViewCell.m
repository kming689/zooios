//
//  MFCurrencyCollectionViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrencyCollectionViewCell.h"
#import <Masonry/Masonry.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFKitBases.h"
#import "MFCurrencyType.h"
#import "UIImage+MFAdd.h"

@interface MFCurrencyCollectionViewCell ()

@property (nonatomic, strong) MFImageView *avatarImageView;

@property (nonatomic, strong) MFLabel *nameLabel;

@property (nonatomic, strong) MFButton *editButton;

@end

@implementation MFCurrencyCollectionViewCell

{
    UIColor *_textColor;
    UIImage *_placeHolderImage;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _textColor = [MFColor mf_fontGrayStyleColor];
    _placeHolderImage = [UIImage imageNamed:@"default_avatar_man" bundleClass:[self class]];
    
    MFImageView *imageView = [[MFImageView alloc] init];
    _avatarImageView = imageView;
    _avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:imageView];
    
    _nameLabel = [[MFLabel alloc] init];
    _nameLabel.textAlignment = NSTextAlignmentCenter;
    _nameLabel.font = [UIFont systemFontOfSize:12.0];
    _nameLabel.textColor = [MFColor mf_fontGrayStyleColor];
    [self.contentView addSubview:_nameLabel];
    
    _editButton = [MFButton buttonWithType:UIButtonTypeCustom];
    [_editButton setImage:[UIImage imageNamed:@"icon_edit_delete"] forState:UIControlStateNormal];
    [_editButton addTarget:self action:@selector(editButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:_editButton];
    self.editButton.hidden = YES;
    
    //添加长按手势
    UILongPressGestureRecognizer *longPressGesture =
    [[UILongPressGestureRecognizer alloc]initWithTarget:self
                                                 action:@selector(onlongPress:)];
    [self addGestureRecognizer:longPressGesture];
}

- (void)prepareForReuse
{
    _textColor = [MFColor mf_fontGrayStyleColor];
    _placeHolderImage = [UIImage imageNamed:@"default_avatar_man" bundleClass:[self class]];
    _editButton.hidden = YES;
    self.hidden = NO;
}

- (void)setLabelTextColor:(UIColor *)color
{
    _textColor = color;
}

- (void)setPlaceHolderImage:(UIImage *)image
{
    _placeHolderImage = image;
}

- (void)configCellWithCurrency:(MFCurrencyType *)currency
{
    _placeHolderImage = [UIImage imageNamed:@"currency"];
    [self configCellWithCurrency:currency placeHolderImage:_placeHolderImage];
}

- (void)configCellWithCurrency:(MFCurrencyType *)currency placeHolderImage:(UIImage *)image
{
    [_avatarImageView sd_setImageWithURL:[NSURL URLWithString:currency.icon] placeholderImage:image];
    _nameLabel.text = currency.currencyName;
    _nameLabel.textColor = _textColor;
    
    @weakify(self)
    [_avatarImageView mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(self.contentView).offset(8);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    
    [_nameLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.equalTo(self.contentView);
        make.top.equalTo(_avatarImageView.mas_bottom).offset(2.5);
        make.height.mas_equalTo(20);
        make.left.equalTo(self.contentView).offset(2);
        make.right.equalTo(self.contentView).offset(-2);
    }];
    
    [self.editButton mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.avatarImageView).offset(-2);
        make.right.equalTo(self.avatarImageView).offset(8);
        make.width.equalTo(@(18));
        make.height.equalTo(@(18));
    }];
}

- (void)setEditMode:(BOOL)mode
{
    self.editButton.hidden = !mode;
}

#pragma mark -- actions
- (void)editButtonClicked:(id)sender
{
    if (self.delegate&&[self.delegate respondsToSelector:@selector(onEditButtonDone:)]) {
        [self.delegate onEditButtonDone:self];
    }
}

- (void)onlongPress:(UILongPressGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateBegan) {
        
        if (self.delegate&&[self.delegate respondsToSelector:@selector(onItemLongPress:)]) {
            [self.delegate onItemLongPress:self];
        }
    }
}

@end
