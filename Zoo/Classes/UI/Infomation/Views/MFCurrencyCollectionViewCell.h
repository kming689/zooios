//
//  MFCurrencyCollectionViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import <UIKit/UIKit.h>

@class MFCurrencyCollectionViewCell;
@class MFCurrencyType;

@protocol MFMemberCollectionDelegate <NSObject>

- (void)onEditButtonDone:(MFCurrencyCollectionViewCell *)cell;

- (void)onItemLongPress:(MFCurrencyCollectionViewCell *)cell;

@end

@interface MFCurrencyCollectionViewCell : UICollectionViewCell

@property (nonatomic, weak) id<MFMemberCollectionDelegate> delegate;

//下面两个函数对cell进行一些设置，但是必须在config方法之前被调用
- (void)setLabelTextColor:(UIColor *)color;

- (void)setPlaceHolderImage:(UIImage *)image;

- (void)configCellWithCurrency:(MFCurrencyType *)currency;

@end
