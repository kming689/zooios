//
//  MFBaseInfoTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTableViewCell.h"

@class MFBaseInfo;

@interface MFBaseInfoTableViewCell : MFTableViewCell

- (void)configCellWithBaseInfos:(NSArray<MFBaseInfo *> *)baseInfos title:(NSString *)title;

@end
