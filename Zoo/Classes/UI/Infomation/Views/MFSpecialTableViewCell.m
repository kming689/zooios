//
//  MFSpecialTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFSpecialTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "MFKitBases.h"
#import "MFSpecialInfo.h"

@interface MFSpecialTableViewCell()

@property (nonatomic, strong) MFImageView *avatarImageView;

@property (nonatomic, strong) MFLabel *authorLabel;

@property (nonatomic, strong) MFLabel *dateLabel;

@property (nonatomic, strong) MFLabel *timeLabel;

@property (nonatomic, strong) MFLabel *titleLabel;

@property (nonatomic, strong) MFLabel *infoLabel;

@property (nonatomic, strong) MFView *seperateView;

@end

@implementation MFSpecialTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    MFImageView *imageView = [[MFImageView alloc] init];
    _avatarImageView = imageView;
    _avatarImageView.layer.cornerRadius = 15;
    _avatarImageView.layer.masksToBounds = YES;
    _avatarImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:imageView];
    
    _authorLabel = [[MFLabel alloc] init];
    _authorLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _authorLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _authorLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_authorLabel];
    
    _dateLabel = [[MFLabel alloc] init];
    _dateLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _dateLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _dateLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_dateLabel];
    
    _timeLabel = [[MFLabel alloc] init];
    _timeLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _timeLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _timeLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_timeLabel];
    
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.numberOfLines = 2;
    _titleLabel.textColor = [MFColor mf_navigationBarTitleColor];
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _titleLabel.font = [UIFont systemFontOfSize:13.0];
    [self.contentView addSubview:_titleLabel];
    
    _infoLabel = [[MFLabel alloc] init];
    _infoLabel.numberOfLines = 0;
    _infoLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _infoLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _infoLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_infoLabel];
    
    _seperateView = [[MFView alloc] init];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
    [self.contentView addSubview:_seperateView];
    
    @weakify(self);
    [_avatarImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.contentView).offset(10);
        make.size.mas_equalTo(CGSizeMake(30, 30));
    }];
    [_authorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerY.equalTo(self.avatarImageView);
        make.left.equalTo(self.avatarImageView.mas_right).offset(10);
    }];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.infoLabel.mas_right);
        make.centerY.equalTo(self.avatarImageView);
    }];
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.timeLabel.mas_left).offset(-5);
        make.centerY.equalTo(self.timeLabel);
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.avatarImageView.mas_bottom).offset(10);
        make.right.equalTo(self.contentView).offset(-16);
    }];
    [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(10);
        make.right.equalTo(self.contentView).offset(-16);
    }];
    
    [_seperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.infoLabel.mas_bottom).offset(10);
        make.left.and.right.and.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(6);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)configCellWithInfo:(MFSpecialInfo *)info
{
    [self.avatarImageView sd_setImageWithURL:[NSURL URLWithString:info.icon] placeholderImage:[UIImage imageNamed:@"user_avatar"]];
    self.authorLabel.text = info.author;
    self.dateLabel.text = info.date;
    self.timeLabel.text = info.time;
    self.titleLabel.text = info.title;
    self.infoLabel.text = info.infomation;
}

@end
