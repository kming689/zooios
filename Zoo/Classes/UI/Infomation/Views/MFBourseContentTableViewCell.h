//
//  MFBourseContentTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTableViewCell.h"

@interface MFBourseContentTableViewCell : MFTableViewCell

- (void)configCellWithTitle:(NSString *)title content:(NSString *)content;

@end
