//
//  MFTimeLineTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTimeLineTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFKitBases.h"

#import "MFTimeLineInfo.h"

@interface MFTimeLineTableViewCell()

@property (nonatomic, strong) MFLabel *dateLabel;

@property (nonatomic, strong) MFLabel *timeLabel;

@property (nonatomic, strong) MFLabel *infoLabel;

@property (nonatomic, strong) MFView *lineView;

@property (nonatomic, strong) MFView *dotView;

@end

@implementation MFTimeLineTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _dateLabel = [[MFLabel alloc] init];
    _dateLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _dateLabel.font = [UIFont systemFontOfSize:13.0];
    [self.contentView addSubview:_dateLabel];
    
    _timeLabel = [[MFLabel alloc] init];
    _timeLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _timeLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _timeLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_timeLabel];
    
    _infoLabel = [[MFLabel alloc] init];
    _infoLabel.numberOfLines = 0;
    _infoLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _infoLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    _infoLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_infoLabel];
    
    _lineView = [[MFView alloc] init];
    _lineView.backgroundColor = [MFColor mf_lineColor];
    [self.contentView addSubview:_lineView];
    
    _dotView = [[MFView alloc] init];
    _dotView.backgroundColor = [MFColor mf_colorWithHexString:@"00DAAA"];
    _dotView.layer.cornerRadius = 1.5;
    _dotView.layer.masksToBounds = YES;
    [self.contentView addSubview:_dotView];
    
    @weakify(self);
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(10);
        make.top.equalTo(self.contentView).offset(5);
        make.height.mas_equalTo(18);
    }];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.dateLabel);
        make.top.equalTo(self.dateLabel.mas_bottom);
        make.height.mas_equalTo(18);
    }];
    [_dotView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerY.equalTo(self.timeLabel);
        make.left.mas_equalTo(50);
        make.size.mas_equalTo(CGSizeMake(3, 3));
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.centerX.equalTo(self.dotView);
        make.top.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
        make.width.mas_equalTo(0.5);
    }];
    [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.timeLabel);
        make.left.equalTo(self.lineView.mas_right).offset(10);
        make.right.equalTo(self.contentView).offset(-40);
        make.bottom.equalTo(self.contentView).offset(-5);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    _lineView.backgroundColor = [MFColor mf_lineColor];
    _dotView.backgroundColor = [MFColor mf_colorWithHexString:@"00DAAA"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
    _lineView.backgroundColor = [MFColor mf_lineColor];
    _dotView.backgroundColor = [MFColor mf_colorWithHexString:@"00DAAA"];
}

- (void)configCellViewTimeLineInfo:(MFTimeLineInfo *)info
{
    self.dateLabel.text = info.date;
    self.timeLabel.text = info.time;
    self.infoLabel.text = info.infomation;
}

- (CGFloat)maxHeightWithText:(NSString *)text
{
    CGSize size = [text boundingRectWithSize:CGSizeMake(200, MAXFLOAT) options:NSStringDrawingUsesFontLeading|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:self.infoLabel.font} context:nil].size;
    return size.height;
}

@end
