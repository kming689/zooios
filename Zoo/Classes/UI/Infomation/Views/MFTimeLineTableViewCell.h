//
//  MFTimeLineTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTableViewCell.h"

@class MFTimeLineInfo;

@interface MFTimeLineTableViewCell : MFTableViewCell

- (CGFloat)maxHeightWithText:(NSString *)text;

- (void)configCellViewTimeLineInfo:(MFTimeLineInfo *)info;

@end
