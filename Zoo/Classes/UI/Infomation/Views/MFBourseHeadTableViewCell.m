//
//  MFBourseHeadTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseHeadTableViewCell.h"
#import "MFKitBases.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFBourseDetail.h"


@interface MFBourseHeadTableViewCell()

@property (nonatomic, strong) MFLabel *volumeLabel;

@property (nonatomic, strong) MFLabel *orderLabel;

@end

@implementation MFBourseHeadTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _volumeLabel = [[MFLabel alloc] init];
    _volumeLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _volumeLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _volumeLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_volumeLabel];
    
    MFLabel *volumeTipLabel = [[MFLabel alloc] init];
    volumeTipLabel.textColor = [MFColor mf_fontGrayStyleColor];
    volumeTipLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    volumeTipLabel.font = [UIFont systemFontOfSize:12.0];
    volumeTipLabel.text = @"交易量:";
    [self.contentView addSubview:volumeTipLabel];
    
    _orderLabel = [[MFLabel alloc] init];
    _orderLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _orderLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _orderLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_orderLabel];
    
    MFLabel *orderTipLabel = [[MFLabel alloc] init];
    orderTipLabel.textColor = [MFColor mf_fontGrayStyleColor];
    orderTipLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    orderTipLabel.font = [UIFont systemFontOfSize:12.0];
    orderTipLabel.text = @"全球排名:";
    [self.contentView addSubview:orderTipLabel];
    
    @weakify(self);
    [_volumeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.contentView).offset(-20);
        make.centerY.equalTo(self.titleLabel);
    }];
    [_orderLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.volumeLabel);
        make.centerY.equalTo(self.subTitleLabel);
    }];
    [orderTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.orderLabel.mas_left).offset(-10);
        make.centerY.equalTo(self.orderLabel);
    }];
    [volumeTipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(orderTipLabel);
        make.center.equalTo(self.volumeLabel);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithBourseDetailInfo:(MFBourseDetail *)info
{
    [self configCellWithBourseInfo:info];
    self.volumeLabel.text = info.businessVolume;
    self.orderLabel.text = info.order;
}

@end
