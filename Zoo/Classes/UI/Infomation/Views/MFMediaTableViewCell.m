//
//  MFMediaTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFMediaTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "MFKitBases.h"

#import "MFMediaInfo.h"

@interface MFMediaTableViewCell()

@property (nonatomic, strong) MFLabel *dateLabel;

@property (nonatomic, strong) MFLabel *timeLabel;

@property (nonatomic, strong) MFLabel *infoLabel;

@property (nonatomic, strong) MFLabel *authorLabel;

@property (nonatomic, strong) MFView *seperateView;

@end

@implementation MFMediaTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    _dateLabel = [[MFLabel alloc] init];
    _dateLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _dateLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _dateLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_dateLabel];
    
    _timeLabel = [[MFLabel alloc] init];
    _timeLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _timeLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _timeLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_timeLabel];
    
    _infoLabel = [[MFLabel alloc] init];
    _infoLabel.numberOfLines = 0;
    _infoLabel.textColor = [MFColor mf_navigationBarTitleColor];
    _infoLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _infoLabel.font = [UIFont systemFontOfSize:14.0];
    [self.contentView addSubview:_infoLabel];
    
    _authorLabel = [[MFLabel alloc] init];
    _authorLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _authorLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _authorLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_authorLabel];
    
    _seperateView = [[MFView alloc] init];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
    [self.contentView addSubview:_seperateView];
    
    @weakify(self);
    [_infoLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.contentView).offset(15);
        make.right.equalTo(self.contentView).offset(-16);
    }];
    [_authorLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.infoLabel).offset(2);
        make.top.equalTo(self.infoLabel.mas_bottom).offset(10);
    }];
    [_timeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.infoLabel.mas_right);
        make.centerY.equalTo(self.authorLabel);
    }];
    [_dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.timeLabel.mas_left).offset(-5);
        make.centerY.equalTo(self.authorLabel);
    }];
    
    [_seperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.equalTo(self.authorLabel.mas_bottom).offset(10);
        make.left.and.right.and.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(6);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)configCellWithMediaInfo:(MFMediaInfo *)info
{
    self.infoLabel.text = info.infomation;
    self.authorLabel.text = info.author;
    self.dateLabel.text = info.date;
    self.timeLabel.text = info.time;
}

@end
