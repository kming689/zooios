//
//  MFCurrencyClassTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrencyClassTableViewCell.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <BlocksKit/UIView+BlocksKit.h>
#import "MFKitBases.h"
#import "MFCurrencyCollectionViewCell.h"

#import "MFCurrencyType.h"

typedef void(^SelectItem)(NSIndexPath *indexPath);

static NSString * const kMFCurrencyClassCollectionViewCell = @"kMFCurrencyClassCollectionViewCell";

@interface MFCurrencyClassTableViewCell()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) MFLabel *titleLabel;

@property (nonatomic, strong) MFImageView *rightIndicator;

@property (nonatomic, strong) MFView *lineView;

@property (nonatomic, strong) MFView *seperateView;

@end

@implementation MFCurrencyClassTableViewCell
{
    NSArray *_currencyClasses;
    SelectItem _selectItemCompletion;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    //调整不同宽度屏幕之间的间距
    _flowLayout.itemSize = CGSizeMake(60, 80);
    if (self.contentView.bounds.size.width > 320) {
        _flowLayout.itemSize = CGSizeMake((self.contentView.bounds.size.width - 16)/5, 80);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _seperateView = [[MFView alloc] init];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
    [self.contentView addSubview:_seperateView];
    
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.textColor = [MFColor mf_navigationBarTitleColor];
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _titleLabel.font = [UIFont systemFontOfSize:13.0];
    [self.contentView addSubview:_titleLabel];
    
    _rightIndicator = [[MFImageView alloc] init];
    _rightIndicator.image = [UIImage imageNamed:@"mine_cell_left"];
    [self.contentView addSubview:_rightIndicator];
    
    _lineView = [[MFView alloc] init];
    _lineView.backgroundColor = [MFColor mf_lineColor];
    [self.contentView addSubview:_lineView];
    
    [self.contentView addSubview:self.collectionView];
    
    @weakify(self);
    [_seperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.and.left.and.right.equalTo(self.contentView);
        make.height.mas_equalTo(6);
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.seperateView.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
    }];
    [_rightIndicator mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.right.equalTo(self.contentView).offset(-12.5);
        make.centerY.equalTo(self.titleLabel);
        //make.size.mas_equalTo(CGSizeMake(13, 18));
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.equalTo(self.contentView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.height.mas_equalTo(0.5);
    }];
    
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.lineView.mas_bottom).offset(10);
        make.left.equalTo(self.contentView).offset(8);
        make.right.equalTo(self.contentView).offset(-8);
        make.bottom.equalTo(self.contentView).offset(-15);
        make.height.mas_equalTo(80).priorityHigh();
    }];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    _lineView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    _lineView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)configCellWithCurrencies:(NSArray<MFCurrencyType *> *)currencies itemSelected:(void (^)(NSIndexPath *indexPath))selectedItem
{
    _currencyClasses = currencies;
    _selectItemCompletion = selectedItem;
    
    self.titleLabel.text = @"支持币种";
    
    NSInteger nCount = [currencies count];
    CGFloat cellHeight = nCount/5*80;
    NSInteger yushu = nCount%5;
    if (yushu != 0) {
        cellHeight += 80;
    }
    
    @weakify(self)
    [self.collectionView mas_remakeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self.lineView.mas_bottom).offset(10);
        make.left.equalTo(self.contentView);
        make.right.equalTo(self.contentView);
        make.bottom.equalTo(self.contentView);
        make.height.mas_equalTo(cellHeight).priorityHigh();
    }];
    
    [self.collectionView reloadData];
}

- (void)refresh
{
    [self.collectionView reloadData];
}

#pragma mark -- 懒加载属性
- (UICollectionView *)collectionView
{
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionVertical;
        _flowLayout = flowLayout;
        
        
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:flowLayout];
        _collectionView.backgroundColor = [UIColor redColor];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.backgroundColor = [MFColor whiteColor];
        [_collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([MFCurrencyCollectionViewCell class]) bundle:[NSBundle bundleForClass:[self class]]] forCellWithReuseIdentifier:kMFCurrencyClassCollectionViewCell];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
    }
    return _collectionView;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    NSInteger nCount = [_currencyClasses count];
    return nCount;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    MFCurrencyCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kMFCurrencyClassCollectionViewCell forIndexPath:indexPath];
    //
    MFCurrencyType *currency = [_currencyClasses objectAtIndex:indexPath.item];
    [cell configCellWithCurrency:currency];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];

    if (_selectItemCompletion) {
        _selectItemCompletion(indexPath);
    }
}

@end
