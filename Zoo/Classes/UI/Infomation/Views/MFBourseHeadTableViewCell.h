//
//  MFBourseHeadTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseTableViewCell.h"

@class MFBourseDetail;

@interface MFBourseHeadTableViewCell : MFBourseTableViewCell

- (void)configCellWithBourseDetailInfo:(MFBourseDetail *)info;

@end
