//
//  MFBourseTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTableViewCell.h"
#import "MFKitBases.h"

@class MFBourse;

@interface MFBourseTableViewCell : MFTableViewCell

@property (nonatomic, strong) MFImageView *titleImageView;

@property (nonatomic, strong) MFLabel *titleLabel;

@property (nonatomic, strong) MFLabel *subTitleLabel;

- (void)configCellWithBourseInfo:(MFBourse *)info;

@end
