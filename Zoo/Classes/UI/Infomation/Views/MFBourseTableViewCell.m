//
//  MFBourseTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseTableViewCell.h"
#import "MFKitBases.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "MFBourse.h"

@interface MFBourseTableViewCell()

@end

@implementation MFBourseTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _titleImageView = [[MFImageView alloc] init];
    _titleImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.contentView addSubview:_titleImageView];
    
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.textColor = [MFColor mf_navigationBarTitleColor];
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _titleLabel.font = [UIFont systemFontOfSize:14.0];
    [self.contentView addSubview:_titleLabel];
    
    _subTitleLabel = [[MFLabel alloc] init];
    _subTitleLabel.textColor = [MFColor mf_fontGrayStyleColor];
    _subTitleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _subTitleLabel.font = [UIFont systemFontOfSize:12.0];
    [self.contentView addSubview:_subTitleLabel];
    
    @weakify(self);
    [_titleImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.centerY.equalTo(self.contentView);
        make.size.mas_equalTo(CGSizeMake(40, 40));
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.titleImageView.mas_right).offset(15);
        make.top.equalTo(self.titleImageView);
    }];
    [_subTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.titleLabel);
        make.bottom.equalTo(self.titleImageView);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configCellWithBourseInfo:(MFBourse *)info
{
    self.titleLabel.text = info.bourseName;
    self.subTitleLabel.text = info.country;
    [self.titleImageView sd_setImageWithURL:[NSURL URLWithString:info.icon] placeholderImage:[UIImage imageNamed:@"market_avatar"]];
}

@end
