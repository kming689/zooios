//
//  MFBaseInfoTableViewCell.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBaseInfoTableViewCell.h"
#import "MFKitBases.h"
#import <Masonry/Masonry.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

#import "MFBaseInfo.h"

@interface MFBaseInfoTableViewCell()

@property (nonatomic, strong) MFLabel *titleLabel;

@property (nonatomic, strong) MFView *lineView;

@property (nonatomic, strong) MFView *seperateView;

@end

@implementation MFBaseInfoTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    _seperateView = [[MFView alloc] init];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
    [self.contentView addSubview:_seperateView];
    
    _titleLabel = [[MFLabel alloc] init];
    _titleLabel.textColor = [MFColor mf_navigationBarTitleColor];
    _titleLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
    _titleLabel.font = [UIFont systemFontOfSize:13.0];
    [self.contentView addSubview:_titleLabel];
    
    _lineView = [[MFView alloc] init];
    _lineView.backgroundColor = [MFColor mf_lineColor];
    [self.contentView addSubview:_lineView];
    
    @weakify(self);
    [_seperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.top.and.left.and.right.equalTo(self.contentView);
        make.height.mas_equalTo(6);
    }];
    [_titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.equalTo(self.contentView).offset(16);
        make.top.equalTo(self.seperateView.mas_bottom).offset(5);
        make.height.mas_equalTo(30);
    }];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self);
        make.left.and.right.equalTo(self.contentView);
        make.top.equalTo(self.titleLabel.mas_bottom).offset(5);
        make.height.mas_equalTo(0.5);
    }];
}

- (void)prepareForReuse
{
    [super prepareForReuse];
    
    NSArray *subViews = [self.contentView subviews];
    for (UIView *subView in subViews) {
        if (subView.tag >= 100) {
            [subView removeFromSuperview];
        }
    }
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    _lineView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    _lineView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.backgroundColor = [MFColor mf_lineColor];
    _seperateView.layer.opacity = 0.5;
}

- (void)configCellWithBaseInfos:(NSArray<MFBaseInfo *> *)baseInfos title:(NSString *)title
{
    self.titleLabel.text = title;
    MFLabel *lastTipLabel = nil;
    NSInteger index = 0;
    for (MFBaseInfo *info in baseInfos) {
        MFLabel *tipLabel = [[MFLabel alloc] init];
        tipLabel.tag = 100+index;
        tipLabel.textColor = [MFColor mf_navigationBarTitleColor];
        tipLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        tipLabel.font = [UIFont systemFontOfSize:12.0];
        tipLabel.text = info.name;
        [self.contentView addSubview:tipLabel];
        
        MFLabel *valueLabel = [[MFLabel alloc] init];
        valueLabel.tag = 10000+index;
        valueLabel.textColor = [MFColor mf_fontGrayStyleColor];
        valueLabel.lineBreakMode = NSLineBreakByTruncatingMiddle;
        valueLabel.font = [UIFont systemFontOfSize:12.0];
        valueLabel.text = info.value;
        [self.contentView addSubview:valueLabel];
        
        @weakify(self);
        if (index+1 != baseInfos.count) {
            if (lastTipLabel) {
                [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    @strongify(self);
                    make.left.equalTo(self.contentView).offset(16);
                    make.top.equalTo(lastTipLabel.mas_bottom).offset(5);
                }];
            }else{
                [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    @strongify(self);
                    make.left.equalTo(self.contentView).offset(16);
                    make.top.equalTo(self.lineView.mas_bottom).offset(10);
                }];
            }
            
            [valueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.contentView).offset(130);
                make.centerY.equalTo(tipLabel);
            }];
        }else{
            if (lastTipLabel) {
                [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    @strongify(self);
                    make.left.equalTo(self.contentView).offset(16);
                    make.top.equalTo(lastTipLabel.mas_bottom).offset(5);
                    make.bottom.equalTo(self.contentView.mas_bottom).offset(-15);
                }];
            }else{
                [tipLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                    @strongify(self);
                    make.left.equalTo(self.contentView).offset(16);
                    make.top.equalTo(self.lineView.mas_bottom).offset(10);
                    make.bottom.equalTo(self.contentView.mas_bottom).offset(-15);
                }];
            }
            
            [valueLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self.contentView).offset(130);
                make.centerY.equalTo(tipLabel);
            }];
        }
        
        lastTipLabel = tipLabel;
        
        index++;
    }
}

@end
