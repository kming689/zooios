//
//  MFCurrencyClassTableViewCell.h
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTableViewCell.h"

@class MFCurrencyType;

@interface MFCurrencyClassTableViewCell : MFTableViewCell

@property (nonatomic, copy) void(^loadMoreSupportCurrency)(void);

- (void)configCellWithCurrencies:(NSArray<MFCurrencyType *> *)currencies itemSelected:(void (^)(NSIndexPath *indexPath))selectedItem;

- (void)refresh;

@end
