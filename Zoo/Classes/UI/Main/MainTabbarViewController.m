//
//  MainTabbarViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MainTabbarViewController.h"
#import "UIImage+MFAdd.h"
#import "MFNavigationController.h"
#import "MFKitBases.h"
#import "MFPriceViewController.h"
#import "MFInfomationViewController.h"
#import "MFMineViewController.h"

@interface MainTabbarViewController ()

@end

@implementation MainTabbarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setup];
}

-(void) setup
{
    //行情
    MFPriceViewController *priceController = [[MFPriceViewController alloc] init];
    [self setupController:priceController image:@"tab_icon_hq_nm" selecedImage:@"tab_icon_hq_hl" titleColorNormal:[MFColor mf_colorWithHexString:@"969796"] titleColorSelected:[MFColor mf_colorWithHexString:@"00DAAA"] titleFontSize:10];
    MFNavigationController *priceNav = [[MFNavigationController alloc] initWithRootViewController:priceController];
    priceNav.tabBarItem.title = @"行情";
    
    //币读
    MFInfomationViewController *infoController = [[MFInfomationViewController alloc] init];
    [self setupController:infoController image:@"tab_icon_bd_nm" selecedImage:@"tab_icon_bd_hl" titleColorNormal:[MFColor mf_colorWithHexString:@"969796"] titleColorSelected:[MFColor mf_colorWithHexString:@"00DAAA"] titleFontSize:10];
    MFNavigationController *infoNav = [[MFNavigationController alloc] initWithRootViewController:infoController];
    infoNav.tabBarItem.title = @"币读";

    //我
    MFMineViewController *mineController = [[MFMineViewController alloc] init];
    [self setupController:mineController image:@"tab_icon_mine_nm" selecedImage:@"tab_icon_mine_hl" titleColorNormal:[MFColor mf_colorWithHexString:@"969796"] titleColorSelected:[MFColor mf_colorWithHexString:@"00DAAA"] titleFontSize:10];
    MFNavigationController *mineNav = [[MFNavigationController alloc] initWithRootViewController:mineController];
    mineNav.tabBarItem.title = @"我";
    
    self.viewControllers = @[priceNav,infoNav,mineNav];
}

#pragma mark - private method
-(void) setupController:(UIViewController *) controller
                  image:(NSString *) image
           selecedImage:(NSString *) selecedImage
       titleColorNormal:(UIColor *) titleColorNormal
     titleColorSelected:(UIColor *) titleColorSelected
          titleFontSize:(NSInteger) titleFontSize
{
    UITabBarItem *item = controller.tabBarItem;
    
    if (image && image.length > 0) {
        item.image = [[UIImage imageNamed:image bundleClass:[self class]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    if (selecedImage && selecedImage.length > 0) {
        item.selectedImage = [[UIImage imageNamed:selecedImage bundleClass:[self class]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    }
    
    if (titleColorNormal) {
        NSDictionary *attributes = @{NSForegroundColorAttributeName : titleColorNormal,
                                     NSFontAttributeName : [UIFont systemFontOfSize:titleFontSize]};
        [item setTitleTextAttributes:attributes forState:UIControlStateNormal];
    }
    
    if (titleColorSelected) {
        NSDictionary *attributes = @{NSForegroundColorAttributeName : titleColorSelected,
                                     NSFontAttributeName : [UIFont systemFontOfSize:titleFontSize]};
        [item setTitleTextAttributes:attributes forState:UIControlStateSelected];
    }
}

#pragma mark - UITabBarControllerDelegate
-(void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController
{

}

@end
