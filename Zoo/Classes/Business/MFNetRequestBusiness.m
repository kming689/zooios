//
//  MFNetRequestBusiness.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFNetRequestBusiness.h"
#import <YYModel/YYModel.h>
#import <MGJRouter/MGJRouter.h>
#import "MFRequestManager.h"
#import "MFHttpHelper.h"
#import "MFCurrency.h"
#import "MFBourse.h"
#import "MFCurrencyDetail.h"
#import "MFChartModel.h"

#import "MFBourseFormatter.h"
#import "MFCurrencyFormatter.h"
#import "MFCurrencyDetailFormatter.h"
#import "MFBourseDetailFormatter.h"
#import "MFTimeLineFormatter.h"
#import "MFMediaFormatter.h"
#import "MFSpecialFormatter.h"
#import "MFNewCurrencyFormatter.h"
#import "MFNewCurrencyDetailFormatter.h"
#import "MFLoginFormatter.h"
#import "MFCurrencyClassFormatter.h"
#import "MFPirceChartModelFormatter.h"
#import "MFNewsFormatter.h"

#import "MFInfoType.h"

#import "MFLoginViewController.h"
#import "MFTransitionNavigationController.h"
#import "MFRouterNavigation.h"

@implementation MFNetRequestBusiness

+ (instancetype)shareInstance
{
    static id sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[[self class] alloc] init];
        MFRequestManager *manager = [MFRequestManager manager];
        [manager addCompletionBlock:^(id response) {
            
            if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
                NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
                if (urlResponse.statusCode == 401) {
                    
                    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                    [userDefaults removeObjectForKey:@"kMFAccount"];
//                    [userDefaults removeObjectForKey:@"refresh_token"];
//                    [userDefaults removeObjectForKey:@"access_token"];
//                    [userDefaults removeObjectForKey:@"token_expires"];
                    [userDefaults removeObjectForKey:@"passWord"];
                    [userDefaults synchronize];
                    
                    [MGJRouter openURL:@"zoo://loginpage"];
                }
            }
        }];
        
        [MGJRouter registerURLPattern:@"zoo://loginpage" toHandler:^(NSDictionary *routerParameters) {
            MFLoginViewController *controller = [[MFLoginViewController alloc] init];
            controller.successLogined = ^(BOOL success, UIViewController *controller) {
                [controller dismissViewControllerAnimated:YES completion:^{
                    
                }];
            };
            MFTransitionNavigationController *nav = [[MFTransitionNavigationController alloc] initWithRootViewController:controller];
            [MFRouterNavigation presentViewController:nav animated:YES completion:nil];
        }];
    });
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
    
    }
    return self;
}

- (NSURLSessionTask *)getLoginCodeWithMobile:(NSString *)mobile success:(void (^)(NSString *code))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:mobile forKey:@"mobile"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiLoginCode() params:params formatter:nil success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)loginWithUserName:(NSString *)userName passWord:(NSString *)passWord success:(void (^)(NSString *))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:userName forKey:@"mobile"];
    [params setObject:passWord forKey:@"code"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager postDataWithPostURL:MFZooApiLogin() params:params formatter:nil success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)getRegisterCodeWithMobile:(NSString *)mobile success:(void (^)(NSString *code))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:mobile forKey:@"mobile"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiRegisterCode() params:params formatter:nil success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)registerWithUserName:(NSString *)userName passWord:(NSString *)passWord success:(void (^)(id response))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:userName forKey:@"mobile"];
    [params setObject:passWord forKey:@"code"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager postDataWithPostURL:MFZooApiRegister() params:params formatter:[MFLoginFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestCurrencyListWithBourse:(NSString *)bourse page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFCurrency *> *))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:bourse forKey:@"market"];
    [params setObject:@(page) forKey:@"page"];
    [params setObject:@(pageSize) forKey:@"pagesize"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiPriceList() params:params formatter:[MFCurrencyFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestCustomCurrencyListWithBourse:(NSString *)bourse page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFCurrency *> *))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
//    if (bourse) {
//        [params setObject:bourse forKey:@"market"];
//    }
//    [params setObject:@(page) forKey:@"page"];
//    [params setObject:@(pageSize) forKey:@"pagesize"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiCustomPriceList() params:params formatter:[MFCurrencyFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)addCustomPriceWithBourse:(NSString *)bourse currency:(NSString *)currencyId success:(void (^)(id response))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:bourse forKey:@"market"];
    [params setObject:currencyId forKey:@"symbol"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager postDataWithPostURL:MFZooApiAddCustom() params:params formatter:nil success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)deleteCustomPriceWithBourse:(NSString *)bourse currency:(NSString *)currencyId success:(void (^)(id response))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:bourse forKey:@"market"];
    [params setObject:currencyId forKey:@"symbol"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager deleteWithURL:MFZooApiDeleteCustom() params:params formatter:nil success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestCurrencyClassSymbolsListWithBourse:(NSString *)bourse page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFCurrencyType *> *))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:bourse forKey:@"marketid"];
    [params setObject:@(page) forKey:@"page"];
    [params setObject:@(pageSize) forKey:@"pagesize"];
    //[params setObject:@"normal" forKey:@"status"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiCurrencyClassList() params:params formatter:[MFCurrencyClassFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestBourseListWithOptionType:(NSString *)type success:(void (^)(NSArray<MFBourse *> *))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiBourseList() params:params formatter:[MFBourseFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestPriceDetailWithBourse:(NSString *)bourse currency:(NSString *)currency success:(void (^)(MFCurrencyDetail *currencyDetail))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiCurrencyDetail() params:params formatter:[MFCurrencyDetailFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestPriceDetailChartDataWithBourse:(NSString *)bourse currency:(NSString *)currency timeInterval:(NSString *)timeInterval success:(void (^)(MFChartModel *chartModel))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSString *cutString = [NSString stringWithFormat:@"symbol:%@|%@",currency,timeInterval];
    [params setObject:cutString forKey:@"cut"];
    
    NSString *url = [NSString stringWithFormat:@"%@/%@/facts",MFZooApiCurrencyChartData(),bourse];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:url params:params formatter:[MFPirceChartModelFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestPriceDetailChartCountDataWithBourse:(NSString *)bourse currency:(NSString *)currency timeInterval:(NSString *)timeInterval success:(void (^)(id))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    NSString *cutString = [NSString stringWithFormat:@"symbol:%@|%@",currency,timeInterval];
    [params setObject:cutString forKey:@"cut"];
    
    NSString *url = [NSString stringWithFormat:@"%@/%@space/aggregate",MFZooApiCurrencyChartCountData(),bourse];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:url params:params formatter:nil success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestNewsWithKeyWord:(NSString *)keyWord success:(void (^)(NSArray *))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:keyWord forKey:@"keyword"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:@"http://if.zoo.one:80/api/news" params:params formatter:[MFNewsFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestCurrencyBaseInfoWithKeyCurrency:(NSString *)currencyBase success:(void (^)(id))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:currencyBase forKey:@"base"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:@"http://if.zoo.one:80/api/base" params:params formatter:nil success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestBourseDetailWithBourseId:(NSString *)bourseId success:(void (^)(MFBourseDetail *))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:bourseId forKey:@"id"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiBourseDetail() params:params formatter:[MFBourseDetailFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestInfoTypeSuccess:(void (^)(NSArray<MFInfoType *> *))completion failure:(void (^)(NSError *))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiInfoType() params:params formatter:nil success:^(id respose) {
        if (completion) {
            NSMutableArray *array = [[NSMutableArray alloc] init];
            for (NSDictionary *dic in respose) {
                MFInfoType *infoType = [MFInfoType yy_modelWithDictionary:dic];
                [array addObject:infoType];
            }
            completion(array);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestTimeLineInfoListWithInfoTypeId:(NSString *)infoTypeId page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFTimeLineInfo *> *infoList))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:infoTypeId forKey:@"seriesid"];
    [params setObject:@(page) forKey:@"page"];
    [params setObject:@(pageSize) forKey:@"pagesize"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiMediaInfoList() params:params formatter:[MFTimeLineFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestMediaInfoListWithInfoTypeId:(NSString *)infoTypeId page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFMediaInfo *> *infoList))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:infoTypeId forKey:@"seriesid"];
    [params setObject:@(page) forKey:@"page"];
    [params setObject:@(pageSize) forKey:@"pagesize"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiMediaInfoList() params:params formatter:[MFMediaFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestSpecialInfoListWithInfoTypeId:(NSString *)infoTypeId page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFSpecialInfo *> *infoList))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:infoTypeId forKey:@"seriesid"];
    [params setObject:@(page) forKey:@"page"];
    [params setObject:@(pageSize) forKey:@"pagesize"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiSpecialInfoList() params:params formatter:[MFSpecialFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestNewCurrencyListPage:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFNewCurrency *> *currencyList))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:@(page) forKey:@"page"];
    [params setObject:@(pageSize) forKey:@"pagesize"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiNewCurrencyList() params:params formatter:[MFNewCurrencyFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

- (NSURLSessionTask *)requestNewCurrencyDetailWithId:(NSString *)currencyId success:(void (^)(MFNewCurrencyDetail *currencyDetail))completion failure:(void (^)(NSError *error))failure
{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:currencyId forKey:@"coinid"];
    
    MFRequestManager *manager = [MFRequestManager manager];
    return [manager getResultWithURL:MFZooApiNewCurrencyDetail() params:params formatter:[MFNewCurrencyDetailFormatter new] success:^(id respose) {
        if (completion) {
            completion(respose);
        }
    } failure:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}

@end
