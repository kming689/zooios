//
//  MFNetRequestBusiness.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBusiness.h"

@class MFCurrency;
@class MFCurrencyType;
@class MFBourse;
@class MFCurrencyDetail;
@class MFBourseDetail;
@class MFInfoType;
@class MFTimeLineInfo;
@class MFMediaInfo;
@class MFSpecialInfo;
@class MFNewCurrency;
@class MFNewCurrencyDetail;
@class MFChartModel;

@interface MFNetRequestBusiness : MFBusiness

+(instancetype)shareInstance;

/**
 获取登录验证码

 @param mobile 手机号
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)getLoginCodeWithMobile:(NSString *)mobile success:(void (^)(NSString *code))completion failure:(void (^)(NSError *error))failure;

/**
 登录

 @param userName 用户名
 @param passWord 密码
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)loginWithUserName:(NSString *)userName passWord:(NSString *)passWord success:(void (^)(NSString *token))completion failure:(void (^)(NSError *error))failure;

/**
 获取注册验证码
 
 @param mobile 手机号
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)getRegisterCodeWithMobile:(NSString *)mobile success:(void (^)(NSString *code))completion failure:(void (^)(NSError *error))failure;

/**
 注册并登录
 
 @param userName 用户名
 @param passWord 密码
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)registerWithUserName:(NSString *)userName passWord:(NSString *)passWord success:(void (^)(id response))completion failure:(void (^)(NSError *error))failure;

/**
 根据交易所类型获取行情列表

 @param bourse 交易所类型，不能为nil
 @param completion 成功
 @param failure 失败
 */
- (NSURLSessionTask *)requestCurrencyListWithBourse:(NSString *)bourse page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFCurrency *> *currencyList))completion failure:(void (^)(NSError *error))failure;

/**
 请求自选类行情列表

 @param bourse 交易所，可以为nil，如果为nil，查询所有交易所自选行情
 @param page 页码
 @param pageSize 分页大小
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestCustomCurrencyListWithBourse:(NSString *)bourse page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFCurrency *> *currencyList))completion failure:(void (^)(NSError *error))failure;

/**
 添加自选币对

 @param bourse 交易所id，目前是backend
 @param currencyId 币对ID，目前是symbol
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)addCustomPriceWithBourse:(NSString *)bourse currency:(NSString *)currencyId success:(void (^)(id response))completion failure:(void (^)(NSError *error))failure;

/**
 取消自选币对
 
 @param bourse 交易所id，目前是backend
 @param currencyId 币对ID，目前是symbol
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)deleteCustomPriceWithBourse:(NSString *)bourse currency:(NSString *)currencyId success:(void (^)(id response))completion failure:(void (^)(NSError *error))failure;

/**
 根据交易所获取支持的货币种类

 @param bourse 交易所
 @param page 页码
 @param pageSize 每页请求数量
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestCurrencyClassSymbolsListWithBourse:(NSString *)bourse page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFCurrencyType *> *currencyList))completion failure:(void (^)(NSError *error))failure;

/**
 获取交易所列表

 @param type 类型，自选还是全部
 @param completion 成功
 @param failure 失败
 @return 任务对象
 */
- (NSURLSessionTask *)requestBourseListWithOptionType:(NSString *)type success:(void (^)(NSArray<MFBourse *> *bourseList))completion failure:(void (^)(NSError *error))failure;

/**
 获取货币详情

 @param bourse 交易所
 @param currency 货币
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestPriceDetailWithBourse:(NSString *)bourse currency:(NSString *)currency success:(void (^)(MFCurrencyDetail *currencyDetail))completion failure:(void (^)(NSError *error))failure;

/**
 获取行情折线图数据

 @param bourse 交易所的backend
 @param currency 货币的symbol
 @param timeInterval 时间段，按照接口要求组装
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestPriceDetailChartDataWithBourse:(NSString *)bourse currency:(NSString *)currency timeInterval:(NSString *)timeInterval success:(void (^)(MFChartModel *chartModel))completion failure:(void (^)(NSError *error))failure;

/**
 获取行情折线图数据（全部）
 
 @param bourse 交易所的backend
 @param currency 货币的symbol
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestPriceDetailChartDataWithBourse:(NSString *)bourse currency:(NSString *)currency success:(void (^)(MFChartModel *chartModel))completion failure:(void (^)(NSError *error))failure;

/**
 获取行情详情折线图统计数据
 
 @param bourse 交易所的backend
 @param currency 货币的symbol
 @param timeInterval 时间段，按照接口要求组装
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestPriceDetailChartCountDataWithBourse:(NSString *)bourse currency:(NSString *)currency timeInterval:(NSString *)timeInterval success:(void (^)(id response))completion failure:(void (^)(NSError *error))failure;

/**
 根据关键字返回最新的新闻

 @param keyWord 关键字
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestNewsWithKeyWord:(NSString *)keyWord success:(void (^)(NSArray *newsList))completion failure:(void (^)(NSError *error))failure;

/**
 获取币对简介等信息

 @param currencyBase 货币basename
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestCurrencyBaseInfoWithKeyCurrency:(NSString *)currencyBase success:(void (^)(id response))completion failure:(void (^)(NSError *error))failure;

/**
 获取交易所详情

 @param bourseId 交易所ID
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestBourseDetailWithBourseId:(NSString *)bourseId success:(void (^)(MFBourseDetail *bourseDetail))completion failure:(void (^)(NSError *error))failure;

/**
 获取信息的分类

 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestInfoTypeSuccess:(void (^)(NSArray<MFInfoType *> *infoTypes))completion failure:(void (^)(NSError *error))failure;

/**
 获取快讯信息列表

 @param infoTypeId 媒体类型ID
 @param page 请求页码
 @param pageSize 每页多少条
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestTimeLineInfoListWithInfoTypeId:(NSString *)infoTypeId page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFTimeLineInfo *> *infoList))completion failure:(void (^)(NSError *error))failure;

/**
 获取媒体信息列表

 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestMediaInfoListWithInfoTypeId:(NSString *)infoTypeId page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFMediaInfo *> *infoList))completion failure:(void (^)(NSError *error))failure;

/**
 获取专栏信息列表

 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestSpecialInfoListWithInfoTypeId:(NSString *)infoTypeId page:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFSpecialInfo *> *infoList))completion failure:(void (^)(NSError *error))failure;

/**
 获取新币列表

 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestNewCurrencyListPage:(NSInteger)page pageSize:(NSInteger)pageSize success:(void (^)(NSArray<MFNewCurrency *> *currencyList))completion failure:(void (^)(NSError *error))failure;

/**
 获取新币详情
 
 @param completion 成功
 @param failure 失败
 @return 任务
 */
- (NSURLSessionTask *)requestNewCurrencyDetailWithId:(NSString *)currencyId success:(void (^)(MFNewCurrencyDetail *currencyDetail))completion failure:(void (^)(NSError *error))failure;

@end
