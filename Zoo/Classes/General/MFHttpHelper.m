//
//  MFHttpHelper.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFHttpHelper.h"

#define HOST_IP @"http://if.zoo.one"

#define HOST_URL @"http://ifm.zoo.one"

#define MAIN_PORT @"80"


@implementation MFHttpHelper

NSString * const MFZooApiLoginCode(){
    return [NSString stringWithFormat:@"%@:%@/app/logverifycode",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiLogin(){
    return [NSString stringWithFormat:@"%@:%@/app/login",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiRegisterCode(){
    return [NSString stringWithFormat:@"%@:%@/app/regverifycode",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiRegister(){
    return [NSString stringWithFormat:@"%@:%@/app/register",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiPriceList(){
    return [NSString stringWithFormat:@"%@:%@/ticker",HOST_URL,MAIN_PORT];
};

NSString * const MFZooApiCustomPriceList(){
    return [NSString stringWithFormat:@"%@:%@/api/follow",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiAddCustom(){
    return [NSString stringWithFormat:@"%@:%@/api/follow",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiDeleteCustom(){
    return [NSString stringWithFormat:@"%@:%@/api/follow",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiCurrencyClassList(){
    return [NSString stringWithFormat:@"%@:%@/api/symbols",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiBourseList(){
    return [NSString stringWithFormat:@"%@:%@/api/markets",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiCurrencyDetail(){
    return [NSString stringWithFormat:@"%@:%@/slicer/cube/ticker/facts",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiCurrencyChartData(){
    return [NSString stringWithFormat:@"%@:%@/slicer/cube",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiCurrencyChartCountData(){
    return [NSString stringWithFormat:@"%@:%@/slicer/cube",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiBourseDetail(){
    return [NSString stringWithFormat:@"%@:%@/api/market",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiInfoType(){
    return [NSString stringWithFormat:@"%@:%@/api/series",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiTimeLineInfoList(){
    return [NSString stringWithFormat:@"%@:%@/api/blogs",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiMediaInfoList(){
    return [NSString stringWithFormat:@"%@:%@/api/blogs",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiSpecialInfoList(){
    return [NSString stringWithFormat:@"%@:%@/api/blogs",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiNewCurrencyList(){
    return [NSString stringWithFormat:@"%@:%@/api/coins",HOST_IP,MAIN_PORT];
};

NSString * const MFZooApiNewCurrencyDetail(){
    return [NSString stringWithFormat:@"%@:%@/api/coin",HOST_IP,MAIN_PORT];
};

@end
