//
//  MFTransitionNavigationController.m
//  Zoo
//
//  Created by tanfameng on 2018/3/3.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTransitionNavigationController.h"
#import <YPNavigationBarTransition/YPNavigationBarTransition.h>

@interface MFTransitionNavigationController ()<UINavigationControllerDelegate,YPNavigationBarConfigureStyle>

@property (nonatomic, strong) YPNavigationBarTransitionCenter *transitionCenter;
@property (nonatomic, strong) NSString *navigationBackgroundImageName;
@property (nonatomic, strong) UIImage *navigationBackgroundImage;

@end

@implementation MFTransitionNavigationController

-(instancetype)initWithRootViewController:(UIViewController *)rootViewController
{
    self = [super initWithRootViewController:rootViewController];
    if (self) {
        self.navigationBackgroundImageName = @"nav_BackGround";
        UIImage *image = [UIImage imageNamed:self.navigationBackgroundImageName];
        self.navigationBackgroundImage = [image stretchableImageWithLeftCapWidth:image.size.width / 2 topCapHeight:image.size.height / 2];
        
        self.delegate = self;
        _transitionCenter = [[YPNavigationBarTransitionCenter alloc] initWithDefaultBarConfiguration:self];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.navigationBackgroundImageName = @"nav_BackGround";
        UIImage *image = [UIImage imageNamed:self.navigationBackgroundImageName];
        self.navigationBackgroundImage = [image stretchableImageWithLeftCapWidth:image.size.width / 2 topCapHeight:image.size.height / 2];
        
        self.delegate = self;
        _transitionCenter = [[YPNavigationBarTransitionCenter alloc] initWithDefaultBarConfiguration:self];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UINavigationControllerDelegate
- (void) navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [_transitionCenter navigationController:navigationController
                     willShowViewController:viewController
                                   animated:animated];
}

- (void) navigationController:(UINavigationController *)navigationController didShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    [_transitionCenter navigationController:navigationController
                      didShowViewController:viewController
                                   animated:animated];
}

#pragma mark - YPNavigationBarConfigureStyle
- (YPNavigationBarConfigurations) yp_navigtionBarConfiguration {
    YPNavigationBarConfigurations conf = YPNavigationBarConfigurationsDefault;
    conf |= YPNavigationBarBackgroundStyleOpaque;
    conf |= YPNavigationBarBackgroundStyleImage;
    return conf;
}

- (UIColor *) yp_navigationBarTintColor
{
    return [UIColor whiteColor];
}

-(UIImage *)yp_navigationBackgroundImageWithIdentifier:(NSString *__autoreleasing *)identifier
{
    if (identifier) *identifier = self.navigationBackgroundImageName;
    return self.navigationBackgroundImage;
}

@end
