//
//  MFMarco.h
//  Zoo
//
//  Created by tanfameng on 2018/3/11.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import <Foundation/Foundation.h>

#ifndef MFMarco_h
#define MFMarco_h

typedef NS_ENUM(NSInteger, eCTAuthenticationState) {
    eCTAuthenticationStateNone = 0,
    eCTAuthenticationStateBeing,//认证中
    eCTAuthenticationStateFail, //认证失败
    eCTAuthenticationStateDone, //认证完成
};

typedef NS_ENUM(NSUInteger, MFPriceType) {
    MFPriceTypeCustom,
    MFPriceTypeAll,
};

#endif /* MFMarco_h */
