//
//  MFCurrencyFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrencyFormatter.h"
#import "MFCurrency.h"
#import <YYModel/YYModel.h>

@implementation MFCurrencyFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    NSArray *symbols = nil;
    if ([jsonObject isKindOfClass:[NSArray class]]) {
        symbols = jsonObject;
    }else{
        symbols = [jsonObject objectForKey:@"data"];
    }
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *symbolDic in symbols) {
        if (![symbolDic isEqual:[NSNull null]]) {
            MFCurrency *currency = [MFCurrency yy_modelWithDictionary:symbolDic];
            //本地计算涨跌幅
            NSNumber *openNumber = [symbolDic objectForKey:@"open"];
            NSNumber *closeNumber = [symbolDic objectForKey:@"close"];
            double open = openNumber.doubleValue;
            double close = closeNumber.doubleValue;
            double percent = (close - open)/open;
            NSString *percentString = [NSString stringWithFormat:@"%2.2f%%",percent*100];
            currency.changePercent = percentString;
            [array addObject:currency];
        }
    }
    return array;
}

@end
