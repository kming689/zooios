//
//  MFMediaFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFMediaFormatter.h"
#import "NSDate+MFAdd.h"
#import "MFMediaInfo.h"

@implementation MFMediaFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    NSArray *data = [jsonObject objectForKey:@"data"];
    NSMutableArray *infoList = [[NSMutableArray alloc] init];
    for (NSDictionary *infoDic in data) {
        MFMediaInfo *info = [[MFMediaInfo alloc] init];
        info.infoId = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"id"]];
        info.title = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"title"]];
        if ([infoDic objectForKey:@"author"] != nil&&[infoDic objectForKey:@"author"] != [NSNull null]) {
            info.author = [infoDic objectForKey:@"author"];
        }else{
            info.author = @"  ";
        }
        if ([infoDic objectForKey:@"content_raw"] != nil&&[infoDic objectForKey:@"content_raw"] != [NSNull null]) {
            NSString *str = [infoDic objectForKey:@"content_raw"];
            info.infomation = str;
        }
        NSString *publish = [infoDic objectForKey:@"published_at"];
        NSDate *date = [NSDate dateWithString:publish format:@"yyyy-MM-dd HH:mm:ss"];
        info.date = [date stringWithFormat:@"MM-dd"];
        info.time = [date stringWithFormat:@"hh:mm"];
        info.contentUrl = [infoDic objectForKey:@"url"];
        info.htmlContent = [infoDic objectForKey:@"content_html"];
        [infoList addObject:info];
    }
    return infoList;
}

@end
