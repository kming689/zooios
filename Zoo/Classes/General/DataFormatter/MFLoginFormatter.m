//
//  MFLoginFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/3/4.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFLoginFormatter.h"
#import <YYModel/YYModel.h>
#import "NSDate+MFAdd.h"
#import "MFAccountManager.h"
#import "MFAccount.h"
#import "MFAccountInfo.h"

@implementation MFLoginFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    MFAccountManager *accountMgr = [MFAccountManager shareInstance];
    MFAccount *account = [[MFAccount alloc] init];
    accountMgr.account = account;
    
    //解析登录返回数据
    NSDictionary *userDic = [jsonObject objectForKey:@"user"];
    NSDictionary *codeDic = [jsonObject objectForKey:@"code"];
    
    account.refreshToken = [NSString stringWithFormat:@"%@",[codeDic objectForKey:@"refresh_token"]];
    account.accessToken = [NSString stringWithFormat:@"%@",[codeDic objectForKey:@"access_token"]];
    account.tokenType = [NSString stringWithFormat:@"%@",[codeDic objectForKey:@"token_type"]];
    account.tokenExpires = ((NSNumber *)[codeDic objectForKey:@"expires_in"]).longLongValue;
    account.userId = [NSString stringWithFormat:@"%@",[userDic objectForKey:@"id"]];
    if (![self checkNullOrNilValue:[userDic objectForKey:@"email"]]) {
        account.email = [userDic objectForKey:@"email"];
    }
    if (![self checkNullOrNilValue:[userDic objectForKey:@"mobile"]]) {
        account.mobile = [userDic objectForKey:@"mobile"];
    }
    if (![self checkNullOrNilValue:[userDic objectForKey:@"name"]]) {
        account.userName = [userDic objectForKey:@"name"];
    }
    NSString *create = [userDic objectForKey:@"create_at"];
    NSDate *date = [NSDate dateWithString:create format:@"yyyy-MM-dd HH:mm:ss"];
    account.createTime = date;
    
    MFAccountInfo *accountInfo = [[MFAccountInfo alloc] init];
    account.accountInfo = accountInfo;
    accountInfo.fund = @"8,365.3221";
    
    //保存token
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:[account yy_modelToJSONString] forKey:@"kMFAccount"];
//    [userDefaults setObject:account.refreshToken forKey:@"refresh_token"];
//    [userDefaults setObject:account.accessToken forKey:@"access_token"];
//    [userDefaults setObject:[NSNumber numberWithLongLong:account.tokenExpires] forKey:@"token_expires"];
    [userDefaults synchronize];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kMFAccountLogined" object:account];
    
    return jsonObject;
}

@end
