//
//  MFCurrencyDetailFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/2/10.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrencyDetailFormatter.h"
#import "MFChartModel.h"
#import "MFCurrencyDetail.h"

@implementation MFCurrencyDetailFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    MFCurrencyDetail *detail = [[MFCurrencyDetail alloc] init];
    detail.currencyName = @"BTC";
    detail.currencyChinaName = @"比特币";
    detail.bourseName = @"OKEx";
    detail.currencyCashPrice = @"8,365.3221";
    detail.priceUnit = @"CNY";
    detail.changePercent = @"3.55%";
    detail.currencyDetailName = @"Bit Coin 比特币";
    detail.bourseIntroduction = @"全国最大交易所";
    detail.introduction = @"iOS支持两套图形API族：Core Graphics/QuartZ 2D 和OpenGL ES。OpenGL ES是跨平台的图形API，属于OpenGL的一个简化版本。QuartZ 2D是苹果公司开发的一套API，它是Core Graphics Framework的一部分。需要注意的是：OpenGL ES是应用程序编程接口，该接口描述了方法、结构、函数应具有的行为以及应该如何被使用的语义。也就是说它只定义了一套规范，具体的实现由设备制造商根据规范去做。而往往很多人对接口和实现存在误解。举一个不恰当的比喻：上发条的时钟和装电池的时钟都有相同的可视行为，但两者的内部实现截然不同。因为制造商可以自由的实现Open GL ES，所以不同系统实现的OpenGL ES也存在着巨大的性能差异。";
    
    MFChartModel *chartModel = [[MFChartModel alloc] init];
    chartModel.title = @"24小时折线";
    chartModel.chartType = @"line";
    
    NSMutableArray *xValues = [[NSMutableArray alloc] init];
    NSMutableArray *yValueArray = [[NSMutableArray alloc] init];
    
    NSArray *yArrayKeys = @[@"1"];
    for (int j = 0; j < [yArrayKeys count]; j++) {
        //Y轴,多组数据
        NSMutableArray *yValues = [[NSMutableArray alloc] init];
        
        [yValueArray addObject:yValues];
    }
    
    for (int i = 0; i < 25; i++) {
        
        //X轴
        [xValues addObject:@"8:00"];
        
        //y
        NSArray *yArrayKeys = @[@"1"];;
        for (int j = 0; j < [yArrayKeys count]; j++) {
            int rand = abs(((int)arc4random()))%1000;
            NSString *yValue = [NSString stringWithFormat:@"%i",rand];
            NSString *lastYvalue = yValue;
            if ([lastYvalue containsString:@"%"]) {
                lastYvalue = [lastYvalue substringToIndex:lastYvalue.length - 1];
            }
            
            NSMutableArray *yValues = [yValueArray objectAtIndex:j];
            [yValues addObject:lastYvalue];
        }
    }
    chartModel.xValue = xValues;
    chartModel.yValue = yValueArray;
    
    NSMutableArray *selects = [[NSMutableArray alloc] init];
    for (int i = 0; i < yArrayKeys.count; i++) {
        [selects addObject:[NSNumber numberWithInt:i]];
    }
    chartModel.chartSelect = selects;
    
    [detail.chartModels addObject:chartModel];
    
    return detail;
}

@end
