//
//  MFBourseFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseFormatter.h"
#import "MFBourse.h"
#import <YYModel/YYModel.h>

@implementation MFBourseFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    NSArray *bourseArray = jsonObject;
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *bourseDic in bourseArray) {
        MFBourse *bourse = [MFBourse yy_modelWithDictionary:bourseDic];
        [array addObject:bourse];
    }

    return array;
}

@end
