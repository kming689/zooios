//
//  MFCurrencyClassFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/3/15.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrencyClassFormatter.h"
#import <YYModel/YYModel.h>
#import "MFCurrencyType.h"

@implementation MFCurrencyClassFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    NSArray *symbols = [jsonObject objectForKey:@"data"];
    NSMutableArray *array = [[NSMutableArray alloc] init];
    for (NSDictionary *symbolDic in symbols) {
        MFCurrencyType *currency = [MFCurrencyType yy_modelWithDictionary:symbolDic];
        [array addObject:currency];
    }
    return array;
}

@end
