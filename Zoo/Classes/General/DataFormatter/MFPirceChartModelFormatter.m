//
//  MFPirceChartModelFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/3/18.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFPirceChartModelFormatter.h"
#import <YYModel/YYModel.h>
#import "NSDate+MFAdd.h"
#import "MFChartModel.h"

@implementation MFPirceChartModelFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    NSArray *data = jsonObject;
    
    MFChartModel *chartModel = [[MFChartModel alloc] init];
    chartModel.chartType = @"line";
    
    NSMutableArray *xValues = [[NSMutableArray alloc] init];
    NSMutableArray *yValueArray = [[NSMutableArray alloc] init];
    
    NSArray *yArrayKeys = @[@"1"];
    for (int j = 0; j < [yArrayKeys count]; j++) {
        //Y轴,多组数据
        NSMutableArray *yValues = [[NSMutableArray alloc] init];
        
        [yValueArray addObject:yValues];
    }
    
    for (NSDictionary *dic in data) {
        
        
        NSArray *yArrayKeys = @[@"1"];;
        for (int j = 0; j < [yArrayKeys count]; j++) {
            
            if (![self checkNullOrNilValue:[dic objectForKey:@"close"]]) {
                //X轴
                NSString *xValue = [dic objectForKey:@"published_at"];
                NSDate *date = [NSDate dateWithString:xValue format:@"yyyy-MM-dd'T'HH:mm:ss"];
                NSString *lastXvalue = [date stringWithFormat:@"yyyy-MM-dd'T'HH:mm"];
                [xValues addObject:lastXvalue];
                
                //y
                NSNumber *number = [dic objectForKey:@"close"];
                double closeValue = number.doubleValue;
                NSString *yValue = [NSString stringWithFormat:@"%5.5f",closeValue];
                NSString *lastYvalue = yValue;
                if ([lastYvalue containsString:@"%"]) {
                    lastYvalue = [lastYvalue substringToIndex:lastYvalue.length - 1];
                }
                
                NSMutableArray *yValues = [yValueArray objectAtIndex:j];
                [yValues addObject:lastYvalue];
            }
        }
    }
    chartModel.xValue = xValues;
    chartModel.yValue = yValueArray;
    
    NSMutableArray *selects = [[NSMutableArray alloc] init];
    for (int i = 0; i < yArrayKeys.count; i++) {
        [selects addObject:[NSNumber numberWithInt:i]];
    }
    chartModel.chartSelect = selects;
    
    return chartModel;
}

@end
