//
//  MFTimeLineFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFTimeLineFormatter.h"
#import "MFTimeLineInfo.h"
#import "NSDate+MFAdd.h"

@implementation MFTimeLineFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    NSArray *data = [jsonObject objectForKey:@"data"];
    NSMutableArray *infoList = [[NSMutableArray alloc] init];
    for (NSDictionary *infoDic in data) {
        MFTimeLineInfo *info = [[MFTimeLineInfo alloc] init];
        info.infoId = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"id"]];
        if ([infoDic objectForKey:@"content_raw"] != nil&&[infoDic objectForKey:@"content_raw"] != [NSNull null]) {
            NSString *str = [infoDic objectForKey:@"content_raw"];
            info.infomation = str;
        }
        NSString *publish = [infoDic objectForKey:@"published_at"];
        NSDate *date = [NSDate dateWithString:publish format:@"yyyy-MM-dd HH:mm:ss"];
        info.date = [date stringWithFormat:@"MM-dd"];
        info.time = [date stringWithFormat:@"hh:mm"];
        [infoList addObject:info];
    }
    return infoList;
}

@end
