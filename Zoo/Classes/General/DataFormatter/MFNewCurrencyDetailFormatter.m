//
//  MFNewCurrencyDetailFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFNewCurrencyDetailFormatter.h"
#import "MFNewCurrencyDetail.h"
#import "MFBaseInfo.h"

@implementation MFNewCurrencyDetailFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    NSDictionary *currencyDic = jsonObject;;
    MFNewCurrencyDetail *info = [[MFNewCurrencyDetail alloc] init];
    info.currencyName = [NSString stringWithFormat:@"%@",[currencyDic objectForKey:@"name"]];
    info.abstract = @"一句话介绍";
    info.introduction = [currencyDic objectForKey:@"content_raw"];
    info.icon = [currencyDic objectForKey:@"icon_url"];
    info.teamIcon = [currencyDic objectForKey:@"team_icon_url"];
    if (![self checkNullOrNilValue:[currencyDic objectForKey:@"team"]]) {
        info.teamName = [currencyDic objectForKey:@"team"];
    }
    if (![self checkNullOrNilValue:[currencyDic objectForKey:@"team_raw"]]) {
        info.teamIntro = [currencyDic objectForKey:@"team_raw"];
    }
    
    for (int i = 0; i < 5; i++) {
        MFBaseInfo *baseInfo = [[MFBaseInfo alloc] init];
        if (i == 0) {
            baseInfo.name = @"PLATFORM";
            baseInfo.value = [NSString stringWithFormat:@"%@",[currencyDic objectForKey:@"platform"]];
        }else if (i == 1) {
            baseInfo.name = @"TYPE";
            baseInfo.value = [NSString stringWithFormat:@"%@",[currencyDic objectForKey:@"type"]];
        }else if (i == 2) {
            baseInfo.name = @"认证方式";
            baseInfo.value = @"未公开";
        }else if (i == 3) {
            baseInfo.name = @"供给率";
            baseInfo.value = [currencyDic objectForKey:@"supply"];
        }else if (i == 4) {
            baseInfo.name = @"交易所";
            baseInfo.value = @"未公开";
        }else if (i == 5) {
            baseInfo.name = @"官方网站";
            baseInfo.value = @"http://www.baidu.com";
        }
        [info.baseInfos addObject:baseInfo];
    }
    
    return info;
}

@end
