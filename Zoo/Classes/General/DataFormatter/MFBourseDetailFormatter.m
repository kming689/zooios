//
//  MFBourseDetailFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseDetailFormatter.h"
#import "MFBourseDetail.h"
#import "MFCurrency.h"
#import <YYModel/YYModel.h>

@implementation MFBourseDetailFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    MFBourseDetail *bourseDetail = [MFBourseDetail yy_modelWithDictionary:jsonObject];
    
    return bourseDetail;
}

@end
