//
//  MFNewCurrencyFormatter.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFNewCurrencyFormatter.h"
#import "NSDate+MFAdd.h"
#import "MFNewCurrency.h"

@implementation MFNewCurrencyFormatter

- (id)convertModelFromJsonObject:(id)jsonObject
{
    NSArray *data = [jsonObject objectForKey:@"data"];
    NSMutableArray *infoList = [[NSMutableArray alloc] init];
    for (NSDictionary *infoDic in data) {
        MFNewCurrency *info = [[MFNewCurrency alloc] init];
        info.currencyId = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"id"]];
        info.currencyName = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"name"]];
        info.abstract = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"content"]];
        info.startStatus = @"未开始";
        if (![self checkNullOrNilValue:[infoDic objectForKey:@"follow"]]) {
            info.attention = [NSString stringWithFormat:@"%@",[infoDic objectForKey:@"follow"]];
        }
        if (![self checkNullOrNilValue:[infoDic objectForKey:@"icon_url"]]) {
            info.icon = [infoDic objectForKey:@"icon_url"];
        }
        if (![self checkNullOrNilValue:[infoDic objectForKey:@"begin"]]&&![self checkNullOrNilValue:[infoDic objectForKey:@"end"]]) {
            NSString *begin = [infoDic objectForKey:@"begin"];
            NSString *end = [infoDic objectForKey:@"end"];
            NSDate *beginDate = [NSDate dateWithString:begin format:@"yyyy-MM-dd HH:mm:ss"];
            NSDate *endDate = [NSDate dateWithString:end format:@"yyyy-MM-dd HH:mm:ss"];
            NSString *beginStr = [beginDate stringWithFormat:@"yyyy.MM.dd"];
            NSString *endStr = [endDate stringWithFormat:@"yyyy.MM.dd"];
            info.publishTime = [NSString stringWithFormat:@"%@-%@",beginStr,endStr];
        }
        [infoList addObject:info];
    }
    return infoList;
}

@end
