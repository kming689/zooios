//
//  MFHttpHelper.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@interface MFHttpHelper : MFObject

/**
 获取登录验证码
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiLoginCode(void);

/**
 登录
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiLogin(void);

/**
 获取注册验证码
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiRegisterCode(void);

/**
 注册并登录
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiRegister(void);

/**
 获取行情列表
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiPriceList(void);

/**
 获取自选行情列表
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiCustomPriceList(void);

/**
 添加自选
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiAddCustom(void);

/**
 取消自选
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiDeleteCustom(void);

/**
 获取交易所支持币种
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiCurrencyClassList(void);

/**
 获取交易所列表
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiBourseList(void);

/**
 获取货币详情
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiCurrencyDetail(void);

/**
 获取货币折线图数据
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiCurrencyChartData(void);

/**
 获取货币折线图统计数据
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiCurrencyChartCountData(void);

/**
 获取交易所详情
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiBourseDetail(void);

/**
 获取币读信息分类
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiInfoType(void);

/**
 获取快讯信息列表
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiTimeLineInfoList(void);

/**
 获取媒体信息列表
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiMediaInfoList(void);

/**
 获取专栏信息列表
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiSpecialInfoList(void);

/**
 获取新币列表
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiNewCurrencyList(void);

/**
 获取新币详情
 
 @return NSString
 */
UIKIT_EXTERN NSString * const MFZooApiNewCurrencyDetail(void);

@end
