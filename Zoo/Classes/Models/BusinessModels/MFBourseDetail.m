//
//  MFBourseDetail.m
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourseDetail.h"
#import <YYModel/YYModel.h>

@implementation MFBourseDetail

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"icon" : @"icon_url",
             @"bourseId" : @"id",
             @"backend" : @"backend",
             @"bourseName" : @"name",
             @"businessVolume" : @"volume",
             @"order" : @"position",
             @"demand" : @"openaccount",
             @"introduction" : @"content",
             @"country" : @"country"};
}

- (NSDictionary *)modelCustomWillTransformFromDictionary:(NSDictionary *)dic
{
    NSMutableDictionary *newDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
    
    return newDic;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _currencyClasses = [[NSMutableArray alloc] init];
    }
    return self;
}

@end
