//
//  MFSpecialInfo.m
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFSpecialInfo.h"
#import <YYModel/YYModel.h>

@implementation MFSpecialInfo

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"icon" : @"icon_url",
             @"date" : @"date",
             @"time" : @"time",
             @"title" : @"title",
             @"author" : @"author",
             @"infomation" : @"infomation"
             };
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
