//
//  MFCurrencyDetail.h
//  Zoo
//
//  Created by tanfameng on 2018/2/10.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrency.h"

@class MFChartModel;

@interface MFCurrencyDetail : MFCurrency

@property (nonatomic, strong) NSString *currencyChinaName;//货币名字,中文名字

@property (nonatomic, strong) NSString *currencyDetailName;//货币名字,中文名字

@property (nonatomic, strong) NSString *introduction;//简介

@property (nonatomic, strong) NSString *bourseIntroduction;//交易所简介

@property (nonatomic, strong) NSMutableArray *newsList;//新闻列表

@property (nonatomic, strong) NSMutableArray *remarkList;//评论列表

@property (nonatomic, strong) NSMutableArray *chartModels;

@property (nonatomic, strong) NSMutableArray *chartTitles;

@end
