//
//  MFBourse.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@class MFCurrency;

@interface MFBourse : MFObject

@property (nonatomic, strong) NSString *bourseId;//交易所id

@property (nonatomic, strong) NSString *backend;//不知道是啥，反正可以作为区别交易所的一个字段，用于很多接口的参数

@property (nonatomic, strong) NSString *bourseName;//交易所名字

@property (nonatomic, strong) NSString *country;//国家

@property (nonatomic, strong) NSString *icon;

@property (nonatomic, strong) NSMutableArray <MFCurrency *> *currencyList;//货币交易所的货币列表

@end
