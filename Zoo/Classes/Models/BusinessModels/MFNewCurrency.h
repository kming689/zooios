//
//  MFNewCurrency.h
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrency.h"

@interface MFNewCurrency : MFCurrency

@property (nonatomic, strong) NSString *introduction;//简介

@property (nonatomic, strong) NSString *attention;//关注

@property (nonatomic, strong) NSString *publishTime;//发布时间

@property (nonatomic, strong) NSString *startStatus;

@end
