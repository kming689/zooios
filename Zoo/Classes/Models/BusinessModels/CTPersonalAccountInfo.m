//
//  CTPersonalAccountInfo.m
//  CTYun
//
//  Created by tanfameng on 2018/3/7.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTPersonalAccountInfo.h"
#import <YYModel/YYModel.h>

@implementation CTPersonalAccountInfo

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (NSString *)getSexString
{
    if (self.sex == 0) {
        return @"女";
    }else{
        return @"男";
    }
}

@end
