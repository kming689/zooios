//
//  MFInfoType.h
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@interface MFInfoType : MFObject

@property (nonatomic, strong) NSString *typeId;

@property (nonatomic, strong) NSString *typeName;

@end
