//
//  MFInfomation.m
//  Zoo
//
//  Created by tanfameng on 2018/3/1.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFInfomation.h"

@implementation MFInfomation

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (BOOL)isEqual:(MFInfomation *)object
{
    if ([self.infoId isEqualToString:object.infoId]) {
        return YES;
    }
    return NO;
}

@end
