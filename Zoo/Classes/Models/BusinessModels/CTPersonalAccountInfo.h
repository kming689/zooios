//
//  CTPersonalAccountInfo.h
//  CTYun
//
//  Created by tanfameng on 2018/3/7.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "CTAccountInfomation.h"

@interface CTPersonalAccountInfo : CTAccountInfomation

@property (nonatomic, assign) NSInteger sex;//性别

@property (nonatomic, assign) NSInteger cardType;//证件类型

@property (nonatomic, strong) NSString *cardNo;//证件号码

@end
