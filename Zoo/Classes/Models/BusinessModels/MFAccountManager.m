//
//  MFAccountManager.m
//  Zoo
//
//  Created by tanfameng on 2018/3/4.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFAccountManager.h"
#import "MFAccount.h"

@implementation MFAccountManager

+ (instancetype)shareInstance
{
    static MFAccountManager *accountManager;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        accountManager = [[MFAccountManager alloc] init];
    });
    return accountManager;
}

- (BOOL)isLogin
{
    return (self.account != nil);
}

@end
