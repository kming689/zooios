//
//  MFCurrency.h
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@interface MFCurrency : MFObject

@property (nonatomic, strong) NSString *bourseId;

@property (nonatomic, strong) NSString *bourseName;

@property (nonatomic, strong) NSString *icon;

@property (nonatomic, strong) NSString *symbol;//币对名字

@property (nonatomic, strong) NSString *currencyId;//货币id

@property (nonatomic, strong) NSString *currencyName;//货币名字,英文名字

@property (nonatomic, strong) NSString *conversionName;//货币兑换成的通用货币名字

@property (nonatomic, strong) NSString *conversionPrice;//货币兑换成的通用货币价格

@property (nonatomic, strong) NSString *currencyCashPrice;//货币兑换成法币价格

@property (nonatomic, strong) NSString *priceUnit;//货币价格单位，美元还是人民币

@property (nonatomic, strong) NSString *changePercent;//涨跌幅

@property (nonatomic, strong) NSString *changePrice;//涨跌价格

@property (nonatomic, strong) NSString *abstract;//一句话介绍

@property (nonatomic, assign) BOOL isFollowed;//是否关注

- (BOOL)isEqual:(MFCurrency *)object;

- (void)cloneFromCurrency:(MFCurrency *)currency;

@end
