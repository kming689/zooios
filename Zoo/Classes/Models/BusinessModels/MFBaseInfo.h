//
//  MFBaseInfo.h
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@interface MFBaseInfo : MFObject

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSString *value;

@end
