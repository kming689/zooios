//
//  MFBourseDetail.h
//  Zoo
//
//  Created by tanfameng on 2018/2/13.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourse.h"

@class MFCurrencyType;

@interface MFBourseDetail : MFBourse

@property (nonatomic, strong) NSString *order;//排名

@property (nonatomic, strong) NSString *businessVolume;//交易量

@property (nonatomic, strong) NSString *introduction;//交易所简介

@property (nonatomic, strong) NSMutableArray<MFCurrencyType *> *currencyClasses;//支持币种

@property (nonatomic, strong) NSString *demand;//开户要求

@end
