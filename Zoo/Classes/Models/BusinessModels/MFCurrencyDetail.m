//
//  MFCurrencyDetail.m
//  Zoo
//
//  Created by tanfameng on 2018/2/10.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrencyDetail.h"
#import <YYModel/YYModel.h>

@implementation MFCurrencyDetail

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"icon" : @"icon_url",
             @"currencyId" : @"id",
             @"currencyName" : @"base",
             @"conversionPrice" : @"close",
             @"currencyCashPrice" : @"price",
             @"priceUnit" : @"unit",
             @"isFollowed" : @"follow",
             @"bourseId" : @"market",
             @"bourseName" : @"marketname",
             @"conversionName" : @"quote"
             };
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        _newsList = [[NSMutableArray alloc] init];
        _remarkList = [[NSMutableArray alloc] init];
        _chartModels = [[NSMutableArray alloc] init];
        _chartTitles = [[NSMutableArray alloc] init];
    }
    return self;
}

@end
