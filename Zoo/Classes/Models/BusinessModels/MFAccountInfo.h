//
//  MFAccountInfo.h
//  Zoo
//
//  Created by tanfameng on 2018/3/4.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@interface MFAccountInfo : MFObject

@property (nonatomic, strong) NSString *fund;//资产

@property (nonatomic, strong) NSString *bourseName;//绑定交易所名字

@end
