//
//  MFNewCurrencyDetail.h
//  Zoo
//
//  Created by tanfameng on 2018/2/27.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFNewCurrency.h"

@class MFBaseInfo;

@interface MFNewCurrencyDetail : MFNewCurrency

@property (nonatomic, strong) NSMutableArray<MFBaseInfo *> *baseInfos;

@property (nonatomic, strong) NSString *teamName;

@property (nonatomic, strong) NSString *teamIntro;

@property (nonatomic, strong) NSString *teamIcon;

@end
