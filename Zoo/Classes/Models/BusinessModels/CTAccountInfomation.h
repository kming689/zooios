//
//  CTAccountInfomation.h
//  CTYun
//
//  Created by tanfameng on 2018/3/7.
//  Copyright © 2018年 CTYun. All rights reserved.
//

#import "MFObject.h"
#import "MFMarco.h"

@interface CTAccountInfomation : MFObject

@property (nonatomic, strong) NSString *accountId;//账户ID

@property (nonatomic, strong) NSString *userId;//用户ID

@property (nonatomic, assign) eCTAuthenticationState auditStatus;//认证状态

@property (nonatomic, strong) NSString *telephone;//固定电话

@property (nonatomic, strong) NSString *address;//联系地址

@property (nonatomic, strong) NSString *postNo;//邮政编码

@property (nonatomic, strong) NSString *mobilephone;//绑定手机

@property (nonatomic, strong) NSString *email;//绑定邮箱

@property (nonatomic, strong) NSString *name;//账户名称，如果是企业账户，表示公司名字，如果是个人账户，表示个人姓名

@end
