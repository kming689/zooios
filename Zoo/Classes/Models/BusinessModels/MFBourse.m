//
//  MFBourse.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFBourse.h"
#import <YYModel/YYModel.h>

@implementation MFBourse

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"icon" : @"icon_url",
             @"bourseId" : @"id",
             @"backend" : @"backend",
             @"bourseName" : @"name",
             @"country" : @"country"};
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
