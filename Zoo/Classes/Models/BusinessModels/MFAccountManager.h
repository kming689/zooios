//
//  MFAccountManager.h
//  Zoo
//
//  Created by tanfameng on 2018/3/4.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@class MFAccount;

@interface MFAccountManager : MFObject

@property (nonatomic, strong) MFAccount *account;

+ (instancetype)shareInstance;

- (BOOL)isLogin;

@end
