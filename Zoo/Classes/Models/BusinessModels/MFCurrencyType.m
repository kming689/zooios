//
//  MFCurrencyType.m
//  Zoo
//
//  Created by tanfameng on 2018/3/15.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrencyType.h"
#import <YYModel/YYModel.h>

@implementation MFCurrencyType

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"icon" : @"icon_url",
             @"currencyId" : @"id",
             @"currencyName" : @"base"
             };
}

@end
