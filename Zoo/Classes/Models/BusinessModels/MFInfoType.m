//
//  MFInfoType.m
//  Zoo
//
//  Created by tanfameng on 2018/2/28.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFInfoType.h"
#import <YYModel/YYModel.h>

@implementation MFInfoType

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"typeId" : @"id",
             @"typeName" : @"title"
             };
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
