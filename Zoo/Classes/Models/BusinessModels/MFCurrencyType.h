//
//  MFCurrencyType.h
//  Zoo
//
//  Created by tanfameng on 2018/3/15.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@interface MFCurrencyType : MFObject

@property (nonatomic, strong) NSString *icon;

@property (nonatomic, strong) NSString *symbol;//币对名字

@property (nonatomic, strong) NSString *currencyId;//货币id

@property (nonatomic, strong) NSString *currencyName;//货币名字,英文名字

@end
