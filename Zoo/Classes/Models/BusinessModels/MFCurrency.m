//
//  MFCurrency.m
//  Zoo
//
//  Created by tanfameng on 2018/2/9.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFCurrency.h"
#import <YYModel/YYModel.h>

@implementation MFCurrency

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"icon" : @"icon_url",
             @"currencyId" : @"id",
             @"currencyName" : @"base",
             @"conversionPrice" : @"close",
             @"currencyCashPrice" : @"price",
             @"priceUnit" : @"unit",
             @"isFollowed" : @"follow",
             @"bourseId" : @"market",
             @"bourseName" : @"marketname",
             @"conversionName" : @"quote"
             };
}

- (NSDictionary *)modelCustomWillTransformFromDictionary:(NSDictionary *)dic
{
    NSMutableDictionary *newDic = [[NSMutableDictionary alloc] initWithDictionary:dic];
    
    for (NSString *key in dic) {
        if ([key isEqualToString:@"close"]||[key isEqualToString:@"price"]) {
            NSNumber *number = [dic objectForKey:key];
            double value = number.doubleValue;
            NSString *str = [self stringDisposeWithFloat:value];
            [newDic setObject:str forKey:key];
        }
    }
    
    return newDic;
}

- (NSString *)stringDisposeWithFloat:(float)floatValue
{
    NSString *str = [NSString stringWithFormat:@"%6.6f",floatValue];
    long len = str.length;
    for (int i = 0; i < len; i++)
    {
        if (![str  hasSuffix:@"0"])
            break;
        else
            str = [str substringToIndex:[str length]-1];
    }
    if ([str hasSuffix:@"."])//避免像2.0000这样的被解析成2.
    {
        //s.substring(0, len - i - 1);
        return [str substringToIndex:[str length]-1];
    }
    else
    {
        return str;
    }
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (BOOL)isEqual:(MFCurrency *)object
{
    if ([object.currencyId isEqualToString:self.currencyId]) {
        return YES;
    }
    if ([object.symbol isEqualToString:self.symbol]) {
        return YES;
    }
    return NO;
}

//@property (nonatomic, strong) NSString *bourseId;
//@property (nonatomic, strong) NSString *bourseName;
//@property (nonatomic, strong) NSString *icon;
//@property (nonatomic, strong) NSString *symbol;//币对名字
//@property (nonatomic, strong) NSString *currencyId;//货币id
//@property (nonatomic, strong) NSString *currencyName;//货币名字,英文名字
//@property (nonatomic, strong) NSString *conversionName;//货币兑换成的通用货币名字
//@property (nonatomic, strong) NSString *conversionPrice;//货币兑换成的通用货币价格
//@property (nonatomic, strong) NSString *currencyCashPrice;//货币兑换成法币价格
//@property (nonatomic, strong) NSString *priceUnit;//货币价格单位，美元还是人民币
//@property (nonatomic, strong) NSString *changePercent;//涨跌幅
//@property (nonatomic, strong) NSString *changePrice;//涨跌价格
//@property (nonatomic, strong) NSString *abstract;//一句话介绍
//@property (nonatomic, assign) BOOL isFollowed;//是否关注

- (void)cloneFromCurrency:(MFCurrency *)currency
{
    self.bourseId = currency.bourseId;
    self.bourseName = currency.bourseName;
    self.icon = currency.icon;
    self.symbol = currency.symbol;
    self.currencyId = currency.currencyId;
    self.currencyName = currency.currencyName;
    self.conversionName = currency.conversionName;
    self.conversionPrice = currency.conversionPrice;
    self.currencyCashPrice = currency.currencyCashPrice;
    self.priceUnit = currency.priceUnit;
    self.changePercent = currency.changePercent;
    self.abstract = currency.abstract;
    self.isFollowed = currency.isFollowed;
}

@end
