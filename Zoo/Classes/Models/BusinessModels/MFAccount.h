//
//  MFAccount.h
//  Zoo
//
//  Created by tanfameng on 2018/3/4.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@class MFAccountInfo;

@interface MFAccount : MFObject

@property (nonatomic, strong) NSString *icon;

@property (nonatomic, strong) NSString *refreshToken;

@property (nonatomic, strong) NSString *accessToken;

@property (nonatomic, strong) NSString *userId;

@property (nonatomic, strong) NSString *userName;

@property (nonatomic, strong) NSString *mobile;

@property (nonatomic, strong) NSString *passWord;

@property (nonatomic, strong) NSString *email;

@property (nonatomic, strong) NSDate *createTime;

@property (nonatomic, strong) NSString *tokenType;

@property (nonatomic, assign) long long tokenExpires;//token超时时长

@property (nonatomic, strong) MFAccountInfo *accountInfo;

@end
