//
//  MFInfomation.h
//  Zoo
//
//  Created by tanfameng on 2018/3/1.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "MFObject.h"

@interface MFInfomation : MFObject

@property (nonatomic, strong) NSString *infoId;

@property (nonatomic, strong) NSString *icon;

@property (nonatomic, strong) NSString *date;

@property (nonatomic, strong) NSString *time;

@property (nonatomic, strong) NSString *title;

@property (nonatomic, strong) NSString *infomation;

@property (nonatomic, strong) NSString *author;

@property (nonatomic, strong) NSString *contentUrl;

@property (nonatomic, strong) NSString *htmlContent;

- (BOOL)isEqual:(MFInfomation *)object;

@end
