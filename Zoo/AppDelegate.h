//
//  AppDelegate.h
//  Zoo
//
//  Created by tanfameng on 2018/2/8.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

