//
//  ViewController.m
//  Zoo
//
//  Created by tanfameng on 2018/2/8.
//  Copyright © 2018年 tanfameng. All rights reserved.
//

#import "ViewController.h"
#import "CubicLineChartViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (IBAction)onButtonDone:(id)sender
{
    UIViewController *vc = [[CubicLineChartViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
